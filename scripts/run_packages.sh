#!/usr/bin/env bash

# Must be run from project root
flutter pub get
#flutter pub run change_app_package_name:main eu.hiive.app

mkdir -p android/app/src/production
flutter pub run flutter_launcher_icons -f pkg_configs/flutter_launcher_icons_production.yaml
cp -r android/app/src/main/res/ android/app/src/production/
cp -r ios/Runner/Assets.xcassets/AppIcon.appiconset/ ios/Runner/Assets.xcassets/AppIconProduction.appiconset/

mkdir -p android/app/src/development
flutter pub run flutter_launcher_icons -f pkg_configs/flutter_launcher_icons_development.yaml
cp -r android/app/src/main/res/ android/app/src/development/
cp -r ios/Runner/Assets.xcassets/AppIcon.appiconset/ ios/Runner/Assets.xcassets/AppIconDevelopment.appiconset/

flutter pub run flutter_native_splash:create --flavor production --path=pkg_configs/flutter_native_splash_production.yaml
flutter pub run flutter_native_splash:create --flavor development --path=pkg_configs/flutter_native_splash_development.yaml

flutter pub run build_runner build --delete-conflicting-outputs
