#!/usr/bin/env bash
flutter build apk --flavor production \
  --dart-define FLAVOR="production"
