#!/usr/bin/env bash
flutter build apk --flavor development \
  --dart-define HIIVE_HTTP_AUTH_USER="$HIIVE_HTTP_AUTH_USER" \
  --dart-define HIIVE_HTTP_AUTH_PASS="$HIIVE_HTTP_AUTH_PASS"
