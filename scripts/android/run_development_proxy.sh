#!/usr/bin/env bash
flutter run --flavor development \
  --dart-define HIIVE_HTTP_AUTH_USER="$HIIVE_HTTP_AUTH_USER" \
  --dart-define HIIVE_HTTP_AUTH_PASS="$HIIVE_HTTP_AUTH_PASS" \
  --dart-define PROXY_IP="$1" \
  --dart-define PROXY_PORT="$2"
