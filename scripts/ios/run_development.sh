#!/usr/bin/env bash
flutter run --flavor development \
  --dart-define FLAVOR="development" \
  --dart-define HIIVE_HTTP_AUTH_USER=$HIIVE_HTTP_AUTH_USER \
  --dart-define HIIVE_HTTP_AUTH_PASS=$HIIVE_HTTP_AUTH_PASS
