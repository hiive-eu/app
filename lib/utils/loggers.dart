import 'package:flutter_riverpod/flutter_riverpod.dart';

class ProviderStateLogger extends ProviderObserver {
  const ProviderStateLogger();
  @override
  void didUpdateProvider(
    ProviderBase provider,
    Object? previousValue,
    Object? newValue,
    ProviderContainer container,
  ) {
    print('''
{
  provider: ${provider.name ?? provider.runtimeType},
  oldValue: $previousValue,
  newValue: $newValue
}
''');
    super.didUpdateProvider(provider, previousValue, newValue, container);
  }

  @override
  void didAddProvider(
    ProviderBase<Object?> provider,
    Object? value,
    ProviderContainer container,
  ) {
    print("");
    print('''
{
  NEW PROVIDER: ${provider.name}
}
''');
    super.didAddProvider(provider, value, container);
  }

  @override
  void didDisposeProvider(
    ProviderBase<Object?> provider,
    ProviderContainer container,
  ) {
    print('''
{
  PROVIDER DISPOSED: ${provider.name}
}
''');
    super.didDisposeProvider(provider, container);
  }
}

class DioLogger {
  DioLogger._();

  static bool _showPrettyLogs = false;
  static get showPrettyLogs => _showPrettyLogs;

  static bool _showLogs = false;
  static bool get showLogs => _showLogs;

  static void setLogger({
    required bool showLogs,
    bool showPrettyLogs = false,
  }) {
    _showLogs = showLogs;
    _showPrettyLogs = showPrettyLogs;
  }

  static void log(Object? message) {
    if (_showLogs) log("$message");
  }
}
