import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/models/hive.dart';

void showSnackbar(BuildContext context, String s) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(
        s,
        textAlign: TextAlign.center,
      ),
    ),
  );
}

String b64pad(String b64) {
  int paddingLength = 4 - b64.length % 4;
  return paddingLength == 4 ? b64 : b64 + "=" * paddingLength;
}

bool matchPathWithLocation(String path, location) {
  final forwardSlashEscaped = path.replaceAll(r"/", r"\/");
  final pathWithRegex = forwardSlashEscaped.replaceAllMapped(
      RegExp(r":(\w|\d)+"), (match) => r"(\w|\d)+");
  final regex = RegExp(pathWithRegex);
  final match = regex.hasMatch(location);
  return match;
}

int bleInt16toint(List<int> inputList) {
  List<int> reversedList = inputList.reversed.toList();

  // Step 2: Concatenate the elements
  String concatenatedStr =
      reversedList.map((n) => n.toRadixString(16).padLeft(2, '0')).join();

  // Step 3: Convert to integer
  return int.parse(concatenatedStr, radix: 16);
}

int? rssiToPercentage(int rssi) =>
    mapRange(rssi.toDouble(), -130, 0, 0, 100)?.toInt();

double? mapRange(
  double? value,
  double minInput,
  double maxInput,
  double minOutput,
  double maxOutput,
) {
  if (value != null) {
    final inputRange = maxInput - minInput;
    final outputRange = maxOutput - minOutput;
    final value_ = value.clamp(minInput, maxInput);
    final res = (((value_ - minInput) / inputRange) * outputRange + minOutput);
    return res.isNaN ? 0 : res;
  } else {
    return null;
  }
}

Hive? searchSensorsAssociatedHive(String sensor, List<Hive> hives) {
  return hives.filter((hive) => hive.sensor == sensor).firstOrNull;
}

DateTime datetimeFromUnixTimestamp(int unixTimestamp) {
  return DateTime.fromMillisecondsSinceEpoch(
      unixTimestamp * Duration.millisecondsPerSecond);
}

IconData batteryIconFromBattery(int battery) {
  if (battery >= 87.5) {
    return Icons.battery_full;
  } else if (battery > 75.0) {
    return Icons.battery_6_bar;
  } else if (battery > 62.5) {
    return Icons.battery_5_bar;
  } else if (battery > 50.0) {
    return Icons.battery_4_bar;
  } else if (battery > 37.5) {
    return Icons.battery_3_bar;
  } else if (battery > 25.5) {
    return Icons.battery_2_bar;
  } else if (battery > 12.5) {
    return Icons.battery_1_bar;
  } else {
    return Icons.battery_0_bar;
  }
}

IconData loraIconFromRssi(int rssi) {
  if (rssi >= -30) {
    // Excellent signal strength
    return Icons.network_wifi;
  } else if (rssi >= -55) {
    // Good signal strength
    return Icons.network_wifi_3_bar;
  } else if (rssi >= -75) {
    // Fair signal strength
    return Icons.network_wifi_2_bar;
  } else if (rssi >= -120) {
    // Weak signal strength
    return Icons.network_wifi_1_bar;
  } else {
    // Very weak signal strength
    return Icons.signal_wifi_0_bar;
  }
}

//IconData wifiIconFromRssi(int rssi) {
//  if (rssi >= -50) {
//    // Excellent signal strength
//    return Icons.network_wifi;
//  } else if (rssi >= -60) {
//    // Good signal strength
//    return Icons.network_wifi_3_bar;
//  } else if (rssi >= -70) {
//    // Fair signal strength
//    return Icons.network_wifi_2_bar;
//  } else if (rssi >= -80) {
//    // Weak signal strength
//    return Icons.network_wifi_1_bar;
//  } else {
//    // Very weak signal strength
//    return Icons.signal_wifi_0_bar;
//  }
//}
