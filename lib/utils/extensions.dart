import 'package:flutter/material.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:hiive/data/models/reading.dart';

// THEME MODE
extension ThemeDataExtension on ThemeMode {
  int toInt() {
    switch (this) {
      case ThemeMode.light:
        return 0;
      case ThemeMode.dark:
        return 1;
      case ThemeMode.system:
        return 2;
    }
  }

  String toPresentationString() {
    switch (this) {
      case ThemeMode.light:
        return "light";
      case ThemeMode.dark:
        return "dark";
      case ThemeMode.system:
        return "system";
    }
  }
}

extension ThemeDataIntExtensions on int {
  ThemeMode toThemeMode() {
    switch (this) {
      case 0:
        return ThemeMode.light;
      case 1:
        return ThemeMode.dark;
      default:
        return ThemeMode.system;
    }
  }
}

extension Conversion on String {
  List<int> toIntList() {
    return this.split('').map((String char) => int.parse(char)).toList();
  }

  bool toBool() => this == "true" ? true : false;
}

extension Conversino on int {
  bool toBool() => this == 0 ? false : true;
}

extension SetUtils on Set<int> {
  int min() => this.reduce((v, e) => v < e ? v : e);
  int max() => this.reduce((v, e) => v > e ? v : e);
}

extension ListUtils on List<int?> {
  int min() => this.foldLeft(0, (v, e) {
        if (e != null) {
          return v < e ? v : e;
        } else {
          return v;
        }
      });
  int max() => this.foldLeft(0, (v, e) {
        if (e != null) {
          return v > e ? v : e;
        } else {
          return v;
        }
      });
}

extension GatewayReadingstUtils on List<GatewayReading> {
  GatewayReading latest() =>
      this.reduce((a, b) => a.time.isAfter(b.time) ? a : b);
}

extension HiveReadingstUtils on List<Reading> {
  Reading latest() => this.reduce((a, b) => a.time.isAfter(b.time) ? a : b);
}
