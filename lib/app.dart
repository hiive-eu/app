import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:go_router/go_router.dart";

import "package:hiive/router/app_router.dart";
import "package:hiive/constants/app_theme.dart";
import "package:hiive/theme_mode_notfier.dart";

enum AppFlavor { production, development }

class App extends ConsumerWidget {
  final ScreenRoute firstScreen;
  const App({super.key, required this.firstScreen});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final GoRouter router = genRouter(firstScreen);
    ThemeMode themeMode = ref.watch(themeModeNotifierProvider);
    return MaterialApp.router(
      title: "HIIVE",
      themeMode: themeMode,
      theme: GlobalThemData.lightThemeData,
      darkTheme: GlobalThemData.darkThemeData,
      routeInformationParser: router.routeInformationParser,
      routeInformationProvider: router.routeInformationProvider,
      routerDelegate: router.routerDelegate,
      //routerConfig: router,
    );
  }
}
