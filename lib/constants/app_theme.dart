import "package:flutter/material.dart";

Color colorOldYellow = Color(0xfffbc139);
Color colorOldWhite = Color(0xfffffaec);
Color colorOldLightBrown = Color(0xff917d67);
Color colorOldBrown = Color(0xff452112);

Color colorNewHighlightOrange = Color(0xffeb8922);
Color colorNewHighlightRed = Color(0xffe74121);
Color colorNewHighlightBrown = Color(0xff452112);
Color colorNewHighlightGreen = Color(0xff736c1e);
Color colorNewBaseGrey = Color(0xffccc8bc);
Color colorNewBaseSand = Color(0xffc19c6e);
Color colorNewBaseBrown = Color(0xff917d67);
Color colorNewBaseWhite = Color(0xfffffaec);

Color colorError = Color(0xffff0000);

class GlobalThemData {
  static final Color _lightFocusColor = Colors.black.withOpacity(0.12);
  static final Color _darkFocusColor = Colors.white.withOpacity(0.12);

  static ColorScheme lightColorScheme = ColorScheme(
    primary: colorOldYellow,
    onPrimary: colorOldBrown,
    secondary: colorOldLightBrown,
    onSecondary: colorOldWhite,
    surface: colorOldWhite,
    onSurface: colorOldBrown,
    error: colorError,
    onError: colorOldWhite,
    brightness: Brightness.light,
  );

  static ColorScheme darkColorScheme = ColorScheme(
    primary: colorNewHighlightOrange,
    onPrimary: colorNewBaseWhite,
    secondary: colorNewHighlightBrown,
    onSecondary: colorNewBaseWhite,
    surface: colorNewBaseBrown,
    onSurface: colorNewBaseWhite,
    error: colorError,
    onError: colorNewBaseWhite,
    brightness: Brightness.dark,
  );

  static const TextTheme textTheme = TextTheme(
    displayLarge: TextStyle(
      fontFamily: "DS-Point",
      fontSize: 64,
    ),
    displayMedium: TextStyle(
      fontFamily: "Futura",
      fontSize: 45,
      fontWeight: FontWeight.bold,
    ),
    displaySmall: TextStyle(
      fontFamily: "Futura",
      fontSize: 36,
      fontWeight: FontWeight.bold,
    ),
    headlineLarge: TextStyle(
      fontFamily: "Futura",
      fontSize: 32,
      fontWeight: FontWeight.bold,
    ),
    headlineMedium: TextStyle(
      fontFamily: "Futura",
      fontSize: 28,
      fontWeight: FontWeight.bold,
    ),
    headlineSmall: TextStyle(
      fontFamily: "Futura",
      fontSize: 24,
      fontWeight: FontWeight.bold,
    ),
    titleLarge: TextStyle(
      fontFamily: "Avenir",
      fontSize: 22,
      fontWeight: FontWeight.bold,
    ),
    titleMedium: TextStyle(
      fontFamily: "Avenir",
      fontSize: 16,
      fontWeight: FontWeight.normal,
    ),
    titleSmall: TextStyle(
      fontFamily: "Avenir",
      fontSize: 14,
      fontWeight: FontWeight.normal,
    ),
    bodyLarge: TextStyle(
      fontFamily: "Avenir",
      fontSize: 14,
    ),
    bodyMedium: TextStyle(
      fontFamily: "Avenir",
      fontSize: 12,
    ),
    bodySmall: TextStyle(
      fontFamily: "Avenir",
      fontSize: 11,
    ),
    labelLarge: TextStyle(
      fontFamily: "Avenir",
      fontSize: 16,
      fontWeight: FontWeight.bold,
    ),
    labelMedium: TextStyle(
      fontFamily: "Baskerville",
      fontWeight: FontWeight.normal,
      fontSize: 14,
    ),
    labelSmall: TextStyle(
      fontFamily: "Baskerville",
      fontSize: 14,
      fontWeight: FontWeight.normal,
    ),
  );

  static ThemeData lightThemeData = themeData(
    lightColorScheme,
    _lightFocusColor,
    textTheme,
  );

  static ThemeData darkThemeData = themeData(
    darkColorScheme,
    _darkFocusColor,
    textTheme,
  );

  //TextTheme textTheme(TextTheme base) {
  //  return base.copyWith(
  //    headlineLarge: base.headlineLarge!.copyWith(
  //      fontFamily: "Futura",
  //      color: const Color(0xff452112),
  //      fontWeight: FontWeight.bold,
  //    ),
  //    displayLarge: base.displayLarge!.copyWith(
  //      fontFamily: "Futura",
  //      color: const Color(0xff452112),
  //      fontWeight: FontWeight.bold,
  //    ),
  //    displayMedium: base.displayMedium!.copyWith(fontFamily: "Futura"),
  //    displaySmall: base.displaySmall!.copyWith(
  //      fontFamily: "Futura",
  //      color: const Color(0xff452112),
  //      fontWeight: FontWeight.bold,
  //    ),
  //    bodyLarge: base.bodyLarge!.copyWith(fontFamily: "Avenir"),
  //    bodyMedium: base.bodyMedium!.copyWith(fontFamily: "Avenir"),
  //    bodySmall: base.bodySmall!.copyWith(fontFamily: "Avenir"),
  //    titleLarge: base.titleLarge!.copyWith(
  //      fontFamily: "Avenir",
  //      color: const Color(0xff452112),
  //      fontWeight: FontWeight.bold,
  //    ),
  //    titleMedium: base.titleMedium!.copyWith(fontFamily: "Avenir"),
  //    titleSmall: base.titleSmall!.copyWith(
  //      fontFamily: "Avenir",
  //      color: const Color(0xff452112),
  //      fontWeight: FontWeight.bold,
  //    ),
  //  );
  //}

  static ThemeData themeData(
    ColorScheme colorScheme,
    Color focusColor,
    TextTheme textTheme,
  ) {
    return ThemeData(
      colorScheme: colorScheme,
      canvasColor: colorScheme.surface,
      scaffoldBackgroundColor: colorScheme.surface,
      highlightColor: Colors.transparent,
      focusColor: focusColor,
      fontFamily: "Avenir",
      textTheme: textTheme,
    );
  }
}
