import 'package:flutter/material.dart';

//
// BLE SERVICE
const cBleScanTimeoutSeconds = 10;
const cGatewayBleNamePrefix = "HIIVE-GW";
const cMtu = 247;
const cTriggerBleTaskValue = 1;

const cPrescanSeconds = 5;
const cConnectionTimeout = 5;

// PREFERENCES
const cPreferencesOnboardingKey = "ONBOARDING";
const cPreferencesThemeModeKey = "THEMEMODE";

const cPreferencesAllKeys = [
  cPreferencesOnboardingKey,
  cPreferencesThemeModeKey
];

// DATABASE
const cDatabaseName = "hiive.db";
const cDatabaseVersion = 1;

const int cDefaultInterval = 20;
const List<String> forbiddenNameChars = [
  '/',
  '(',
  ')',
  '"',
  '<',
  '>',
  '\\',
  '{',
  '}'
];

// MISC
const List<int> cIntervalChoices = [5, 15, 20, 30, cMaxIntervalDuration];

const List<Color> cSensorColors = [
  Color(0xFF00FF00), // Green
  Color(0xFF0000FF), // Blue
  Color(0xFF00FFFF), // Cyan (Green + Blue)
  Color(0xFFFF00FF), // Magenta (Red + Blue)
  Color(0xFFFFFF00), // Yellow (Green + Red)
];

const int cMaxIntervalDuration = 60;
