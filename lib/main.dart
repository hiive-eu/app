import "package:flutter/material.dart";
import "package:flutter_native_splash/flutter_native_splash.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";

import "package:hiive/app.dart";
import "package:hiive/constants/defaults.dart";
import "package:hiive/data/state/authentication_notifier.dart";
import "package:hiive/data/state/gateways_notifier.dart";
import "package:hiive/data/state/hives_notifier.dart";
import "package:hiive/data/state/logged_in_notifier.dart";
import "package:hiive/data/state/onboarded_notifier.dart";
import "package:hiive/data/state/sensors_notifier.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/services/sqlite.dart";
import "package:hiive/theme_mode_notfier.dart";
import "package:hiive/utils/extensions.dart";
import "package:hiive/utils/loggers.dart";
import "package:shared_preferences/shared_preferences.dart";

//import 'package:sentry_flutter/sentry_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final container = ProviderContainer(observers: [const ProviderStateLogger()]);
  await initialization(container);

  final bool onboarded = await getAndInitOnboardState();
  print("[ONBOARDTEST] - main onboarded: $onboarded");
  container.read(onboardedNotifierProvider.notifier).setOnboarded(onboarded);
  final loggedIn = container.read(loggedInNotifierProvider);
  late ScreenRoute firstScreen;
  if (loggedIn) {
    if (onboarded) {
      firstScreen = ScreenRoute.dashboard;
    } else {
      firstScreen = ScreenRoute.onboardWelcome;
    }
  } else {
    firstScreen = ScreenRoute.auth;
  }
  //loggedIn ? ScreenRoute.dashboard : ScreenRoute.onboardWelcome;
  final ThemeMode storedThemeMode = await getThemeMode();
  print("[FIRST SCREEN]: ${firstScreen.name}");
  container
      .read(themeModeNotifierProvider.notifier)
      .setThemeMode(storedThemeMode);
  FlutterNativeSplash.remove();

  runApp(
    UncontrolledProviderScope(
      container: container,
      child: App(firstScreen: firstScreen),
    ),
  );

  // If sentry is enabled. Currently disabled because it cannot build for iOS
  //await SentryFlutter.init(
  //  (options) {
  //    const sentryDsn = String.fromEnvironment('HIIVE_SENTRY_DSN');
  //    options.dsn = sentryDsn;
  //    // Set tracesSampleRate to 1.0 to capture 100% of transactions for tracing.
  //    // We recommend adjusting this value in production.
  //    options.tracesSampleRate = 1.0;
  //    // The sampling rate for profiling is relative to tracesSampleRate
  //    // Setting to 1.0 will profile 100% of sampled transactions:
  //    options.profilesSampleRate = 1.0;
  //  },
  //  appRunner: () async {
  //    WidgetsFlutterBinding.ensureInitialized();
  //    final container =
  //        ProviderContainer(observers: [const ProviderStateLogger()]);
  //    await initialization(container);

  //    final bool onboarded = await getAndInitOnboardState();
  //    container
  //        .read(onboardedNotifierProvider.notifier)
  //        .setOnboarded(onboarded);
  //    final loggedIn = container.read(loggedInNotifierProvider);
  //    late ScreenRoute firstScreen;
  //    if (loggedIn) {
  //      if (onboarded) {
  //        firstScreen = ScreenRoute.dashboard;
  //      } else {
  //        firstScreen = ScreenRoute.onboardWelcome;
  //      }
  //    } else {
  //      firstScreen = ScreenRoute.auth;
  //    }
  //    //loggedIn ? ScreenRoute.dashboard : ScreenRoute.onboardWelcome;
  //    final ThemeMode storedThemeMode = await getThemeMode();
  //    print("[FIRST SCREEN]: ${firstScreen.name}");
  //    container
  //        .read(themeModeNotifierProvider.notifier)
  //        .setThemeMode(storedThemeMode);
  //    FlutterNativeSplash.remove();

  //    runApp(
  //      UncontrolledProviderScope(
  //        container: container,
  //        child: App(firstScreen: firstScreen),
  //      ),
  //    );
  //  },
  //);
}

Future initialization(ProviderContainer container) async {
  // Authentication

  container.read(loggedInNotifierProvider);
  await container.read(authenticationNotifierProvider.future);

  // Database
  //await container
  //    .read(sqliteServiceProvider)
  //    .init()
  //    .run()
  //    .then((res) => res.match(
  //          (error) => print("[ERROR:DB] ${error.toString()}"),
  //          (_) => print("[DB] Database ready"),
  //        ));
  DioLogger.setLogger(showLogs: true, showPrettyLogs: true);
  container.read(gatewaysNotifierProvider);
  container.read(hivesNotifierProvider);
  container.read(sensorsNotifierProvider);
}

Future<bool> getAndInitOnboardState() async {
  print("[ONBOARDTEST] - getAndInit");
  final preferences = await SharedPreferences.getInstance();
  final bool? onboarded = preferences.getBool(cPreferencesOnboardingKey);

  print("[ONBOARDTEST] - getAndInit onboarded: $onboarded");
  if (onboarded == null) {
    preferences.setBool(cPreferencesOnboardingKey, false);
    return false;
  } else {
    return onboarded;
  }
}

void completeOnboarding() async {
  print("[ONBOARDTEST] complete");
  final preferences = await SharedPreferences.getInstance();
  preferences.setBool(cPreferencesOnboardingKey, true);
}

Future<ThemeMode> getThemeMode() async {
  final preferences = await SharedPreferences.getInstance();
  final int? themeModeInt = preferences.getInt(cPreferencesThemeModeKey);

  if (themeModeInt == null) {
    return ThemeMode.dark;
  } else {
    print("[THEME] ${themeModeInt.toThemeMode()}");
    return themeModeInt.toThemeMode();
  }
}
