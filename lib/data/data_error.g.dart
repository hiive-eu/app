// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$InvalidCredentialsImpl _$$InvalidCredentialsImplFromJson(
        Map<String, dynamic> json) =>
    _$InvalidCredentialsImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$InvalidCredentialsImplToJson(
        _$InvalidCredentialsImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$MissingJWTTokenImpl _$$MissingJWTTokenImplFromJson(
        Map<String, dynamic> json) =>
    _$MissingJWTTokenImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$MissingJWTTokenImplToJson(
        _$MissingJWTTokenImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$TokenValidationFailureImpl _$$TokenValidationFailureImplFromJson(
        Map<String, dynamic> json) =>
    _$TokenValidationFailureImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$TokenValidationFailureImplToJson(
        _$TokenValidationFailureImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$InvalidDisplaynameImpl _$$InvalidDisplaynameImplFromJson(
        Map<String, dynamic> json) =>
    _$InvalidDisplaynameImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$InvalidDisplaynameImplToJson(
        _$InvalidDisplaynameImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$InvalidEmailImpl _$$InvalidEmailImplFromJson(Map<String, dynamic> json) =>
    _$InvalidEmailImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$InvalidEmailImplToJson(_$InvalidEmailImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$InvalidPasswordImpl _$$InvalidPasswordImplFromJson(
        Map<String, dynamic> json) =>
    _$InvalidPasswordImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$InvalidPasswordImplToJson(
        _$InvalidPasswordImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$UnconfirmedUserImpl _$$UnconfirmedUserImplFromJson(
        Map<String, dynamic> json) =>
    _$UnconfirmedUserImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$UnconfirmedUserImplToJson(
        _$UnconfirmedUserImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$UnknownGatewayImpl _$$UnknownGatewayImplFromJson(Map<String, dynamic> json) =>
    _$UnknownGatewayImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$UnknownGatewayImplToJson(
        _$UnknownGatewayImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$UnknownSensorImpl _$$UnknownSensorImplFromJson(Map<String, dynamic> json) =>
    _$UnknownSensorImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$UnknownSensorImplToJson(_$UnknownSensorImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$UnexpectedResponseDataImpl _$$UnexpectedResponseDataImplFromJson(
        Map<String, dynamic> json) =>
    _$UnexpectedResponseDataImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$UnexpectedResponseDataImplToJson(
        _$UnexpectedResponseDataImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$InternalServerErrorImpl _$$InternalServerErrorImplFromJson(
        Map<String, dynamic> json) =>
    _$InternalServerErrorImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$InternalServerErrorImplToJson(
        _$InternalServerErrorImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$NotFoundImpl _$$NotFoundImplFromJson(Map<String, dynamic> json) =>
    _$NotFoundImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$NotFoundImplToJson(_$NotFoundImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$BadRequestImpl _$$BadRequestImplFromJson(Map<String, dynamic> json) =>
    _$BadRequestImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$BadRequestImplToJson(_$BadRequestImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$NoConnectionImpl _$$NoConnectionImplFromJson(Map<String, dynamic> json) =>
    _$NoConnectionImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$NoConnectionImplToJson(_$NoConnectionImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$NotLoggedInImpl _$$NotLoggedInImplFromJson(Map<String, dynamic> json) =>
    _$NotLoggedInImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$NotLoggedInImplToJson(_$NotLoggedInImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };

_$UnknownImpl _$$UnknownImplFromJson(Map<String, dynamic> json) =>
    _$UnknownImpl(
      $type: json['cause'] as String?,
    );

Map<String, dynamic> _$$UnknownImplToJson(_$UnknownImpl instance) =>
    <String, dynamic>{
      'cause': instance.$type,
    };
