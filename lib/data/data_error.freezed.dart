// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'data_error.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ApiError _$ApiErrorFromJson(Map<String, dynamic> json) {
  switch (json['cause']) {
    case 'InvalidCredentials':
      return InvalidCredentials.fromJson(json);
    case 'MissingJWTToken':
      return MissingJWTToken.fromJson(json);
    case 'TokenValidationFailure':
      return TokenValidationFailure.fromJson(json);
    case 'InvalidDisplayname':
      return InvalidDisplayname.fromJson(json);
    case 'InvalidEmail':
      return InvalidEmail.fromJson(json);
    case 'InvalidPassword':
      return InvalidPassword.fromJson(json);
    case 'UnconfirmedUser':
      return UnconfirmedUser.fromJson(json);
    case 'UnknownGateway':
      return UnknownGateway.fromJson(json);
    case 'UnknownSensor':
      return UnknownSensor.fromJson(json);
    case 'UnexpectedResponseData':
      return UnexpectedResponseData.fromJson(json);
    case 'InternalServerError':
      return InternalServerError.fromJson(json);
    case 'NotFound':
      return NotFound.fromJson(json);
    case 'BadRequest':
      return BadRequest.fromJson(json);
    case 'NoConnection':
      return NoConnection.fromJson(json);
    case 'NotLoggedIn':
      return NotLoggedIn.fromJson(json);
    case 'Unknown':
      return Unknown.fromJson(json);

    default:
      throw CheckedFromJsonException(
          json, 'cause', 'ApiError', 'Invalid union type "${json['cause']}"!');
  }
}

/// @nodoc
mixin _$ApiError {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  /// Serializes this ApiError to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ApiErrorCopyWith<$Res> {
  factory $ApiErrorCopyWith(ApiError value, $Res Function(ApiError) then) =
      _$ApiErrorCopyWithImpl<$Res, ApiError>;
}

/// @nodoc
class _$ApiErrorCopyWithImpl<$Res, $Val extends ApiError>
    implements $ApiErrorCopyWith<$Res> {
  _$ApiErrorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
abstract class _$$InvalidCredentialsImplCopyWith<$Res> {
  factory _$$InvalidCredentialsImplCopyWith(_$InvalidCredentialsImpl value,
          $Res Function(_$InvalidCredentialsImpl) then) =
      __$$InvalidCredentialsImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidCredentialsImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$InvalidCredentialsImpl>
    implements _$$InvalidCredentialsImplCopyWith<$Res> {
  __$$InvalidCredentialsImplCopyWithImpl(_$InvalidCredentialsImpl _value,
      $Res Function(_$InvalidCredentialsImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$InvalidCredentialsImpl implements InvalidCredentials {
  const _$InvalidCredentialsImpl({final String? $type})
      : $type = $type ?? 'InvalidCredentials';

  factory _$InvalidCredentialsImpl.fromJson(Map<String, dynamic> json) =>
      _$$InvalidCredentialsImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.invalidCredentials()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvalidCredentialsImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return invalidCredentials();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return invalidCredentials?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (invalidCredentials != null) {
      return invalidCredentials();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return invalidCredentials(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return invalidCredentials?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (invalidCredentials != null) {
      return invalidCredentials(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$InvalidCredentialsImplToJson(
      this,
    );
  }
}

abstract class InvalidCredentials implements ApiError {
  const factory InvalidCredentials() = _$InvalidCredentialsImpl;

  factory InvalidCredentials.fromJson(Map<String, dynamic> json) =
      _$InvalidCredentialsImpl.fromJson;
}

/// @nodoc
abstract class _$$MissingJWTTokenImplCopyWith<$Res> {
  factory _$$MissingJWTTokenImplCopyWith(_$MissingJWTTokenImpl value,
          $Res Function(_$MissingJWTTokenImpl) then) =
      __$$MissingJWTTokenImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$MissingJWTTokenImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$MissingJWTTokenImpl>
    implements _$$MissingJWTTokenImplCopyWith<$Res> {
  __$$MissingJWTTokenImplCopyWithImpl(
      _$MissingJWTTokenImpl _value, $Res Function(_$MissingJWTTokenImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$MissingJWTTokenImpl implements MissingJWTToken {
  const _$MissingJWTTokenImpl({final String? $type})
      : $type = $type ?? 'MissingJWTToken';

  factory _$MissingJWTTokenImpl.fromJson(Map<String, dynamic> json) =>
      _$$MissingJWTTokenImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.missingJWTToken()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$MissingJWTTokenImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return missingJWTToken();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return missingJWTToken?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (missingJWTToken != null) {
      return missingJWTToken();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return missingJWTToken(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return missingJWTToken?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (missingJWTToken != null) {
      return missingJWTToken(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$MissingJWTTokenImplToJson(
      this,
    );
  }
}

abstract class MissingJWTToken implements ApiError {
  const factory MissingJWTToken() = _$MissingJWTTokenImpl;

  factory MissingJWTToken.fromJson(Map<String, dynamic> json) =
      _$MissingJWTTokenImpl.fromJson;
}

/// @nodoc
abstract class _$$TokenValidationFailureImplCopyWith<$Res> {
  factory _$$TokenValidationFailureImplCopyWith(
          _$TokenValidationFailureImpl value,
          $Res Function(_$TokenValidationFailureImpl) then) =
      __$$TokenValidationFailureImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$TokenValidationFailureImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$TokenValidationFailureImpl>
    implements _$$TokenValidationFailureImplCopyWith<$Res> {
  __$$TokenValidationFailureImplCopyWithImpl(
      _$TokenValidationFailureImpl _value,
      $Res Function(_$TokenValidationFailureImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$TokenValidationFailureImpl implements TokenValidationFailure {
  const _$TokenValidationFailureImpl({final String? $type})
      : $type = $type ?? 'TokenValidationFailure';

  factory _$TokenValidationFailureImpl.fromJson(Map<String, dynamic> json) =>
      _$$TokenValidationFailureImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.tokenValidationFailure()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TokenValidationFailureImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return tokenValidationFailure();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return tokenValidationFailure?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (tokenValidationFailure != null) {
      return tokenValidationFailure();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return tokenValidationFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return tokenValidationFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (tokenValidationFailure != null) {
      return tokenValidationFailure(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$TokenValidationFailureImplToJson(
      this,
    );
  }
}

abstract class TokenValidationFailure implements ApiError {
  const factory TokenValidationFailure() = _$TokenValidationFailureImpl;

  factory TokenValidationFailure.fromJson(Map<String, dynamic> json) =
      _$TokenValidationFailureImpl.fromJson;
}

/// @nodoc
abstract class _$$InvalidDisplaynameImplCopyWith<$Res> {
  factory _$$InvalidDisplaynameImplCopyWith(_$InvalidDisplaynameImpl value,
          $Res Function(_$InvalidDisplaynameImpl) then) =
      __$$InvalidDisplaynameImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidDisplaynameImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$InvalidDisplaynameImpl>
    implements _$$InvalidDisplaynameImplCopyWith<$Res> {
  __$$InvalidDisplaynameImplCopyWithImpl(_$InvalidDisplaynameImpl _value,
      $Res Function(_$InvalidDisplaynameImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$InvalidDisplaynameImpl implements InvalidDisplayname {
  const _$InvalidDisplaynameImpl({final String? $type})
      : $type = $type ?? 'InvalidDisplayname';

  factory _$InvalidDisplaynameImpl.fromJson(Map<String, dynamic> json) =>
      _$$InvalidDisplaynameImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.invalidDisplayname()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvalidDisplaynameImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return invalidDisplayname();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return invalidDisplayname?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (invalidDisplayname != null) {
      return invalidDisplayname();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return invalidDisplayname(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return invalidDisplayname?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (invalidDisplayname != null) {
      return invalidDisplayname(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$InvalidDisplaynameImplToJson(
      this,
    );
  }
}

abstract class InvalidDisplayname implements ApiError {
  const factory InvalidDisplayname() = _$InvalidDisplaynameImpl;

  factory InvalidDisplayname.fromJson(Map<String, dynamic> json) =
      _$InvalidDisplaynameImpl.fromJson;
}

/// @nodoc
abstract class _$$InvalidEmailImplCopyWith<$Res> {
  factory _$$InvalidEmailImplCopyWith(
          _$InvalidEmailImpl value, $Res Function(_$InvalidEmailImpl) then) =
      __$$InvalidEmailImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidEmailImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$InvalidEmailImpl>
    implements _$$InvalidEmailImplCopyWith<$Res> {
  __$$InvalidEmailImplCopyWithImpl(
      _$InvalidEmailImpl _value, $Res Function(_$InvalidEmailImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$InvalidEmailImpl implements InvalidEmail {
  const _$InvalidEmailImpl({final String? $type})
      : $type = $type ?? 'InvalidEmail';

  factory _$InvalidEmailImpl.fromJson(Map<String, dynamic> json) =>
      _$$InvalidEmailImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.invalidEmail()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvalidEmailImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return invalidEmail();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return invalidEmail?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return invalidEmail(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return invalidEmail?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (invalidEmail != null) {
      return invalidEmail(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$InvalidEmailImplToJson(
      this,
    );
  }
}

abstract class InvalidEmail implements ApiError {
  const factory InvalidEmail() = _$InvalidEmailImpl;

  factory InvalidEmail.fromJson(Map<String, dynamic> json) =
      _$InvalidEmailImpl.fromJson;
}

/// @nodoc
abstract class _$$InvalidPasswordImplCopyWith<$Res> {
  factory _$$InvalidPasswordImplCopyWith(_$InvalidPasswordImpl value,
          $Res Function(_$InvalidPasswordImpl) then) =
      __$$InvalidPasswordImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InvalidPasswordImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$InvalidPasswordImpl>
    implements _$$InvalidPasswordImplCopyWith<$Res> {
  __$$InvalidPasswordImplCopyWithImpl(
      _$InvalidPasswordImpl _value, $Res Function(_$InvalidPasswordImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$InvalidPasswordImpl implements InvalidPassword {
  const _$InvalidPasswordImpl({final String? $type})
      : $type = $type ?? 'InvalidPassword';

  factory _$InvalidPasswordImpl.fromJson(Map<String, dynamic> json) =>
      _$$InvalidPasswordImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.invalidPassword()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$InvalidPasswordImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return invalidPassword();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return invalidPassword?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (invalidPassword != null) {
      return invalidPassword();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return invalidPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return invalidPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (invalidPassword != null) {
      return invalidPassword(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$InvalidPasswordImplToJson(
      this,
    );
  }
}

abstract class InvalidPassword implements ApiError {
  const factory InvalidPassword() = _$InvalidPasswordImpl;

  factory InvalidPassword.fromJson(Map<String, dynamic> json) =
      _$InvalidPasswordImpl.fromJson;
}

/// @nodoc
abstract class _$$UnconfirmedUserImplCopyWith<$Res> {
  factory _$$UnconfirmedUserImplCopyWith(_$UnconfirmedUserImpl value,
          $Res Function(_$UnconfirmedUserImpl) then) =
      __$$UnconfirmedUserImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnconfirmedUserImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$UnconfirmedUserImpl>
    implements _$$UnconfirmedUserImplCopyWith<$Res> {
  __$$UnconfirmedUserImplCopyWithImpl(
      _$UnconfirmedUserImpl _value, $Res Function(_$UnconfirmedUserImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$UnconfirmedUserImpl implements UnconfirmedUser {
  const _$UnconfirmedUserImpl({final String? $type})
      : $type = $type ?? 'UnconfirmedUser';

  factory _$UnconfirmedUserImpl.fromJson(Map<String, dynamic> json) =>
      _$$UnconfirmedUserImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.unconfirmedUser()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UnconfirmedUserImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return unconfirmedUser();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return unconfirmedUser?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (unconfirmedUser != null) {
      return unconfirmedUser();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return unconfirmedUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return unconfirmedUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unconfirmedUser != null) {
      return unconfirmedUser(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$UnconfirmedUserImplToJson(
      this,
    );
  }
}

abstract class UnconfirmedUser implements ApiError {
  const factory UnconfirmedUser() = _$UnconfirmedUserImpl;

  factory UnconfirmedUser.fromJson(Map<String, dynamic> json) =
      _$UnconfirmedUserImpl.fromJson;
}

/// @nodoc
abstract class _$$UnknownGatewayImplCopyWith<$Res> {
  factory _$$UnknownGatewayImplCopyWith(_$UnknownGatewayImpl value,
          $Res Function(_$UnknownGatewayImpl) then) =
      __$$UnknownGatewayImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnknownGatewayImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$UnknownGatewayImpl>
    implements _$$UnknownGatewayImplCopyWith<$Res> {
  __$$UnknownGatewayImplCopyWithImpl(
      _$UnknownGatewayImpl _value, $Res Function(_$UnknownGatewayImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$UnknownGatewayImpl implements UnknownGateway {
  const _$UnknownGatewayImpl({final String? $type})
      : $type = $type ?? 'UnknownGateway';

  factory _$UnknownGatewayImpl.fromJson(Map<String, dynamic> json) =>
      _$$UnknownGatewayImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.unknownGateway()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UnknownGatewayImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return unknownGateway();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return unknownGateway?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (unknownGateway != null) {
      return unknownGateway();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return unknownGateway(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return unknownGateway?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unknownGateway != null) {
      return unknownGateway(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$UnknownGatewayImplToJson(
      this,
    );
  }
}

abstract class UnknownGateway implements ApiError {
  const factory UnknownGateway() = _$UnknownGatewayImpl;

  factory UnknownGateway.fromJson(Map<String, dynamic> json) =
      _$UnknownGatewayImpl.fromJson;
}

/// @nodoc
abstract class _$$UnknownSensorImplCopyWith<$Res> {
  factory _$$UnknownSensorImplCopyWith(
          _$UnknownSensorImpl value, $Res Function(_$UnknownSensorImpl) then) =
      __$$UnknownSensorImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnknownSensorImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$UnknownSensorImpl>
    implements _$$UnknownSensorImplCopyWith<$Res> {
  __$$UnknownSensorImplCopyWithImpl(
      _$UnknownSensorImpl _value, $Res Function(_$UnknownSensorImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$UnknownSensorImpl implements UnknownSensor {
  const _$UnknownSensorImpl({final String? $type})
      : $type = $type ?? 'UnknownSensor';

  factory _$UnknownSensorImpl.fromJson(Map<String, dynamic> json) =>
      _$$UnknownSensorImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.unknownSensor()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UnknownSensorImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return unknownSensor();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return unknownSensor?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (unknownSensor != null) {
      return unknownSensor();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return unknownSensor(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return unknownSensor?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unknownSensor != null) {
      return unknownSensor(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$UnknownSensorImplToJson(
      this,
    );
  }
}

abstract class UnknownSensor implements ApiError {
  const factory UnknownSensor() = _$UnknownSensorImpl;

  factory UnknownSensor.fromJson(Map<String, dynamic> json) =
      _$UnknownSensorImpl.fromJson;
}

/// @nodoc
abstract class _$$UnexpectedResponseDataImplCopyWith<$Res> {
  factory _$$UnexpectedResponseDataImplCopyWith(
          _$UnexpectedResponseDataImpl value,
          $Res Function(_$UnexpectedResponseDataImpl) then) =
      __$$UnexpectedResponseDataImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnexpectedResponseDataImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$UnexpectedResponseDataImpl>
    implements _$$UnexpectedResponseDataImplCopyWith<$Res> {
  __$$UnexpectedResponseDataImplCopyWithImpl(
      _$UnexpectedResponseDataImpl _value,
      $Res Function(_$UnexpectedResponseDataImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$UnexpectedResponseDataImpl implements UnexpectedResponseData {
  const _$UnexpectedResponseDataImpl({final String? $type})
      : $type = $type ?? 'UnexpectedResponseData';

  factory _$UnexpectedResponseDataImpl.fromJson(Map<String, dynamic> json) =>
      _$$UnexpectedResponseDataImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.unexpectedResponseData()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnexpectedResponseDataImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return unexpectedResponseData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return unexpectedResponseData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (unexpectedResponseData != null) {
      return unexpectedResponseData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return unexpectedResponseData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return unexpectedResponseData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unexpectedResponseData != null) {
      return unexpectedResponseData(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$UnexpectedResponseDataImplToJson(
      this,
    );
  }
}

abstract class UnexpectedResponseData implements ApiError {
  const factory UnexpectedResponseData() = _$UnexpectedResponseDataImpl;

  factory UnexpectedResponseData.fromJson(Map<String, dynamic> json) =
      _$UnexpectedResponseDataImpl.fromJson;
}

/// @nodoc
abstract class _$$InternalServerErrorImplCopyWith<$Res> {
  factory _$$InternalServerErrorImplCopyWith(_$InternalServerErrorImpl value,
          $Res Function(_$InternalServerErrorImpl) then) =
      __$$InternalServerErrorImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InternalServerErrorImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$InternalServerErrorImpl>
    implements _$$InternalServerErrorImplCopyWith<$Res> {
  __$$InternalServerErrorImplCopyWithImpl(_$InternalServerErrorImpl _value,
      $Res Function(_$InternalServerErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$InternalServerErrorImpl implements InternalServerError {
  const _$InternalServerErrorImpl({final String? $type})
      : $type = $type ?? 'InternalServerError';

  factory _$InternalServerErrorImpl.fromJson(Map<String, dynamic> json) =>
      _$$InternalServerErrorImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.internalServerError()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InternalServerErrorImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return internalServerError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return internalServerError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (internalServerError != null) {
      return internalServerError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return internalServerError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return internalServerError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (internalServerError != null) {
      return internalServerError(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$InternalServerErrorImplToJson(
      this,
    );
  }
}

abstract class InternalServerError implements ApiError {
  const factory InternalServerError() = _$InternalServerErrorImpl;

  factory InternalServerError.fromJson(Map<String, dynamic> json) =
      _$InternalServerErrorImpl.fromJson;
}

/// @nodoc
abstract class _$$NotFoundImplCopyWith<$Res> {
  factory _$$NotFoundImplCopyWith(
          _$NotFoundImpl value, $Res Function(_$NotFoundImpl) then) =
      __$$NotFoundImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NotFoundImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$NotFoundImpl>
    implements _$$NotFoundImplCopyWith<$Res> {
  __$$NotFoundImplCopyWithImpl(
      _$NotFoundImpl _value, $Res Function(_$NotFoundImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$NotFoundImpl implements NotFound {
  const _$NotFoundImpl({final String? $type}) : $type = $type ?? 'NotFound';

  factory _$NotFoundImpl.fromJson(Map<String, dynamic> json) =>
      _$$NotFoundImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.notFound()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NotFoundImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return notFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return notFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return notFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return notFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (notFound != null) {
      return notFound(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NotFoundImplToJson(
      this,
    );
  }
}

abstract class NotFound implements ApiError {
  const factory NotFound() = _$NotFoundImpl;

  factory NotFound.fromJson(Map<String, dynamic> json) =
      _$NotFoundImpl.fromJson;
}

/// @nodoc
abstract class _$$BadRequestImplCopyWith<$Res> {
  factory _$$BadRequestImplCopyWith(
          _$BadRequestImpl value, $Res Function(_$BadRequestImpl) then) =
      __$$BadRequestImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$BadRequestImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$BadRequestImpl>
    implements _$$BadRequestImplCopyWith<$Res> {
  __$$BadRequestImplCopyWithImpl(
      _$BadRequestImpl _value, $Res Function(_$BadRequestImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$BadRequestImpl implements BadRequest {
  const _$BadRequestImpl({final String? $type}) : $type = $type ?? 'BadRequest';

  factory _$BadRequestImpl.fromJson(Map<String, dynamic> json) =>
      _$$BadRequestImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.badRequest()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$BadRequestImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return badRequest();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return badRequest?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (badRequest != null) {
      return badRequest();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return badRequest(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return badRequest?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (badRequest != null) {
      return badRequest(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$BadRequestImplToJson(
      this,
    );
  }
}

abstract class BadRequest implements ApiError {
  const factory BadRequest() = _$BadRequestImpl;

  factory BadRequest.fromJson(Map<String, dynamic> json) =
      _$BadRequestImpl.fromJson;
}

/// @nodoc
abstract class _$$NoConnectionImplCopyWith<$Res> {
  factory _$$NoConnectionImplCopyWith(
          _$NoConnectionImpl value, $Res Function(_$NoConnectionImpl) then) =
      __$$NoConnectionImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoConnectionImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$NoConnectionImpl>
    implements _$$NoConnectionImplCopyWith<$Res> {
  __$$NoConnectionImplCopyWithImpl(
      _$NoConnectionImpl _value, $Res Function(_$NoConnectionImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$NoConnectionImpl implements NoConnection {
  const _$NoConnectionImpl({final String? $type})
      : $type = $type ?? 'NoConnection';

  factory _$NoConnectionImpl.fromJson(Map<String, dynamic> json) =>
      _$$NoConnectionImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.noConnection()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NoConnectionImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return noConnection();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return noConnection?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (noConnection != null) {
      return noConnection();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return noConnection(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return noConnection?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (noConnection != null) {
      return noConnection(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NoConnectionImplToJson(
      this,
    );
  }
}

abstract class NoConnection implements ApiError {
  const factory NoConnection() = _$NoConnectionImpl;

  factory NoConnection.fromJson(Map<String, dynamic> json) =
      _$NoConnectionImpl.fromJson;
}

/// @nodoc
abstract class _$$NotLoggedInImplCopyWith<$Res> {
  factory _$$NotLoggedInImplCopyWith(
          _$NotLoggedInImpl value, $Res Function(_$NotLoggedInImpl) then) =
      __$$NotLoggedInImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NotLoggedInImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$NotLoggedInImpl>
    implements _$$NotLoggedInImplCopyWith<$Res> {
  __$$NotLoggedInImplCopyWithImpl(
      _$NotLoggedInImpl _value, $Res Function(_$NotLoggedInImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$NotLoggedInImpl implements NotLoggedIn {
  const _$NotLoggedInImpl({final String? $type})
      : $type = $type ?? 'NotLoggedIn';

  factory _$NotLoggedInImpl.fromJson(Map<String, dynamic> json) =>
      _$$NotLoggedInImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.notLoggedIn()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NotLoggedInImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return notLoggedIn();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return notLoggedIn?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (notLoggedIn != null) {
      return notLoggedIn();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return notLoggedIn(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return notLoggedIn?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (notLoggedIn != null) {
      return notLoggedIn(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$NotLoggedInImplToJson(
      this,
    );
  }
}

abstract class NotLoggedIn implements ApiError {
  const factory NotLoggedIn() = _$NotLoggedInImpl;

  factory NotLoggedIn.fromJson(Map<String, dynamic> json) =
      _$NotLoggedInImpl.fromJson;
}

/// @nodoc
abstract class _$$UnknownImplCopyWith<$Res> {
  factory _$$UnknownImplCopyWith(
          _$UnknownImpl value, $Res Function(_$UnknownImpl) then) =
      __$$UnknownImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnknownImplCopyWithImpl<$Res>
    extends _$ApiErrorCopyWithImpl<$Res, _$UnknownImpl>
    implements _$$UnknownImplCopyWith<$Res> {
  __$$UnknownImplCopyWithImpl(
      _$UnknownImpl _value, $Res Function(_$UnknownImpl) _then)
      : super(_value, _then);

  /// Create a copy of ApiError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
@JsonSerializable()
class _$UnknownImpl implements Unknown {
  const _$UnknownImpl({final String? $type}) : $type = $type ?? 'Unknown';

  factory _$UnknownImpl.fromJson(Map<String, dynamic> json) =>
      _$$UnknownImplFromJson(json);

  @JsonKey(name: 'cause')
  final String $type;

  @override
  String toString() {
    return 'ApiError.unknown()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$UnknownImpl);
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() invalidCredentials,
    required TResult Function() missingJWTToken,
    required TResult Function() tokenValidationFailure,
    required TResult Function() invalidDisplayname,
    required TResult Function() invalidEmail,
    required TResult Function() invalidPassword,
    required TResult Function() unconfirmedUser,
    required TResult Function() unknownGateway,
    required TResult Function() unknownSensor,
    required TResult Function() unexpectedResponseData,
    required TResult Function() internalServerError,
    required TResult Function() notFound,
    required TResult Function() badRequest,
    required TResult Function() noConnection,
    required TResult Function() notLoggedIn,
    required TResult Function() unknown,
  }) {
    return unknown();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? invalidCredentials,
    TResult? Function()? missingJWTToken,
    TResult? Function()? tokenValidationFailure,
    TResult? Function()? invalidDisplayname,
    TResult? Function()? invalidEmail,
    TResult? Function()? invalidPassword,
    TResult? Function()? unconfirmedUser,
    TResult? Function()? unknownGateway,
    TResult? Function()? unknownSensor,
    TResult? Function()? unexpectedResponseData,
    TResult? Function()? internalServerError,
    TResult? Function()? notFound,
    TResult? Function()? badRequest,
    TResult? Function()? noConnection,
    TResult? Function()? notLoggedIn,
    TResult? Function()? unknown,
  }) {
    return unknown?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? invalidCredentials,
    TResult Function()? missingJWTToken,
    TResult Function()? tokenValidationFailure,
    TResult Function()? invalidDisplayname,
    TResult Function()? invalidEmail,
    TResult Function()? invalidPassword,
    TResult Function()? unconfirmedUser,
    TResult Function()? unknownGateway,
    TResult Function()? unknownSensor,
    TResult Function()? unexpectedResponseData,
    TResult Function()? internalServerError,
    TResult Function()? notFound,
    TResult Function()? badRequest,
    TResult Function()? noConnection,
    TResult Function()? notLoggedIn,
    TResult Function()? unknown,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(InvalidCredentials value) invalidCredentials,
    required TResult Function(MissingJWTToken value) missingJWTToken,
    required TResult Function(TokenValidationFailure value)
        tokenValidationFailure,
    required TResult Function(InvalidDisplayname value) invalidDisplayname,
    required TResult Function(InvalidEmail value) invalidEmail,
    required TResult Function(InvalidPassword value) invalidPassword,
    required TResult Function(UnconfirmedUser value) unconfirmedUser,
    required TResult Function(UnknownGateway value) unknownGateway,
    required TResult Function(UnknownSensor value) unknownSensor,
    required TResult Function(UnexpectedResponseData value)
        unexpectedResponseData,
    required TResult Function(InternalServerError value) internalServerError,
    required TResult Function(NotFound value) notFound,
    required TResult Function(BadRequest value) badRequest,
    required TResult Function(NoConnection value) noConnection,
    required TResult Function(NotLoggedIn value) notLoggedIn,
    required TResult Function(Unknown value) unknown,
  }) {
    return unknown(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(InvalidCredentials value)? invalidCredentials,
    TResult? Function(MissingJWTToken value)? missingJWTToken,
    TResult? Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult? Function(InvalidDisplayname value)? invalidDisplayname,
    TResult? Function(InvalidEmail value)? invalidEmail,
    TResult? Function(InvalidPassword value)? invalidPassword,
    TResult? Function(UnconfirmedUser value)? unconfirmedUser,
    TResult? Function(UnknownGateway value)? unknownGateway,
    TResult? Function(UnknownSensor value)? unknownSensor,
    TResult? Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult? Function(InternalServerError value)? internalServerError,
    TResult? Function(NotFound value)? notFound,
    TResult? Function(BadRequest value)? badRequest,
    TResult? Function(NoConnection value)? noConnection,
    TResult? Function(NotLoggedIn value)? notLoggedIn,
    TResult? Function(Unknown value)? unknown,
  }) {
    return unknown?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(InvalidCredentials value)? invalidCredentials,
    TResult Function(MissingJWTToken value)? missingJWTToken,
    TResult Function(TokenValidationFailure value)? tokenValidationFailure,
    TResult Function(InvalidDisplayname value)? invalidDisplayname,
    TResult Function(InvalidEmail value)? invalidEmail,
    TResult Function(InvalidPassword value)? invalidPassword,
    TResult Function(UnconfirmedUser value)? unconfirmedUser,
    TResult Function(UnknownGateway value)? unknownGateway,
    TResult Function(UnknownSensor value)? unknownSensor,
    TResult Function(UnexpectedResponseData value)? unexpectedResponseData,
    TResult Function(InternalServerError value)? internalServerError,
    TResult Function(NotFound value)? notFound,
    TResult Function(BadRequest value)? badRequest,
    TResult Function(NoConnection value)? noConnection,
    TResult Function(NotLoggedIn value)? notLoggedIn,
    TResult Function(Unknown value)? unknown,
    required TResult orElse(),
  }) {
    if (unknown != null) {
      return unknown(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$$UnknownImplToJson(
      this,
    );
  }
}

abstract class Unknown implements ApiError {
  const factory Unknown() = _$UnknownImpl;

  factory Unknown.fromJson(Map<String, dynamic> json) = _$UnknownImpl.fromJson;
}

/// @nodoc
mixin _$DbError {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DbErrorCopyWith<$Res> {
  factory $DbErrorCopyWith(DbError value, $Res Function(DbError) then) =
      _$DbErrorCopyWithImpl<$Res, DbError>;
}

/// @nodoc
class _$DbErrorCopyWithImpl<$Res, $Val extends DbError>
    implements $DbErrorCopyWith<$Res> {
  _$DbErrorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
abstract class _$$CreateTableFailedImplCopyWith<$Res> {
  factory _$$CreateTableFailedImplCopyWith(_$CreateTableFailedImpl value,
          $Res Function(_$CreateTableFailedImpl) then) =
      __$$CreateTableFailedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String tableName, Exception error});
}

/// @nodoc
class __$$CreateTableFailedImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$CreateTableFailedImpl>
    implements _$$CreateTableFailedImplCopyWith<$Res> {
  __$$CreateTableFailedImplCopyWithImpl(_$CreateTableFailedImpl _value,
      $Res Function(_$CreateTableFailedImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? tableName = null,
    Object? error = null,
  }) {
    return _then(_$CreateTableFailedImpl(
      null == tableName
          ? _value.tableName
          : tableName // ignore: cast_nullable_to_non_nullable
              as String,
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$CreateTableFailedImpl implements CreateTableFailed {
  const _$CreateTableFailedImpl(this.tableName, this.error);

  @override
  final String tableName;
  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.createTableError(tableName: $tableName, error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateTableFailedImpl &&
            (identical(other.tableName, tableName) ||
                other.tableName == tableName) &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, tableName, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateTableFailedImplCopyWith<_$CreateTableFailedImpl> get copyWith =>
      __$$CreateTableFailedImplCopyWithImpl<_$CreateTableFailedImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return createTableError(tableName, error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return createTableError?.call(tableName, error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (createTableError != null) {
      return createTableError(tableName, error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return createTableError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return createTableError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (createTableError != null) {
      return createTableError(this);
    }
    return orElse();
  }
}

abstract class CreateTableFailed implements DbError {
  const factory CreateTableFailed(
      final String tableName, final Exception error) = _$CreateTableFailedImpl;

  String get tableName;
  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$CreateTableFailedImplCopyWith<_$CreateTableFailedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DeleteErrorImplCopyWith<$Res> {
  factory _$$DeleteErrorImplCopyWith(
          _$DeleteErrorImpl value, $Res Function(_$DeleteErrorImpl) then) =
      __$$DeleteErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Exception error});
}

/// @nodoc
class __$$DeleteErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$DeleteErrorImpl>
    implements _$$DeleteErrorImplCopyWith<$Res> {
  __$$DeleteErrorImplCopyWithImpl(
      _$DeleteErrorImpl _value, $Res Function(_$DeleteErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$DeleteErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$DeleteErrorImpl implements DeleteError {
  const _$DeleteErrorImpl(this.error);

  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.deleteError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DeleteErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$DeleteErrorImplCopyWith<_$DeleteErrorImpl> get copyWith =>
      __$$DeleteErrorImplCopyWithImpl<_$DeleteErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return deleteError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return deleteError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (deleteError != null) {
      return deleteError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return deleteError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return deleteError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (deleteError != null) {
      return deleteError(this);
    }
    return orElse();
  }
}

abstract class DeleteError implements DbError {
  const factory DeleteError(final Exception error) = _$DeleteErrorImpl;

  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$DeleteErrorImplCopyWith<_$DeleteErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetSingleErrorImplCopyWith<$Res> {
  factory _$$GetSingleErrorImplCopyWith(_$GetSingleErrorImpl value,
          $Res Function(_$GetSingleErrorImpl) then) =
      __$$GetSingleErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Exception error});
}

/// @nodoc
class __$$GetSingleErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$GetSingleErrorImpl>
    implements _$$GetSingleErrorImplCopyWith<$Res> {
  __$$GetSingleErrorImplCopyWithImpl(
      _$GetSingleErrorImpl _value, $Res Function(_$GetSingleErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$GetSingleErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$GetSingleErrorImpl implements GetSingleError {
  const _$GetSingleErrorImpl(this.error);

  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.getSingleError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetSingleErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GetSingleErrorImplCopyWith<_$GetSingleErrorImpl> get copyWith =>
      __$$GetSingleErrorImplCopyWithImpl<_$GetSingleErrorImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return getSingleError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return getSingleError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (getSingleError != null) {
      return getSingleError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return getSingleError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return getSingleError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (getSingleError != null) {
      return getSingleError(this);
    }
    return orElse();
  }
}

abstract class GetSingleError implements DbError {
  const factory GetSingleError(final Exception error) = _$GetSingleErrorImpl;

  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GetSingleErrorImplCopyWith<_$GetSingleErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetAllErrorImplCopyWith<$Res> {
  factory _$$GetAllErrorImplCopyWith(
          _$GetAllErrorImpl value, $Res Function(_$GetAllErrorImpl) then) =
      __$$GetAllErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Exception error});
}

/// @nodoc
class __$$GetAllErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$GetAllErrorImpl>
    implements _$$GetAllErrorImplCopyWith<$Res> {
  __$$GetAllErrorImplCopyWithImpl(
      _$GetAllErrorImpl _value, $Res Function(_$GetAllErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$GetAllErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$GetAllErrorImpl implements GetAllError {
  const _$GetAllErrorImpl(this.error);

  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.getAllError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetAllErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GetAllErrorImplCopyWith<_$GetAllErrorImpl> get copyWith =>
      __$$GetAllErrorImplCopyWithImpl<_$GetAllErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return getAllError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return getAllError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (getAllError != null) {
      return getAllError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return getAllError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return getAllError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (getAllError != null) {
      return getAllError(this);
    }
    return orElse();
  }
}

abstract class GetAllError implements DbError {
  const factory GetAllError(final Exception error) = _$GetAllErrorImpl;

  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GetAllErrorImplCopyWith<_$GetAllErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InsertErrorImplCopyWith<$Res> {
  factory _$$InsertErrorImplCopyWith(
          _$InsertErrorImpl value, $Res Function(_$InsertErrorImpl) then) =
      __$$InsertErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Exception error});
}

/// @nodoc
class __$$InsertErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$InsertErrorImpl>
    implements _$$InsertErrorImplCopyWith<$Res> {
  __$$InsertErrorImplCopyWithImpl(
      _$InsertErrorImpl _value, $Res Function(_$InsertErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$InsertErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$InsertErrorImpl implements InsertError {
  const _$InsertErrorImpl(this.error);

  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.insertError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InsertErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$InsertErrorImplCopyWith<_$InsertErrorImpl> get copyWith =>
      __$$InsertErrorImplCopyWithImpl<_$InsertErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return insertError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return insertError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (insertError != null) {
      return insertError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return insertError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return insertError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (insertError != null) {
      return insertError(this);
    }
    return orElse();
  }
}

abstract class InsertError implements DbError {
  const factory InsertError(final Exception error) = _$InsertErrorImpl;

  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$InsertErrorImplCopyWith<_$InsertErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UpdatetErrorImplCopyWith<$Res> {
  factory _$$UpdatetErrorImplCopyWith(
          _$UpdatetErrorImpl value, $Res Function(_$UpdatetErrorImpl) then) =
      __$$UpdatetErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Exception error});
}

/// @nodoc
class __$$UpdatetErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$UpdatetErrorImpl>
    implements _$$UpdatetErrorImplCopyWith<$Res> {
  __$$UpdatetErrorImplCopyWithImpl(
      _$UpdatetErrorImpl _value, $Res Function(_$UpdatetErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$UpdatetErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$UpdatetErrorImpl implements UpdatetError {
  const _$UpdatetErrorImpl(this.error);

  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.updateError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdatetErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdatetErrorImplCopyWith<_$UpdatetErrorImpl> get copyWith =>
      __$$UpdatetErrorImplCopyWithImpl<_$UpdatetErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return updateError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return updateError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (updateError != null) {
      return updateError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return updateError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return updateError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (updateError != null) {
      return updateError(this);
    }
    return orElse();
  }
}

abstract class UpdatetError implements DbError {
  const factory UpdatetError(final Exception error) = _$UpdatetErrorImpl;

  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UpdatetErrorImplCopyWith<_$UpdatetErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$TransactionErrorImplCopyWith<$Res> {
  factory _$$TransactionErrorImplCopyWith(_$TransactionErrorImpl value,
          $Res Function(_$TransactionErrorImpl) then) =
      __$$TransactionErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({Exception error});
}

/// @nodoc
class __$$TransactionErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$TransactionErrorImpl>
    implements _$$TransactionErrorImplCopyWith<$Res> {
  __$$TransactionErrorImplCopyWithImpl(_$TransactionErrorImpl _value,
      $Res Function(_$TransactionErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$TransactionErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as Exception,
    ));
  }
}

/// @nodoc

class _$TransactionErrorImpl implements TransactionError {
  const _$TransactionErrorImpl(this.error);

  @override
  final Exception error;

  @override
  String toString() {
    return 'DbError.transactionError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$TransactionErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$TransactionErrorImplCopyWith<_$TransactionErrorImpl> get copyWith =>
      __$$TransactionErrorImplCopyWithImpl<_$TransactionErrorImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return transactionError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return transactionError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (transactionError != null) {
      return transactionError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return transactionError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return transactionError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (transactionError != null) {
      return transactionError(this);
    }
    return orElse();
  }
}

abstract class TransactionError implements DbError {
  const factory TransactionError(final Exception error) =
      _$TransactionErrorImpl;

  Exception get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$TransactionErrorImplCopyWith<_$TransactionErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$InitErrorImplCopyWith<$Res> {
  factory _$$InitErrorImplCopyWith(
          _$InitErrorImpl value, $Res Function(_$InitErrorImpl) then) =
      __$$InitErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$InitErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$InitErrorImpl>
    implements _$$InitErrorImplCopyWith<$Res> {
  __$$InitErrorImplCopyWithImpl(
      _$InitErrorImpl _value, $Res Function(_$InitErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$InitErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$InitErrorImpl implements InitError {
  const _$InitErrorImpl(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'DbError.initError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$InitErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$InitErrorImplCopyWith<_$InitErrorImpl> get copyWith =>
      __$$InitErrorImplCopyWithImpl<_$InitErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return initError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return initError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (initError != null) {
      return initError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return initError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return initError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (initError != null) {
      return initError(this);
    }
    return orElse();
  }
}

abstract class InitError implements DbError {
  const factory InitError(final String error) = _$InitErrorImpl;

  String get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$InitErrorImplCopyWith<_$InitErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NoDatabaseImplCopyWith<$Res> {
  factory _$$NoDatabaseImplCopyWith(
          _$NoDatabaseImpl value, $Res Function(_$NoDatabaseImpl) then) =
      __$$NoDatabaseImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoDatabaseImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$NoDatabaseImpl>
    implements _$$NoDatabaseImplCopyWith<$Res> {
  __$$NoDatabaseImplCopyWithImpl(
      _$NoDatabaseImpl _value, $Res Function(_$NoDatabaseImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$NoDatabaseImpl implements NoDatabase {
  const _$NoDatabaseImpl();

  @override
  String toString() {
    return 'DbError.noDatabase()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NoDatabaseImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return noDatabase();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return noDatabase?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (noDatabase != null) {
      return noDatabase();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return noDatabase(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return noDatabase?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (noDatabase != null) {
      return noDatabase(this);
    }
    return orElse();
  }
}

abstract class NoDatabase implements DbError {
  const factory NoDatabase() = _$NoDatabaseImpl;
}

/// @nodoc
abstract class _$$NoReferenceToDatabaseImplCopyWith<$Res> {
  factory _$$NoReferenceToDatabaseImplCopyWith(
          _$NoReferenceToDatabaseImpl value,
          $Res Function(_$NoReferenceToDatabaseImpl) then) =
      __$$NoReferenceToDatabaseImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoReferenceToDatabaseImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$NoReferenceToDatabaseImpl>
    implements _$$NoReferenceToDatabaseImplCopyWith<$Res> {
  __$$NoReferenceToDatabaseImplCopyWithImpl(_$NoReferenceToDatabaseImpl _value,
      $Res Function(_$NoReferenceToDatabaseImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$NoReferenceToDatabaseImpl implements NoReferenceToDatabase {
  const _$NoReferenceToDatabaseImpl();

  @override
  String toString() {
    return 'DbError.noReferenceToDatabase()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$NoReferenceToDatabaseImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return noReferenceToDatabase();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return noReferenceToDatabase?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (noReferenceToDatabase != null) {
      return noReferenceToDatabase();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return noReferenceToDatabase(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return noReferenceToDatabase?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (noReferenceToDatabase != null) {
      return noReferenceToDatabase(this);
    }
    return orElse();
  }
}

abstract class NoReferenceToDatabase implements DbError {
  const factory NoReferenceToDatabase() = _$NoReferenceToDatabaseImpl;
}

/// @nodoc
abstract class _$$UnknownDbTaskErrorImplCopyWith<$Res> {
  factory _$$UnknownDbTaskErrorImplCopyWith(_$UnknownDbTaskErrorImpl value,
          $Res Function(_$UnknownDbTaskErrorImpl) then) =
      __$$UnknownDbTaskErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$UnknownDbTaskErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$UnknownDbTaskErrorImpl>
    implements _$$UnknownDbTaskErrorImplCopyWith<$Res> {
  __$$UnknownDbTaskErrorImplCopyWithImpl(_$UnknownDbTaskErrorImpl _value,
      $Res Function(_$UnknownDbTaskErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$UnknownDbTaskErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UnknownDbTaskErrorImpl implements UnknownDbTaskError {
  const _$UnknownDbTaskErrorImpl(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'DbError.unknownDbTaskError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownDbTaskErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UnknownDbTaskErrorImplCopyWith<_$UnknownDbTaskErrorImpl> get copyWith =>
      __$$UnknownDbTaskErrorImplCopyWithImpl<_$UnknownDbTaskErrorImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return unknownDbTaskError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return unknownDbTaskError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (unknownDbTaskError != null) {
      return unknownDbTaskError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return unknownDbTaskError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return unknownDbTaskError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (unknownDbTaskError != null) {
      return unknownDbTaskError(this);
    }
    return orElse();
  }
}

abstract class UnknownDbTaskError implements DbError {
  const factory UnknownDbTaskError(final String error) =
      _$UnknownDbTaskErrorImpl;

  String get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UnknownDbTaskErrorImplCopyWith<_$UnknownDbTaskErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UnknownEntityErrorImplCopyWith<$Res> {
  factory _$$UnknownEntityErrorImplCopyWith(_$UnknownEntityErrorImpl value,
          $Res Function(_$UnknownEntityErrorImpl) then) =
      __$$UnknownEntityErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$UnknownEntityErrorImplCopyWithImpl<$Res>
    extends _$DbErrorCopyWithImpl<$Res, _$UnknownEntityErrorImpl>
    implements _$$UnknownEntityErrorImplCopyWith<$Res> {
  __$$UnknownEntityErrorImplCopyWithImpl(_$UnknownEntityErrorImpl _value,
      $Res Function(_$UnknownEntityErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$UnknownEntityErrorImpl(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$UnknownEntityErrorImpl implements UnknownEntityError {
  const _$UnknownEntityErrorImpl(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'DbError.unknownEntityError(error: $error)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownEntityErrorImpl &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UnknownEntityErrorImplCopyWith<_$UnknownEntityErrorImpl> get copyWith =>
      __$$UnknownEntityErrorImplCopyWithImpl<_$UnknownEntityErrorImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String tableName, Exception error)
        createTableError,
    required TResult Function(Exception error) deleteError,
    required TResult Function(Exception error) getSingleError,
    required TResult Function(Exception error) getAllError,
    required TResult Function(Exception error) insertError,
    required TResult Function(Exception error) updateError,
    required TResult Function(Exception error) transactionError,
    required TResult Function(String error) initError,
    required TResult Function() noDatabase,
    required TResult Function() noReferenceToDatabase,
    required TResult Function(String error) unknownDbTaskError,
    required TResult Function(String error) unknownEntityError,
  }) {
    return unknownEntityError(error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String tableName, Exception error)? createTableError,
    TResult? Function(Exception error)? deleteError,
    TResult? Function(Exception error)? getSingleError,
    TResult? Function(Exception error)? getAllError,
    TResult? Function(Exception error)? insertError,
    TResult? Function(Exception error)? updateError,
    TResult? Function(Exception error)? transactionError,
    TResult? Function(String error)? initError,
    TResult? Function()? noDatabase,
    TResult? Function()? noReferenceToDatabase,
    TResult? Function(String error)? unknownDbTaskError,
    TResult? Function(String error)? unknownEntityError,
  }) {
    return unknownEntityError?.call(error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String tableName, Exception error)? createTableError,
    TResult Function(Exception error)? deleteError,
    TResult Function(Exception error)? getSingleError,
    TResult Function(Exception error)? getAllError,
    TResult Function(Exception error)? insertError,
    TResult Function(Exception error)? updateError,
    TResult Function(Exception error)? transactionError,
    TResult Function(String error)? initError,
    TResult Function()? noDatabase,
    TResult Function()? noReferenceToDatabase,
    TResult Function(String error)? unknownDbTaskError,
    TResult Function(String error)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (unknownEntityError != null) {
      return unknownEntityError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(CreateTableFailed value) createTableError,
    required TResult Function(DeleteError value) deleteError,
    required TResult Function(GetSingleError value) getSingleError,
    required TResult Function(GetAllError value) getAllError,
    required TResult Function(InsertError value) insertError,
    required TResult Function(UpdatetError value) updateError,
    required TResult Function(TransactionError value) transactionError,
    required TResult Function(InitError value) initError,
    required TResult Function(NoDatabase value) noDatabase,
    required TResult Function(NoReferenceToDatabase value)
        noReferenceToDatabase,
    required TResult Function(UnknownDbTaskError value) unknownDbTaskError,
    required TResult Function(UnknownEntityError value) unknownEntityError,
  }) {
    return unknownEntityError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(CreateTableFailed value)? createTableError,
    TResult? Function(DeleteError value)? deleteError,
    TResult? Function(GetSingleError value)? getSingleError,
    TResult? Function(GetAllError value)? getAllError,
    TResult? Function(InsertError value)? insertError,
    TResult? Function(UpdatetError value)? updateError,
    TResult? Function(TransactionError value)? transactionError,
    TResult? Function(InitError value)? initError,
    TResult? Function(NoDatabase value)? noDatabase,
    TResult? Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult? Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult? Function(UnknownEntityError value)? unknownEntityError,
  }) {
    return unknownEntityError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(CreateTableFailed value)? createTableError,
    TResult Function(DeleteError value)? deleteError,
    TResult Function(GetSingleError value)? getSingleError,
    TResult Function(GetAllError value)? getAllError,
    TResult Function(InsertError value)? insertError,
    TResult Function(UpdatetError value)? updateError,
    TResult Function(TransactionError value)? transactionError,
    TResult Function(InitError value)? initError,
    TResult Function(NoDatabase value)? noDatabase,
    TResult Function(NoReferenceToDatabase value)? noReferenceToDatabase,
    TResult Function(UnknownDbTaskError value)? unknownDbTaskError,
    TResult Function(UnknownEntityError value)? unknownEntityError,
    required TResult orElse(),
  }) {
    if (unknownEntityError != null) {
      return unknownEntityError(this);
    }
    return orElse();
  }
}

abstract class UnknownEntityError implements DbError {
  const factory UnknownEntityError(final String error) =
      _$UnknownEntityErrorImpl;

  String get error;

  /// Create a copy of DbError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UnknownEntityErrorImplCopyWith<_$UnknownEntityErrorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$DataError {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ApiError apiError) api,
    required TResult Function(DbError dbError) db,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ApiError apiError)? api,
    TResult? Function(DbError dbError)? db,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ApiError apiError)? api,
    TResult Function(DbError dbError)? db,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Api value) api,
    required TResult Function(Db value) db,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Api value)? api,
    TResult? Function(Db value)? db,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Api value)? api,
    TResult Function(Db value)? db,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DataErrorCopyWith<$Res> {
  factory $DataErrorCopyWith(DataError value, $Res Function(DataError) then) =
      _$DataErrorCopyWithImpl<$Res, DataError>;
}

/// @nodoc
class _$DataErrorCopyWithImpl<$Res, $Val extends DataError>
    implements $DataErrorCopyWith<$Res> {
  _$DataErrorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
abstract class _$$ApiImplCopyWith<$Res> {
  factory _$$ApiImplCopyWith(_$ApiImpl value, $Res Function(_$ApiImpl) then) =
      __$$ApiImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ApiError apiError});

  $ApiErrorCopyWith<$Res> get apiError;
}

/// @nodoc
class __$$ApiImplCopyWithImpl<$Res>
    extends _$DataErrorCopyWithImpl<$Res, _$ApiImpl>
    implements _$$ApiImplCopyWith<$Res> {
  __$$ApiImplCopyWithImpl(_$ApiImpl _value, $Res Function(_$ApiImpl) _then)
      : super(_value, _then);

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? apiError = null,
  }) {
    return _then(_$ApiImpl(
      null == apiError
          ? _value.apiError
          : apiError // ignore: cast_nullable_to_non_nullable
              as ApiError,
    ));
  }

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $ApiErrorCopyWith<$Res> get apiError {
    return $ApiErrorCopyWith<$Res>(_value.apiError, (value) {
      return _then(_value.copyWith(apiError: value));
    });
  }
}

/// @nodoc

class _$ApiImpl implements Api {
  const _$ApiImpl(this.apiError);

  @override
  final ApiError apiError;

  @override
  String toString() {
    return 'DataError.api(apiError: $apiError)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ApiImpl &&
            (identical(other.apiError, apiError) ||
                other.apiError == apiError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, apiError);

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ApiImplCopyWith<_$ApiImpl> get copyWith =>
      __$$ApiImplCopyWithImpl<_$ApiImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ApiError apiError) api,
    required TResult Function(DbError dbError) db,
  }) {
    return api(apiError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ApiError apiError)? api,
    TResult? Function(DbError dbError)? db,
  }) {
    return api?.call(apiError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ApiError apiError)? api,
    TResult Function(DbError dbError)? db,
    required TResult orElse(),
  }) {
    if (api != null) {
      return api(apiError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Api value) api,
    required TResult Function(Db value) db,
  }) {
    return api(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Api value)? api,
    TResult? Function(Db value)? db,
  }) {
    return api?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Api value)? api,
    TResult Function(Db value)? db,
    required TResult orElse(),
  }) {
    if (api != null) {
      return api(this);
    }
    return orElse();
  }
}

abstract class Api implements DataError {
  const factory Api(final ApiError apiError) = _$ApiImpl;

  ApiError get apiError;

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ApiImplCopyWith<_$ApiImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$DbImplCopyWith<$Res> {
  factory _$$DbImplCopyWith(_$DbImpl value, $Res Function(_$DbImpl) then) =
      __$$DbImplCopyWithImpl<$Res>;
  @useResult
  $Res call({DbError dbError});

  $DbErrorCopyWith<$Res> get dbError;
}

/// @nodoc
class __$$DbImplCopyWithImpl<$Res>
    extends _$DataErrorCopyWithImpl<$Res, _$DbImpl>
    implements _$$DbImplCopyWith<$Res> {
  __$$DbImplCopyWithImpl(_$DbImpl _value, $Res Function(_$DbImpl) _then)
      : super(_value, _then);

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dbError = null,
  }) {
    return _then(_$DbImpl(
      null == dbError
          ? _value.dbError
          : dbError // ignore: cast_nullable_to_non_nullable
              as DbError,
    ));
  }

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $DbErrorCopyWith<$Res> get dbError {
    return $DbErrorCopyWith<$Res>(_value.dbError, (value) {
      return _then(_value.copyWith(dbError: value));
    });
  }
}

/// @nodoc

class _$DbImpl implements Db {
  const _$DbImpl(this.dbError);

  @override
  final DbError dbError;

  @override
  String toString() {
    return 'DataError.db(dbError: $dbError)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DbImpl &&
            (identical(other.dbError, dbError) || other.dbError == dbError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, dbError);

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$DbImplCopyWith<_$DbImpl> get copyWith =>
      __$$DbImplCopyWithImpl<_$DbImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ApiError apiError) api,
    required TResult Function(DbError dbError) db,
  }) {
    return db(dbError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ApiError apiError)? api,
    TResult? Function(DbError dbError)? db,
  }) {
    return db?.call(dbError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ApiError apiError)? api,
    TResult Function(DbError dbError)? db,
    required TResult orElse(),
  }) {
    if (db != null) {
      return db(dbError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Api value) api,
    required TResult Function(Db value) db,
  }) {
    return db(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(Api value)? api,
    TResult? Function(Db value)? db,
  }) {
    return db?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Api value)? api,
    TResult Function(Db value)? db,
    required TResult orElse(),
  }) {
    if (db != null) {
      return db(this);
    }
    return orElse();
  }
}

abstract class Db implements DataError {
  const factory Db(final DbError dbError) = _$DbImpl;

  DbError get dbError;

  /// Create a copy of DataError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$DbImplCopyWith<_$DbImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
