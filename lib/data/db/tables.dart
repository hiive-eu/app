import 'package:hiive/data/db/db.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/models/reading.dart';
import 'package:hiive/data/models/sensor.dart';

DbTable gatewaysTable = DbTable<GatewayStorage>(
  tableName: "gateways",
  primaryKey: "mac",
  columnDefinitions: '''
    last_reading INTEGER,
    mac TEXT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    sd_space_used SMALLINT,
    sd_status SMALLINT,
    status TEXT NOT NULL,
    token TEXT,
    wifi_rssi SMALLINT,
    FOREIGN KEY (last_reading) REFERENCES gw_readings(id) ON DELETE SET NULL
  ''',
  entityConstructor: GatewayStorage.fromJson,
);

DbTable gwReadingsTable = DbTable<GatewayReadingStorage>(
  tableName: "gw_readings",
  primaryKey: "id",
  columnDefinitions: '''
    battery SMALLINT NOT NULL,
    gateway TEXT NOT NULL,
    humidity SMALLINT NOT NULL,
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    temperature SMALLINT NOT NULL,
    time DATETIME NOT NULL,
    FOREIGN KEY (gateway) REFERENCES gateways(mac) ON DELETE CASCADE
  ''',
  entityConstructor: GatewayReadingStorage.fromJson,
);

DbTable hivesTable = DbTable<HiveStorage>(
  tableName: "hives",
  primaryKey: "id",
  columnDefinitions: '''
    id TEXT PRIMARY KEY,
    is_hiive BOOLEAN NOT NULL,
    last_reading INTEGER,
    name TEXT NOT NULL,
    sensor TEXT,
    FOREIGN KEY (sensor) REFERENCES sensors (mac) ON DELETE SET NULL
    FOREIGN KEY (last_reading) REFERENCES readings (id) ON DELETE SET NULL
  ''',
  entityConstructor: HiveStorage.fromJson,
);

DbTable readingsTable = DbTable<ReadingStorage>(
  tableName: "readings",
  primaryKey: "id",
  columnDefinitions: '''
    frequency_bins BLOB NOT NULL,
    humidity SMALLINT NOT NULL,
    hive TEXT NOT NULL,
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    sound_level SMALLINT NOT NULL,
    temperature SMALLINT NOT NULL,
    time DATETIME NOT NULL,
    FOREIGN KEY (hive) REFERENCES hives(id) ON DELETE CASCADE
  ''',
  entityConstructor: ReadingStorage.fromJson,
);

DbTable sensorsTable = DbTable<SensorStorage>(
  tableName: "sensors",
  primaryKey: "mac",
  columnDefinitions: '''
    battery SMALLINT NOT NULL,
    gateway TEXT,
    mac TEXT PRIMARY KEY NOT NULL,
    rssi SMALLINT,
    status TEXT NOT NULL,
    FOREIGN KEY (gateway) REFERENCES gateways(mac) ON DELETE SET NULL
  ''',
  entityConstructor: SensorStorage.fromJson,
);
