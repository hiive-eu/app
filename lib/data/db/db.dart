import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/hive/update_hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/update_sensor_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/db/tables.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/models/reading.dart';
import 'package:hiive/data/models/sensor.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/services/sqlite.dart';
import "package:riverpod_annotation/riverpod_annotation.dart";

part "db.g.dart";

@riverpod
Db db(Ref ref) => Db(ref);

class Db {
  late final Ref _ref;
  Db(Ref ref) : this._ref = ref;

///////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// GATEWAY // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  TaskEither<DbError, List<SensorStorage>> deleteGateway(String mac) {
    return _ref
        .read(sqliteServiceProvider)
        .delete<GatewayStorage>(gatewaysTable, mac)
        .flatMap(
          (_) => _ref
              .read(sqliteServiceProvider)
              .getAll<SensorStorage>(sensorsTable),
        );
  }

  //TaskEither<DbError, GatewayStorage?> getGateway(String mac) {
  //  return _ref
  //      .read(sqliteServiceProvider)
  //      .getSingle<GatewayStorage>(gatewaysTable, mac);
  //}

  TaskEither<DbError, List<GatewayStorage>> getGateways() {
    return _ref
        .read(sqliteServiceProvider)
        .getAll<GatewayStorage>(gatewaysTable);
  }

  TaskEither<DbError, GatewayStorage> insertGateway(Gateway gateway) {
    return _ref
        .read(sqliteServiceProvider)
        .insert<GatewayStorage>(gatewaysTable, gateway.toGatewayStorage());
  }

  TaskEither<DbError, GatewayStorage> updateGateway(Gateway gateway) {
    return _ref
        .read(sqliteServiceProvider)
        .update<GatewayStorage>(gatewaysTable, gateway.toGatewayStorage());
  }

////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// HIVE // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  TaskEither<DbError, Unit> deleteHive(String id) {
    return _ref.read(sqliteServiceProvider).delete<HiveStorage>(hivesTable, id);
  }

  TaskEither<DbError, HiveStorage?> getHive(String id) {
    return _ref
        .read(sqliteServiceProvider)
        .getSingle<HiveStorage>(hivesTable, id);
  }

  TaskEither<DbError, List<HiveStorage>> getHives() {
    return _ref.read(sqliteServiceProvider).getAll<HiveStorage>(hivesTable);
  }

  TaskEither<DbError, HiveStorage> insertHive(Hive hive) {
    return _ref
        .read(sqliteServiceProvider)
        .insert<HiveStorage>(hivesTable, hive.toHiveStorage());
  }

  TaskEither<DbError, List<HiveStorage>> updateHive(
    String id,
    UpdateHiveData updateHiveData,
  ) {
    List<TransactionOperation> txnOps = [];

    // HIVE TO UPDATE
    return getHive(id).flatMap(
      (hiveStorage) {
        if (hiveStorage == null) {
          throw DbError.unknownEntityError("Failed to get Hive with id: $id");
        }
        Hive newHive = hiveStorage.toHive().copyWith(
              sensor: updateHiveData.sensor,
              name: updateHiveData.name ?? hiveStorage.name,
            );
        txnOps.add(TransactionOperation(
          operationType: TransactionOperationType.update,
          table: hivesTable,
          object: newHive.toHiveStorage(),
        ));

        // AFFECTED HIVE
        Hive? hivePairedWithSensor = updateHiveData.sensor == null
            ? null
            : _ref
                .read(hivesNotifierProvider)
                .getBySensor(updateHiveData.sensor!)
                ?.copyWith(sensor: null);

        if (hivePairedWithSensor != null) {
          txnOps.add(TransactionOperation(
            operationType: TransactionOperationType.update,
            table: hivesTable,
            object: hivePairedWithSensor.toHiveStorage(),
          ));
        }
        return _ref
            .read(sqliteServiceProvider)
            .transaction(txnOps)
            .map((res) => res[hivesTable.tableName]?.cast<HiveStorage>() ?? []);
      },
    );
  }

//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// SENSOR // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  //TaskEither<DbError, HiveStorage> deleteSensor(String mac) {
  //  return _ref
  //      .read(sqliteServiceProvider)
  //      .delete<SensorStorage>(sensorsTable, mac);
  //}

  TaskEither<DbError, SensorStorage?> getSensor(String mac) {
    return _ref
        .read(sqliteServiceProvider)
        .getSingle<SensorStorage>(sensorsTable, mac);
  }

  TaskEither<DbError, List<SensorStorage>> getSensors() {
    return _ref.read(sqliteServiceProvider).getAll<SensorStorage>(sensorsTable);
  }

  TaskEither<DbError, (HiveStorage, SensorStorage)> insertSensor(
    String hiveId,
    Hive? maybeHiveFromApi,
    Sensor sensor,
  ) {
    List<TransactionOperation> txnOps = [];
    return getHive(hiveId).flatMap((hiveStorage) {
      if (hiveStorage == null) {
        throw DbError.unknownEntityError("Failed to get Hive with id: $hiveId");
      }
      Hive updatedHive = maybeHiveFromApi ??
          hiveStorage.toHive().copyWith(
                sensor: sensor.mac,
              );
      txnOps.add(TransactionOperation(
        operationType: TransactionOperationType.insert,
        table: sensorsTable,
        object: sensor.toSensorStorage(),
      ));
      txnOps.add(TransactionOperation(
        operationType: TransactionOperationType.update,
        table: hivesTable,
        object: updatedHive.toHiveStorage(),
      ));

      return _ref.read(sqliteServiceProvider).transaction(txnOps).map((res) => (
            res[hivesTable.tableName]?.cast<HiveStorage>().first ?? [],
            res[sensorsTable.tableName]?.cast<SensorStorage>().first ?? [],
          ) as (HiveStorage, SensorStorage));
    });
  }

  TaskEither<DbError, SensorStorage> updateSensor(
    String mac,
    UpdateSensorData updateSensorData,
  ) {
    return getSensor(mac).flatMap((sensorStorage) {
      if (sensorStorage == null) {
        throw DbError.unknownEntityError("Failed to get sensor with mac: $mac");
      }
      Sensor newSensor = sensorStorage.toSensor().copyWith(
            gateway: updateSensorData.gateway,
          );
      return _ref
          .read(sqliteServiceProvider)
          .update<SensorStorage>(sensorsTable, newSensor.toSensorStorage());
    });
  }

////////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// READINGS // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
////////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  TaskEither<DbError, Unit> deleteReading(String id) {
    return _ref
        .read(sqliteServiceProvider)
        .delete<ReadingStorage>(readingsTable, id);
  }

  //TaskEither<DbError, ReadingStorage?> getReading(String mac) {
  //  return _ref
  //      .read(sqliteServiceProvider)
  //      .getSingle<ReadingStorage>(readingsTable, mac);
  //}

  TaskEither<DbError, List<ReadingStorage>> getReadings() {
    return _ref
        .read(sqliteServiceProvider)
        .getAll<ReadingStorage>(readingsTable);
  }

  TaskEither<DbError, ReadingStorage> insertReading(Reading reading) {
    return _ref
        .read(sqliteServiceProvider)
        .insert<ReadingStorage>(hivesTable, reading.toReadingStorage());
  }

  TaskEither<DbError, ReadingStorage> updateReading(Reading reading) {
    return _ref
        .read(sqliteServiceProvider)
        .update<ReadingStorage>(readingsTable, reading.toReadingStorage());
  }

///////////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// GW_READINGS // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  TaskEither<DbError, Unit> deleteGatewayReading(String id) {
    return _ref
        .read(sqliteServiceProvider)
        .delete<GatewayReadingStorage>(gwReadingsTable, id);
  }

  //TaskEither<DbError, GatewayReadingStorage?> getGatewayReading(String mac) {
  //  return _ref
  //      .read(sqliteServiceProvider)
  //      .getSingle<GatewayReadingStorage>(gwReadingsTable, mac);
  //}

  TaskEither<DbError, List<GatewayReadingStorage>> getGatewayReadings() {
    return _ref
        .read(sqliteServiceProvider)
        .getAll<GatewayReadingStorage>(gwReadingsTable);
  }

  TaskEither<DbError, GatewayReadingStorage> insertGatewayReading(
      GatewayReading reading) {
    return _ref.read(sqliteServiceProvider).insert<GatewayReadingStorage>(
        hivesTable, reading.toGatewayReadingStorage());
  }

  TaskEither<DbError, GatewayReadingStorage> updateGatewayReading(
      GatewayReading reading) {
    return _ref.read(sqliteServiceProvider).update<GatewayReadingStorage>(
        gwReadingsTable, reading.toGatewayReadingStorage());
  }
}

class DbTable<T extends DbEntity> {
  final String tableName;
  final String primaryKey;
  final String columnDefinitions;
  final T Function(Map<String, dynamic>) entityConstructor;

  DbTable({
    required this.tableName,
    required this.primaryKey,
    required this.columnDefinitions,
    required this.entityConstructor,
  });
}

abstract class DbEntity {
  Map<String, dynamic> toJson();
}
