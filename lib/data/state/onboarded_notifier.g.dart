// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'onboarded_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$onboardedNotifierHash() => r'07847efc7956ba3ca7e4be45a47b406bb240b814';

/// See also [OnboardedNotifier].
@ProviderFor(OnboardedNotifier)
final onboardedNotifierProvider =
    NotifierProvider<OnboardedNotifier, bool>.internal(
  OnboardedNotifier.new,
  name: r'onboardedNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$onboardedNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$OnboardedNotifier = Notifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
