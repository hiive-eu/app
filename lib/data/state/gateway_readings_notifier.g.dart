// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway_readings_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewayReadingsNotifierHash() =>
    r'b10fca45cd96c85f3ed2ab16245a6f7f35f7bb74';

/// See also [GatewayReadingsNotifier].
@ProviderFor(GatewayReadingsNotifier)
final gatewayReadingsNotifierProvider = AsyncNotifierProvider<
    GatewayReadingsNotifier, Map<String, List<GatewayReading>>>.internal(
  GatewayReadingsNotifier.new,
  name: r'gatewayReadingsNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewayReadingsNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$GatewayReadingsNotifier
    = AsyncNotifier<Map<String, List<GatewayReading>>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
