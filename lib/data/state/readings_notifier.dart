import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/models/reading.dart';
import 'package:hiive/data/repositories/readings_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "readings_notifier.g.dart";

@Riverpod(keepAlive: true)
class ReadingsNotifier extends _$ReadingsNotifier {
  @override
  Future<Map<String, List<Reading>>> build() async {
    return {};
  }

  Future<Unit> getReadings(String id) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(readingsRepositoryProvider)
          .getReadings(id)
          .match(
            (dataError) => throw dataError,
            (readings) => _manuallyUpdateReadings(id, readings),
          )
          .run(),
    );
    return unit;
  }

  Map<String, List<Reading>> _manuallyUpdateReadings(
    String id,
    List<Reading> gatewayReadings,
  ) {
    Map<String, List<Reading>> prevState = state.value ?? {};
    List<Reading> prevReadings = prevState[id] ?? [];
    List<DateTime> prevReadingTimes =
        prevReadings.map((reading) => reading.time).toList();
    List<Reading> readingsToAdd = gatewayReadings
        .where((reading) => !prevReadingTimes.contains(reading.time))
        .toList();
    Map<String, List<Reading>> newState = prevState;
    newState[id] = [...prevReadings, ...readingsToAdd];
    return newState;
  }
}

extension ReadingsHelpers on AsyncValue<Map<String, List<Reading>>> {
  List<Reading> getByid(String id) {
    return this.value?[id] ?? [];
  }
}
