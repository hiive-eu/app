import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'onboarded_notifier.g.dart';

@Riverpod(keepAlive: true)
class OnboardedNotifier extends _$OnboardedNotifier {
  @override
  bool build() => false;

  void setOnboarded(bool onboarded) {
    print("[ONBOARDTEST - onboardedNotifier] setOnboarded: $onboarded");
    state = onboarded;
  }
}
