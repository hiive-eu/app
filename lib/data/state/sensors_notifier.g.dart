// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensors_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sensorsNotifierHash() => r'06e6bfab9157f0529eb7b782793ab772f04fb2b6';

/// See also [SensorsNotifier].
@ProviderFor(SensorsNotifier)
final sensorsNotifierProvider =
    AsyncNotifierProvider<SensorsNotifier, List<Sensor>>.internal(
  SensorsNotifier.new,
  name: r'sensorsNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sensorsNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$SensorsNotifier = AsyncNotifier<List<Sensor>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
