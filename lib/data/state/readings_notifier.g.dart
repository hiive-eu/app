// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'readings_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$readingsNotifierHash() => r'7551c001a6bd63e672b856920a6a8efccf06b96f';

/// See also [ReadingsNotifier].
@ProviderFor(ReadingsNotifier)
final readingsNotifierProvider = AsyncNotifierProvider<ReadingsNotifier,
    Map<String, List<Reading>>>.internal(
  ReadingsNotifier.new,
  name: r'readingsNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$readingsNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$ReadingsNotifier = AsyncNotifier<Map<String, List<Reading>>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
