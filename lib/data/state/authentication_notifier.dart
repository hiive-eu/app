import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/authentication/forgot_password_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/login_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/refresh_tokens_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/signup_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/authentication_tokens.dart';
import 'package:hiive/data/repositories/authentication_repository.dart';
import 'package:hiive/data/state/logged_in_notifier.dart';
import 'package:hiive/services/secure_storage.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "authentication_notifier.g.dart";

@Riverpod(keepAlive: true)
class AuthenticationNotifier extends _$AuthenticationNotifier {
  @override
  Future<AuthenticationTokens?> build() async {
    return _attemptRefreshSessionOrLogin();
  }

  Future<Unit> forgotPassword(ForgotPasswordData forgotPasswordData) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(authenticationRepositoryProvider)
          .forgotPassword(forgotPasswordData)
          .match(
            (apiError) => throw apiError,
            (_) => state.value,
          )
          .run(),
    );
    return unit;
  }

  Future<Either<ApiError, Unit>> login(
    LoginData loginData,
    bool rememberCredentials,
  ) async {
    ApiError? maybeError;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () {
        final authenticationTokens =
            ref.read(authenticationRepositoryProvider).login(loginData).match(
          (apiError) {
            maybeError = apiError;
            throw apiError;
          },
          (response) {
            if (rememberCredentials) {
              _storeCredentials(loginData.email, loginData.password).match(
                (apiError) {
                  // TODO use other error
                  maybeError = apiError as ApiError;
                  throw apiError;
                },
                (_) => unit,
              ).run();
            }
            _storeAuthenticationTokens(
                    response.accessToken.token, response.refreshToken.token)
                .match(
              (apiError) {
                // TODO use other error
                maybeError = apiError as ApiError;
                throw apiError;
              },
              (_) => unit,
            ).run();
            ref.read(loggedInNotifierProvider.notifier).setLoggedIn();
            return AuthenticationTokens(
                accessToken: response.accessToken,
                refreshToken: response.refreshToken);
          },
        ).run();
        return authenticationTokens;
      },
    );
    return maybeError == null ? Either.right(unit) : Either.left(maybeError!);
  }

  Future<Either<ApiError, Unit>> logout() async {
    ApiError? maybeError;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref.read(authenticationRepositoryProvider).logout().match(
        (apiError) {
          maybeError = apiError;
          throw apiError;
        },
        (_) {
          _deleteAuthenticationStorage().match(
            (apiError) {
              // TODO use other error
              maybeError = apiError as ApiError;
              throw apiError;
            },
            (_) => unit,
          ).run();
          return null;
        },
      ).run(),
    );
    ref.read(loggedInNotifierProvider.notifier).setLoggedOut();
    return maybeError == null ? Either.right(unit) : Either.left(maybeError!);
  }

  Future<Unit> signup(SignupData signupData) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(authenticationRepositoryProvider)
          .signup(signupData)
          .match(
            (apiError) => throw apiError,
            (_) => state.value,
          )
          .run(),
    );
    return unit;
  }

  TaskEither<String, AuthenticationTokens> _getStoredTokens() {
    return [
      ref.read(secureStorageProvider).read(StorageKey.accessToken),
      ref.read(secureStorageProvider).read(StorageKey.refreshToken)
    ].sequenceTaskEither().map(
          (tokens) => AuthenticationTokens(
            accessToken: AccessToken(tokens[0]),
            refreshToken: RefreshToken(tokens[1]),
          ),
        );
  }

  Future<AuthenticationTokens?> _attemptRefreshSessionOrLogin() async {
    //
    // CHECK TOKENS
    //
    final AuthenticationTokens? authenticationTokens =
        (await _getStoredTokens().match((l) => null, (r) => r).run());

    if (authenticationTokens != null) {
      print(
          "[AUTHENTICATION NOTIFIER] access expires: ${authenticationTokens.accessToken.data.exp}");
      print(
          "[AUTHENTICATION NOTIFIER] refresh expires: ${authenticationTokens.refreshToken.data.exp}");
      if (authenticationTokens.accessToken.isValid()) {
        print("[AUTHENTICATION NOTIFIER] ACCESS TOKEN VALID");
        ref.read(loggedInNotifierProvider.notifier).setLoggedIn();
        return authenticationTokens;
      } else {
        if (authenticationTokens.refreshToken.isValid()) {
          // REFRESH TOKENS
          final AuthenticationTokens? refreshedAuthenticationTokens = (await ref
                  .read(authenticationRepositoryProvider)
                  .refreshTokens(
                    RefreshTokensData(
                        refreshToken: authenticationTokens.refreshToken.token),
                  )
                  .run())
              .toNullable();

          if (refreshedAuthenticationTokens != null) {
            if (refreshedAuthenticationTokens.accessToken.isValid()) {
              print("[AUTHENTICATION NOTIFIER BUILD] REFRESH TOKEN VALID");
              _storeAuthenticationTokens(
                      refreshedAuthenticationTokens.accessToken.token,
                      refreshedAuthenticationTokens.refreshToken.token)
                  .match((apiError) => throw apiError, (_) => unit)
                  .run();
              ref.read(loggedInNotifierProvider.notifier).setLoggedIn();
              return refreshedAuthenticationTokens;
            }
          }
        }
      }
    }

    //
    // CHECK CREDENTIALS
    //
    final (String, String)? credentials =
        (await _getStoredCredentials().run()).toNullable();

    if (credentials != null) {
      final AuthenticationTokens? newAuthenticationTokens = (await ref
              .read(authenticationRepositoryProvider)
              .login(LoginData(email: credentials.$1, password: credentials.$2))
              .run())
          .toNullable();

      if (newAuthenticationTokens != null) {
        if (newAuthenticationTokens.accessToken.isValid()) {
          print("[AUTHENTICATION NOTIFIER] CREDENTIALS STORED");
          _storeAuthenticationTokens(newAuthenticationTokens.accessToken.token,
                  newAuthenticationTokens.refreshToken.token)
              .match((apiError) => throw apiError, (_) => unit)
              .run();
          ref.read(loggedInNotifierProvider.notifier).setLoggedIn();
          return newAuthenticationTokens;
        }
      } else {
        _deleteAuthenticationStorage().run();
      }
    }

    ref.read(loggedInNotifierProvider.notifier).setLoggedOut();
    return null;
  }

  TaskEither<String, (String, String)> _getStoredCredentials() {
    return [
      ref.read(secureStorageProvider).read(StorageKey.email),
      ref.read(secureStorageProvider).read(StorageKey.password),
    ].sequenceTaskEither().map(
          (creds) => (creds[0], creds[1]),
        );
  }

  TaskEither<String, Unit> _storeAuthenticationTokens(
      String accessToken, String refreshToken) {
    return [
      ref
          .read(secureStorageProvider)
          .write(StorageKey.accessToken, accessToken),
      ref
          .read(secureStorageProvider)
          .write(StorageKey.refreshToken, refreshToken),
    ].sequenceTaskEither().map((r) => unit);
  }

  TaskEither<String, Unit> _storeCredentials(String email, String password) {
    return [
      ref.read(secureStorageProvider).write(StorageKey.email, email),
      ref.read(secureStorageProvider).write(StorageKey.password, password),
    ].sequenceTaskEither().map((r) => unit);
  }

  TaskEither<String, Unit> _deleteAuthenticationStorage() {
    return [
      ref.read(secureStorageProvider).delete(StorageKey.accessToken),
      ref.read(secureStorageProvider).delete(StorageKey.refreshToken),
      ref.read(secureStorageProvider).delete(StorageKey.email),
      ref.read(secureStorageProvider).delete(StorageKey.password),
    ].sequenceTaskEither().map((r) => unit);
  }
}
