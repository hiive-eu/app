import 'package:fpdart/fpdart.dart';
import 'package:collection/collection.dart';
import 'package:hiive/data/api/endpoints/gateway/claim_gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/update_gateway_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/data/repositories/gateway_repository.dart';
import 'package:hiive/data/state/sensors_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "gateways_notifier.g.dart";

@Riverpod(keepAlive: true)
class GatewaysNotifier extends _$GatewaysNotifier {
  @override
  Future<List<Gateway>> build() async {
    return [];
  }

  Future<Either<DataError, String?>> claimGateway(
    ClaimGatewayData claimGatewayData,
  ) async {
    DataError? maybeError;
    String? maybeToken;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(gatewayRepositoryProvider)
          .claimGateway(claimGatewayData)
          .match(
        (dataError) {
          maybeError = dataError;
          throw dataError;
        },
        (gateway) {
          maybeToken = gateway.token;
          return _manuallyAddOrUpdateGateways([gateway]);
        },
      ).run(),
    );
    return maybeError == null
        ? Either.right(maybeToken)
        : Either.left(maybeError!);
  }

  Future<Unit> getGateway(
    String mac,
  ) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref.read(gatewayRepositoryProvider).getGateway(mac).match(
        (apiError) {
          throw apiError;
        },
        (gateway) {
          return _manuallyAddOrUpdateGateways([gateway]);
        },
      ).run(),
    );
    return unit;
  }

  Future<Unit> getGateways() async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(gatewayRepositoryProvider)
          .getGateways()
          .match(
            (apiError) => throw apiError,
            (gateways) => gateways,
          )
          .run(),
    );
    return unit;
  }

  Future<Either<DataError, Unit>> removeGateway(
    String mac,
  ) async {
    DataError? maybeError;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref.read(gatewayRepositoryProvider).removeGateway(mac).match(
        (dataError) {
          maybeError = dataError;
          throw dataError;
        },
        (sensors) {
          ref
              .read(sensorsNotifierProvider.notifier)
              .manuallyAddOrUpdateSensors(sensors);
          return state.value!.where((gw) => gw.mac != mac).toList();
        },
      ).run(),
    );
    return maybeError == null ? Either.right(unit) : Either.left(maybeError!);
  }

  Future<Unit> updateGateway(
    String mac,
    UpdateGatewayData updateGatewayData,
  ) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(gatewayRepositoryProvider)
          .updateGateway(mac, updateGatewayData)
          .match(
            (apiError) => throw apiError,
            (response) => _manuallyAddOrUpdateGateways([response]),
          )
          .run(),
    );
    return unit;
  }

  void manuallyAddOrUpdateGateways(List<Gateway> updatedGateways) {
    state = AsyncValue.data(_manuallyAddOrUpdateGateways(updatedGateways));
  }

  List<Gateway> _manuallyAddOrUpdateGateways(List<Gateway> updatedGateways) {
    List<String> idsOfUpdatedGateways =
        updatedGateways.map((gw) => gw.mac).toList();
    List<Gateway> unchangedGateways = state.value!
        .where((gateway) => !idsOfUpdatedGateways.contains(gateway.mac))
        .toList();
    return [...unchangedGateways, ...updatedGateways];
  }
}

extension GatewaysHelpers on AsyncValue<List<Gateway>> {
  Gateway? getByMac(String mac) {
    return this.value?.firstWhereOrNull((gw) => gw.mac == mac);
  }
}
