// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$authenticationNotifierHash() =>
    r'2a54bce4283c79e954cded6f63c479d8344749f7';

/// See also [AuthenticationNotifier].
@ProviderFor(AuthenticationNotifier)
final authenticationNotifierProvider = AsyncNotifierProvider<
    AuthenticationNotifier, AuthenticationTokens?>.internal(
  AuthenticationNotifier.new,
  name: r'authenticationNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$authenticationNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AuthenticationNotifier = AsyncNotifier<AuthenticationTokens?>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
