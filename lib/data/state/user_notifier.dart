import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/user/update_password_endpoint.dart';
import 'package:hiive/data/models/user.dart';
import 'package:hiive/data/repositories/user_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "user_notifier.g.dart";

@Riverpod(keepAlive: true)
class UserNotifier extends _$UserNotifier {
  @override
  Future<User?> build() async {
    return null;
  }

  Future<Unit> removeUser() async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref.read(userRepositoryProvider).deleteUser().match(
        (apiError) => throw apiError,
        (response) {
          return null;
        },
      ).run(),
    );
    return unit;
  }

  Future<Unit> updatePassword(UpdatePasswordData updatePasswordData) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(userRepositoryProvider)
          .updatePassword(updatePasswordData)
          .match(
            (apiError) => throw apiError,
            (response) => state.value,
          )
          .run(),
    );
    return unit;
  }

  void manuallyAddOrUpdateUser(User user) {
    state = AsyncValue.data(user);
  }
}
