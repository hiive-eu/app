import 'dart:io';

import 'package:fpdart/fpdart.dart';
import 'package:collection/collection.dart';
import 'package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/update_hive_endpoint.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/repositories/hive_repository.dart';
import "package:hiive/data/data_error.dart";
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "hives_notifier.g.dart";

@Riverpod(keepAlive: true)
class HivesNotifier extends _$HivesNotifier {
  @override
  Future<List<Hive>> build() async {
    return [];
  }

  Future<Either<DataError, Hive>> createHive(
    CreateHiveData createHiveData,
  ) async {
    DataError? maybeError;
    Hive? maybeHive;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref.read(hiveRepositoryProvider).createHive(createHiveData).match(
        (dataError) {
          maybeError = dataError;
          throw dataError;
        },
        (hive) {
          maybeHive = hive;
          return _manuallyAddOrUpdateHives([hive]);
        },
      ).run(),
    );
    return maybeError == null
        ? Either.right(maybeHive!)
        : Either.left(maybeError!);
  }

  Future<Unit> getHive(String id) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(hiveRepositoryProvider)
          .getHive(id)
          .match((dataError) => throw dataError,
              (hive) => _manuallyAddOrUpdateHives([hive]))
          .run(),
    );
    return unit;
  }

  Future<Unit> getHives() async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(hiveRepositoryProvider)
          .getHives()
          .match(
            (dataError) => throw dataError,
            (hives) => hives,
          )
          .run(),
    );
    return unit;
  }

  Future<Either<DataError, Unit>> updateHive(
    String id,
    UpdateHiveData updateHiveData,
  ) async {
    DataError? maybeError;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () =>
          ref.read(hiveRepositoryProvider).updateHive(id, updateHiveData).match(
        (dataError) {
          maybeError = dataError;
          throw dataError;
        },
        (updatedHives) {
          return _manuallyAddOrUpdateHives(updatedHives);
        },
      ).run(),
    );
    return maybeError == null ? Either.right(unit) : Either.left(maybeError!);
  }

  Future<Unit> removeHive(String id) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(() => ref
        .read(hiveRepositoryProvider)
        .removeHive(id)
        .match(
          (apiError) => throw apiError,
          (_) => state.value!.where((hive) => hive.id != id).toList(),
        )
        .run());
    return unit;
  }

  void manuallyAddOrUpdateHives(List<Hive> updatedHives) {
    state = AsyncValue.data(_manuallyAddOrUpdateHives(updatedHives));
  }

  List<Hive> _manuallyAddOrUpdateHives(List<Hive> updatedHives) {
    List<String> idsOfUpdatedHives =
        updatedHives.map((hive) => hive.id).toList();
    List<Hive> unchangedHives = state.value!
        .where((hive) => !idsOfUpdatedHives.contains(hive.id))
        .toList();
    return [...unchangedHives, ...updatedHives];
  }
}

extension HivesHelpers on AsyncValue<List<Hive>> {
  Hive? getById(String id) {
    return this.value?.firstWhereOrNull((hive) => hive.id == id);
  }

  Hive? getByName(String name) {
    return this.value?.firstWhereOrNull((hive) => hive.name == name);
  }

  Hive? getBySensor(String mac) {
    return this.value?.firstWhereOrNull((hive) => hive.sensor == mac);
  }
}
