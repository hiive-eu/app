// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateways_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewaysNotifierHash() => r'4b5d41585e562449cc210a29b9817827dc420418';

/// See also [GatewaysNotifier].
@ProviderFor(GatewaysNotifier)
final gatewaysNotifierProvider =
    AsyncNotifierProvider<GatewaysNotifier, List<Gateway>>.internal(
  GatewaysNotifier.new,
  name: r'gatewaysNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewaysNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$GatewaysNotifier = AsyncNotifier<List<Gateway>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
