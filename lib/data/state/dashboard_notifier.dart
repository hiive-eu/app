import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/repositories/dashboard_repository.dart';
import 'package:hiive/data/state/gateways_notifier.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/data/state/logged_in_notifier.dart';
import 'package:hiive/data/state/sensors_notifier.dart';
import 'package:hiive/data/state/user_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "dashboard_notifier.g.dart";

@Riverpod(keepAlive: true)
class DashboardNotifier extends _$DashboardNotifier {
  // Dashboard gets all information from a single endpoint, and populates the other notifier providers
  @override
  Future<Unit> build() async {
    // rebuild dashboard notifier when login state changes
    ref.watch(loggedInNotifierProvider);
    await getDashboard();
    return unit;
  }

  Future<Unit> getDashboard() {
    return ref.read(dashboardRepositoryProvider).getDashboard().match(
      (apiError) => throw apiError,
      (dashboard) {
        ref
            .read(gatewaysNotifierProvider.notifier)
            .manuallyAddOrUpdateGateways(dashboard.gateways);
        ref
            .read(hivesNotifierProvider.notifier)
            .manuallyAddOrUpdateHives(dashboard.hives);
        ref
            .read(sensorsNotifierProvider.notifier)
            .manuallyAddOrUpdateSensors(dashboard.sensors);
        if (dashboard.user != null) {
          ref
              .read(userNotifierProvider.notifier)
              .manuallyAddOrUpdateUser(dashboard.user!);
        }
        return unit;
      },
    ).run();
  }
}
