import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'logged_in_notifier.g.dart';

@Riverpod(keepAlive: true)
class LoggedInNotifier extends _$LoggedInNotifier {
  @override
  bool build() => false;

  void setLoggedIn() {
    if (!state) {
      state = true;
    }
  }

  void setLoggedOut() {
    if (state) {
      state = false;
    }
  }
}
