import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:hiive/data/repositories/gateway_readings_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "gateway_readings_notifier.g.dart";

@Riverpod(keepAlive: true)
class GatewayReadingsNotifier extends _$GatewayReadingsNotifier {
  @override
  Future<Map<String, List<GatewayReading>>> build() async {
    return {};
  }

  Future<Unit> getGatewayReadings(String mac) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(gatewayReadingsRepositoryProvider)
          .getGatewayReadings(mac)
          .match(
            (dataError) => throw dataError,
            (gatewayReadings) =>
                _manuallyUpdateGatewayReadings(mac, gatewayReadings),
          )
          .run(),
    );
    return unit;
  }

  Map<String, List<GatewayReading>> _manuallyUpdateGatewayReadings(
    String mac,
    List<GatewayReading> gatewayReadings,
  ) {
    Map<String, List<GatewayReading>> prevState = state.value ?? {};
    List<GatewayReading> prevReadings = prevState[mac] ?? [];
    List<DateTime> prevReadingTimes =
        prevReadings.map((reading) => reading.time).toList();
    List<GatewayReading> readingsToAdd = gatewayReadings
        .where((reading) => !prevReadingTimes.contains(reading.time))
        .toList();
    Map<String, List<GatewayReading>> newState = prevState;
    newState[mac] = [...prevReadings, ...readingsToAdd];
    return newState;
  }
}

extension GatewayReadingsHelpers
    on AsyncValue<Map<String, List<GatewayReading>>> {
  List<GatewayReading> getByMac(String mac) {
    return this.value?[mac] ?? [];
  }
}
