import 'package:fpdart/fpdart.dart';
import 'package:collection/collection.dart';
import 'package:hiive/data/api/endpoints/sensor/claim_sensor_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/update_sensor_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/sensor.dart';
import 'package:hiive/data/repositories/sensor_repository.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "sensors_notifier.g.dart";

@Riverpod(keepAlive: true)
class SensorsNotifier extends _$SensorsNotifier {
  @override
  Future<List<Sensor>> build() async {
    return [];
  }

  Future<Either<DataError, Sensor>> claimSensor(
    ClaimSensorData claimSensorData,
  ) async {
    DataError? maybeError;
    Sensor? maybeSensor;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () =>
          ref.read(sensorRepositoryProvider).claimSensor(claimSensorData).match(
        (dataError) {
          maybeError = dataError;
          throw dataError;
        },
        (response) {
          maybeSensor = response.$2;
          ref
              .read(hivesNotifierProvider.notifier)
              .manuallyAddOrUpdateHives([response.$1]);
          return _manuallyAddOrUpdateSensors([response.$2]);
        },
      ).run(),
    );
    return maybeError == null
        ? Either.right(maybeSensor!)
        : Either.left(maybeError!);
  }

  Future<Unit> getSensors() async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(sensorRepositoryProvider)
          .getSensors()
          .match(
            (dataError) => throw dataError,
            (sensors) => sensors,
          )
          .run(),
    );
    return unit;
  }

  Future<Either<DataError, Unit>> updateSensor(
    String mac,
    UpdateSensorData updateSensorData,
  ) async {
    DataError? maybeError;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () => ref
          .read(sensorRepositoryProvider)
          .updateSensor(mac, updateSensorData)
          .match((dataError) {
        maybeError = dataError;
        throw dataError;
      }, (updatedSensor) {
        return _manuallyAddOrUpdateSensors([updatedSensor]);
      }).run(),
    );
    return maybeError == null ? Either.right(unit) : Either.left(maybeError!);
  }

  void manuallyAddOrUpdateSensors(List<Sensor> updatedSensors) {
    state = AsyncValue.data(_manuallyAddOrUpdateSensors(updatedSensors));
  }

  List<Sensor> _manuallyAddOrUpdateSensors(List<Sensor> updatedSensors) {
    List<String> idsOfUpdatedSensors =
        updatedSensors.map((sensor) => sensor.mac).toList();
    List<Sensor> unchangedSensors = state.value!
        .where((sensor) => !idsOfUpdatedSensors.contains(sensor.mac))
        .toList();
    return [...unchangedSensors, ...updatedSensors];
  }
}

extension SensorsHelpers on AsyncValue<List<Sensor>> {
  Sensor? getByMac(String? mac) {
    return this.value?.firstWhereOrNull((sensor) => sensor.mac == mac);
  }
}
