// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hives_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$hivesNotifierHash() => r'ac05f1598f1b9fc507df53d29fc4c9adcf50ea11';

/// See also [HivesNotifier].
@ProviderFor(HivesNotifier)
final hivesNotifierProvider =
    AsyncNotifierProvider<HivesNotifier, List<Hive>>.internal(
  HivesNotifier.new,
  name: r'hivesNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$hivesNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HivesNotifier = AsyncNotifier<List<Hive>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
