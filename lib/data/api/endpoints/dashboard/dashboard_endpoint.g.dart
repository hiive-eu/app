// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DashboardApiResponseImpl _$$DashboardApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$DashboardApiResponseImpl(
      dashboard:
          DashboardNetwork.fromJson(json['dashboard'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$DashboardApiResponseImplToJson(
        _$DashboardApiResponseImpl instance) =>
    <String, dynamic>{
      'dashboard': instance.dashboard,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$dashboardEndpointHash() => r'ec83535631fcc707038774a36b259f20c7f016dd';

/// See also [dashboardEndpoint].
@ProviderFor(dashboardEndpoint)
final dashboardEndpointProvider =
    AutoDisposeProvider<DashboardEndpoint>.internal(
  dashboardEndpoint,
  name: r'dashboardEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$dashboardEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef DashboardEndpointRef = AutoDisposeProviderRef<DashboardEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
