// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dashboard_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

DashboardApiResponse _$DashboardApiResponseFromJson(Map<String, dynamic> json) {
  return _DashboardApiResponse.fromJson(json);
}

/// @nodoc
mixin _$DashboardApiResponse {
  DashboardNetwork get dashboard => throw _privateConstructorUsedError;

  /// Serializes this DashboardApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of DashboardApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $DashboardApiResponseCopyWith<DashboardApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashboardApiResponseCopyWith<$Res> {
  factory $DashboardApiResponseCopyWith(DashboardApiResponse value,
          $Res Function(DashboardApiResponse) then) =
      _$DashboardApiResponseCopyWithImpl<$Res, DashboardApiResponse>;
  @useResult
  $Res call({DashboardNetwork dashboard});

  $DashboardNetworkCopyWith<$Res> get dashboard;
}

/// @nodoc
class _$DashboardApiResponseCopyWithImpl<$Res,
        $Val extends DashboardApiResponse>
    implements $DashboardApiResponseCopyWith<$Res> {
  _$DashboardApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of DashboardApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dashboard = null,
  }) {
    return _then(_value.copyWith(
      dashboard: null == dashboard
          ? _value.dashboard
          : dashboard // ignore: cast_nullable_to_non_nullable
              as DashboardNetwork,
    ) as $Val);
  }

  /// Create a copy of DashboardApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $DashboardNetworkCopyWith<$Res> get dashboard {
    return $DashboardNetworkCopyWith<$Res>(_value.dashboard, (value) {
      return _then(_value.copyWith(dashboard: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$DashboardApiResponseImplCopyWith<$Res>
    implements $DashboardApiResponseCopyWith<$Res> {
  factory _$$DashboardApiResponseImplCopyWith(_$DashboardApiResponseImpl value,
          $Res Function(_$DashboardApiResponseImpl) then) =
      __$$DashboardApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({DashboardNetwork dashboard});

  @override
  $DashboardNetworkCopyWith<$Res> get dashboard;
}

/// @nodoc
class __$$DashboardApiResponseImplCopyWithImpl<$Res>
    extends _$DashboardApiResponseCopyWithImpl<$Res, _$DashboardApiResponseImpl>
    implements _$$DashboardApiResponseImplCopyWith<$Res> {
  __$$DashboardApiResponseImplCopyWithImpl(_$DashboardApiResponseImpl _value,
      $Res Function(_$DashboardApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of DashboardApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? dashboard = null,
  }) {
    return _then(_$DashboardApiResponseImpl(
      dashboard: null == dashboard
          ? _value.dashboard
          : dashboard // ignore: cast_nullable_to_non_nullable
              as DashboardNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$DashboardApiResponseImpl implements _DashboardApiResponse {
  const _$DashboardApiResponseImpl({required this.dashboard});

  factory _$DashboardApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$DashboardApiResponseImplFromJson(json);

  @override
  final DashboardNetwork dashboard;

  @override
  String toString() {
    return 'DashboardApiResponse(dashboard: $dashboard)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashboardApiResponseImpl &&
            (identical(other.dashboard, dashboard) ||
                other.dashboard == dashboard));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, dashboard);

  /// Create a copy of DashboardApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$DashboardApiResponseImplCopyWith<_$DashboardApiResponseImpl>
      get copyWith =>
          __$$DashboardApiResponseImplCopyWithImpl<_$DashboardApiResponseImpl>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$DashboardApiResponseImplToJson(
      this,
    );
  }
}

abstract class _DashboardApiResponse implements DashboardApiResponse {
  const factory _DashboardApiResponse(
      {required final DashboardNetwork dashboard}) = _$DashboardApiResponseImpl;

  factory _DashboardApiResponse.fromJson(Map<String, dynamic> json) =
      _$DashboardApiResponseImpl.fromJson;

  @override
  DashboardNetwork get dashboard;

  /// Create a copy of DashboardApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$DashboardApiResponseImplCopyWith<_$DashboardApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
