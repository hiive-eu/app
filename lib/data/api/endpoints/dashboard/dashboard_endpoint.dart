import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/dashboard.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "dashboard_endpoint.freezed.dart";
part "dashboard_endpoint.g.dart";

@riverpod
DashboardEndpoint dashboardEndpoint(Ref ref) => DashboardEndpoint(ref);

class DashboardEndpoint extends Endpoint {
  static const String path = "/user/dashboard";
  static const bool authRequired = true;

  late final Ref _ref;
  DashboardEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, DashboardApiResponse> get() {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(DashboardEndpoint.path, DashboardEndpoint.authRequired)
        .flatMap(_parseDashboardResponse);
  }
}

TaskEither<ApiError, DashboardApiResponse> _parseDashboardResponse(
  Map<String, dynamic>? data,
) {
  return TaskEither.tryCatch(
    () async {
      return DashboardApiResponse.fromJson(data!);
    },
    (error, stackTrace) {
      print(error);
      print(stackTrace);
      return const ApiError.unexpectedResponseData();
    },
  );
}

@freezed
class DashboardApiResponse with _$DashboardApiResponse {
  const factory DashboardApiResponse({
    required DashboardNetwork dashboard,
  }) = _DashboardApiResponse;

  factory DashboardApiResponse.fromJson(Map<String, dynamic> json) =>
      _$DashboardApiResponseFromJson(json);
}
