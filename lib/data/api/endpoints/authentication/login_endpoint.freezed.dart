// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'login_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

LoginApiResponse _$LoginApiResponseFromJson(Map<String, dynamic> json) {
  return _LoginApiResponse.fromJson(json);
}

/// @nodoc
mixin _$LoginApiResponse {
  @JsonKey(name: "authentication_tokens")
  AuthenticationTokensNetwork get authenticationTokens =>
      throw _privateConstructorUsedError;

  /// Serializes this LoginApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of LoginApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $LoginApiResponseCopyWith<LoginApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LoginApiResponseCopyWith<$Res> {
  factory $LoginApiResponseCopyWith(
          LoginApiResponse value, $Res Function(LoginApiResponse) then) =
      _$LoginApiResponseCopyWithImpl<$Res, LoginApiResponse>;
  @useResult
  $Res call(
      {@JsonKey(name: "authentication_tokens")
      AuthenticationTokensNetwork authenticationTokens});

  $AuthenticationTokensNetworkCopyWith<$Res> get authenticationTokens;
}

/// @nodoc
class _$LoginApiResponseCopyWithImpl<$Res, $Val extends LoginApiResponse>
    implements $LoginApiResponseCopyWith<$Res> {
  _$LoginApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of LoginApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authenticationTokens = null,
  }) {
    return _then(_value.copyWith(
      authenticationTokens: null == authenticationTokens
          ? _value.authenticationTokens
          : authenticationTokens // ignore: cast_nullable_to_non_nullable
              as AuthenticationTokensNetwork,
    ) as $Val);
  }

  /// Create a copy of LoginApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $AuthenticationTokensNetworkCopyWith<$Res> get authenticationTokens {
    return $AuthenticationTokensNetworkCopyWith<$Res>(
        _value.authenticationTokens, (value) {
      return _then(_value.copyWith(authenticationTokens: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$LoginApiResponseImplCopyWith<$Res>
    implements $LoginApiResponseCopyWith<$Res> {
  factory _$$LoginApiResponseImplCopyWith(_$LoginApiResponseImpl value,
          $Res Function(_$LoginApiResponseImpl) then) =
      __$$LoginApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "authentication_tokens")
      AuthenticationTokensNetwork authenticationTokens});

  @override
  $AuthenticationTokensNetworkCopyWith<$Res> get authenticationTokens;
}

/// @nodoc
class __$$LoginApiResponseImplCopyWithImpl<$Res>
    extends _$LoginApiResponseCopyWithImpl<$Res, _$LoginApiResponseImpl>
    implements _$$LoginApiResponseImplCopyWith<$Res> {
  __$$LoginApiResponseImplCopyWithImpl(_$LoginApiResponseImpl _value,
      $Res Function(_$LoginApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of LoginApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authenticationTokens = null,
  }) {
    return _then(_$LoginApiResponseImpl(
      authenticationTokens: null == authenticationTokens
          ? _value.authenticationTokens
          : authenticationTokens // ignore: cast_nullable_to_non_nullable
              as AuthenticationTokensNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$LoginApiResponseImpl implements _LoginApiResponse {
  const _$LoginApiResponseImpl(
      {@JsonKey(name: "authentication_tokens")
      required this.authenticationTokens});

  factory _$LoginApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$LoginApiResponseImplFromJson(json);

  @override
  @JsonKey(name: "authentication_tokens")
  final AuthenticationTokensNetwork authenticationTokens;

  @override
  String toString() {
    return 'LoginApiResponse(authenticationTokens: $authenticationTokens)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoginApiResponseImpl &&
            (identical(other.authenticationTokens, authenticationTokens) ||
                other.authenticationTokens == authenticationTokens));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, authenticationTokens);

  /// Create a copy of LoginApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$LoginApiResponseImplCopyWith<_$LoginApiResponseImpl> get copyWith =>
      __$$LoginApiResponseImplCopyWithImpl<_$LoginApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$LoginApiResponseImplToJson(
      this,
    );
  }
}

abstract class _LoginApiResponse implements LoginApiResponse {
  const factory _LoginApiResponse(
          {@JsonKey(name: "authentication_tokens")
          required final AuthenticationTokensNetwork authenticationTokens}) =
      _$LoginApiResponseImpl;

  factory _LoginApiResponse.fromJson(Map<String, dynamic> json) =
      _$LoginApiResponseImpl.fromJson;

  @override
  @JsonKey(name: "authentication_tokens")
  AuthenticationTokensNetwork get authenticationTokens;

  /// Create a copy of LoginApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$LoginApiResponseImplCopyWith<_$LoginApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
