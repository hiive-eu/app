// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginData _$LoginDataFromJson(Map<String, dynamic> json) => LoginData(
      email: json['email'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$LoginDataToJson(LoginData instance) => <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };

_$LoginApiResponseImpl _$$LoginApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$LoginApiResponseImpl(
      authenticationTokens: AuthenticationTokensNetwork.fromJson(
          json['authentication_tokens'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$LoginApiResponseImplToJson(
        _$LoginApiResponseImpl instance) =>
    <String, dynamic>{
      'authentication_tokens': instance.authenticationTokens,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$loginEndpointHash() => r'4033755ec0e1391414f05549003e724d23066364';

/// See also [loginEndpoint].
@ProviderFor(loginEndpoint)
final loginEndpointProvider = AutoDisposeProvider<LoginEndpoint>.internal(
  loginEndpoint,
  name: r'loginEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$loginEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef LoginEndpointRef = AutoDisposeProviderRef<LoginEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
