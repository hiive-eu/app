// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reset_password_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResetPasswordData _$ResetPasswordDataFromJson(Map<String, dynamic> json) =>
    ResetPasswordData(
      token: json['token'] as String,
      newPassword: json['new_password'] as String,
      newPasswordConfirm: json['new_password_confirm'] as String,
    );

Map<String, dynamic> _$ResetPasswordDataToJson(ResetPasswordData instance) =>
    <String, dynamic>{
      'token': instance.token,
      'new_password': instance.newPassword,
      'new_password_confirm': instance.newPasswordConfirm,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$resetPasswordEndpointHash() =>
    r'7e297c07c7ac100a5672fd49aafb4b1bbb55e7fb';

/// See also [resetPasswordEndpoint].
@ProviderFor(resetPasswordEndpoint)
final resetPasswordEndpointProvider =
    AutoDisposeProvider<ResetPasswordEndpoint>.internal(
  resetPasswordEndpoint,
  name: r'resetPasswordEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$resetPasswordEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ResetPasswordEndpointRef
    = AutoDisposeProviderRef<ResetPasswordEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
