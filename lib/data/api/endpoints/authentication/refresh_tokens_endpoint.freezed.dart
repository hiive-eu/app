// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'refresh_tokens_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

RefreshTokensApiResponse _$RefreshTokensApiResponseFromJson(
    Map<String, dynamic> json) {
  return _RefreshTokensApiResponse.fromJson(json);
}

/// @nodoc
mixin _$RefreshTokensApiResponse {
  @JsonKey(name: "authentication_tokens")
  AuthenticationTokensNetwork get authenticationTokens =>
      throw _privateConstructorUsedError;

  /// Serializes this RefreshTokensApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of RefreshTokensApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $RefreshTokensApiResponseCopyWith<RefreshTokensApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RefreshTokensApiResponseCopyWith<$Res> {
  factory $RefreshTokensApiResponseCopyWith(RefreshTokensApiResponse value,
          $Res Function(RefreshTokensApiResponse) then) =
      _$RefreshTokensApiResponseCopyWithImpl<$Res, RefreshTokensApiResponse>;
  @useResult
  $Res call(
      {@JsonKey(name: "authentication_tokens")
      AuthenticationTokensNetwork authenticationTokens});

  $AuthenticationTokensNetworkCopyWith<$Res> get authenticationTokens;
}

/// @nodoc
class _$RefreshTokensApiResponseCopyWithImpl<$Res,
        $Val extends RefreshTokensApiResponse>
    implements $RefreshTokensApiResponseCopyWith<$Res> {
  _$RefreshTokensApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of RefreshTokensApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authenticationTokens = null,
  }) {
    return _then(_value.copyWith(
      authenticationTokens: null == authenticationTokens
          ? _value.authenticationTokens
          : authenticationTokens // ignore: cast_nullable_to_non_nullable
              as AuthenticationTokensNetwork,
    ) as $Val);
  }

  /// Create a copy of RefreshTokensApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $AuthenticationTokensNetworkCopyWith<$Res> get authenticationTokens {
    return $AuthenticationTokensNetworkCopyWith<$Res>(
        _value.authenticationTokens, (value) {
      return _then(_value.copyWith(authenticationTokens: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$RefreshTokensApiResponseImplCopyWith<$Res>
    implements $RefreshTokensApiResponseCopyWith<$Res> {
  factory _$$RefreshTokensApiResponseImplCopyWith(
          _$RefreshTokensApiResponseImpl value,
          $Res Function(_$RefreshTokensApiResponseImpl) then) =
      __$$RefreshTokensApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "authentication_tokens")
      AuthenticationTokensNetwork authenticationTokens});

  @override
  $AuthenticationTokensNetworkCopyWith<$Res> get authenticationTokens;
}

/// @nodoc
class __$$RefreshTokensApiResponseImplCopyWithImpl<$Res>
    extends _$RefreshTokensApiResponseCopyWithImpl<$Res,
        _$RefreshTokensApiResponseImpl>
    implements _$$RefreshTokensApiResponseImplCopyWith<$Res> {
  __$$RefreshTokensApiResponseImplCopyWithImpl(
      _$RefreshTokensApiResponseImpl _value,
      $Res Function(_$RefreshTokensApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of RefreshTokensApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authenticationTokens = null,
  }) {
    return _then(_$RefreshTokensApiResponseImpl(
      authenticationTokens: null == authenticationTokens
          ? _value.authenticationTokens
          : authenticationTokens // ignore: cast_nullable_to_non_nullable
              as AuthenticationTokensNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RefreshTokensApiResponseImpl implements _RefreshTokensApiResponse {
  const _$RefreshTokensApiResponseImpl(
      {@JsonKey(name: "authentication_tokens")
      required this.authenticationTokens});

  factory _$RefreshTokensApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$RefreshTokensApiResponseImplFromJson(json);

  @override
  @JsonKey(name: "authentication_tokens")
  final AuthenticationTokensNetwork authenticationTokens;

  @override
  String toString() {
    return 'RefreshTokensApiResponse(authenticationTokens: $authenticationTokens)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RefreshTokensApiResponseImpl &&
            (identical(other.authenticationTokens, authenticationTokens) ||
                other.authenticationTokens == authenticationTokens));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, authenticationTokens);

  /// Create a copy of RefreshTokensApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$RefreshTokensApiResponseImplCopyWith<_$RefreshTokensApiResponseImpl>
      get copyWith => __$$RefreshTokensApiResponseImplCopyWithImpl<
          _$RefreshTokensApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RefreshTokensApiResponseImplToJson(
      this,
    );
  }
}

abstract class _RefreshTokensApiResponse implements RefreshTokensApiResponse {
  const factory _RefreshTokensApiResponse(
          {@JsonKey(name: "authentication_tokens")
          required final AuthenticationTokensNetwork authenticationTokens}) =
      _$RefreshTokensApiResponseImpl;

  factory _RefreshTokensApiResponse.fromJson(Map<String, dynamic> json) =
      _$RefreshTokensApiResponseImpl.fromJson;

  @override
  @JsonKey(name: "authentication_tokens")
  AuthenticationTokensNetwork get authenticationTokens;

  /// Create a copy of RefreshTokensApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$RefreshTokensApiResponseImplCopyWith<_$RefreshTokensApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
