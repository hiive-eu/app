import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "forgot_password_endpoint.g.dart";

@riverpod
ForgotPasswordEndpoint forgotPasswordEndpoint(Ref ref) =>
    ForgotPasswordEndpoint(ref);

class ForgotPasswordEndpoint extends Endpoint {
  static const String path = "/auth/forgot";
  static const bool authRequired = false;

  late final Ref _ref;
  ForgotPasswordEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> post(ForgotPasswordData forgotPassowrdData) {
    return this._ref.read(httpServiceProvider).post(ForgotPasswordEndpoint.path,
        ForgotPasswordEndpoint.authRequired, forgotPassowrdData.toJson());
  }
}

@JsonSerializable()
class ForgotPasswordData {
  String email;

  ForgotPasswordData({required this.email});

  factory ForgotPasswordData.fromJson(Map<String, dynamic> json) =>
      _$ForgotPasswordDataFromJson(json);

  Map<String, dynamic> toJson() => _$ForgotPasswordDataToJson(this);
}
