// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'forgot_password_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ForgotPasswordData _$ForgotPasswordDataFromJson(Map<String, dynamic> json) =>
    ForgotPasswordData(
      email: json['email'] as String,
    );

Map<String, dynamic> _$ForgotPasswordDataToJson(ForgotPasswordData instance) =>
    <String, dynamic>{
      'email': instance.email,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$forgotPasswordEndpointHash() =>
    r'3d34fbfe767dc362fd5b6d218bf136bda2528246';

/// See also [forgotPasswordEndpoint].
@ProviderFor(forgotPasswordEndpoint)
final forgotPasswordEndpointProvider =
    AutoDisposeProvider<ForgotPasswordEndpoint>.internal(
  forgotPasswordEndpoint,
  name: r'forgotPasswordEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$forgotPasswordEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ForgotPasswordEndpointRef
    = AutoDisposeProviderRef<ForgotPasswordEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
