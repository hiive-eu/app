import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "signup_confirm_endpoint.g.dart";

@riverpod
SignupConfirmEndpoint signupConfirmEndpoint(Ref ref) =>
    SignupConfirmEndpoint(ref);

class SignupConfirmEndpoint extends Endpoint {
  static const String path = "/auth/confirm";
  static const bool authRequired = false;

  late final Ref _ref;
  SignupConfirmEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> post(SignupConfirmData signupConfirmData) {
    return this._ref.read(httpServiceProvider).post(SignupConfirmEndpoint.path,
        SignupConfirmEndpoint.authRequired, signupConfirmData.toJson());
  }
}

@JsonSerializable()
class SignupConfirmData {
  @JsonKey(name: "token")
  String token;

  SignupConfirmData({required this.token});

  factory SignupConfirmData.fromJson(Map<String, dynamic> json) =>
      _$SignupConfirmDataFromJson(json);

  Map<String, dynamic> toJson() => _$SignupConfirmDataToJson(this);
}
