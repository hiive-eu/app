import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/authentication_tokens.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "refresh_tokens_endpoint.freezed.dart";
part "refresh_tokens_endpoint.g.dart";

@riverpod
RefreshTokensEndpoint refreshTokensEndpoint(Ref ref) =>
    RefreshTokensEndpoint(ref);

class RefreshTokensEndpoint extends Endpoint {
  static const String path = "/auth/refresh";
  static const bool authRequired = false;

  late final Ref _ref;
  RefreshTokensEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, RefreshTokensApiResponse> post(
      RefreshTokensData refreshData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .post(RefreshTokensEndpoint.path, RefreshTokensEndpoint.authRequired,
            refreshData.toJson())
        .flatMap(_parseRefreshResponse);
  }

  TaskEither<ApiError, RefreshTokensApiResponse> _parseRefreshResponse(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return RefreshTokensApiResponse.fromJson(data!);
      },
      (error, stackTrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@JsonSerializable()
class RefreshTokensData {
  @JsonKey(name: "refresh_token")
  String refreshToken;

  RefreshTokensData({this.refreshToken = ""});

  factory RefreshTokensData.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokensDataFromJson(json);

  Map<String, dynamic> toJson() => _$RefreshTokensDataToJson(this);
}

@freezed
class RefreshTokensApiResponse with _$RefreshTokensApiResponse {
  const factory RefreshTokensApiResponse({
    @JsonKey(name: "authentication_tokens")
    required AuthenticationTokensNetwork authenticationTokens,
  }) = _RefreshTokensApiResponse;

  factory RefreshTokensApiResponse.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokensApiResponseFromJson(json);
}
