// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_tokens_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RefreshTokensData _$RefreshTokensDataFromJson(Map<String, dynamic> json) =>
    RefreshTokensData(
      refreshToken: json['refresh_token'] as String? ?? "",
    );

Map<String, dynamic> _$RefreshTokensDataToJson(RefreshTokensData instance) =>
    <String, dynamic>{
      'refresh_token': instance.refreshToken,
    };

_$RefreshTokensApiResponseImpl _$$RefreshTokensApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$RefreshTokensApiResponseImpl(
      authenticationTokens: AuthenticationTokensNetwork.fromJson(
          json['authentication_tokens'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$RefreshTokensApiResponseImplToJson(
        _$RefreshTokensApiResponseImpl instance) =>
    <String, dynamic>{
      'authentication_tokens': instance.authenticationTokens,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$refreshTokensEndpointHash() =>
    r'8cf65819257e5451f2348869a3825ff84e7adf9a';

/// See also [refreshTokensEndpoint].
@ProviderFor(refreshTokensEndpoint)
final refreshTokensEndpointProvider =
    AutoDisposeProvider<RefreshTokensEndpoint>.internal(
  refreshTokensEndpoint,
  name: r'refreshTokensEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$refreshTokensEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef RefreshTokensEndpointRef
    = AutoDisposeProviderRef<RefreshTokensEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
