// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_confirm_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupConfirmData _$SignupConfirmDataFromJson(Map<String, dynamic> json) =>
    SignupConfirmData(
      token: json['token'] as String,
    );

Map<String, dynamic> _$SignupConfirmDataToJson(SignupConfirmData instance) =>
    <String, dynamic>{
      'token': instance.token,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$signupConfirmEndpointHash() =>
    r'a6a755f913f47eb7c3fcf5c7d11391351cca171b';

/// See also [signupConfirmEndpoint].
@ProviderFor(signupConfirmEndpoint)
final signupConfirmEndpointProvider =
    AutoDisposeProvider<SignupConfirmEndpoint>.internal(
  signupConfirmEndpoint,
  name: r'signupConfirmEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$signupConfirmEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SignupConfirmEndpointRef
    = AutoDisposeProviderRef<SignupConfirmEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
