import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "reset_password_endpoint.g.dart";

@riverpod
ResetPasswordEndpoint resetPasswordEndpoint(Ref ref) =>
    ResetPasswordEndpoint(ref);

class ResetPasswordEndpoint extends Endpoint {
  static const String path = "/auth/reset";
  static const bool authRequired = false;

  late final Ref _ref;
  ResetPasswordEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> post(ResetPasswordData resetPassowrdData) {
    return this._ref.read(httpServiceProvider).post(ResetPasswordEndpoint.path,
        ResetPasswordEndpoint.authRequired, resetPassowrdData.toJson());
  }
}

@JsonSerializable()
class ResetPasswordData {
  String token;
  @JsonKey(name: "new_password")
  String newPassword;
  @JsonKey(name: "new_password_confirm")
  String newPasswordConfirm;

  ResetPasswordData(
      {required this.token,
      required this.newPassword,
      required this.newPasswordConfirm});

  factory ResetPasswordData.fromJson(Map<String, dynamic> json) =>
      _$ResetPasswordDataFromJson(json);

  Map<String, dynamic> toJson() => _$ResetPasswordDataToJson(this);
}
