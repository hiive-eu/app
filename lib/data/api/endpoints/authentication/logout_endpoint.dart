import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "logout_endpoint.g.dart";

@riverpod
LogoutEndpoint logoutEndpoint(Ref ref) => LogoutEndpoint(ref);

class LogoutEndpoint extends Endpoint {
  static const String path = "/user/logout";
  static const bool authRequired = true;

  late final Ref _ref;
  LogoutEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> get() {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(LogoutEndpoint.path, LogoutEndpoint.authRequired);
  }
}
