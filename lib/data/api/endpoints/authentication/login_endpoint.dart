import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/authentication_tokens.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "login_endpoint.freezed.dart";
part "login_endpoint.g.dart";

@riverpod
LoginEndpoint loginEndpoint(Ref ref) => LoginEndpoint(ref);

class LoginEndpoint extends Endpoint {
  static const String path = "/auth/login";
  static const bool authRequired = false;

  late final Ref _ref;
  LoginEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, LoginApiResponse> post(LoginData loginData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .post(
            LoginEndpoint.path, LoginEndpoint.authRequired, loginData.toJson())
        .flatMap(_parseAuthTokensNetwork);
  }

  TaskEither<ApiError, LoginApiResponse> _parseAuthTokensNetwork(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return LoginApiResponse.fromJson(data!);
      },
      (error, stackTrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@JsonSerializable()
class LoginData {
  String email, password;

  LoginData({required this.email, required this.password});

  factory LoginData.fromJson(Map<String, dynamic> json) =>
      _$LoginDataFromJson(json);

  Map<String, dynamic> toJson() => _$LoginDataToJson(this);
}

@freezed
class LoginApiResponse with _$LoginApiResponse {
  const factory LoginApiResponse({
    @JsonKey(name: "authentication_tokens")
    required AuthenticationTokensNetwork authenticationTokens,
  }) = _LoginApiResponse;

  factory LoginApiResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginApiResponseFromJson(json);
}
