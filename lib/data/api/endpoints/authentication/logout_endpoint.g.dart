// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logout_endpoint.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$logoutEndpointHash() => r'33d75864b823a21541cc5131d980e3070f4878b0';

/// See also [logoutEndpoint].
@ProviderFor(logoutEndpoint)
final logoutEndpointProvider = AutoDisposeProvider<LogoutEndpoint>.internal(
  logoutEndpoint,
  name: r'logoutEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$logoutEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef LogoutEndpointRef = AutoDisposeProviderRef<LogoutEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
