import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "signup_endpoint.g.dart";

@riverpod
SignupEndpoint signupEndpoint(Ref ref) => SignupEndpoint(ref);

class SignupEndpoint extends Endpoint {
  static const String path = "/auth/signup";
  static const bool authRequired = false;

  late final Ref _ref;
  SignupEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> post(SignupData signupData) {
    return this._ref.read(httpServiceProvider).post(
        SignupEndpoint.path, SignupEndpoint.authRequired, signupData.toJson());
  }
}

@JsonSerializable()
class SignupData {
  String email, password;
  @JsonKey(name: "displayname")
  String displayName;

  SignupData(
      {required this.email, required this.displayName, required this.password});

  factory SignupData.fromJson(Map<String, dynamic> json) =>
      _$SignupDataFromJson(json);

  Map<String, dynamic> toJson() => _$SignupDataToJson(this);
}
