// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SignupData _$SignupDataFromJson(Map<String, dynamic> json) => SignupData(
      email: json['email'] as String,
      displayName: json['displayname'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$SignupDataToJson(SignupData instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'displayname': instance.displayName,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$signupEndpointHash() => r'7fa467deab7466ebce2effa691fded9d92369a66';

/// See also [signupEndpoint].
@ProviderFor(signupEndpoint)
final signupEndpointProvider = AutoDisposeProvider<SignupEndpoint>.internal(
  signupEndpoint,
  name: r'signupEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$signupEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SignupEndpointRef = AutoDisposeProviderRef<SignupEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
