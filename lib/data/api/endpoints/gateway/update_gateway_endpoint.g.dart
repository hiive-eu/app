// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_gateway_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateGatewayData _$UpdateGatewayDataFromJson(Map<String, dynamic> json) =>
    UpdateGatewayData(
      name: json['name'] as String?,
    );

Map<String, dynamic> _$UpdateGatewayDataToJson(UpdateGatewayData instance) =>
    <String, dynamic>{
      'name': instance.name,
    };

_$UpdateGatewayApiResponseImpl _$$UpdateGatewayApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$UpdateGatewayApiResponseImpl(
      gateway: GatewayNetwork.fromJson(json['gateway'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$UpdateGatewayApiResponseImplToJson(
        _$UpdateGatewayApiResponseImpl instance) =>
    <String, dynamic>{
      'gateway': instance.gateway,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$updateGatewayEndpointHash() =>
    r'afd9560de5e3aab1dc97471d296d986d05805cda'; //////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [updateGatewayEndpoint].
@ProviderFor(updateGatewayEndpoint)
final updateGatewayEndpointProvider =
    AutoDisposeProvider<UpdateGatewayEndpoint>.internal(
  updateGatewayEndpoint,
  name: r'updateGatewayEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$updateGatewayEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UpdateGatewayEndpointRef
    = AutoDisposeProviderRef<UpdateGatewayEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
