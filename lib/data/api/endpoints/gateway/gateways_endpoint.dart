import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/data_error.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/services/http.dart";

part "gateways_endpoint.freezed.dart";
part "gateways_endpoint.g.dart";

///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// GET // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
GatewaysEndpoint gatewaysEndpoint(Ref ref) => GatewaysEndpoint(ref);

class GatewaysEndpoint extends Endpoint {
  static const String path = "/user/gateways";
  static const bool authRequired = true;

  late final Ref _ref;
  GatewaysEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, GatewaysApiResponse> get() {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(GatewaysEndpoint.path, GatewaysEndpoint.authRequired)
        .flatMap(_parseGatewaysResponse);
  }

  TaskEither<ApiError, GatewaysApiResponse> _parseGatewaysResponse(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return GatewaysApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        print(error);
        print(stacktrace);
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@freezed
class GatewaysApiResponse with _$GatewaysApiResponse {
  const factory GatewaysApiResponse({
    required List<GatewayNetwork> gateways,
  }) = _GatewaysApiResponse;

  factory GatewaysApiResponse.fromJson(Map<String, dynamic> json) =>
      _$GatewaysApiResponseFromJson(json);
}
