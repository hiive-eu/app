// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'gateway_readings_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

GatewayReadingsApiResponse _$GatewayReadingsApiResponseFromJson(
    Map<String, dynamic> json) {
  return _GatewayReadingsApiResponse.fromJson(json);
}

/// @nodoc
mixin _$GatewayReadingsApiResponse {
  List<GatewayReadingNetwork> get readings =>
      throw _privateConstructorUsedError;

  /// Serializes this GatewayReadingsApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayReadingsApiResponseCopyWith<GatewayReadingsApiResponse>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayReadingsApiResponseCopyWith<$Res> {
  factory $GatewayReadingsApiResponseCopyWith(GatewayReadingsApiResponse value,
          $Res Function(GatewayReadingsApiResponse) then) =
      _$GatewayReadingsApiResponseCopyWithImpl<$Res,
          GatewayReadingsApiResponse>;
  @useResult
  $Res call({List<GatewayReadingNetwork> readings});
}

/// @nodoc
class _$GatewayReadingsApiResponseCopyWithImpl<$Res,
        $Val extends GatewayReadingsApiResponse>
    implements $GatewayReadingsApiResponseCopyWith<$Res> {
  _$GatewayReadingsApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? readings = null,
  }) {
    return _then(_value.copyWith(
      readings: null == readings
          ? _value.readings
          : readings // ignore: cast_nullable_to_non_nullable
              as List<GatewayReadingNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewayReadingsApiResponseImplCopyWith<$Res>
    implements $GatewayReadingsApiResponseCopyWith<$Res> {
  factory _$$GatewayReadingsApiResponseImplCopyWith(
          _$GatewayReadingsApiResponseImpl value,
          $Res Function(_$GatewayReadingsApiResponseImpl) then) =
      __$$GatewayReadingsApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<GatewayReadingNetwork> readings});
}

/// @nodoc
class __$$GatewayReadingsApiResponseImplCopyWithImpl<$Res>
    extends _$GatewayReadingsApiResponseCopyWithImpl<$Res,
        _$GatewayReadingsApiResponseImpl>
    implements _$$GatewayReadingsApiResponseImplCopyWith<$Res> {
  __$$GatewayReadingsApiResponseImplCopyWithImpl(
      _$GatewayReadingsApiResponseImpl _value,
      $Res Function(_$GatewayReadingsApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? readings = null,
  }) {
    return _then(_$GatewayReadingsApiResponseImpl(
      readings: null == readings
          ? _value._readings
          : readings // ignore: cast_nullable_to_non_nullable
              as List<GatewayReadingNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayReadingsApiResponseImpl implements _GatewayReadingsApiResponse {
  const _$GatewayReadingsApiResponseImpl(
      {required final List<GatewayReadingNetwork> readings})
      : _readings = readings;

  factory _$GatewayReadingsApiResponseImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$GatewayReadingsApiResponseImplFromJson(json);

  final List<GatewayReadingNetwork> _readings;
  @override
  List<GatewayReadingNetwork> get readings {
    if (_readings is EqualUnmodifiableListView) return _readings;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_readings);
  }

  @override
  String toString() {
    return 'GatewayReadingsApiResponse(readings: $readings)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayReadingsApiResponseImpl &&
            const DeepCollectionEquality().equals(other._readings, _readings));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_readings));

  /// Create a copy of GatewayReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayReadingsApiResponseImplCopyWith<_$GatewayReadingsApiResponseImpl>
      get copyWith => __$$GatewayReadingsApiResponseImplCopyWithImpl<
          _$GatewayReadingsApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayReadingsApiResponseImplToJson(
      this,
    );
  }
}

abstract class _GatewayReadingsApiResponse
    implements GatewayReadingsApiResponse {
  const factory _GatewayReadingsApiResponse(
          {required final List<GatewayReadingNetwork> readings}) =
      _$GatewayReadingsApiResponseImpl;

  factory _GatewayReadingsApiResponse.fromJson(Map<String, dynamic> json) =
      _$GatewayReadingsApiResponseImpl.fromJson;

  @override
  List<GatewayReadingNetwork> get readings;

  /// Create a copy of GatewayReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayReadingsApiResponseImplCopyWith<_$GatewayReadingsApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
