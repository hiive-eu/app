// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'claim_gateway_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClaimGatewayData _$ClaimGatewayDataFromJson(Map<String, dynamic> json) =>
    ClaimGatewayData(
      mac: json['mac'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$ClaimGatewayDataToJson(ClaimGatewayData instance) =>
    <String, dynamic>{
      'mac': instance.mac,
      'name': instance.name,
    };

_$ClaimGatewayApiResponseImpl _$$ClaimGatewayApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$ClaimGatewayApiResponseImpl(
      gateway: GatewayNetwork.fromJson(json['gateway'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ClaimGatewayApiResponseImplToJson(
        _$ClaimGatewayApiResponseImpl instance) =>
    <String, dynamic>{
      'gateway': instance.gateway,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$claimGatewayEndpointHash() =>
    r'cd2f616b409fa2c434653e4c0d82cdd0c459c5ba'; /////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [claimGatewayEndpoint].
@ProviderFor(claimGatewayEndpoint)
final claimGatewayEndpointProvider =
    AutoDisposeProvider<ClaimGatewayEndpoint>.internal(
  claimGatewayEndpoint,
  name: r'claimGatewayEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$claimGatewayEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ClaimGatewayEndpointRef = AutoDisposeProviderRef<ClaimGatewayEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
