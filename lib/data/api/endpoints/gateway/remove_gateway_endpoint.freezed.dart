// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'remove_gateway_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

RemoveGatewayApiResponse _$RemoveGatewayApiResponseFromJson(
    Map<String, dynamic> json) {
  return _RemoveGatewayApiResponse.fromJson(json);
}

/// @nodoc
mixin _$RemoveGatewayApiResponse {
  List<SensorNetwork> get sensors => throw _privateConstructorUsedError;

  /// Serializes this RemoveGatewayApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of RemoveGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $RemoveGatewayApiResponseCopyWith<RemoveGatewayApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RemoveGatewayApiResponseCopyWith<$Res> {
  factory $RemoveGatewayApiResponseCopyWith(RemoveGatewayApiResponse value,
          $Res Function(RemoveGatewayApiResponse) then) =
      _$RemoveGatewayApiResponseCopyWithImpl<$Res, RemoveGatewayApiResponse>;
  @useResult
  $Res call({List<SensorNetwork> sensors});
}

/// @nodoc
class _$RemoveGatewayApiResponseCopyWithImpl<$Res,
        $Val extends RemoveGatewayApiResponse>
    implements $RemoveGatewayApiResponseCopyWith<$Res> {
  _$RemoveGatewayApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of RemoveGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sensors = null,
  }) {
    return _then(_value.copyWith(
      sensors: null == sensors
          ? _value.sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<SensorNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RemoveGatewayApiResponseImplCopyWith<$Res>
    implements $RemoveGatewayApiResponseCopyWith<$Res> {
  factory _$$RemoveGatewayApiResponseImplCopyWith(
          _$RemoveGatewayApiResponseImpl value,
          $Res Function(_$RemoveGatewayApiResponseImpl) then) =
      __$$RemoveGatewayApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<SensorNetwork> sensors});
}

/// @nodoc
class __$$RemoveGatewayApiResponseImplCopyWithImpl<$Res>
    extends _$RemoveGatewayApiResponseCopyWithImpl<$Res,
        _$RemoveGatewayApiResponseImpl>
    implements _$$RemoveGatewayApiResponseImplCopyWith<$Res> {
  __$$RemoveGatewayApiResponseImplCopyWithImpl(
      _$RemoveGatewayApiResponseImpl _value,
      $Res Function(_$RemoveGatewayApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of RemoveGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sensors = null,
  }) {
    return _then(_$RemoveGatewayApiResponseImpl(
      sensors: null == sensors
          ? _value._sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<SensorNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RemoveGatewayApiResponseImpl implements _RemoveGatewayApiResponse {
  const _$RemoveGatewayApiResponseImpl(
      {required final List<SensorNetwork> sensors})
      : _sensors = sensors;

  factory _$RemoveGatewayApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$RemoveGatewayApiResponseImplFromJson(json);

  final List<SensorNetwork> _sensors;
  @override
  List<SensorNetwork> get sensors {
    if (_sensors is EqualUnmodifiableListView) return _sensors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_sensors);
  }

  @override
  String toString() {
    return 'RemoveGatewayApiResponse(sensors: $sensors)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RemoveGatewayApiResponseImpl &&
            const DeepCollectionEquality().equals(other._sensors, _sensors));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_sensors));

  /// Create a copy of RemoveGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$RemoveGatewayApiResponseImplCopyWith<_$RemoveGatewayApiResponseImpl>
      get copyWith => __$$RemoveGatewayApiResponseImplCopyWithImpl<
          _$RemoveGatewayApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RemoveGatewayApiResponseImplToJson(
      this,
    );
  }
}

abstract class _RemoveGatewayApiResponse implements RemoveGatewayApiResponse {
  const factory _RemoveGatewayApiResponse(
          {required final List<SensorNetwork> sensors}) =
      _$RemoveGatewayApiResponseImpl;

  factory _RemoveGatewayApiResponse.fromJson(Map<String, dynamic> json) =
      _$RemoveGatewayApiResponseImpl.fromJson;

  @override
  List<SensorNetwork> get sensors;

  /// Create a copy of RemoveGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$RemoveGatewayApiResponseImplCopyWith<_$RemoveGatewayApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
