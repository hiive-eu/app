import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/sensor.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "remove_gateway_endpoint.freezed.dart";
part "remove_gateway_endpoint.g.dart";

@riverpod
RemoveGatewayEndpoint removeGatewayEndpoint(Ref ref) =>
    RemoveGatewayEndpoint(ref);

class RemoveGatewayEndpoint extends Endpoint {
  String path(String mac) => "/user/gateway/$mac";
  static const bool authRequired = true;

  late final Ref _ref;
  RemoveGatewayEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, RemoveGatewayApiResponse> delete(String mac) {
    return this
        ._ref
        .read(httpServiceProvider)
        .delete(this.path(mac), RemoveGatewayEndpoint.authRequired)
        .flatMap(_parseRemoveGatewayResponse);
  }

  TaskEither<ApiError, RemoveGatewayApiResponse> _parseRemoveGatewayResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return RemoveGatewayApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@freezed
class RemoveGatewayApiResponse with _$RemoveGatewayApiResponse {
  const factory RemoveGatewayApiResponse({
    required List<SensorNetwork> sensors,
  }) = _RemoveGatewayApiResponse;

  factory RemoveGatewayApiResponse.fromJson(Map<String, dynamic> json) =>
      _$RemoveGatewayApiResponseFromJson(json);
}
