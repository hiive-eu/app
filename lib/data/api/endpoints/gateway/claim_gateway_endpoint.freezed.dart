// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'claim_gateway_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ClaimGatewayApiResponse _$ClaimGatewayApiResponseFromJson(
    Map<String, dynamic> json) {
  return _ClaimGatewayApiResponse.fromJson(json);
}

/// @nodoc
mixin _$ClaimGatewayApiResponse {
  GatewayNetwork get gateway => throw _privateConstructorUsedError;

  /// Serializes this ClaimGatewayApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of ClaimGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ClaimGatewayApiResponseCopyWith<ClaimGatewayApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ClaimGatewayApiResponseCopyWith<$Res> {
  factory $ClaimGatewayApiResponseCopyWith(ClaimGatewayApiResponse value,
          $Res Function(ClaimGatewayApiResponse) then) =
      _$ClaimGatewayApiResponseCopyWithImpl<$Res, ClaimGatewayApiResponse>;
  @useResult
  $Res call({GatewayNetwork gateway});

  $GatewayNetworkCopyWith<$Res> get gateway;
}

/// @nodoc
class _$ClaimGatewayApiResponseCopyWithImpl<$Res,
        $Val extends ClaimGatewayApiResponse>
    implements $ClaimGatewayApiResponseCopyWith<$Res> {
  _$ClaimGatewayApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ClaimGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateway = null,
  }) {
    return _then(_value.copyWith(
      gateway: null == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as GatewayNetwork,
    ) as $Val);
  }

  /// Create a copy of ClaimGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayNetworkCopyWith<$Res> get gateway {
    return $GatewayNetworkCopyWith<$Res>(_value.gateway, (value) {
      return _then(_value.copyWith(gateway: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ClaimGatewayApiResponseImplCopyWith<$Res>
    implements $ClaimGatewayApiResponseCopyWith<$Res> {
  factory _$$ClaimGatewayApiResponseImplCopyWith(
          _$ClaimGatewayApiResponseImpl value,
          $Res Function(_$ClaimGatewayApiResponseImpl) then) =
      __$$ClaimGatewayApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({GatewayNetwork gateway});

  @override
  $GatewayNetworkCopyWith<$Res> get gateway;
}

/// @nodoc
class __$$ClaimGatewayApiResponseImplCopyWithImpl<$Res>
    extends _$ClaimGatewayApiResponseCopyWithImpl<$Res,
        _$ClaimGatewayApiResponseImpl>
    implements _$$ClaimGatewayApiResponseImplCopyWith<$Res> {
  __$$ClaimGatewayApiResponseImplCopyWithImpl(
      _$ClaimGatewayApiResponseImpl _value,
      $Res Function(_$ClaimGatewayApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of ClaimGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateway = null,
  }) {
    return _then(_$ClaimGatewayApiResponseImpl(
      gateway: null == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as GatewayNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ClaimGatewayApiResponseImpl implements _ClaimGatewayApiResponse {
  const _$ClaimGatewayApiResponseImpl({required this.gateway});

  factory _$ClaimGatewayApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$ClaimGatewayApiResponseImplFromJson(json);

  @override
  final GatewayNetwork gateway;

  @override
  String toString() {
    return 'ClaimGatewayApiResponse(gateway: $gateway)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ClaimGatewayApiResponseImpl &&
            (identical(other.gateway, gateway) || other.gateway == gateway));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, gateway);

  /// Create a copy of ClaimGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ClaimGatewayApiResponseImplCopyWith<_$ClaimGatewayApiResponseImpl>
      get copyWith => __$$ClaimGatewayApiResponseImplCopyWithImpl<
          _$ClaimGatewayApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ClaimGatewayApiResponseImplToJson(
      this,
    );
  }
}

abstract class _ClaimGatewayApiResponse implements ClaimGatewayApiResponse {
  const factory _ClaimGatewayApiResponse(
      {required final GatewayNetwork gateway}) = _$ClaimGatewayApiResponseImpl;

  factory _ClaimGatewayApiResponse.fromJson(Map<String, dynamic> json) =
      _$ClaimGatewayApiResponseImpl.fromJson;

  @override
  GatewayNetwork get gateway;

  /// Create a copy of ClaimGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ClaimGatewayApiResponseImplCopyWith<_$ClaimGatewayApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
