// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$GatewayApiResponseImpl _$$GatewayApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$GatewayApiResponseImpl(
      gateway: GatewayNetwork.fromJson(json['gateway'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$GatewayApiResponseImplToJson(
        _$GatewayApiResponseImpl instance) =>
    <String, dynamic>{
      'gateway': instance.gateway,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewayEndpointHash() => r'998e33980b398b92ff985bfd0006288a483b82d7';

/// See also [gatewayEndpoint].
@ProviderFor(gatewayEndpoint)
final gatewayEndpointProvider = AutoDisposeProvider<GatewayEndpoint>.internal(
  gatewayEndpoint,
  name: r'gatewayEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewayEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef GatewayEndpointRef = AutoDisposeProviderRef<GatewayEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
