// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_gateway_endpoint.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$checkGatewayEndpointHash() =>
    r'929bc053ffcfaccd715be395d0e40a565bf81246';

/// See also [checkGatewayEndpoint].
@ProviderFor(checkGatewayEndpoint)
final checkGatewayEndpointProvider =
    AutoDisposeProvider<CheckGatewayEndpoint>.internal(
  checkGatewayEndpoint,
  name: r'checkGatewayEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$checkGatewayEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CheckGatewayEndpointRef = AutoDisposeProviderRef<CheckGatewayEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
