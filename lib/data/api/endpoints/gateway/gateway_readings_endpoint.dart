import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway_reading.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "gateway_readings_endpoint.freezed.dart";
part "gateway_readings_endpoint.g.dart";

@riverpod
GatewayReadingsEndpoint gatewayReadingsEndpoint(Ref ref) =>
    GatewayReadingsEndpoint(ref);

class GatewayReadingsEndpoint extends Endpoint {
  String path(String mac) => "/user/gateway/$mac/readings";
  static const authRequired = true;

  late final Ref _ref;
  GatewayReadingsEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, GatewayReadingsApiResponse> get(String mac) {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(this.path(mac), GatewayReadingsEndpoint.authRequired)
        .flatMap(_parseGatewayReadings);
  }

  TaskEither<ApiError, GatewayReadingsApiResponse> _parseGatewayReadings(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(() => GatewayReadingsApiResponse.fromJson(data!),
        (error, stacktrace) => const ApiError.unexpectedResponseData());
    return e.toTaskEither();
  }
}

@freezed
class GatewayReadingsApiResponse with _$GatewayReadingsApiResponse {
  const factory GatewayReadingsApiResponse({
    required List<GatewayReadingNetwork> readings,
  }) = _GatewayReadingsApiResponse;

  factory GatewayReadingsApiResponse.fromJson(Map<String, dynamic> json) =>
      _$GatewayReadingsApiResponseFromJson(json);
}
