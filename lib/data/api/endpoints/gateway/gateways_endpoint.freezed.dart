// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'gateways_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

GatewaysApiResponse _$GatewaysApiResponseFromJson(Map<String, dynamic> json) {
  return _GatewaysApiResponse.fromJson(json);
}

/// @nodoc
mixin _$GatewaysApiResponse {
  List<GatewayNetwork> get gateways => throw _privateConstructorUsedError;

  /// Serializes this GatewaysApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewaysApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewaysApiResponseCopyWith<GatewaysApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewaysApiResponseCopyWith<$Res> {
  factory $GatewaysApiResponseCopyWith(
          GatewaysApiResponse value, $Res Function(GatewaysApiResponse) then) =
      _$GatewaysApiResponseCopyWithImpl<$Res, GatewaysApiResponse>;
  @useResult
  $Res call({List<GatewayNetwork> gateways});
}

/// @nodoc
class _$GatewaysApiResponseCopyWithImpl<$Res, $Val extends GatewaysApiResponse>
    implements $GatewaysApiResponseCopyWith<$Res> {
  _$GatewaysApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewaysApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateways = null,
  }) {
    return _then(_value.copyWith(
      gateways: null == gateways
          ? _value.gateways
          : gateways // ignore: cast_nullable_to_non_nullable
              as List<GatewayNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewaysApiResponseImplCopyWith<$Res>
    implements $GatewaysApiResponseCopyWith<$Res> {
  factory _$$GatewaysApiResponseImplCopyWith(_$GatewaysApiResponseImpl value,
          $Res Function(_$GatewaysApiResponseImpl) then) =
      __$$GatewaysApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<GatewayNetwork> gateways});
}

/// @nodoc
class __$$GatewaysApiResponseImplCopyWithImpl<$Res>
    extends _$GatewaysApiResponseCopyWithImpl<$Res, _$GatewaysApiResponseImpl>
    implements _$$GatewaysApiResponseImplCopyWith<$Res> {
  __$$GatewaysApiResponseImplCopyWithImpl(_$GatewaysApiResponseImpl _value,
      $Res Function(_$GatewaysApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewaysApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateways = null,
  }) {
    return _then(_$GatewaysApiResponseImpl(
      gateways: null == gateways
          ? _value._gateways
          : gateways // ignore: cast_nullable_to_non_nullable
              as List<GatewayNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewaysApiResponseImpl implements _GatewaysApiResponse {
  const _$GatewaysApiResponseImpl(
      {required final List<GatewayNetwork> gateways})
      : _gateways = gateways;

  factory _$GatewaysApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewaysApiResponseImplFromJson(json);

  final List<GatewayNetwork> _gateways;
  @override
  List<GatewayNetwork> get gateways {
    if (_gateways is EqualUnmodifiableListView) return _gateways;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_gateways);
  }

  @override
  String toString() {
    return 'GatewaysApiResponse(gateways: $gateways)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewaysApiResponseImpl &&
            const DeepCollectionEquality().equals(other._gateways, _gateways));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_gateways));

  /// Create a copy of GatewaysApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewaysApiResponseImplCopyWith<_$GatewaysApiResponseImpl> get copyWith =>
      __$$GatewaysApiResponseImplCopyWithImpl<_$GatewaysApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewaysApiResponseImplToJson(
      this,
    );
  }
}

abstract class _GatewaysApiResponse implements GatewaysApiResponse {
  const factory _GatewaysApiResponse(
          {required final List<GatewayNetwork> gateways}) =
      _$GatewaysApiResponseImpl;

  factory _GatewaysApiResponse.fromJson(Map<String, dynamic> json) =
      _$GatewaysApiResponseImpl.fromJson;

  @override
  List<GatewayNetwork> get gateways;

  /// Create a copy of GatewaysApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewaysApiResponseImplCopyWith<_$GatewaysApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
