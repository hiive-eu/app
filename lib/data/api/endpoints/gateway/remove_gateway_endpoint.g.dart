// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_gateway_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RemoveGatewayApiResponseImpl _$$RemoveGatewayApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$RemoveGatewayApiResponseImpl(
      sensors: (json['sensors'] as List<dynamic>)
          .map((e) => SensorNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$RemoveGatewayApiResponseImplToJson(
        _$RemoveGatewayApiResponseImpl instance) =>
    <String, dynamic>{
      'sensors': instance.sensors,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$removeGatewayEndpointHash() =>
    r'21bc6831db6417b15cac3b7af0d5c614b8853db7';

/// See also [removeGatewayEndpoint].
@ProviderFor(removeGatewayEndpoint)
final removeGatewayEndpointProvider =
    AutoDisposeProvider<RemoveGatewayEndpoint>.internal(
  removeGatewayEndpoint,
  name: r'removeGatewayEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$removeGatewayEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef RemoveGatewayEndpointRef
    = AutoDisposeProviderRef<RemoveGatewayEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
