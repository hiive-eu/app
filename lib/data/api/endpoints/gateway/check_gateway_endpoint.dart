import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "check_gateway_endpoint.g.dart";

@riverpod
CheckGatewayEndpoint checkGatewayEndpoint(Ref ref) => CheckGatewayEndpoint(ref);

class CheckGatewayEndpoint extends Endpoint {
  String path(String mac) => "/user/gateway/$mac/check";
  static const bool authRequired = true;

  late final Ref _ref;
  CheckGatewayEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> get(String mac) {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(this.path(mac), CheckGatewayEndpoint.authRequired);
  }
}
