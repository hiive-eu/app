// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'update_gateway_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UpdateGatewayApiResponse _$UpdateGatewayApiResponseFromJson(
    Map<String, dynamic> json) {
  return _UpdateGatewayApiResponse.fromJson(json);
}

/// @nodoc
mixin _$UpdateGatewayApiResponse {
  GatewayNetwork get gateway => throw _privateConstructorUsedError;

  /// Serializes this UpdateGatewayApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of UpdateGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $UpdateGatewayApiResponseCopyWith<UpdateGatewayApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpdateGatewayApiResponseCopyWith<$Res> {
  factory $UpdateGatewayApiResponseCopyWith(UpdateGatewayApiResponse value,
          $Res Function(UpdateGatewayApiResponse) then) =
      _$UpdateGatewayApiResponseCopyWithImpl<$Res, UpdateGatewayApiResponse>;
  @useResult
  $Res call({GatewayNetwork gateway});

  $GatewayNetworkCopyWith<$Res> get gateway;
}

/// @nodoc
class _$UpdateGatewayApiResponseCopyWithImpl<$Res,
        $Val extends UpdateGatewayApiResponse>
    implements $UpdateGatewayApiResponseCopyWith<$Res> {
  _$UpdateGatewayApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of UpdateGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateway = null,
  }) {
    return _then(_value.copyWith(
      gateway: null == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as GatewayNetwork,
    ) as $Val);
  }

  /// Create a copy of UpdateGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayNetworkCopyWith<$Res> get gateway {
    return $GatewayNetworkCopyWith<$Res>(_value.gateway, (value) {
      return _then(_value.copyWith(gateway: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$UpdateGatewayApiResponseImplCopyWith<$Res>
    implements $UpdateGatewayApiResponseCopyWith<$Res> {
  factory _$$UpdateGatewayApiResponseImplCopyWith(
          _$UpdateGatewayApiResponseImpl value,
          $Res Function(_$UpdateGatewayApiResponseImpl) then) =
      __$$UpdateGatewayApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({GatewayNetwork gateway});

  @override
  $GatewayNetworkCopyWith<$Res> get gateway;
}

/// @nodoc
class __$$UpdateGatewayApiResponseImplCopyWithImpl<$Res>
    extends _$UpdateGatewayApiResponseCopyWithImpl<$Res,
        _$UpdateGatewayApiResponseImpl>
    implements _$$UpdateGatewayApiResponseImplCopyWith<$Res> {
  __$$UpdateGatewayApiResponseImplCopyWithImpl(
      _$UpdateGatewayApiResponseImpl _value,
      $Res Function(_$UpdateGatewayApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of UpdateGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateway = null,
  }) {
    return _then(_$UpdateGatewayApiResponseImpl(
      gateway: null == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as GatewayNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$UpdateGatewayApiResponseImpl implements _UpdateGatewayApiResponse {
  const _$UpdateGatewayApiResponseImpl({required this.gateway});

  factory _$UpdateGatewayApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$UpdateGatewayApiResponseImplFromJson(json);

  @override
  final GatewayNetwork gateway;

  @override
  String toString() {
    return 'UpdateGatewayApiResponse(gateway: $gateway)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateGatewayApiResponseImpl &&
            (identical(other.gateway, gateway) || other.gateway == gateway));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, gateway);

  /// Create a copy of UpdateGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateGatewayApiResponseImplCopyWith<_$UpdateGatewayApiResponseImpl>
      get copyWith => __$$UpdateGatewayApiResponseImplCopyWithImpl<
          _$UpdateGatewayApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$UpdateGatewayApiResponseImplToJson(
      this,
    );
  }
}

abstract class _UpdateGatewayApiResponse implements UpdateGatewayApiResponse {
  const factory _UpdateGatewayApiResponse(
      {required final GatewayNetwork gateway}) = _$UpdateGatewayApiResponseImpl;

  factory _UpdateGatewayApiResponse.fromJson(Map<String, dynamic> json) =
      _$UpdateGatewayApiResponseImpl.fromJson;

  @override
  GatewayNetwork get gateway;

  /// Create a copy of UpdateGatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UpdateGatewayApiResponseImplCopyWith<_$UpdateGatewayApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
