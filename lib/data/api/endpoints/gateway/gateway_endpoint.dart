import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "gateway_endpoint.g.dart";
part "gateway_endpoint.freezed.dart";

@riverpod
GatewayEndpoint gatewayEndpoint(Ref ref) => GatewayEndpoint(ref);

class GatewayEndpoint extends Endpoint {
  String path(String mac) => "/user/gateway/$mac";
  static const bool authRequired = true;

  late final Ref _ref;
  GatewayEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, GatewayApiResponse> get(String mac) {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(this.path(mac), GatewayEndpoint.authRequired)
        .flatMap(_parseGatewayResponse);
  }

  TaskEither<ApiError, GatewayApiResponse> _parseGatewayResponse(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return GatewayApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        print(error);
        print(stacktrace);
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@freezed
class GatewayApiResponse with _$GatewayApiResponse {
  const factory GatewayApiResponse({
    required GatewayNetwork gateway,
  }) = _GatewayApiResponse;

  factory GatewayApiResponse.fromJson(Map<String, dynamic> json) =>
      _$GatewayApiResponseFromJson(json);
}
