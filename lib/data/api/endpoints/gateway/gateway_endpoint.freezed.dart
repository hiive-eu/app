// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'gateway_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

GatewayApiResponse _$GatewayApiResponseFromJson(Map<String, dynamic> json) {
  return _GatewayApiResponse.fromJson(json);
}

/// @nodoc
mixin _$GatewayApiResponse {
  GatewayNetwork get gateway => throw _privateConstructorUsedError;

  /// Serializes this GatewayApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayApiResponseCopyWith<GatewayApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayApiResponseCopyWith<$Res> {
  factory $GatewayApiResponseCopyWith(
          GatewayApiResponse value, $Res Function(GatewayApiResponse) then) =
      _$GatewayApiResponseCopyWithImpl<$Res, GatewayApiResponse>;
  @useResult
  $Res call({GatewayNetwork gateway});

  $GatewayNetworkCopyWith<$Res> get gateway;
}

/// @nodoc
class _$GatewayApiResponseCopyWithImpl<$Res, $Val extends GatewayApiResponse>
    implements $GatewayApiResponseCopyWith<$Res> {
  _$GatewayApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateway = null,
  }) {
    return _then(_value.copyWith(
      gateway: null == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as GatewayNetwork,
    ) as $Val);
  }

  /// Create a copy of GatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayNetworkCopyWith<$Res> get gateway {
    return $GatewayNetworkCopyWith<$Res>(_value.gateway, (value) {
      return _then(_value.copyWith(gateway: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$GatewayApiResponseImplCopyWith<$Res>
    implements $GatewayApiResponseCopyWith<$Res> {
  factory _$$GatewayApiResponseImplCopyWith(_$GatewayApiResponseImpl value,
          $Res Function(_$GatewayApiResponseImpl) then) =
      __$$GatewayApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({GatewayNetwork gateway});

  @override
  $GatewayNetworkCopyWith<$Res> get gateway;
}

/// @nodoc
class __$$GatewayApiResponseImplCopyWithImpl<$Res>
    extends _$GatewayApiResponseCopyWithImpl<$Res, _$GatewayApiResponseImpl>
    implements _$$GatewayApiResponseImplCopyWith<$Res> {
  __$$GatewayApiResponseImplCopyWithImpl(_$GatewayApiResponseImpl _value,
      $Res Function(_$GatewayApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateway = null,
  }) {
    return _then(_$GatewayApiResponseImpl(
      gateway: null == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as GatewayNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayApiResponseImpl implements _GatewayApiResponse {
  const _$GatewayApiResponseImpl({required this.gateway});

  factory _$GatewayApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayApiResponseImplFromJson(json);

  @override
  final GatewayNetwork gateway;

  @override
  String toString() {
    return 'GatewayApiResponse(gateway: $gateway)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayApiResponseImpl &&
            (identical(other.gateway, gateway) || other.gateway == gateway));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, gateway);

  /// Create a copy of GatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayApiResponseImplCopyWith<_$GatewayApiResponseImpl> get copyWith =>
      __$$GatewayApiResponseImplCopyWithImpl<_$GatewayApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayApiResponseImplToJson(
      this,
    );
  }
}

abstract class _GatewayApiResponse implements GatewayApiResponse {
  const factory _GatewayApiResponse({required final GatewayNetwork gateway}) =
      _$GatewayApiResponseImpl;

  factory _GatewayApiResponse.fromJson(Map<String, dynamic> json) =
      _$GatewayApiResponseImpl.fromJson;

  @override
  GatewayNetwork get gateway;

  /// Create a copy of GatewayApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayApiResponseImplCopyWith<_$GatewayApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
