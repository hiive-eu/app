import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "claim_gateway_endpoint.freezed.dart";
part "claim_gateway_endpoint.g.dart";

/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// CLAIM // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
ClaimGatewayEndpoint claimGatewayEndpoint(Ref ref) => ClaimGatewayEndpoint(ref);

class ClaimGatewayEndpoint extends Endpoint {
  static const String path = "/user/gateway";
  static const bool authRequired = true;

  late final Ref _ref;
  ClaimGatewayEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, ClaimGatewayApiResponse> post(
      ClaimGatewayData claimGatewayData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .post(ClaimGatewayEndpoint.path, ClaimGatewayEndpoint.authRequired,
            claimGatewayData.toJson())
        .flatMap(_parseClaimGatewayResponse);
  }

  TaskEither<ApiError, ClaimGatewayApiResponse> _parseClaimGatewayResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return ClaimGatewayApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@JsonSerializable()
class ClaimGatewayData {
  String mac;
  String name;

  ClaimGatewayData({required this.mac, required this.name});

  factory ClaimGatewayData.fromJson(Map<String, dynamic> json) =>
      _$ClaimGatewayDataFromJson(json);

  Map<String, dynamic> toJson() => _$ClaimGatewayDataToJson(this);
}

@freezed
class ClaimGatewayApiResponse with _$ClaimGatewayApiResponse {
  const factory ClaimGatewayApiResponse({
    required GatewayNetwork gateway,
  }) = _ClaimGatewayApiResponse;

  factory ClaimGatewayApiResponse.fromJson(Map<String, dynamic> json) =>
      _$ClaimGatewayApiResponseFromJson(json);
}
