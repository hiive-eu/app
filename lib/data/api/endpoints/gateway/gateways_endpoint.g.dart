// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateways_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$GatewaysApiResponseImpl _$$GatewaysApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$GatewaysApiResponseImpl(
      gateways: (json['gateways'] as List<dynamic>)
          .map((e) => GatewayNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$GatewaysApiResponseImplToJson(
        _$GatewaysApiResponseImpl instance) =>
    <String, dynamic>{
      'gateways': instance.gateways,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewaysEndpointHash() =>
    r'8ecabc4493478616cf1c52afcf783a5d61a0ae62'; ///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [gatewaysEndpoint].
@ProviderFor(gatewaysEndpoint)
final gatewaysEndpointProvider = AutoDisposeProvider<GatewaysEndpoint>.internal(
  gatewaysEndpoint,
  name: r'gatewaysEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewaysEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef GatewaysEndpointRef = AutoDisposeProviderRef<GatewaysEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
