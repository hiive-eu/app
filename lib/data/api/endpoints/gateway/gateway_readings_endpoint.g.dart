// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway_readings_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$GatewayReadingsApiResponseImpl _$$GatewayReadingsApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$GatewayReadingsApiResponseImpl(
      readings: (json['readings'] as List<dynamic>)
          .map((e) => GatewayReadingNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$GatewayReadingsApiResponseImplToJson(
        _$GatewayReadingsApiResponseImpl instance) =>
    <String, dynamic>{
      'readings': instance.readings,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewayReadingsEndpointHash() =>
    r'56ac3f13a2c422b1e9e2b94f29406d9089e1f123';

/// See also [gatewayReadingsEndpoint].
@ProviderFor(gatewayReadingsEndpoint)
final gatewayReadingsEndpointProvider =
    AutoDisposeProvider<GatewayReadingsEndpoint>.internal(
  gatewayReadingsEndpoint,
  name: r'gatewayReadingsEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewayReadingsEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef GatewayReadingsEndpointRef
    = AutoDisposeProviderRef<GatewayReadingsEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
