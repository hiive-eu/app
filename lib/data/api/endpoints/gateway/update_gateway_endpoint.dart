import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "update_gateway_endpoint.freezed.dart";
part "update_gateway_endpoint.g.dart";

//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// UPDATE // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
UpdateGatewayEndpoint updateGatewayEndpoint(Ref ref) =>
    UpdateGatewayEndpoint(ref);

class UpdateGatewayEndpoint extends Endpoint {
  String path(String mac) => "/user/gateway/$mac";
  static const bool authRequired = true;

  late final Ref _ref;
  UpdateGatewayEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, UpdateGatewayApiResponse> patch(
      String mac, UpdateGatewayData updateGatewayData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .patch(this.path(mac), UpdateGatewayEndpoint.authRequired,
            updateGatewayData.toJson())
        .flatMap(_parseUpdateGatewayResponse);
  }

  TaskEither<ApiError, UpdateGatewayApiResponse> _parseUpdateGatewayResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return UpdateGatewayApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@JsonSerializable()
class UpdateGatewayData {
  String? name;

  UpdateGatewayData({this.name});

  factory UpdateGatewayData.fromJson(Map<String, dynamic> json) =>
      _$UpdateGatewayDataFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateGatewayDataToJson(this);
}

@freezed
class UpdateGatewayApiResponse with _$UpdateGatewayApiResponse {
  const factory UpdateGatewayApiResponse({
    required GatewayNetwork gateway,
  }) = _UpdateGatewayApiResponse;

  factory UpdateGatewayApiResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateGatewayApiResponseFromJson(json);
}
