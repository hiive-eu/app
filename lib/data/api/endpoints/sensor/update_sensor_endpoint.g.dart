// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_sensor_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateSensorData _$UpdateSensorDataFromJson(Map<String, dynamic> json) =>
    UpdateSensorData(
      gateway: json['gateway'] as String?,
    );

Map<String, dynamic> _$UpdateSensorDataToJson(UpdateSensorData instance) =>
    <String, dynamic>{
      'gateway': instance.gateway,
    };

_$UpdateSensorApiResponseImpl _$$UpdateSensorApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$UpdateSensorApiResponseImpl(
      sensor: SensorNetwork.fromJson(json['sensor'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$UpdateSensorApiResponseImplToJson(
        _$UpdateSensorApiResponseImpl instance) =>
    <String, dynamic>{
      'sensor': instance.sensor,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$updateSensorEndpointHash() =>
    r'26cbb77f38b595546e5c72d2417642340a7831ef'; //////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [updateSensorEndpoint].
@ProviderFor(updateSensorEndpoint)
final updateSensorEndpointProvider =
    AutoDisposeProvider<UpdateSensorEndpoint>.internal(
  updateSensorEndpoint,
  name: r'updateSensorEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$updateSensorEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UpdateSensorEndpointRef = AutoDisposeProviderRef<UpdateSensorEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
