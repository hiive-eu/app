// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sensors_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

SensorsApiResponse _$SensorsApiResponseFromJson(Map<String, dynamic> json) {
  return _SensorsApiResponse.fromJson(json);
}

/// @nodoc
mixin _$SensorsApiResponse {
  List<SensorNetwork> get sensors => throw _privateConstructorUsedError;

  /// Serializes this SensorsApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of SensorsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SensorsApiResponseCopyWith<SensorsApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SensorsApiResponseCopyWith<$Res> {
  factory $SensorsApiResponseCopyWith(
          SensorsApiResponse value, $Res Function(SensorsApiResponse) then) =
      _$SensorsApiResponseCopyWithImpl<$Res, SensorsApiResponse>;
  @useResult
  $Res call({List<SensorNetwork> sensors});
}

/// @nodoc
class _$SensorsApiResponseCopyWithImpl<$Res, $Val extends SensorsApiResponse>
    implements $SensorsApiResponseCopyWith<$Res> {
  _$SensorsApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of SensorsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sensors = null,
  }) {
    return _then(_value.copyWith(
      sensors: null == sensors
          ? _value.sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<SensorNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SensorsApiResponseImplCopyWith<$Res>
    implements $SensorsApiResponseCopyWith<$Res> {
  factory _$$SensorsApiResponseImplCopyWith(_$SensorsApiResponseImpl value,
          $Res Function(_$SensorsApiResponseImpl) then) =
      __$$SensorsApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<SensorNetwork> sensors});
}

/// @nodoc
class __$$SensorsApiResponseImplCopyWithImpl<$Res>
    extends _$SensorsApiResponseCopyWithImpl<$Res, _$SensorsApiResponseImpl>
    implements _$$SensorsApiResponseImplCopyWith<$Res> {
  __$$SensorsApiResponseImplCopyWithImpl(_$SensorsApiResponseImpl _value,
      $Res Function(_$SensorsApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of SensorsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sensors = null,
  }) {
    return _then(_$SensorsApiResponseImpl(
      sensors: null == sensors
          ? _value._sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<SensorNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SensorsApiResponseImpl implements _SensorsApiResponse {
  const _$SensorsApiResponseImpl({required final List<SensorNetwork> sensors})
      : _sensors = sensors;

  factory _$SensorsApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$SensorsApiResponseImplFromJson(json);

  final List<SensorNetwork> _sensors;
  @override
  List<SensorNetwork> get sensors {
    if (_sensors is EqualUnmodifiableListView) return _sensors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_sensors);
  }

  @override
  String toString() {
    return 'SensorsApiResponse(sensors: $sensors)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SensorsApiResponseImpl &&
            const DeepCollectionEquality().equals(other._sensors, _sensors));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_sensors));

  /// Create a copy of SensorsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SensorsApiResponseImplCopyWith<_$SensorsApiResponseImpl> get copyWith =>
      __$$SensorsApiResponseImplCopyWithImpl<_$SensorsApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SensorsApiResponseImplToJson(
      this,
    );
  }
}

abstract class _SensorsApiResponse implements SensorsApiResponse {
  const factory _SensorsApiResponse(
      {required final List<SensorNetwork> sensors}) = _$SensorsApiResponseImpl;

  factory _SensorsApiResponse.fromJson(Map<String, dynamic> json) =
      _$SensorsApiResponseImpl.fromJson;

  @override
  List<SensorNetwork> get sensors;

  /// Create a copy of SensorsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SensorsApiResponseImplCopyWith<_$SensorsApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
