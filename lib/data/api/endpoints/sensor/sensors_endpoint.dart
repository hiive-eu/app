import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/sensor.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "sensors_endpoint.freezed.dart";
part "sensors_endpoint.g.dart";

///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// GET // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
SensorsEndpoint sensorsEndpoint(Ref ref) => SensorsEndpoint(ref);

class SensorsEndpoint extends Endpoint {
  static const String path = "/user/sensors";
  static const bool authRequired = true;

  late final Ref _ref;
  SensorsEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, SensorsApiResponse> get() {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(SensorsEndpoint.path, SensorsEndpoint.authRequired)
        .flatMap(_parseSensorsResponse);
  }

  TaskEither<ApiError, SensorsApiResponse> _parseSensorsResponse(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return SensorsApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        print(error);
        print(stacktrace);
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@freezed
class SensorsApiResponse with _$SensorsApiResponse {
  const factory SensorsApiResponse({
    required List<SensorNetwork> sensors,
  }) = _SensorsApiResponse;

  factory SensorsApiResponse.fromJson(Map<String, dynamic> json) =>
      _$SensorsApiResponseFromJson(json);
}
