// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'claim_sensor_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ClaimSensorApiResponse _$ClaimSensorApiResponseFromJson(
    Map<String, dynamic> json) {
  return _ClaimSensorApiResponse.fromJson(json);
}

/// @nodoc
mixin _$ClaimSensorApiResponse {
  HiveNetwork get hive => throw _privateConstructorUsedError;
  SensorNetwork get sensor => throw _privateConstructorUsedError;

  /// Serializes this ClaimSensorApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ClaimSensorApiResponseCopyWith<ClaimSensorApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ClaimSensorApiResponseCopyWith<$Res> {
  factory $ClaimSensorApiResponseCopyWith(ClaimSensorApiResponse value,
          $Res Function(ClaimSensorApiResponse) then) =
      _$ClaimSensorApiResponseCopyWithImpl<$Res, ClaimSensorApiResponse>;
  @useResult
  $Res call({HiveNetwork hive, SensorNetwork sensor});

  $HiveNetworkCopyWith<$Res> get hive;
  $SensorNetworkCopyWith<$Res> get sensor;
}

/// @nodoc
class _$ClaimSensorApiResponseCopyWithImpl<$Res,
        $Val extends ClaimSensorApiResponse>
    implements $ClaimSensorApiResponseCopyWith<$Res> {
  _$ClaimSensorApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hive = null,
    Object? sensor = null,
  }) {
    return _then(_value.copyWith(
      hive: null == hive
          ? _value.hive
          : hive // ignore: cast_nullable_to_non_nullable
              as HiveNetwork,
      sensor: null == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as SensorNetwork,
    ) as $Val);
  }

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $HiveNetworkCopyWith<$Res> get hive {
    return $HiveNetworkCopyWith<$Res>(_value.hive, (value) {
      return _then(_value.copyWith(hive: value) as $Val);
    });
  }

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $SensorNetworkCopyWith<$Res> get sensor {
    return $SensorNetworkCopyWith<$Res>(_value.sensor, (value) {
      return _then(_value.copyWith(sensor: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$ClaimSensorApiResponseImplCopyWith<$Res>
    implements $ClaimSensorApiResponseCopyWith<$Res> {
  factory _$$ClaimSensorApiResponseImplCopyWith(
          _$ClaimSensorApiResponseImpl value,
          $Res Function(_$ClaimSensorApiResponseImpl) then) =
      __$$ClaimSensorApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({HiveNetwork hive, SensorNetwork sensor});

  @override
  $HiveNetworkCopyWith<$Res> get hive;
  @override
  $SensorNetworkCopyWith<$Res> get sensor;
}

/// @nodoc
class __$$ClaimSensorApiResponseImplCopyWithImpl<$Res>
    extends _$ClaimSensorApiResponseCopyWithImpl<$Res,
        _$ClaimSensorApiResponseImpl>
    implements _$$ClaimSensorApiResponseImplCopyWith<$Res> {
  __$$ClaimSensorApiResponseImplCopyWithImpl(
      _$ClaimSensorApiResponseImpl _value,
      $Res Function(_$ClaimSensorApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hive = null,
    Object? sensor = null,
  }) {
    return _then(_$ClaimSensorApiResponseImpl(
      hive: null == hive
          ? _value.hive
          : hive // ignore: cast_nullable_to_non_nullable
              as HiveNetwork,
      sensor: null == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as SensorNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ClaimSensorApiResponseImpl implements _ClaimSensorApiResponse {
  const _$ClaimSensorApiResponseImpl(
      {required this.hive, required this.sensor});

  factory _$ClaimSensorApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$ClaimSensorApiResponseImplFromJson(json);

  @override
  final HiveNetwork hive;
  @override
  final SensorNetwork sensor;

  @override
  String toString() {
    return 'ClaimSensorApiResponse(hive: $hive, sensor: $sensor)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ClaimSensorApiResponseImpl &&
            (identical(other.hive, hive) || other.hive == hive) &&
            (identical(other.sensor, sensor) || other.sensor == sensor));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, hive, sensor);

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ClaimSensorApiResponseImplCopyWith<_$ClaimSensorApiResponseImpl>
      get copyWith => __$$ClaimSensorApiResponseImplCopyWithImpl<
          _$ClaimSensorApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ClaimSensorApiResponseImplToJson(
      this,
    );
  }
}

abstract class _ClaimSensorApiResponse implements ClaimSensorApiResponse {
  const factory _ClaimSensorApiResponse(
      {required final HiveNetwork hive,
      required final SensorNetwork sensor}) = _$ClaimSensorApiResponseImpl;

  factory _ClaimSensorApiResponse.fromJson(Map<String, dynamic> json) =
      _$ClaimSensorApiResponseImpl.fromJson;

  @override
  HiveNetwork get hive;
  @override
  SensorNetwork get sensor;

  /// Create a copy of ClaimSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ClaimSensorApiResponseImplCopyWith<_$ClaimSensorApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
