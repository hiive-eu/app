// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'update_sensor_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UpdateSensorApiResponse _$UpdateSensorApiResponseFromJson(
    Map<String, dynamic> json) {
  return _UpdateSensorApiResponse.fromJson(json);
}

/// @nodoc
mixin _$UpdateSensorApiResponse {
  SensorNetwork get sensor => throw _privateConstructorUsedError;

  /// Serializes this UpdateSensorApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of UpdateSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $UpdateSensorApiResponseCopyWith<UpdateSensorApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpdateSensorApiResponseCopyWith<$Res> {
  factory $UpdateSensorApiResponseCopyWith(UpdateSensorApiResponse value,
          $Res Function(UpdateSensorApiResponse) then) =
      _$UpdateSensorApiResponseCopyWithImpl<$Res, UpdateSensorApiResponse>;
  @useResult
  $Res call({SensorNetwork sensor});

  $SensorNetworkCopyWith<$Res> get sensor;
}

/// @nodoc
class _$UpdateSensorApiResponseCopyWithImpl<$Res,
        $Val extends UpdateSensorApiResponse>
    implements $UpdateSensorApiResponseCopyWith<$Res> {
  _$UpdateSensorApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of UpdateSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sensor = null,
  }) {
    return _then(_value.copyWith(
      sensor: null == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as SensorNetwork,
    ) as $Val);
  }

  /// Create a copy of UpdateSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $SensorNetworkCopyWith<$Res> get sensor {
    return $SensorNetworkCopyWith<$Res>(_value.sensor, (value) {
      return _then(_value.copyWith(sensor: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$UpdateSensorApiResponseImplCopyWith<$Res>
    implements $UpdateSensorApiResponseCopyWith<$Res> {
  factory _$$UpdateSensorApiResponseImplCopyWith(
          _$UpdateSensorApiResponseImpl value,
          $Res Function(_$UpdateSensorApiResponseImpl) then) =
      __$$UpdateSensorApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({SensorNetwork sensor});

  @override
  $SensorNetworkCopyWith<$Res> get sensor;
}

/// @nodoc
class __$$UpdateSensorApiResponseImplCopyWithImpl<$Res>
    extends _$UpdateSensorApiResponseCopyWithImpl<$Res,
        _$UpdateSensorApiResponseImpl>
    implements _$$UpdateSensorApiResponseImplCopyWith<$Res> {
  __$$UpdateSensorApiResponseImplCopyWithImpl(
      _$UpdateSensorApiResponseImpl _value,
      $Res Function(_$UpdateSensorApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of UpdateSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? sensor = null,
  }) {
    return _then(_$UpdateSensorApiResponseImpl(
      sensor: null == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as SensorNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$UpdateSensorApiResponseImpl implements _UpdateSensorApiResponse {
  const _$UpdateSensorApiResponseImpl({required this.sensor});

  factory _$UpdateSensorApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$UpdateSensorApiResponseImplFromJson(json);

  @override
  final SensorNetwork sensor;

  @override
  String toString() {
    return 'UpdateSensorApiResponse(sensor: $sensor)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateSensorApiResponseImpl &&
            (identical(other.sensor, sensor) || other.sensor == sensor));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, sensor);

  /// Create a copy of UpdateSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateSensorApiResponseImplCopyWith<_$UpdateSensorApiResponseImpl>
      get copyWith => __$$UpdateSensorApiResponseImplCopyWithImpl<
          _$UpdateSensorApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$UpdateSensorApiResponseImplToJson(
      this,
    );
  }
}

abstract class _UpdateSensorApiResponse implements UpdateSensorApiResponse {
  const factory _UpdateSensorApiResponse(
      {required final SensorNetwork sensor}) = _$UpdateSensorApiResponseImpl;

  factory _UpdateSensorApiResponse.fromJson(Map<String, dynamic> json) =
      _$UpdateSensorApiResponseImpl.fromJson;

  @override
  SensorNetwork get sensor;

  /// Create a copy of UpdateSensorApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UpdateSensorApiResponseImplCopyWith<_$UpdateSensorApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
