// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_sensor_endpoint.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$checkSensorEndpointHash() =>
    r'1ffe79dde5f5da2a5844fe7a25e256bb868faf0a'; /////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [checkSensorEndpoint].
@ProviderFor(checkSensorEndpoint)
final checkSensorEndpointProvider =
    AutoDisposeProvider<CheckSensorEndpoint>.internal(
  checkSensorEndpoint,
  name: r'checkSensorEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$checkSensorEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CheckSensorEndpointRef = AutoDisposeProviderRef<CheckSensorEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
