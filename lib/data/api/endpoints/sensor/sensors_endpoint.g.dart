// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensors_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SensorsApiResponseImpl _$$SensorsApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$SensorsApiResponseImpl(
      sensors: (json['sensors'] as List<dynamic>)
          .map((e) => SensorNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$SensorsApiResponseImplToJson(
        _$SensorsApiResponseImpl instance) =>
    <String, dynamic>{
      'sensors': instance.sensors,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sensorsEndpointHash() =>
    r'9d63f0cf80207d47c6214d8459f63a2f353fb8b5'; ///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [sensorsEndpoint].
@ProviderFor(sensorsEndpoint)
final sensorsEndpointProvider = AutoDisposeProvider<SensorsEndpoint>.internal(
  sensorsEndpoint,
  name: r'sensorsEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sensorsEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SensorsEndpointRef = AutoDisposeProviderRef<SensorsEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
