// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'claim_sensor_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ClaimSensorData _$ClaimSensorDataFromJson(Map<String, dynamic> json) =>
    ClaimSensorData(
      battery: (json['battery'] as num).toInt(),
      gateway: json['gateway'] as String,
      hive: json['hive'] as String,
      mac: json['mac'] as String,
      rssi: (json['rssi'] as num).toInt(),
    );

Map<String, dynamic> _$ClaimSensorDataToJson(ClaimSensorData instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'gateway': instance.gateway,
      'hive': instance.hive,
      'mac': instance.mac,
      'rssi': instance.rssi,
    };

_$ClaimSensorApiResponseImpl _$$ClaimSensorApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$ClaimSensorApiResponseImpl(
      hive: HiveNetwork.fromJson(json['hive'] as Map<String, dynamic>),
      sensor: SensorNetwork.fromJson(json['sensor'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$ClaimSensorApiResponseImplToJson(
        _$ClaimSensorApiResponseImpl instance) =>
    <String, dynamic>{
      'hive': instance.hive,
      'sensor': instance.sensor,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$claimSensorEndpointHash() =>
    r'a355a23d3ec13bcb2906dbd1512ffd1f45a3d3bc'; /////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [claimSensorEndpoint].
@ProviderFor(claimSensorEndpoint)
final claimSensorEndpointProvider =
    AutoDisposeProvider<ClaimSensorEndpoint>.internal(
  claimSensorEndpoint,
  name: r'claimSensorEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$claimSensorEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ClaimSensorEndpointRef = AutoDisposeProviderRef<ClaimSensorEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
