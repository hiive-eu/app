import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "check_sensor_endpoint.g.dart";

/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// CHECK // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
CheckSensorEndpoint checkSensorEndpoint(Ref ref) => CheckSensorEndpoint(ref);

class CheckSensorEndpoint extends Endpoint {
  String path(String mac) => "/user/sensor/$mac/check";
  static const bool authRequired = true;

  late final Ref _ref;
  CheckSensorEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> get(String mac) {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(this.path(mac), CheckSensorEndpoint.authRequired);
  }
}
