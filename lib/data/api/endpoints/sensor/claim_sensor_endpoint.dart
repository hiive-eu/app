import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/data/models/hive.dart";
import "package:hiive/data/models/sensor.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "claim_sensor_endpoint.freezed.dart";
part "claim_sensor_endpoint.g.dart";

/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// CLAIM // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
ClaimSensorEndpoint claimSensorEndpoint(Ref ref) => ClaimSensorEndpoint(ref);

class ClaimSensorEndpoint extends Endpoint {
  static const String path = "/user/sensor";
  static const bool authRequired = true;

  late final Ref _ref;
  ClaimSensorEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, ClaimSensorApiResponse> post(
      ClaimSensorData claimSensorData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .post(ClaimSensorEndpoint.path, ClaimSensorEndpoint.authRequired,
            claimSensorData.toJson())
        .flatMap(_parseClaimSensorResponse);
  }

  TaskEither<ApiError, ClaimSensorApiResponse> _parseClaimSensorResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return ClaimSensorApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@JsonSerializable()
class ClaimSensorData {
  int battery;
  String gateway;
  String hive;
  String mac;
  int rssi;

  ClaimSensorData(
      {required this.battery,
      required this.gateway,
      required this.hive,
      required this.mac,
      required this.rssi});

  factory ClaimSensorData.fromJson(Map<String, dynamic> json) =>
      _$ClaimSensorDataFromJson(json);

  Map<String, dynamic> toJson() => _$ClaimSensorDataToJson(this);
}

@freezed
class ClaimSensorApiResponse with _$ClaimSensorApiResponse {
  const factory ClaimSensorApiResponse({
    required HiveNetwork hive,
    required SensorNetwork sensor,
  }) = _ClaimSensorApiResponse;

  factory ClaimSensorApiResponse.fromJson(Map<String, dynamic> json) =>
      _$ClaimSensorApiResponseFromJson(json);
}
