import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/data/models/sensor.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "update_sensor_endpoint.freezed.dart";
part "update_sensor_endpoint.g.dart";

//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// UPDATE // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
UpdateSensorEndpoint updateSensorEndpoint(Ref ref) => UpdateSensorEndpoint(ref);

class UpdateSensorEndpoint extends Endpoint {
  String path(String mac) => "/user/sensor/$mac";
  static const bool authRequired = true;

  late final Ref _ref;
  UpdateSensorEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, UpdateSensorApiResponse> patch(
      String mac, UpdateSensorData updateSensorData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .patch(this.path(mac), UpdateSensorEndpoint.authRequired,
            updateSensorData.toJson())
        .flatMap(_parseUpdateSensorResponse);
  }

  TaskEither<ApiError, UpdateSensorApiResponse> _parseUpdateSensorResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return UpdateSensorApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@JsonSerializable()
class UpdateSensorData {
  String? gateway;

  UpdateSensorData({this.gateway});

  factory UpdateSensorData.fromJson(Map<String, dynamic> json) =>
      _$UpdateSensorDataFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateSensorDataToJson(this);
}

@freezed
class UpdateSensorApiResponse with _$UpdateSensorApiResponse {
  const factory UpdateSensorApiResponse({
    required SensorNetwork sensor,
  }) = _UpdateSensorApiResponse;

  factory UpdateSensorApiResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateSensorApiResponseFromJson(json);
}
