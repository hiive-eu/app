import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/hive.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "create_hive_endpoint.freezed.dart";
part "create_hive_endpoint.g.dart";

/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// CLAIM // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
CreateHiveEndpoint createHiveEndpoint(Ref ref) => CreateHiveEndpoint(ref);

class CreateHiveEndpoint extends Endpoint {
  static const String path = "/user/hive";
  static const bool authRequired = true;

  late final Ref _ref;
  CreateHiveEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, CreateHiveApiResponse> post(
      CreateHiveData createHiveData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .post(CreateHiveEndpoint.path, CreateHiveEndpoint.authRequired,
            createHiveData.toJson())
        .flatMap(_parseCreateHiveResponse);
  }

  TaskEither<ApiError, CreateHiveApiResponse> _parseCreateHiveResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return CreateHiveApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@JsonSerializable()
class CreateHiveData {
  String name;
  @JsonKey(name: "is_hiive")
  bool isHiive;

  CreateHiveData({required this.name, required this.isHiive});

  factory CreateHiveData.fromJson(Map<String, dynamic> json) =>
      _$CreateHiveDataFromJson(json);

  Map<String, dynamic> toJson() => _$CreateHiveDataToJson(this);
}

@freezed
class CreateHiveApiResponse with _$CreateHiveApiResponse {
  const factory CreateHiveApiResponse({
    required HiveNetwork hive,
  }) = _CreateHiveApiResponse;

  factory CreateHiveApiResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateHiveApiResponseFromJson(json);
}
