// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'hive_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

HiveApiResponse _$HiveApiResponseFromJson(Map<String, dynamic> json) {
  return _HiveApiResponse.fromJson(json);
}

/// @nodoc
mixin _$HiveApiResponse {
  HiveNetwork get hive => throw _privateConstructorUsedError;

  /// Serializes this HiveApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of HiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $HiveApiResponseCopyWith<HiveApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HiveApiResponseCopyWith<$Res> {
  factory $HiveApiResponseCopyWith(
          HiveApiResponse value, $Res Function(HiveApiResponse) then) =
      _$HiveApiResponseCopyWithImpl<$Res, HiveApiResponse>;
  @useResult
  $Res call({HiveNetwork hive});

  $HiveNetworkCopyWith<$Res> get hive;
}

/// @nodoc
class _$HiveApiResponseCopyWithImpl<$Res, $Val extends HiveApiResponse>
    implements $HiveApiResponseCopyWith<$Res> {
  _$HiveApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of HiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hive = null,
  }) {
    return _then(_value.copyWith(
      hive: null == hive
          ? _value.hive
          : hive // ignore: cast_nullable_to_non_nullable
              as HiveNetwork,
    ) as $Val);
  }

  /// Create a copy of HiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $HiveNetworkCopyWith<$Res> get hive {
    return $HiveNetworkCopyWith<$Res>(_value.hive, (value) {
      return _then(_value.copyWith(hive: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$HiveApiResponseImplCopyWith<$Res>
    implements $HiveApiResponseCopyWith<$Res> {
  factory _$$HiveApiResponseImplCopyWith(_$HiveApiResponseImpl value,
          $Res Function(_$HiveApiResponseImpl) then) =
      __$$HiveApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({HiveNetwork hive});

  @override
  $HiveNetworkCopyWith<$Res> get hive;
}

/// @nodoc
class __$$HiveApiResponseImplCopyWithImpl<$Res>
    extends _$HiveApiResponseCopyWithImpl<$Res, _$HiveApiResponseImpl>
    implements _$$HiveApiResponseImplCopyWith<$Res> {
  __$$HiveApiResponseImplCopyWithImpl(
      _$HiveApiResponseImpl _value, $Res Function(_$HiveApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of HiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hive = null,
  }) {
    return _then(_$HiveApiResponseImpl(
      hive: null == hive
          ? _value.hive
          : hive // ignore: cast_nullable_to_non_nullable
              as HiveNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HiveApiResponseImpl implements _HiveApiResponse {
  const _$HiveApiResponseImpl({required this.hive});

  factory _$HiveApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$HiveApiResponseImplFromJson(json);

  @override
  final HiveNetwork hive;

  @override
  String toString() {
    return 'HiveApiResponse(hive: $hive)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HiveApiResponseImpl &&
            (identical(other.hive, hive) || other.hive == hive));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, hive);

  /// Create a copy of HiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$HiveApiResponseImplCopyWith<_$HiveApiResponseImpl> get copyWith =>
      __$$HiveApiResponseImplCopyWithImpl<_$HiveApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HiveApiResponseImplToJson(
      this,
    );
  }
}

abstract class _HiveApiResponse implements HiveApiResponse {
  const factory _HiveApiResponse({required final HiveNetwork hive}) =
      _$HiveApiResponseImpl;

  factory _HiveApiResponse.fromJson(Map<String, dynamic> json) =
      _$HiveApiResponseImpl.fromJson;

  @override
  HiveNetwork get hive;

  /// Create a copy of HiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$HiveApiResponseImplCopyWith<_$HiveApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
