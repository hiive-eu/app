import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/hive.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "hive_endpoint.freezed.dart";
part "hive_endpoint.g.dart";

@riverpod
HiveEndpoint hiveEndpoint(Ref ref) => HiveEndpoint(ref);

class HiveEndpoint extends Endpoint {
  String path(String id) => "/user/hive/$id";
  static const bool authRequired = true;

  late final Ref _ref;
  HiveEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, HiveApiResponse> get(String id) {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(this.path(id), HiveEndpoint.authRequired)
        .flatMap(_parseHiveResponse);
  }

  TaskEither<ApiError, HiveApiResponse> _parseHiveResponse(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return HiveApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        print(error);
        print(stacktrace);
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@freezed
class HiveApiResponse with _$HiveApiResponse {
  const factory HiveApiResponse({
    required HiveNetwork hive,
  }) = _HiveApiResponse;

  factory HiveApiResponse.fromJson(Map<String, dynamic> json) =>
      _$HiveApiResponseFromJson(json);
}
