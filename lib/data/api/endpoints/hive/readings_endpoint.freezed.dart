// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'readings_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

ReadingsApiResponse _$ReadingsApiResponseFromJson(Map<String, dynamic> json) {
  return _ReadingsApiResponse.fromJson(json);
}

/// @nodoc
mixin _$ReadingsApiResponse {
  List<ReadingNetwork> get readings => throw _privateConstructorUsedError;

  /// Serializes this ReadingsApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of ReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadingsApiResponseCopyWith<ReadingsApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadingsApiResponseCopyWith<$Res> {
  factory $ReadingsApiResponseCopyWith(
          ReadingsApiResponse value, $Res Function(ReadingsApiResponse) then) =
      _$ReadingsApiResponseCopyWithImpl<$Res, ReadingsApiResponse>;
  @useResult
  $Res call({List<ReadingNetwork> readings});
}

/// @nodoc
class _$ReadingsApiResponseCopyWithImpl<$Res, $Val extends ReadingsApiResponse>
    implements $ReadingsApiResponseCopyWith<$Res> {
  _$ReadingsApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? readings = null,
  }) {
    return _then(_value.copyWith(
      readings: null == readings
          ? _value.readings
          : readings // ignore: cast_nullable_to_non_nullable
              as List<ReadingNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadingsApiResponseImplCopyWith<$Res>
    implements $ReadingsApiResponseCopyWith<$Res> {
  factory _$$ReadingsApiResponseImplCopyWith(_$ReadingsApiResponseImpl value,
          $Res Function(_$ReadingsApiResponseImpl) then) =
      __$$ReadingsApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<ReadingNetwork> readings});
}

/// @nodoc
class __$$ReadingsApiResponseImplCopyWithImpl<$Res>
    extends _$ReadingsApiResponseCopyWithImpl<$Res, _$ReadingsApiResponseImpl>
    implements _$$ReadingsApiResponseImplCopyWith<$Res> {
  __$$ReadingsApiResponseImplCopyWithImpl(_$ReadingsApiResponseImpl _value,
      $Res Function(_$ReadingsApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of ReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? readings = null,
  }) {
    return _then(_$ReadingsApiResponseImpl(
      readings: null == readings
          ? _value._readings
          : readings // ignore: cast_nullable_to_non_nullable
              as List<ReadingNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReadingsApiResponseImpl implements _ReadingsApiResponse {
  const _$ReadingsApiResponseImpl(
      {required final List<ReadingNetwork> readings})
      : _readings = readings;

  factory _$ReadingsApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadingsApiResponseImplFromJson(json);

  final List<ReadingNetwork> _readings;
  @override
  List<ReadingNetwork> get readings {
    if (_readings is EqualUnmodifiableListView) return _readings;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_readings);
  }

  @override
  String toString() {
    return 'ReadingsApiResponse(readings: $readings)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadingsApiResponseImpl &&
            const DeepCollectionEquality().equals(other._readings, _readings));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_readings));

  /// Create a copy of ReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadingsApiResponseImplCopyWith<_$ReadingsApiResponseImpl> get copyWith =>
      __$$ReadingsApiResponseImplCopyWithImpl<_$ReadingsApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadingsApiResponseImplToJson(
      this,
    );
  }
}

abstract class _ReadingsApiResponse implements ReadingsApiResponse {
  const factory _ReadingsApiResponse(
          {required final List<ReadingNetwork> readings}) =
      _$ReadingsApiResponseImpl;

  factory _ReadingsApiResponse.fromJson(Map<String, dynamic> json) =
      _$ReadingsApiResponseImpl.fromJson;

  @override
  List<ReadingNetwork> get readings;

  /// Create a copy of ReadingsApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadingsApiResponseImplCopyWith<_$ReadingsApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
