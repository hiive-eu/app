import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/reading.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "readings_endpoint.freezed.dart";
part "readings_endpoint.g.dart";

@riverpod
ReadingsEndpoint readingsEndpoint(Ref ref) => ReadingsEndpoint(ref);

class ReadingsEndpoint extends Endpoint {
  String path(String mac) => "/user/hive/$mac/readings";
  static const authRequired = true;

  late final Ref _ref;
  ReadingsEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, ReadingsApiResponse> get(String mac) {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(this.path(mac), ReadingsEndpoint.authRequired)
        .flatMap(_parseReadings);
  }

  TaskEither<ApiError, ReadingsApiResponse> _parseReadings(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(() => ReadingsApiResponse.fromJson(data!),
        (error, stacktrace) => const ApiError.unexpectedResponseData());
    return e.toTaskEither();
  }
}

@freezed
class ReadingsApiResponse with _$ReadingsApiResponse {
  const factory ReadingsApiResponse({
    required List<ReadingNetwork> readings,
  }) = _ReadingsApiResponse;

  factory ReadingsApiResponse.fromJson(Map<String, dynamic> json) =>
      _$ReadingsApiResponseFromJson(json);
}
