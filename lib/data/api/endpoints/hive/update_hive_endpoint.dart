import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/hive.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "update_hive_endpoint.freezed.dart";
part "update_hive_endpoint.g.dart";

//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// UPDATE // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
UpdateHiveEndpoint updateHiveEndpoint(Ref ref) => UpdateHiveEndpoint(ref);

class UpdateHiveEndpoint extends Endpoint {
  String path(String id) => "/user/hive/$id";
  static const bool authRequired = true;

  late final Ref _ref;
  UpdateHiveEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, UpdateHiveApiResponse> patch(
      String id, UpdateHiveData updateHiveData) {
    return this
        ._ref
        .read(httpServiceProvider)
        .patch(this.path(id), UpdateHiveEndpoint.authRequired,
            updateHiveData.toJson())
        .flatMap(_parseUpdateHiveResponse);
  }

  TaskEither<ApiError, UpdateHiveApiResponse> _parseUpdateHiveResponse(
    Map<String, dynamic>? data,
  ) {
    final e = Either.tryCatch(
      () {
        return UpdateHiveApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        return const ApiError.unexpectedResponseData();
      },
    );
    return e.toTaskEither();
  }
}

@JsonSerializable()
class UpdateHiveData {
  String? name;
  String? sensor;

  UpdateHiveData({this.name, this.sensor});

  factory UpdateHiveData.fromJson(Map<String, dynamic> json) =>
      _$UpdateHiveDataFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateHiveDataToJson(this);
}

@freezed
class UpdateHiveApiResponse with _$UpdateHiveApiResponse {
  const factory UpdateHiveApiResponse({
    required List<HiveNetwork> hives,
  }) = _UpdateHiveApiResponse;

  factory UpdateHiveApiResponse.fromJson(Map<String, dynamic> json) =>
      _$UpdateHiveApiResponseFromJson(json);
}
