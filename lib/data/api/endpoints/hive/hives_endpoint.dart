import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/hive.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "hives_endpoint.freezed.dart";
part "hives_endpoint.g.dart";

///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// GET // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
HivesEndpoint hivesEndpoint(Ref ref) => HivesEndpoint(ref);

class HivesEndpoint extends Endpoint {
  static const String path = "/user/hives";
  static const bool authRequired = true;

  late final Ref _ref;
  HivesEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, HivesApiResponse> get() {
    return this
        ._ref
        .read(httpServiceProvider)
        .get(HivesEndpoint.path, HivesEndpoint.authRequired)
        .flatMap(_parseHivesResponse);
  }

  TaskEither<ApiError, HivesApiResponse> _parseHivesResponse(
    Map<String, dynamic>? data,
  ) {
    return TaskEither.tryCatch(
      () async {
        return HivesApiResponse.fromJson(data!);
      },
      (error, stacktrace) {
        print(error);
        print(stacktrace);
        return const ApiError.unexpectedResponseData();
      },
    );
  }
}

@freezed
class HivesApiResponse with _$HivesApiResponse {
  const factory HivesApiResponse({
    required List<HiveNetwork> hives,
  }) = _HivesApiResponse;

  factory HivesApiResponse.fromJson(Map<String, dynamic> json) =>
      _$HivesApiResponseFromJson(json);
}
