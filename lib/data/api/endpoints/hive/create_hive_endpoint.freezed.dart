// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'create_hive_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

CreateHiveApiResponse _$CreateHiveApiResponseFromJson(
    Map<String, dynamic> json) {
  return _CreateHiveApiResponse.fromJson(json);
}

/// @nodoc
mixin _$CreateHiveApiResponse {
  HiveNetwork get hive => throw _privateConstructorUsedError;

  /// Serializes this CreateHiveApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of CreateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $CreateHiveApiResponseCopyWith<CreateHiveApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateHiveApiResponseCopyWith<$Res> {
  factory $CreateHiveApiResponseCopyWith(CreateHiveApiResponse value,
          $Res Function(CreateHiveApiResponse) then) =
      _$CreateHiveApiResponseCopyWithImpl<$Res, CreateHiveApiResponse>;
  @useResult
  $Res call({HiveNetwork hive});

  $HiveNetworkCopyWith<$Res> get hive;
}

/// @nodoc
class _$CreateHiveApiResponseCopyWithImpl<$Res,
        $Val extends CreateHiveApiResponse>
    implements $CreateHiveApiResponseCopyWith<$Res> {
  _$CreateHiveApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of CreateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hive = null,
  }) {
    return _then(_value.copyWith(
      hive: null == hive
          ? _value.hive
          : hive // ignore: cast_nullable_to_non_nullable
              as HiveNetwork,
    ) as $Val);
  }

  /// Create a copy of CreateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $HiveNetworkCopyWith<$Res> get hive {
    return $HiveNetworkCopyWith<$Res>(_value.hive, (value) {
      return _then(_value.copyWith(hive: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$CreateHiveApiResponseImplCopyWith<$Res>
    implements $CreateHiveApiResponseCopyWith<$Res> {
  factory _$$CreateHiveApiResponseImplCopyWith(
          _$CreateHiveApiResponseImpl value,
          $Res Function(_$CreateHiveApiResponseImpl) then) =
      __$$CreateHiveApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({HiveNetwork hive});

  @override
  $HiveNetworkCopyWith<$Res> get hive;
}

/// @nodoc
class __$$CreateHiveApiResponseImplCopyWithImpl<$Res>
    extends _$CreateHiveApiResponseCopyWithImpl<$Res,
        _$CreateHiveApiResponseImpl>
    implements _$$CreateHiveApiResponseImplCopyWith<$Res> {
  __$$CreateHiveApiResponseImplCopyWithImpl(_$CreateHiveApiResponseImpl _value,
      $Res Function(_$CreateHiveApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of CreateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hive = null,
  }) {
    return _then(_$CreateHiveApiResponseImpl(
      hive: null == hive
          ? _value.hive
          : hive // ignore: cast_nullable_to_non_nullable
              as HiveNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$CreateHiveApiResponseImpl implements _CreateHiveApiResponse {
  const _$CreateHiveApiResponseImpl({required this.hive});

  factory _$CreateHiveApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$CreateHiveApiResponseImplFromJson(json);

  @override
  final HiveNetwork hive;

  @override
  String toString() {
    return 'CreateHiveApiResponse(hive: $hive)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CreateHiveApiResponseImpl &&
            (identical(other.hive, hive) || other.hive == hive));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, hive);

  /// Create a copy of CreateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$CreateHiveApiResponseImplCopyWith<_$CreateHiveApiResponseImpl>
      get copyWith => __$$CreateHiveApiResponseImplCopyWithImpl<
          _$CreateHiveApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$CreateHiveApiResponseImplToJson(
      this,
    );
  }
}

abstract class _CreateHiveApiResponse implements CreateHiveApiResponse {
  const factory _CreateHiveApiResponse({required final HiveNetwork hive}) =
      _$CreateHiveApiResponseImpl;

  factory _CreateHiveApiResponse.fromJson(Map<String, dynamic> json) =
      _$CreateHiveApiResponseImpl.fromJson;

  @override
  HiveNetwork get hive;

  /// Create a copy of CreateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$CreateHiveApiResponseImplCopyWith<_$CreateHiveApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
