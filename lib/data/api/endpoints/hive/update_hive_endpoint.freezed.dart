// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'update_hive_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UpdateHiveApiResponse _$UpdateHiveApiResponseFromJson(
    Map<String, dynamic> json) {
  return _UpdateHiveApiResponse.fromJson(json);
}

/// @nodoc
mixin _$UpdateHiveApiResponse {
  List<HiveNetwork> get hives => throw _privateConstructorUsedError;

  /// Serializes this UpdateHiveApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of UpdateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $UpdateHiveApiResponseCopyWith<UpdateHiveApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UpdateHiveApiResponseCopyWith<$Res> {
  factory $UpdateHiveApiResponseCopyWith(UpdateHiveApiResponse value,
          $Res Function(UpdateHiveApiResponse) then) =
      _$UpdateHiveApiResponseCopyWithImpl<$Res, UpdateHiveApiResponse>;
  @useResult
  $Res call({List<HiveNetwork> hives});
}

/// @nodoc
class _$UpdateHiveApiResponseCopyWithImpl<$Res,
        $Val extends UpdateHiveApiResponse>
    implements $UpdateHiveApiResponseCopyWith<$Res> {
  _$UpdateHiveApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of UpdateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hives = null,
  }) {
    return _then(_value.copyWith(
      hives: null == hives
          ? _value.hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<HiveNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UpdateHiveApiResponseImplCopyWith<$Res>
    implements $UpdateHiveApiResponseCopyWith<$Res> {
  factory _$$UpdateHiveApiResponseImplCopyWith(
          _$UpdateHiveApiResponseImpl value,
          $Res Function(_$UpdateHiveApiResponseImpl) then) =
      __$$UpdateHiveApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<HiveNetwork> hives});
}

/// @nodoc
class __$$UpdateHiveApiResponseImplCopyWithImpl<$Res>
    extends _$UpdateHiveApiResponseCopyWithImpl<$Res,
        _$UpdateHiveApiResponseImpl>
    implements _$$UpdateHiveApiResponseImplCopyWith<$Res> {
  __$$UpdateHiveApiResponseImplCopyWithImpl(_$UpdateHiveApiResponseImpl _value,
      $Res Function(_$UpdateHiveApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of UpdateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hives = null,
  }) {
    return _then(_$UpdateHiveApiResponseImpl(
      hives: null == hives
          ? _value._hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<HiveNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$UpdateHiveApiResponseImpl implements _UpdateHiveApiResponse {
  const _$UpdateHiveApiResponseImpl({required final List<HiveNetwork> hives})
      : _hives = hives;

  factory _$UpdateHiveApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$UpdateHiveApiResponseImplFromJson(json);

  final List<HiveNetwork> _hives;
  @override
  List<HiveNetwork> get hives {
    if (_hives is EqualUnmodifiableListView) return _hives;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_hives);
  }

  @override
  String toString() {
    return 'UpdateHiveApiResponse(hives: $hives)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UpdateHiveApiResponseImpl &&
            const DeepCollectionEquality().equals(other._hives, _hives));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_hives));

  /// Create a copy of UpdateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UpdateHiveApiResponseImplCopyWith<_$UpdateHiveApiResponseImpl>
      get copyWith => __$$UpdateHiveApiResponseImplCopyWithImpl<
          _$UpdateHiveApiResponseImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$UpdateHiveApiResponseImplToJson(
      this,
    );
  }
}

abstract class _UpdateHiveApiResponse implements UpdateHiveApiResponse {
  const factory _UpdateHiveApiResponse(
      {required final List<HiveNetwork> hives}) = _$UpdateHiveApiResponseImpl;

  factory _UpdateHiveApiResponse.fromJson(Map<String, dynamic> json) =
      _$UpdateHiveApiResponseImpl.fromJson;

  @override
  List<HiveNetwork> get hives;

  /// Create a copy of UpdateHiveApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UpdateHiveApiResponseImplCopyWith<_$UpdateHiveApiResponseImpl>
      get copyWith => throw _privateConstructorUsedError;
}
