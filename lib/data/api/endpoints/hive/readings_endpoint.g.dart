// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'readings_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadingsApiResponseImpl _$$ReadingsApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$ReadingsApiResponseImpl(
      readings: (json['readings'] as List<dynamic>)
          .map((e) => ReadingNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$ReadingsApiResponseImplToJson(
        _$ReadingsApiResponseImpl instance) =>
    <String, dynamic>{
      'readings': instance.readings,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$readingsEndpointHash() => r'2e6b17df34760109a87b26baf311f4deff185961';

/// See also [readingsEndpoint].
@ProviderFor(readingsEndpoint)
final readingsEndpointProvider = AutoDisposeProvider<ReadingsEndpoint>.internal(
  readingsEndpoint,
  name: r'readingsEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$readingsEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ReadingsEndpointRef = AutoDisposeProviderRef<ReadingsEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
