// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hive_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$HiveApiResponseImpl _$$HiveApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$HiveApiResponseImpl(
      hive: HiveNetwork.fromJson(json['hive'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$HiveApiResponseImplToJson(
        _$HiveApiResponseImpl instance) =>
    <String, dynamic>{
      'hive': instance.hive,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$hiveEndpointHash() => r'6e30108806d1b6909a4ff8f324aa45489cc5ac51';

/// See also [hiveEndpoint].
@ProviderFor(hiveEndpoint)
final hiveEndpointProvider = AutoDisposeProvider<HiveEndpoint>.internal(
  hiveEndpoint,
  name: r'hiveEndpointProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$hiveEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef HiveEndpointRef = AutoDisposeProviderRef<HiveEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
