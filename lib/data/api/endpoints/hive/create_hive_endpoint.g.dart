// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_hive_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateHiveData _$CreateHiveDataFromJson(Map<String, dynamic> json) =>
    CreateHiveData(
      name: json['name'] as String,
      isHiive: json['is_hiive'] as bool,
    );

Map<String, dynamic> _$CreateHiveDataToJson(CreateHiveData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'is_hiive': instance.isHiive,
    };

_$CreateHiveApiResponseImpl _$$CreateHiveApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$CreateHiveApiResponseImpl(
      hive: HiveNetwork.fromJson(json['hive'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$CreateHiveApiResponseImplToJson(
        _$CreateHiveApiResponseImpl instance) =>
    <String, dynamic>{
      'hive': instance.hive,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$createHiveEndpointHash() =>
    r'8cf92b32ec6db2b5d25759d5b89d51c52ea181aa'; /////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [createHiveEndpoint].
@ProviderFor(createHiveEndpoint)
final createHiveEndpointProvider =
    AutoDisposeProvider<CreateHiveEndpoint>.internal(
  createHiveEndpoint,
  name: r'createHiveEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$createHiveEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CreateHiveEndpointRef = AutoDisposeProviderRef<CreateHiveEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
