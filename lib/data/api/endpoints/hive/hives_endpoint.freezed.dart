// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'hives_endpoint.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

HivesApiResponse _$HivesApiResponseFromJson(Map<String, dynamic> json) {
  return _HivesApiResponse.fromJson(json);
}

/// @nodoc
mixin _$HivesApiResponse {
  List<HiveNetwork> get hives => throw _privateConstructorUsedError;

  /// Serializes this HivesApiResponse to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of HivesApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $HivesApiResponseCopyWith<HivesApiResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HivesApiResponseCopyWith<$Res> {
  factory $HivesApiResponseCopyWith(
          HivesApiResponse value, $Res Function(HivesApiResponse) then) =
      _$HivesApiResponseCopyWithImpl<$Res, HivesApiResponse>;
  @useResult
  $Res call({List<HiveNetwork> hives});
}

/// @nodoc
class _$HivesApiResponseCopyWithImpl<$Res, $Val extends HivesApiResponse>
    implements $HivesApiResponseCopyWith<$Res> {
  _$HivesApiResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of HivesApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hives = null,
  }) {
    return _then(_value.copyWith(
      hives: null == hives
          ? _value.hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<HiveNetwork>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$HivesApiResponseImplCopyWith<$Res>
    implements $HivesApiResponseCopyWith<$Res> {
  factory _$$HivesApiResponseImplCopyWith(_$HivesApiResponseImpl value,
          $Res Function(_$HivesApiResponseImpl) then) =
      __$$HivesApiResponseImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<HiveNetwork> hives});
}

/// @nodoc
class __$$HivesApiResponseImplCopyWithImpl<$Res>
    extends _$HivesApiResponseCopyWithImpl<$Res, _$HivesApiResponseImpl>
    implements _$$HivesApiResponseImplCopyWith<$Res> {
  __$$HivesApiResponseImplCopyWithImpl(_$HivesApiResponseImpl _value,
      $Res Function(_$HivesApiResponseImpl) _then)
      : super(_value, _then);

  /// Create a copy of HivesApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hives = null,
  }) {
    return _then(_$HivesApiResponseImpl(
      hives: null == hives
          ? _value._hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<HiveNetwork>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HivesApiResponseImpl implements _HivesApiResponse {
  const _$HivesApiResponseImpl({required final List<HiveNetwork> hives})
      : _hives = hives;

  factory _$HivesApiResponseImpl.fromJson(Map<String, dynamic> json) =>
      _$$HivesApiResponseImplFromJson(json);

  final List<HiveNetwork> _hives;
  @override
  List<HiveNetwork> get hives {
    if (_hives is EqualUnmodifiableListView) return _hives;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_hives);
  }

  @override
  String toString() {
    return 'HivesApiResponse(hives: $hives)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HivesApiResponseImpl &&
            const DeepCollectionEquality().equals(other._hives, _hives));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_hives));

  /// Create a copy of HivesApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$HivesApiResponseImplCopyWith<_$HivesApiResponseImpl> get copyWith =>
      __$$HivesApiResponseImplCopyWithImpl<_$HivesApiResponseImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HivesApiResponseImplToJson(
      this,
    );
  }
}

abstract class _HivesApiResponse implements HivesApiResponse {
  const factory _HivesApiResponse({required final List<HiveNetwork> hives}) =
      _$HivesApiResponseImpl;

  factory _HivesApiResponse.fromJson(Map<String, dynamic> json) =
      _$HivesApiResponseImpl.fromJson;

  @override
  List<HiveNetwork> get hives;

  /// Create a copy of HivesApiResponse
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$HivesApiResponseImplCopyWith<_$HivesApiResponseImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
