// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_hive_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateHiveData _$UpdateHiveDataFromJson(Map<String, dynamic> json) =>
    UpdateHiveData(
      name: json['name'] as String?,
      sensor: json['sensor'] as String?,
    );

Map<String, dynamic> _$UpdateHiveDataToJson(UpdateHiveData instance) =>
    <String, dynamic>{
      'name': instance.name,
      'sensor': instance.sensor,
    };

_$UpdateHiveApiResponseImpl _$$UpdateHiveApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$UpdateHiveApiResponseImpl(
      hives: (json['hives'] as List<dynamic>)
          .map((e) => HiveNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$UpdateHiveApiResponseImplToJson(
        _$UpdateHiveApiResponseImpl instance) =>
    <String, dynamic>{
      'hives': instance.hives,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$updateHiveEndpointHash() =>
    r'ebbeab55c5dc1ff9ff947f6b8d9fe6f5ef4587aa'; //////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [updateHiveEndpoint].
@ProviderFor(updateHiveEndpoint)
final updateHiveEndpointProvider =
    AutoDisposeProvider<UpdateHiveEndpoint>.internal(
  updateHiveEndpoint,
  name: r'updateHiveEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$updateHiveEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UpdateHiveEndpointRef = AutoDisposeProviderRef<UpdateHiveEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
