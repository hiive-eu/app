// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_hive_endpoint.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$removeHiveEndpointHash() =>
    r'2b8b7a73dd0b9e67a8a2eaa10b18a829119f1ed7'; /////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [removeHiveEndpoint].
@ProviderFor(removeHiveEndpoint)
final removeHiveEndpointProvider =
    AutoDisposeProvider<RemoveHiveEndpoint>.internal(
  removeHiveEndpoint,
  name: r'removeHiveEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$removeHiveEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef RemoveHiveEndpointRef = AutoDisposeProviderRef<RemoveHiveEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
