import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "remove_hive_endpoint.g.dart";

/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// CHECK // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

@riverpod
RemoveHiveEndpoint removeHiveEndpoint(Ref ref) => RemoveHiveEndpoint(ref);

class RemoveHiveEndpoint extends Endpoint {
  String path(String id) => "/user/hive/$id";
  static const bool authRequired = true;

  late final Ref _ref;
  RemoveHiveEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, Unit> delete(String id) {
    return this
        ._ref
        .read(httpServiceProvider)
        .delete(this.path(id), RemoveHiveEndpoint.authRequired)
        .map((_) => unit);
  }
}
