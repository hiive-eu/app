// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hives_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$HivesApiResponseImpl _$$HivesApiResponseImplFromJson(
        Map<String, dynamic> json) =>
    _$HivesApiResponseImpl(
      hives: (json['hives'] as List<dynamic>)
          .map((e) => HiveNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$HivesApiResponseImplToJson(
        _$HivesApiResponseImpl instance) =>
    <String, dynamic>{
      'hives': instance.hives,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$hivesEndpointHash() =>
    r'04605ccfbd6a5dac95b8d58d0192cc02a6bb89ec'; ///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///
/// Copied from [hivesEndpoint].
@ProviderFor(hivesEndpoint)
final hivesEndpointProvider = AutoDisposeProvider<HivesEndpoint>.internal(
  hivesEndpoint,
  name: r'hivesEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$hivesEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef HivesEndpointRef = AutoDisposeProviderRef<HivesEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
