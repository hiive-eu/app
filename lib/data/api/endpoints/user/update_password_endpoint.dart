import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "update_password_endpoint.g.dart";

@riverpod
UpdatePasswordEndpoint updatePasswordEndpoint(Ref ref) =>
    UpdatePasswordEndpoint(ref);

class UpdatePasswordEndpoint extends Endpoint {
  static const String path = "/user/change_password";
  static const bool authRequired = true;

  late final Ref _ref;
  UpdatePasswordEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> post(UpdatePasswordData updatePasswordData) {
    return this._ref.read(httpServiceProvider).post(UpdatePasswordEndpoint.path,
        UpdatePasswordEndpoint.authRequired, updatePasswordData.toJson());
  }
}

@JsonSerializable()
class UpdatePasswordData {
  @JsonKey(name: "current_password")
  String currentPassword;
  @JsonKey(name: "new_password")
  String newPassword;
  @JsonKey(name: "new_password_confirm")
  String newPasswordConfirm;

  UpdatePasswordData(
      {required this.currentPassword,
      required this.newPassword,
      required this.newPasswordConfirm});

  factory UpdatePasswordData.fromJson(Map<String, dynamic> json) =>
      _$UpdatePasswordDataFromJson(json);

  Map<String, dynamic> toJson() => _$UpdatePasswordDataToJson(this);
}
