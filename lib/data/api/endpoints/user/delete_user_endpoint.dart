import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:hiive/data/api/endpoints/endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/services/http.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";

part "delete_user_endpoint.g.dart";

@riverpod
DeleteUserEndpoint deleteUserEndpoint(Ref ref) => DeleteUserEndpoint(ref);

class DeleteUserEndpoint extends Endpoint {
  static const String path = "/user";
  static const bool authRequired = true;

  late final Ref _ref;
  DeleteUserEndpoint(Ref ref) : this._ref = ref;

  TaskEither<ApiError, void> delete() {
    return this._ref.read(httpServiceProvider).delete(
          DeleteUserEndpoint.path,
          DeleteUserEndpoint.authRequired,
        );
  }
}
