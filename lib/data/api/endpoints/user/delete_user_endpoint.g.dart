// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'delete_user_endpoint.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$deleteUserEndpointHash() =>
    r'e77881176c945dc8b78d47548751a16d6d7a599d';

/// See also [deleteUserEndpoint].
@ProviderFor(deleteUserEndpoint)
final deleteUserEndpointProvider =
    AutoDisposeProvider<DeleteUserEndpoint>.internal(
  deleteUserEndpoint,
  name: r'deleteUserEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$deleteUserEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef DeleteUserEndpointRef = AutoDisposeProviderRef<DeleteUserEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
