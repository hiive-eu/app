// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_password_endpoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdatePasswordData _$UpdatePasswordDataFromJson(Map<String, dynamic> json) =>
    UpdatePasswordData(
      currentPassword: json['current_password'] as String,
      newPassword: json['new_password'] as String,
      newPasswordConfirm: json['new_password_confirm'] as String,
    );

Map<String, dynamic> _$UpdatePasswordDataToJson(UpdatePasswordData instance) =>
    <String, dynamic>{
      'current_password': instance.currentPassword,
      'new_password': instance.newPassword,
      'new_password_confirm': instance.newPasswordConfirm,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$updatePasswordEndpointHash() =>
    r'6dbd6593e48263d1785d74f4856423d27d607f42';

/// See also [updatePasswordEndpoint].
@ProviderFor(updatePasswordEndpoint)
final updatePasswordEndpointProvider =
    AutoDisposeProvider<UpdatePasswordEndpoint>.internal(
  updatePasswordEndpoint,
  name: r'updatePasswordEndpointProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$updatePasswordEndpointHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UpdatePasswordEndpointRef
    = AutoDisposeProviderRef<UpdatePasswordEndpoint>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
