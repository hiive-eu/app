import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import "package:riverpod_annotation/riverpod_annotation.dart";
import 'package:hiive/data/api/endpoints/authentication/forgot_password_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/refresh_tokens_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/reset_password_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/signup_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/signup_confirm_endpoint.dart';
import 'package:hiive/data/api/endpoints/dashboard/dashboard_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/login_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/logout_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/check_gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/claim_gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/gateway_readings_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/gateways_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/remove_gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/update_gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/readings_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/hives_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/remove_hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/update_hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/check_sensor_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/claim_sensor_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/sensors_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/update_sensor_endpoint.dart';
import 'package:hiive/data/api/endpoints/user/delete_user_endpoint.dart';
import 'package:hiive/data/api/endpoints/user/update_password_endpoint.dart';
import 'package:hiive/data/data_error.dart';

part "api.g.dart";

@riverpod
Api api(Ref ref) => Api(ref);

class Api {
  late final Ref _ref;
  Api(Ref ref) : this._ref = ref;

//////////////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// AUTHENTICATION // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // DONE
  TaskEither<ApiError, Unit> signup(SignupData signupData) {
    return this
        ._ref
        .read(signupEndpointProvider)
        .post(signupData)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, Unit> signupConfirm(
    SignupConfirmData signupConfirmData,
  ) {
    return this
        ._ref
        .read(signupConfirmEndpointProvider)
        .post(signupConfirmData)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, LoginApiResponse> login(LoginData loginData) {
    return this._ref.read(loginEndpointProvider).post(loginData);
  }

  // DONE
  TaskEither<ApiError, Unit> forgotPassword(
    ForgotPasswordData forgotPasswordData,
  ) {
    return this
        ._ref
        .read(forgotPasswordEndpointProvider)
        .post(forgotPasswordData)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, Unit> resetPassword(
    ResetPasswordData resetPasswordData,
  ) {
    return this
        ._ref
        .read(resetPasswordEndpointProvider)
        .post(resetPasswordData)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, RefreshTokensApiResponse> refreshTokens(
      RefreshTokensData refreshData) {
    return this._ref.read(refreshTokensEndpointProvider).post(refreshData);
  }

  // DONE
  TaskEither<ApiError, Unit> logout() {
    return this._ref.read(logoutEndpointProvider).get().map((_) => unit);
  }

/////////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// DASHBOARD // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
/////////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // DONE
  TaskEither<ApiError, DashboardApiResponse> getDashboard() {
    return this._ref.read(dashboardEndpointProvider).get();
  }

////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// HIVE // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // DONE
  TaskEither<ApiError, HiveApiResponse> getHive(String id) {
    return this._ref.read(hiveEndpointProvider).get(id);
  }

  // DONE
  TaskEither<ApiError, HivesApiResponse> getHives() {
    return this._ref.read(hivesEndpointProvider).get();
  }

  // DONE
  TaskEither<ApiError, CreateHiveApiResponse> createHive(
    CreateHiveData createHiveData,
  ) {
    return this._ref.read(createHiveEndpointProvider).post(createHiveData);
  }

  // DONE
  TaskEither<ApiError, Unit> removeHive(String id) {
    return this._ref.read(removeHiveEndpointProvider).delete(id);
  }

  // DONE
  TaskEither<ApiError, UpdateHiveApiResponse> updateHive(
    String id,
    UpdateHiveData updateHiveData,
  ) {
    return this._ref.read(updateHiveEndpointProvider).patch(id, updateHiveData);
  }

  TaskEither<ApiError, ReadingsApiResponse> getReadings(String id) {
    return this._ref.read(readingsEndpointProvider).get(id);
  }

///////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// GATEWAY // ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
///////////// ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // DONE
  TaskEither<ApiError, Unit> checkGateway(String mac) {
    return this
        ._ref
        .read(checkGatewayEndpointProvider)
        .get(mac)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, ClaimGatewayApiResponse> claimGateway(
    ClaimGatewayData claimGatewayData,
  ) {
    return this._ref.read(claimGatewayEndpointProvider).post(claimGatewayData);
  }

  // DONE
  TaskEither<ApiError, GatewayApiResponse> getGateway(String mac) {
    return this._ref.read(gatewayEndpointProvider).get(mac);
  }

  // DONE
  TaskEither<ApiError, GatewaysApiResponse> getGateways() {
    return this._ref.read(gatewaysEndpointProvider).get();
  }

  // DONE
  TaskEither<ApiError, RemoveGatewayApiResponse> removeGateway(String mac) {
    return this._ref.read(removeGatewayEndpointProvider).delete(mac);
  }

  // DONE
  TaskEither<ApiError, UpdateGatewayApiResponse> updateGateway(
    String mac,
    UpdateGatewayData updateGatewayData,
  ) {
    return this
        ._ref
        .read(updateGatewayEndpointProvider)
        .patch(mac, updateGatewayData);
  }

  TaskEither<ApiError, GatewayReadingsApiResponse> getGatewayReadings(
      String mac) {
    return this._ref.read(gatewayReadingsEndpointProvider).get(mac);
  }

//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// SENSOR // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // DONE
  TaskEither<ApiError, Unit> checkSensor(String mac) {
    return this
        ._ref
        .read(checkSensorEndpointProvider)
        .get(mac)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, ClaimSensorApiResponse> claimSensor(
    ClaimSensorData claimSensorData,
  ) {
    return this._ref.read(claimSensorEndpointProvider).post(claimSensorData);
  }

  // DONE
  TaskEither<ApiError, SensorsApiResponse> getSensors() {
    return this._ref.read(sensorsEndpointProvider).get();
  }

  // DONE
  TaskEither<ApiError, UpdateSensorApiResponse> updateSensor(
    String mac,
    UpdateSensorData updateSensorData,
  ) {
    return this
        ._ref
        .read(updateSensorEndpointProvider)
        .patch(mac, updateSensorData);
  }

////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
// USER // |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
////////// |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  // DONE
  TaskEither<ApiError, Unit> updatePassword(
    UpdatePasswordData updatePasswordData,
  ) {
    return this
        ._ref
        .read(updatePasswordEndpointProvider)
        .post(updatePasswordData)
        .map((_) => unit);
  }

  // DONE
  TaskEither<ApiError, Unit> deleteUser() {
    return this._ref.read(deleteUserEndpointProvider).delete().map((_) => unit);
  }
}
