import "package:freezed_annotation/freezed_annotation.dart";
import "package:flutter/foundation.dart";
import "package:hiive/data/api/endpoints/sensor/claim_sensor_endpoint.dart";
import "package:hiive/data/db/db.dart";

part "sensor.freezed.dart";
part "sensor.g.dart";

@freezed
class Sensor with _$Sensor {
  Sensor._();

  factory Sensor({
    required int battery,
    required String? gateway,
    required String mac,
    required int rssi,
    required SensorStatus status,
  }) = _Sensor;

  SensorStorage toSensorStorage() => SensorStorage(
        battery: this.battery,
        gateway: this.gateway,
        mac: this.mac,
        rssi: this.battery,
        status: this.status,
      );

  factory Sensor.fromClaimSensorData(ClaimSensorData data) => Sensor(
        battery: data.battery,
        gateway: data.gateway,
        mac: data.mac,
        rssi: data.rssi,
        status: SensorStatus.inactive,
      );

  factory Sensor.fromJson(Map<String, dynamic> json) => _$SensorFromJson(json);
}

@freezed
class SensorNetwork with _$SensorNetwork {
  const SensorNetwork._();

  const factory SensorNetwork({
    required int battery,
    required String? gateway,
    required int rssi,
    required String mac,
    @JsonKey(name: "status") required SensorStatus status,
  }) = _SensorNetwork;

  Sensor toSensor() => Sensor(
        battery: this.battery,
        gateway: this.gateway,
        mac: this.mac,
        rssi: this.rssi,
        status: this.status,
      );
  factory SensorNetwork.fromJson(Map<String, dynamic> json) =>
      _$SensorNetworkFromJson(json);
}

@freezed
class SensorStorage extends DbEntity with _$SensorStorage {
  SensorStorage._();

  factory SensorStorage({
    required int battery,
    required String? gateway,
    required String mac,
    required int rssi,
    @JsonKey(name: "status") required SensorStatus status,
  }) = _SensorStorage;

  Sensor toSensor() => Sensor(
        battery: this.battery,
        gateway: this.gateway,
        mac: this.mac,
        rssi: this.battery,
        status: this.status,
      );

  factory SensorStorage.fromJson(Map<String, dynamic> json) =>
      _$SensorStorageFromJson(json);
}

@freezed
class SensorGateway with _$SensorGateway {
  SensorGateway._();

  factory SensorGateway({
    required int number,
    required int battery,
    required String mac,
    required int rssi,
  }) = _SensorGateway;

  factory SensorGateway.fromGatewayString(String sensorString) {
    List<String> parts = sensorString.split(" ");
    return SensorGateway(
      number: int.parse(parts[3]),
      battery: int.parse(parts[1]),
      mac: parts[0],
      rssi: int.parse(parts[2]),
    );
  }

  Sensor toSensor() => Sensor(
        battery: this.battery,
        gateway: null,
        mac: this.mac,
        rssi: this.rssi,
        status: SensorStatus.inactive,
      );
}

enum SensorStatus {
  @JsonValue("active")
  active,
  @JsonValue("inactive")
  inactive
}
