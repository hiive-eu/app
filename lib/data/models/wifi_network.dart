import 'package:flutter/material.dart';

class WiFiNetwork {
  String ssid;
  int rssi;
  bool secure;

  WiFiNetwork({required this.ssid, required this.rssi, required this.secure});
  factory WiFiNetwork.fromGatewayString(String wifiNetworkString) {
    List<String> parts = wifiNetworkString.split("#");
    return WiFiNetwork(
        ssid: parts[0],
        rssi: int.parse(parts[1]),
        secure: int.parse(parts[2]) == 0 ? false : true);
  }
}

IconData? wifiIconFromRssi(int rssi) {
  if (rssi >= 0) {
    return null;
  } else if (rssi >= -40) {
    // Excellent signal strength
    return Icons.network_wifi;
  } else if (rssi >= -60) {
    // Good signal strength
    return Icons.network_wifi_3_bar;
  } else if (rssi >= -70) {
    // Fair signal strength
    return Icons.network_wifi_2_bar;
  } else if (rssi >= -80) {
    // Weak signal strength
    return Icons.network_wifi_1_bar;
  } else {
    // Very weak signal strength
    return Icons.signal_wifi_0_bar;
  }
}
