// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$DashboardNetworkImpl _$$DashboardNetworkImplFromJson(
        Map<String, dynamic> json) =>
    _$DashboardNetworkImpl(
      gateways: (json['gateways'] as List<dynamic>)
          .map((e) => GatewayNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
      hives: (json['hives'] as List<dynamic>)
          .map((e) => HiveNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
      sensors: (json['sensors'] as List<dynamic>)
          .map((e) => SensorNetwork.fromJson(e as Map<String, dynamic>))
          .toList(),
      user: UserNetwork.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$DashboardNetworkImplToJson(
        _$DashboardNetworkImpl instance) =>
    <String, dynamic>{
      'gateways': instance.gateways,
      'hives': instance.hives,
      'sensors': instance.sensors,
      'user': instance.user,
    };
