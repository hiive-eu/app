import "package:freezed_annotation/freezed_annotation.dart";

part "user.freezed.dart";
part "user.g.dart";

@freezed
class User with _$User {
  const factory User({
    required String id,
    required String displayName,
    required String email,
  }) = _User;

  factory User.fromJson(Map<String, Object?> json) => _$UserFromJson(json);
}

@freezed
class UserNetwork with _$UserNetwork {
  const UserNetwork._();

  const factory UserNetwork({
    required String id,
    @JsonKey(name: "displayname") required String displayName,
    required String email,
  }) = _UserNetwork;

  User toUser() => User(
        id: this.id,
        displayName: this.displayName,
        email: this.email,
      );

  factory UserNetwork.fromJson(Map<String, Object?> json) =>
      _$UserNetworkFromJson(json);
}
