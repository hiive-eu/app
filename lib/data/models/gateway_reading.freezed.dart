// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'gateway_reading.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

GatewayReading _$GatewayReadingFromJson(Map<String, dynamic> json) {
  return _GatewayReading.fromJson(json);
}

/// @nodoc
mixin _$GatewayReading {
  int get battery => throw _privateConstructorUsedError;
  int get temperature => throw _privateConstructorUsedError;
  int get humidity => throw _privateConstructorUsedError;
  DateTime get time => throw _privateConstructorUsedError;

  /// Serializes this GatewayReading to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayReading
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayReadingCopyWith<GatewayReading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayReadingCopyWith<$Res> {
  factory $GatewayReadingCopyWith(
          GatewayReading value, $Res Function(GatewayReading) then) =
      _$GatewayReadingCopyWithImpl<$Res, GatewayReading>;
  @useResult
  $Res call({int battery, int temperature, int humidity, DateTime time});
}

/// @nodoc
class _$GatewayReadingCopyWithImpl<$Res, $Val extends GatewayReading>
    implements $GatewayReadingCopyWith<$Res> {
  _$GatewayReadingCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayReading
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? time = null,
  }) {
    return _then(_value.copyWith(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewayReadingImplCopyWith<$Res>
    implements $GatewayReadingCopyWith<$Res> {
  factory _$$GatewayReadingImplCopyWith(_$GatewayReadingImpl value,
          $Res Function(_$GatewayReadingImpl) then) =
      __$$GatewayReadingImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int battery, int temperature, int humidity, DateTime time});
}

/// @nodoc
class __$$GatewayReadingImplCopyWithImpl<$Res>
    extends _$GatewayReadingCopyWithImpl<$Res, _$GatewayReadingImpl>
    implements _$$GatewayReadingImplCopyWith<$Res> {
  __$$GatewayReadingImplCopyWithImpl(
      _$GatewayReadingImpl _value, $Res Function(_$GatewayReadingImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayReading
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? time = null,
  }) {
    return _then(_$GatewayReadingImpl(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayReadingImpl extends _GatewayReading {
  _$GatewayReadingImpl(
      {required this.battery,
      required this.temperature,
      required this.humidity,
      required this.time})
      : super._();

  factory _$GatewayReadingImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayReadingImplFromJson(json);

  @override
  final int battery;
  @override
  final int temperature;
  @override
  final int humidity;
  @override
  final DateTime time;

  @override
  String toString() {
    return 'GatewayReading(battery: $battery, temperature: $temperature, humidity: $humidity, time: $time)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayReadingImpl &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.temperature, temperature) ||
                other.temperature == temperature) &&
            (identical(other.humidity, humidity) ||
                other.humidity == humidity) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, battery, temperature, humidity, time);

  /// Create a copy of GatewayReading
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayReadingImplCopyWith<_$GatewayReadingImpl> get copyWith =>
      __$$GatewayReadingImplCopyWithImpl<_$GatewayReadingImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayReadingImplToJson(
      this,
    );
  }
}

abstract class _GatewayReading extends GatewayReading {
  factory _GatewayReading(
      {required final int battery,
      required final int temperature,
      required final int humidity,
      required final DateTime time}) = _$GatewayReadingImpl;
  _GatewayReading._() : super._();

  factory _GatewayReading.fromJson(Map<String, dynamic> json) =
      _$GatewayReadingImpl.fromJson;

  @override
  int get battery;
  @override
  int get temperature;
  @override
  int get humidity;
  @override
  DateTime get time;

  /// Create a copy of GatewayReading
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayReadingImplCopyWith<_$GatewayReadingImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

GatewayReadingNetwork _$GatewayReadingNetworkFromJson(
    Map<String, dynamic> json) {
  return _GatewayReadingNetwork.fromJson(json);
}

/// @nodoc
mixin _$GatewayReadingNetwork {
  int get battery => throw _privateConstructorUsedError;
  int get temperature => throw _privateConstructorUsedError;
  int get humidity => throw _privateConstructorUsedError;
  DateTime get time => throw _privateConstructorUsedError;

  /// Serializes this GatewayReadingNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayReadingNetworkCopyWith<GatewayReadingNetwork> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayReadingNetworkCopyWith<$Res> {
  factory $GatewayReadingNetworkCopyWith(GatewayReadingNetwork value,
          $Res Function(GatewayReadingNetwork) then) =
      _$GatewayReadingNetworkCopyWithImpl<$Res, GatewayReadingNetwork>;
  @useResult
  $Res call({int battery, int temperature, int humidity, DateTime time});
}

/// @nodoc
class _$GatewayReadingNetworkCopyWithImpl<$Res,
        $Val extends GatewayReadingNetwork>
    implements $GatewayReadingNetworkCopyWith<$Res> {
  _$GatewayReadingNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? time = null,
  }) {
    return _then(_value.copyWith(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewayReadingNetworkImplCopyWith<$Res>
    implements $GatewayReadingNetworkCopyWith<$Res> {
  factory _$$GatewayReadingNetworkImplCopyWith(
          _$GatewayReadingNetworkImpl value,
          $Res Function(_$GatewayReadingNetworkImpl) then) =
      __$$GatewayReadingNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int battery, int temperature, int humidity, DateTime time});
}

/// @nodoc
class __$$GatewayReadingNetworkImplCopyWithImpl<$Res>
    extends _$GatewayReadingNetworkCopyWithImpl<$Res,
        _$GatewayReadingNetworkImpl>
    implements _$$GatewayReadingNetworkImplCopyWith<$Res> {
  __$$GatewayReadingNetworkImplCopyWithImpl(_$GatewayReadingNetworkImpl _value,
      $Res Function(_$GatewayReadingNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? time = null,
  }) {
    return _then(_$GatewayReadingNetworkImpl(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayReadingNetworkImpl extends _GatewayReadingNetwork {
  const _$GatewayReadingNetworkImpl(
      {required this.battery,
      required this.temperature,
      required this.humidity,
      required this.time})
      : super._();

  factory _$GatewayReadingNetworkImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayReadingNetworkImplFromJson(json);

  @override
  final int battery;
  @override
  final int temperature;
  @override
  final int humidity;
  @override
  final DateTime time;

  @override
  String toString() {
    return 'GatewayReadingNetwork(battery: $battery, temperature: $temperature, humidity: $humidity, time: $time)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayReadingNetworkImpl &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.temperature, temperature) ||
                other.temperature == temperature) &&
            (identical(other.humidity, humidity) ||
                other.humidity == humidity) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, battery, temperature, humidity, time);

  /// Create a copy of GatewayReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayReadingNetworkImplCopyWith<_$GatewayReadingNetworkImpl>
      get copyWith => __$$GatewayReadingNetworkImplCopyWithImpl<
          _$GatewayReadingNetworkImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayReadingNetworkImplToJson(
      this,
    );
  }
}

abstract class _GatewayReadingNetwork extends GatewayReadingNetwork {
  const factory _GatewayReadingNetwork(
      {required final int battery,
      required final int temperature,
      required final int humidity,
      required final DateTime time}) = _$GatewayReadingNetworkImpl;
  const _GatewayReadingNetwork._() : super._();

  factory _GatewayReadingNetwork.fromJson(Map<String, dynamic> json) =
      _$GatewayReadingNetworkImpl.fromJson;

  @override
  int get battery;
  @override
  int get temperature;
  @override
  int get humidity;
  @override
  DateTime get time;

  /// Create a copy of GatewayReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayReadingNetworkImplCopyWith<_$GatewayReadingNetworkImpl>
      get copyWith => throw _privateConstructorUsedError;
}

GatewayReadingStorage _$GatewayReadingStorageFromJson(
    Map<String, dynamic> json) {
  return _GatewayReadingStorage.fromJson(json);
}

/// @nodoc
mixin _$GatewayReadingStorage {
  int get battery => throw _privateConstructorUsedError;
  int get temperature => throw _privateConstructorUsedError;
  int get humidity => throw _privateConstructorUsedError;
  DateTime get time => throw _privateConstructorUsedError;

  /// Serializes this GatewayReadingStorage to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayReadingStorageCopyWith<GatewayReadingStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayReadingStorageCopyWith<$Res> {
  factory $GatewayReadingStorageCopyWith(GatewayReadingStorage value,
          $Res Function(GatewayReadingStorage) then) =
      _$GatewayReadingStorageCopyWithImpl<$Res, GatewayReadingStorage>;
  @useResult
  $Res call({int battery, int temperature, int humidity, DateTime time});
}

/// @nodoc
class _$GatewayReadingStorageCopyWithImpl<$Res,
        $Val extends GatewayReadingStorage>
    implements $GatewayReadingStorageCopyWith<$Res> {
  _$GatewayReadingStorageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? time = null,
  }) {
    return _then(_value.copyWith(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewayReadingStorageImplCopyWith<$Res>
    implements $GatewayReadingStorageCopyWith<$Res> {
  factory _$$GatewayReadingStorageImplCopyWith(
          _$GatewayReadingStorageImpl value,
          $Res Function(_$GatewayReadingStorageImpl) then) =
      __$$GatewayReadingStorageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int battery, int temperature, int humidity, DateTime time});
}

/// @nodoc
class __$$GatewayReadingStorageImplCopyWithImpl<$Res>
    extends _$GatewayReadingStorageCopyWithImpl<$Res,
        _$GatewayReadingStorageImpl>
    implements _$$GatewayReadingStorageImplCopyWith<$Res> {
  __$$GatewayReadingStorageImplCopyWithImpl(_$GatewayReadingStorageImpl _value,
      $Res Function(_$GatewayReadingStorageImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? time = null,
  }) {
    return _then(_$GatewayReadingStorageImpl(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayReadingStorageImpl extends _GatewayReadingStorage {
  _$GatewayReadingStorageImpl(
      {required this.battery,
      required this.temperature,
      required this.humidity,
      required this.time})
      : super._();

  factory _$GatewayReadingStorageImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayReadingStorageImplFromJson(json);

  @override
  final int battery;
  @override
  final int temperature;
  @override
  final int humidity;
  @override
  final DateTime time;

  @override
  String toString() {
    return 'GatewayReadingStorage(battery: $battery, temperature: $temperature, humidity: $humidity, time: $time)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayReadingStorageImpl &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.temperature, temperature) ||
                other.temperature == temperature) &&
            (identical(other.humidity, humidity) ||
                other.humidity == humidity) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, battery, temperature, humidity, time);

  /// Create a copy of GatewayReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayReadingStorageImplCopyWith<_$GatewayReadingStorageImpl>
      get copyWith => __$$GatewayReadingStorageImplCopyWithImpl<
          _$GatewayReadingStorageImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayReadingStorageImplToJson(
      this,
    );
  }
}

abstract class _GatewayReadingStorage extends GatewayReadingStorage {
  factory _GatewayReadingStorage(
      {required final int battery,
      required final int temperature,
      required final int humidity,
      required final DateTime time}) = _$GatewayReadingStorageImpl;
  _GatewayReadingStorage._() : super._();

  factory _GatewayReadingStorage.fromJson(Map<String, dynamic> json) =
      _$GatewayReadingStorageImpl.fromJson;

  @override
  int get battery;
  @override
  int get temperature;
  @override
  int get humidity;
  @override
  DateTime get time;

  /// Create a copy of GatewayReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayReadingStorageImplCopyWith<_$GatewayReadingStorageImpl>
      get copyWith => throw _privateConstructorUsedError;
}
