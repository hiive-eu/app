import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/db/db.dart";

part "gateway_reading.freezed.dart";
part "gateway_reading.g.dart";

@freezed
class GatewayReading with _$GatewayReading {
  GatewayReading._();
  factory GatewayReading({
    required int battery,
    required int temperature,
    required int humidity,
    required DateTime time,
  }) = _GatewayReading;

  GatewayReadingStorage toGatewayReadingStorage() => GatewayReadingStorage(
        battery: this.battery,
        temperature: this.temperature,
        humidity: this.humidity,
        time: this.time,
      );

  factory GatewayReading.fromJson(Map<String, dynamic> json) =>
      _$GatewayReadingFromJson(json);
}

@freezed
class GatewayReadingNetwork with _$GatewayReadingNetwork {
  const GatewayReadingNetwork._();

  const factory GatewayReadingNetwork({
    required int battery,
    required int temperature,
    required int humidity,
    required DateTime time,
  }) = _GatewayReadingNetwork;

  GatewayReading toGatewayReading() => GatewayReading(
        battery: this.battery,
        temperature: this.temperature,
        humidity: this.humidity,
        time: this.time,
      );
  factory GatewayReadingNetwork.fromJson(Map<String, dynamic> json) =>
      _$GatewayReadingNetworkFromJson(json);
}

@freezed
class GatewayReadingStorage extends DbEntity with _$GatewayReadingStorage {
  GatewayReadingStorage._();

  factory GatewayReadingStorage({
    required int battery,
    required int temperature,
    required int humidity,
    required DateTime time,
  }) = _GatewayReadingStorage;

  GatewayReading toGatewayReading() => GatewayReading(
        battery: this.battery,
        temperature: this.temperature,
        humidity: this.humidity,
        time: this.time,
      );
  factory GatewayReadingStorage.fromJson(Map<String, dynamic> json) =>
      _$GatewayReadingStorageFromJson(json);
}
