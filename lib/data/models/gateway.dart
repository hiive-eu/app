import "package:freezed_annotation/freezed_annotation.dart";
import "package:flutter/foundation.dart";
import "package:hiive/data/api/endpoints/gateway/claim_gateway_endpoint.dart";
import "package:hiive/data/db/db.dart";

import "package:hiive/data/models/gateway_reading.dart";

part "gateway.freezed.dart";
part "gateway.g.dart";

@freezed
class Gateway with _$Gateway {
  Gateway._();
  factory Gateway({
    required GatewayReading? lastReading,
    required String mac,
    required String name,
    required int? sdSpaceUsed,
    required int? sdStatus,
    required GatewayStatus status,
    required String? token,
    required int? wifiRssi,
  }) = _Gateway;

  GatewayStorage toGatewayStorage() => GatewayStorage(
        lastReading: this.lastReading?.toGatewayReadingStorage(),
        mac: this.mac,
        name: this.name,
        sdSpaceUsed: this.sdSpaceUsed,
        sdStatus: this.sdStatus,
        status: this.status,
        token: this.token,
        wifiRssi: this.wifiRssi,
      );

  factory Gateway.fromClaimGatewayData(ClaimGatewayData data) => Gateway(
        lastReading: null,
        mac: data.mac,
        name: data.name,
        sdSpaceUsed: null,
        sdStatus: null,
        status: GatewayStatus.inactive,
        token: null,
        wifiRssi: null,
      );

  factory Gateway.fromJson(Map<String, dynamic> json) =>
      _$GatewayFromJson(json);
}

@freezed
class GatewayNetwork with _$GatewayNetwork {
  const GatewayNetwork._();
  const factory GatewayNetwork({
    @JsonKey(name: "last_reading") required GatewayReading? lastReading,
    required String mac,
    required String name,
    @JsonKey(name: "sd_space_used") required int? sdSpaceUsed,
    @JsonKey(name: "sd_status") required int? sdStatus,
    required GatewayStatus status,
    required String token,
    @JsonKey(name: "wifi_rssi") required int? wifiRssi,
  }) = _GatewayNetwork;

  Gateway toGateway() => Gateway(
        lastReading: this.lastReading,
        mac: this.mac,
        name: this.name,
        sdSpaceUsed: this.sdSpaceUsed,
        sdStatus: this.sdStatus,
        status: this.status,
        token: this.token,
        wifiRssi: this.wifiRssi,
      );
  factory GatewayNetwork.fromJson(Map<String, dynamic> json) =>
      _$GatewayNetworkFromJson(json);
}

@freezed
class GatewayStorage extends DbEntity with _$GatewayStorage {
  GatewayStorage._();

  factory GatewayStorage({
    @JsonKey(name: "last_reading") required GatewayReadingStorage? lastReading,
    required String mac,
    required String name,
    @JsonKey(name: "sd_space_used") required int? sdSpaceUsed,
    @JsonKey(name: "sd_status") required int? sdStatus,
    required GatewayStatus status,
    required String? token,
    @JsonKey(name: "wifi_rssi") required int? wifiRssi,
  }) = _GatewayStorage;

  Gateway toGateway() => Gateway(
        lastReading: this.lastReading?.toGatewayReading(),
        mac: this.mac,
        name: this.name,
        sdSpaceUsed: this.sdSpaceUsed,
        sdStatus: this.sdStatus,
        status: this.status,
        token: this.token,
        wifiRssi: this.wifiRssi,
      );
  factory GatewayStorage.fromJson(Map<String, dynamic> json) =>
      _$GatewayStorageFromJson(json);
}

enum GatewayStatus {
  @JsonValue("active")
  active,
  @JsonValue("inactive")
  inactive
}
