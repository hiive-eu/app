import "package:freezed_annotation/freezed_annotation.dart";
import "package:flutter/foundation.dart";
import "package:hiive/data/db/db.dart";

part "reading.freezed.dart";
part "reading.g.dart";

@freezed
class Reading with _$Reading {
  Reading._();

  factory Reading({
    required List<int> frequencyBins,
    required int temperature,
    required int humidity,
    required int soundLevel,
    required DateTime time,
  }) = _Reading;

  ReadingStorage toReadingStorage() => ReadingStorage(
        frequencyBins: this.frequencyBins,
        temperature: this.temperature,
        humidity: this.humidity,
        soundLevel: this.soundLevel,
        time: this.time,
      );

  factory Reading.fromJson(Map<String, dynamic> json) =>
      _$ReadingFromJson(json);
}

@freezed
class ReadingNetwork with _$ReadingNetwork {
  ReadingNetwork._();

  factory ReadingNetwork({
    @JsonKey(name: 'frequency_bins') required List<int> frequencyBins,
    required int temperature,
    required int humidity,
    @JsonKey(name: 'sound_level') required int soundLevel,
    required DateTime time,
  }) = _ReadingNetwork;

  Reading toReading() => Reading(
        frequencyBins: this.frequencyBins,
        temperature: this.temperature,
        humidity: this.humidity,
        soundLevel: this.soundLevel,
        time: this.time,
      );
  factory ReadingNetwork.fromJson(Map<String, dynamic> json) =>
      _$ReadingNetworkFromJson(json);
}

@freezed
class ReadingStorage extends DbEntity with _$ReadingStorage {
  ReadingStorage._();

  factory ReadingStorage({
    @JsonKey(name: 'frequency_bins') required List<int> frequencyBins,
    required int temperature,
    required int humidity,
    @JsonKey(name: 'sound_level') required int soundLevel,
    required DateTime time,
  }) = _ReadingStorage;

  Reading toReading() => Reading(
        frequencyBins: this.frequencyBins,
        temperature: this.temperature,
        humidity: this.humidity,
        soundLevel: this.soundLevel,
        time: this.time,
      );
  factory ReadingStorage.fromJson(Map<String, dynamic> json) =>
      _$ReadingStorageFromJson(json);
}
