// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hive.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$HiveImpl _$$HiveImplFromJson(Map<String, dynamic> json) => _$HiveImpl(
      id: json['id'] as String,
      isHiive: json['isHiive'] as bool,
      lastReading: json['lastReading'] == null
          ? null
          : Reading.fromJson(json['lastReading'] as Map<String, dynamic>),
      name: json['name'] as String,
      sensor: json['sensor'] as String?,
    );

Map<String, dynamic> _$$HiveImplToJson(_$HiveImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'isHiive': instance.isHiive,
      'lastReading': instance.lastReading,
      'name': instance.name,
      'sensor': instance.sensor,
    };

_$HiveNetworkImpl _$$HiveNetworkImplFromJson(Map<String, dynamic> json) =>
    _$HiveNetworkImpl(
      id: json['id'] as String,
      isHiive: json['is_hiive'] as bool,
      lastReading: json['last_reading'] == null
          ? null
          : ReadingNetwork.fromJson(
              json['last_reading'] as Map<String, dynamic>),
      name: json['name'] as String,
      sensor: json['sensor'] as String?,
    );

Map<String, dynamic> _$$HiveNetworkImplToJson(_$HiveNetworkImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'is_hiive': instance.isHiive,
      'last_reading': instance.lastReading,
      'name': instance.name,
      'sensor': instance.sensor,
    };

_$HiveStorageImpl _$$HiveStorageImplFromJson(Map<String, dynamic> json) =>
    _$HiveStorageImpl(
      id: json['id'] as String,
      isHiive: json['is_hiive'] as bool,
      lastReading: json['last_reading'] == null
          ? null
          : ReadingStorage.fromJson(
              json['last_reading'] as Map<String, dynamic>),
      name: json['name'] as String,
      sensor: json['sensor'] as String?,
    );

Map<String, dynamic> _$$HiveStorageImplToJson(_$HiveStorageImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'is_hiive': instance.isHiive,
      'last_reading': instance.lastReading,
      'name': instance.name,
      'sensor': instance.sensor,
    };
