import "dart:convert";

import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/utils/misc.dart";

part "authentication_tokens.freezed.dart";
part "authentication_tokens.g.dart";

@freezed
class AuthenticationTokens with _$AuthenticationTokens {
  const factory AuthenticationTokens({
    required AccessToken accessToken,
    required RefreshToken refreshToken,
  }) = _AuthenticationTokens;
}

@freezed
class AuthenticationTokensNetwork with _$AuthenticationTokensNetwork {
  const AuthenticationTokensNetwork._();

  const factory AuthenticationTokensNetwork({
    @JsonKey(name: "access_token") required String accessToken,
    @JsonKey(name: "refresh_token") required String refreshToken,
  }) = _AuthenticationTokensNetwork;

  AuthenticationTokens toAuthenticationTokens() => AuthenticationTokens(
      accessToken: AccessToken(this.accessToken),
      refreshToken: RefreshToken(this.refreshToken));

  factory AuthenticationTokensNetwork.fromJson(Map<String, Object?> json) =>
      _$AuthenticationTokensNetworkFromJson(json);
}

class AccessToken {
  final String token;
  late final AccessTokenData data;

  bool isValid() =>
      DateTime.now().toUtc().isBefore(
            this.data.exp.subtract(
                  const Duration(
                    seconds: 10,
                  ),
                ),
          ) &&
      DateTime.now().toUtc().isAfter(this.data.nbf);

  AccessToken(this.token) {
    this.data = AccessTokenData.fromJson(json
        .decode(utf8.decode(base64.decode(b64pad(this.token.split('.')[1])))));
  }
}

@JsonSerializable()
class AccessTokenData {
  @JsonKey(fromJson: datetimeFromUnixTimestamp)
  DateTime iat, exp, nbf;
  @JsonKey(name: "user_id")
  String userId;
  @JsonKey(name: "is_admin")
  bool isAdmin;

  AccessTokenData({
    required this.iat,
    required this.exp,
    required this.nbf,
    required this.userId,
    required this.isAdmin,
  });

  factory AccessTokenData.fromJson(Map<String, dynamic> json) =>
      _$AccessTokenDataFromJson(json);

  Map<String, dynamic> toJson() => _$AccessTokenDataToJson(this);
}

class RefreshToken {
  final String token;
  late final RefreshTokenData data;

  bool isValid() =>
      DateTime.now().toUtc().isBefore(
            this.data.exp.subtract(
                  const Duration(
                    seconds: 10,
                  ),
                ),
          ) &&
      DateTime.now().toUtc().isAfter(this.data.nbf);

  RefreshToken(this.token) {
    this.data = RefreshTokenData.fromJson(json
        .decode(utf8.decode(base64.decode(b64pad(this.token.split('.')[1])))));
  }
}

@JsonSerializable()
class RefreshTokenData {
  @JsonKey(fromJson: datetimeFromUnixTimestamp)
  DateTime iat, exp, nbf;
  @JsonKey(name: "user_id")
  String userId;

  RefreshTokenData({
    required this.iat,
    required this.exp,
    required this.nbf,
    required this.userId,
  });

  factory RefreshTokenData.fromJson(Map<String, dynamic> json) =>
      _$RefreshTokenDataFromJson(json);

  Map<String, dynamic> toJson() => _$RefreshTokenDataToJson(this);
}
