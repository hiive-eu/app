// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authentication_tokens.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccessTokenData _$AccessTokenDataFromJson(Map<String, dynamic> json) =>
    AccessTokenData(
      iat: datetimeFromUnixTimestamp((json['iat'] as num).toInt()),
      exp: datetimeFromUnixTimestamp((json['exp'] as num).toInt()),
      nbf: datetimeFromUnixTimestamp((json['nbf'] as num).toInt()),
      userId: json['user_id'] as String,
      isAdmin: json['is_admin'] as bool,
    );

Map<String, dynamic> _$AccessTokenDataToJson(AccessTokenData instance) =>
    <String, dynamic>{
      'iat': instance.iat.toIso8601String(),
      'exp': instance.exp.toIso8601String(),
      'nbf': instance.nbf.toIso8601String(),
      'user_id': instance.userId,
      'is_admin': instance.isAdmin,
    };

RefreshTokenData _$RefreshTokenDataFromJson(Map<String, dynamic> json) =>
    RefreshTokenData(
      iat: datetimeFromUnixTimestamp((json['iat'] as num).toInt()),
      exp: datetimeFromUnixTimestamp((json['exp'] as num).toInt()),
      nbf: datetimeFromUnixTimestamp((json['nbf'] as num).toInt()),
      userId: json['user_id'] as String,
    );

Map<String, dynamic> _$RefreshTokenDataToJson(RefreshTokenData instance) =>
    <String, dynamic>{
      'iat': instance.iat.toIso8601String(),
      'exp': instance.exp.toIso8601String(),
      'nbf': instance.nbf.toIso8601String(),
      'user_id': instance.userId,
    };

_$AuthenticationTokensNetworkImpl _$$AuthenticationTokensNetworkImplFromJson(
        Map<String, dynamic> json) =>
    _$AuthenticationTokensNetworkImpl(
      accessToken: json['access_token'] as String,
      refreshToken: json['refresh_token'] as String,
    );

Map<String, dynamic> _$$AuthenticationTokensNetworkImplToJson(
        _$AuthenticationTokensNetworkImpl instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'refresh_token': instance.refreshToken,
    };
