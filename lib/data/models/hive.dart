import "package:freezed_annotation/freezed_annotation.dart";
import "package:hiive/data/db/db.dart";
import 'package:uuid/uuid.dart';
import "package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart";

import "package:hiive/data/models/reading.dart";

part "hive.freezed.dart";
part "hive.g.dart";

@freezed
class Hive with _$Hive {
  Hive._();

  factory Hive({
    required String id,
    required bool isHiive,
    required Reading? lastReading,
    required String name,
    required String? sensor,
  }) = _Hive;

  HiveStorage toHiveStorage() => HiveStorage(
        id: this.id,
        isHiive: this.isHiive,
        lastReading: this.lastReading?.toReadingStorage(),
        name: this.name,
        sensor: this.sensor,
      );

  factory Hive.fromCreateHiveData(CreateHiveData data) => Hive(
        id: Uuid().v4(),
        isHiive: data.isHiive,
        lastReading: null,
        name: data.name,
        sensor: null,
      );

  factory Hive.fromJson(Map<String, dynamic> json) => _$HiveFromJson(json);
}

@freezed
class HiveNetwork with _$HiveNetwork {
  const HiveNetwork._();

  const factory HiveNetwork({
    required String id,
    @JsonKey(name: "is_hiive") required bool isHiive,
    @JsonKey(name: "last_reading") required ReadingNetwork? lastReading,
    required String name,
    required String? sensor,
  }) = _HiveNetwork;

  Hive toHive() => Hive(
        id: this.id,
        isHiive: this.isHiive,
        lastReading: this.lastReading?.toReading(),
        name: this.name,
        sensor: this.sensor,
      );
  factory HiveNetwork.fromJson(Map<String, dynamic> json) =>
      _$HiveNetworkFromJson(json);
}

@freezed
class HiveStorage extends DbEntity with _$HiveStorage {
  HiveStorage._();

  factory HiveStorage({
    required String id,
    @JsonKey(name: "is_hiive") required bool isHiive,
    @JsonKey(name: "last_reading") required ReadingStorage? lastReading,
    required String name,
    required String? sensor,
  }) = _HiveStorage;

  Hive toHive() => Hive(
        id: this.id,
        isHiive: this.isHiive,
        lastReading: this.lastReading?.toReading(),
        name: this.name,
        sensor: this.sensor,
      );

  factory HiveStorage.fromJson(Map<String, dynamic> json) =>
      _$HiveStorageFromJson(json);
}
