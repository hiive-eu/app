// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$GatewayImpl _$$GatewayImplFromJson(Map<String, dynamic> json) =>
    _$GatewayImpl(
      lastReading: json['lastReading'] == null
          ? null
          : GatewayReading.fromJson(
              json['lastReading'] as Map<String, dynamic>),
      mac: json['mac'] as String,
      name: json['name'] as String,
      sdSpaceUsed: (json['sdSpaceUsed'] as num?)?.toInt(),
      sdStatus: (json['sdStatus'] as num?)?.toInt(),
      status: $enumDecode(_$GatewayStatusEnumMap, json['status']),
      token: json['token'] as String?,
      wifiRssi: (json['wifiRssi'] as num?)?.toInt(),
    );

Map<String, dynamic> _$$GatewayImplToJson(_$GatewayImpl instance) =>
    <String, dynamic>{
      'lastReading': instance.lastReading,
      'mac': instance.mac,
      'name': instance.name,
      'sdSpaceUsed': instance.sdSpaceUsed,
      'sdStatus': instance.sdStatus,
      'status': _$GatewayStatusEnumMap[instance.status]!,
      'token': instance.token,
      'wifiRssi': instance.wifiRssi,
    };

const _$GatewayStatusEnumMap = {
  GatewayStatus.active: 'active',
  GatewayStatus.inactive: 'inactive',
};

_$GatewayNetworkImpl _$$GatewayNetworkImplFromJson(Map<String, dynamic> json) =>
    _$GatewayNetworkImpl(
      lastReading: json['last_reading'] == null
          ? null
          : GatewayReading.fromJson(
              json['last_reading'] as Map<String, dynamic>),
      mac: json['mac'] as String,
      name: json['name'] as String,
      sdSpaceUsed: (json['sd_space_used'] as num?)?.toInt(),
      sdStatus: (json['sd_status'] as num?)?.toInt(),
      status: $enumDecode(_$GatewayStatusEnumMap, json['status']),
      token: json['token'] as String,
      wifiRssi: (json['wifi_rssi'] as num?)?.toInt(),
    );

Map<String, dynamic> _$$GatewayNetworkImplToJson(
        _$GatewayNetworkImpl instance) =>
    <String, dynamic>{
      'last_reading': instance.lastReading,
      'mac': instance.mac,
      'name': instance.name,
      'sd_space_used': instance.sdSpaceUsed,
      'sd_status': instance.sdStatus,
      'status': _$GatewayStatusEnumMap[instance.status]!,
      'token': instance.token,
      'wifi_rssi': instance.wifiRssi,
    };

_$GatewayStorageImpl _$$GatewayStorageImplFromJson(Map<String, dynamic> json) =>
    _$GatewayStorageImpl(
      lastReading: json['last_reading'] == null
          ? null
          : GatewayReadingStorage.fromJson(
              json['last_reading'] as Map<String, dynamic>),
      mac: json['mac'] as String,
      name: json['name'] as String,
      sdSpaceUsed: (json['sd_space_used'] as num?)?.toInt(),
      sdStatus: (json['sd_status'] as num?)?.toInt(),
      status: $enumDecode(_$GatewayStatusEnumMap, json['status']),
      token: json['token'] as String?,
      wifiRssi: (json['wifi_rssi'] as num?)?.toInt(),
    );

Map<String, dynamic> _$$GatewayStorageImplToJson(
        _$GatewayStorageImpl instance) =>
    <String, dynamic>{
      'last_reading': instance.lastReading,
      'mac': instance.mac,
      'name': instance.name,
      'sd_space_used': instance.sdSpaceUsed,
      'sd_status': instance.sdStatus,
      'status': _$GatewayStatusEnumMap[instance.status]!,
      'token': instance.token,
      'wifi_rssi': instance.wifiRssi,
    };
