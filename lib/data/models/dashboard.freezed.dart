// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'dashboard.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Dashboard {
  List<Gateway> get gateways => throw _privateConstructorUsedError;
  List<Hive> get hives => throw _privateConstructorUsedError;
  List<Sensor> get sensors => throw _privateConstructorUsedError;
  User? get user => throw _privateConstructorUsedError;

  /// Create a copy of Dashboard
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $DashboardCopyWith<Dashboard> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashboardCopyWith<$Res> {
  factory $DashboardCopyWith(Dashboard value, $Res Function(Dashboard) then) =
      _$DashboardCopyWithImpl<$Res, Dashboard>;
  @useResult
  $Res call(
      {List<Gateway> gateways,
      List<Hive> hives,
      List<Sensor> sensors,
      User? user});

  $UserCopyWith<$Res>? get user;
}

/// @nodoc
class _$DashboardCopyWithImpl<$Res, $Val extends Dashboard>
    implements $DashboardCopyWith<$Res> {
  _$DashboardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Dashboard
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateways = null,
    Object? hives = null,
    Object? sensors = null,
    Object? user = freezed,
  }) {
    return _then(_value.copyWith(
      gateways: null == gateways
          ? _value.gateways
          : gateways // ignore: cast_nullable_to_non_nullable
              as List<Gateway>,
      hives: null == hives
          ? _value.hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<Hive>,
      sensors: null == sensors
          ? _value.sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<Sensor>,
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
    ) as $Val);
  }

  /// Create a copy of Dashboard
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $UserCopyWith<$Res>? get user {
    if (_value.user == null) {
      return null;
    }

    return $UserCopyWith<$Res>(_value.user!, (value) {
      return _then(_value.copyWith(user: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$DashboardImplCopyWith<$Res>
    implements $DashboardCopyWith<$Res> {
  factory _$$DashboardImplCopyWith(
          _$DashboardImpl value, $Res Function(_$DashboardImpl) then) =
      __$$DashboardImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<Gateway> gateways,
      List<Hive> hives,
      List<Sensor> sensors,
      User? user});

  @override
  $UserCopyWith<$Res>? get user;
}

/// @nodoc
class __$$DashboardImplCopyWithImpl<$Res>
    extends _$DashboardCopyWithImpl<$Res, _$DashboardImpl>
    implements _$$DashboardImplCopyWith<$Res> {
  __$$DashboardImplCopyWithImpl(
      _$DashboardImpl _value, $Res Function(_$DashboardImpl) _then)
      : super(_value, _then);

  /// Create a copy of Dashboard
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateways = null,
    Object? hives = null,
    Object? sensors = null,
    Object? user = freezed,
  }) {
    return _then(_$DashboardImpl(
      gateways: null == gateways
          ? _value._gateways
          : gateways // ignore: cast_nullable_to_non_nullable
              as List<Gateway>,
      hives: null == hives
          ? _value._hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<Hive>,
      sensors: null == sensors
          ? _value._sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<Sensor>,
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
    ));
  }
}

/// @nodoc

class _$DashboardImpl implements _Dashboard {
  const _$DashboardImpl(
      {required final List<Gateway> gateways,
      required final List<Hive> hives,
      required final List<Sensor> sensors,
      required this.user})
      : _gateways = gateways,
        _hives = hives,
        _sensors = sensors;

  final List<Gateway> _gateways;
  @override
  List<Gateway> get gateways {
    if (_gateways is EqualUnmodifiableListView) return _gateways;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_gateways);
  }

  final List<Hive> _hives;
  @override
  List<Hive> get hives {
    if (_hives is EqualUnmodifiableListView) return _hives;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_hives);
  }

  final List<Sensor> _sensors;
  @override
  List<Sensor> get sensors {
    if (_sensors is EqualUnmodifiableListView) return _sensors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_sensors);
  }

  @override
  final User? user;

  @override
  String toString() {
    return 'Dashboard(gateways: $gateways, hives: $hives, sensors: $sensors, user: $user)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashboardImpl &&
            const DeepCollectionEquality().equals(other._gateways, _gateways) &&
            const DeepCollectionEquality().equals(other._hives, _hives) &&
            const DeepCollectionEquality().equals(other._sensors, _sensors) &&
            (identical(other.user, user) || other.user == user));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_gateways),
      const DeepCollectionEquality().hash(_hives),
      const DeepCollectionEquality().hash(_sensors),
      user);

  /// Create a copy of Dashboard
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$DashboardImplCopyWith<_$DashboardImpl> get copyWith =>
      __$$DashboardImplCopyWithImpl<_$DashboardImpl>(this, _$identity);
}

abstract class _Dashboard implements Dashboard {
  const factory _Dashboard(
      {required final List<Gateway> gateways,
      required final List<Hive> hives,
      required final List<Sensor> sensors,
      required final User? user}) = _$DashboardImpl;

  @override
  List<Gateway> get gateways;
  @override
  List<Hive> get hives;
  @override
  List<Sensor> get sensors;
  @override
  User? get user;

  /// Create a copy of Dashboard
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$DashboardImplCopyWith<_$DashboardImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

DashboardNetwork _$DashboardNetworkFromJson(Map<String, dynamic> json) {
  return _DashboardNetwork.fromJson(json);
}

/// @nodoc
mixin _$DashboardNetwork {
  List<GatewayNetwork> get gateways => throw _privateConstructorUsedError;
  List<HiveNetwork> get hives => throw _privateConstructorUsedError;
  List<SensorNetwork> get sensors => throw _privateConstructorUsedError;
  UserNetwork get user => throw _privateConstructorUsedError;

  /// Serializes this DashboardNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of DashboardNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $DashboardNetworkCopyWith<DashboardNetwork> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DashboardNetworkCopyWith<$Res> {
  factory $DashboardNetworkCopyWith(
          DashboardNetwork value, $Res Function(DashboardNetwork) then) =
      _$DashboardNetworkCopyWithImpl<$Res, DashboardNetwork>;
  @useResult
  $Res call(
      {List<GatewayNetwork> gateways,
      List<HiveNetwork> hives,
      List<SensorNetwork> sensors,
      UserNetwork user});

  $UserNetworkCopyWith<$Res> get user;
}

/// @nodoc
class _$DashboardNetworkCopyWithImpl<$Res, $Val extends DashboardNetwork>
    implements $DashboardNetworkCopyWith<$Res> {
  _$DashboardNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of DashboardNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateways = null,
    Object? hives = null,
    Object? sensors = null,
    Object? user = null,
  }) {
    return _then(_value.copyWith(
      gateways: null == gateways
          ? _value.gateways
          : gateways // ignore: cast_nullable_to_non_nullable
              as List<GatewayNetwork>,
      hives: null == hives
          ? _value.hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<HiveNetwork>,
      sensors: null == sensors
          ? _value.sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<SensorNetwork>,
      user: null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserNetwork,
    ) as $Val);
  }

  /// Create a copy of DashboardNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $UserNetworkCopyWith<$Res> get user {
    return $UserNetworkCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$DashboardNetworkImplCopyWith<$Res>
    implements $DashboardNetworkCopyWith<$Res> {
  factory _$$DashboardNetworkImplCopyWith(_$DashboardNetworkImpl value,
          $Res Function(_$DashboardNetworkImpl) then) =
      __$$DashboardNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<GatewayNetwork> gateways,
      List<HiveNetwork> hives,
      List<SensorNetwork> sensors,
      UserNetwork user});

  @override
  $UserNetworkCopyWith<$Res> get user;
}

/// @nodoc
class __$$DashboardNetworkImplCopyWithImpl<$Res>
    extends _$DashboardNetworkCopyWithImpl<$Res, _$DashboardNetworkImpl>
    implements _$$DashboardNetworkImplCopyWith<$Res> {
  __$$DashboardNetworkImplCopyWithImpl(_$DashboardNetworkImpl _value,
      $Res Function(_$DashboardNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of DashboardNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? gateways = null,
    Object? hives = null,
    Object? sensors = null,
    Object? user = null,
  }) {
    return _then(_$DashboardNetworkImpl(
      gateways: null == gateways
          ? _value._gateways
          : gateways // ignore: cast_nullable_to_non_nullable
              as List<GatewayNetwork>,
      hives: null == hives
          ? _value._hives
          : hives // ignore: cast_nullable_to_non_nullable
              as List<HiveNetwork>,
      sensors: null == sensors
          ? _value._sensors
          : sensors // ignore: cast_nullable_to_non_nullable
              as List<SensorNetwork>,
      user: null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserNetwork,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$DashboardNetworkImpl extends _DashboardNetwork {
  const _$DashboardNetworkImpl(
      {required final List<GatewayNetwork> gateways,
      required final List<HiveNetwork> hives,
      required final List<SensorNetwork> sensors,
      required this.user})
      : _gateways = gateways,
        _hives = hives,
        _sensors = sensors,
        super._();

  factory _$DashboardNetworkImpl.fromJson(Map<String, dynamic> json) =>
      _$$DashboardNetworkImplFromJson(json);

  final List<GatewayNetwork> _gateways;
  @override
  List<GatewayNetwork> get gateways {
    if (_gateways is EqualUnmodifiableListView) return _gateways;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_gateways);
  }

  final List<HiveNetwork> _hives;
  @override
  List<HiveNetwork> get hives {
    if (_hives is EqualUnmodifiableListView) return _hives;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_hives);
  }

  final List<SensorNetwork> _sensors;
  @override
  List<SensorNetwork> get sensors {
    if (_sensors is EqualUnmodifiableListView) return _sensors;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_sensors);
  }

  @override
  final UserNetwork user;

  @override
  String toString() {
    return 'DashboardNetwork(gateways: $gateways, hives: $hives, sensors: $sensors, user: $user)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$DashboardNetworkImpl &&
            const DeepCollectionEquality().equals(other._gateways, _gateways) &&
            const DeepCollectionEquality().equals(other._hives, _hives) &&
            const DeepCollectionEquality().equals(other._sensors, _sensors) &&
            (identical(other.user, user) || other.user == user));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_gateways),
      const DeepCollectionEquality().hash(_hives),
      const DeepCollectionEquality().hash(_sensors),
      user);

  /// Create a copy of DashboardNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$DashboardNetworkImplCopyWith<_$DashboardNetworkImpl> get copyWith =>
      __$$DashboardNetworkImplCopyWithImpl<_$DashboardNetworkImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$DashboardNetworkImplToJson(
      this,
    );
  }
}

abstract class _DashboardNetwork extends DashboardNetwork {
  const factory _DashboardNetwork(
      {required final List<GatewayNetwork> gateways,
      required final List<HiveNetwork> hives,
      required final List<SensorNetwork> sensors,
      required final UserNetwork user}) = _$DashboardNetworkImpl;
  const _DashboardNetwork._() : super._();

  factory _DashboardNetwork.fromJson(Map<String, dynamic> json) =
      _$DashboardNetworkImpl.fromJson;

  @override
  List<GatewayNetwork> get gateways;
  @override
  List<HiveNetwork> get hives;
  @override
  List<SensorNetwork> get sensors;
  @override
  UserNetwork get user;

  /// Create a copy of DashboardNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$DashboardNetworkImplCopyWith<_$DashboardNetworkImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
