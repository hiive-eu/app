// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'authentication_tokens.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AuthenticationTokens {
  AccessToken get accessToken => throw _privateConstructorUsedError;
  RefreshToken get refreshToken => throw _privateConstructorUsedError;

  /// Create a copy of AuthenticationTokens
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $AuthenticationTokensCopyWith<AuthenticationTokens> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationTokensCopyWith<$Res> {
  factory $AuthenticationTokensCopyWith(AuthenticationTokens value,
          $Res Function(AuthenticationTokens) then) =
      _$AuthenticationTokensCopyWithImpl<$Res, AuthenticationTokens>;
  @useResult
  $Res call({AccessToken accessToken, RefreshToken refreshToken});
}

/// @nodoc
class _$AuthenticationTokensCopyWithImpl<$Res,
        $Val extends AuthenticationTokens>
    implements $AuthenticationTokensCopyWith<$Res> {
  _$AuthenticationTokensCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of AuthenticationTokens
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accessToken = null,
    Object? refreshToken = null,
  }) {
    return _then(_value.copyWith(
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as AccessToken,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as RefreshToken,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AuthenticationTokensImplCopyWith<$Res>
    implements $AuthenticationTokensCopyWith<$Res> {
  factory _$$AuthenticationTokensImplCopyWith(_$AuthenticationTokensImpl value,
          $Res Function(_$AuthenticationTokensImpl) then) =
      __$$AuthenticationTokensImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AccessToken accessToken, RefreshToken refreshToken});
}

/// @nodoc
class __$$AuthenticationTokensImplCopyWithImpl<$Res>
    extends _$AuthenticationTokensCopyWithImpl<$Res, _$AuthenticationTokensImpl>
    implements _$$AuthenticationTokensImplCopyWith<$Res> {
  __$$AuthenticationTokensImplCopyWithImpl(_$AuthenticationTokensImpl _value,
      $Res Function(_$AuthenticationTokensImpl) _then)
      : super(_value, _then);

  /// Create a copy of AuthenticationTokens
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accessToken = null,
    Object? refreshToken = null,
  }) {
    return _then(_$AuthenticationTokensImpl(
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as AccessToken,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as RefreshToken,
    ));
  }
}

/// @nodoc

class _$AuthenticationTokensImpl implements _AuthenticationTokens {
  const _$AuthenticationTokensImpl(
      {required this.accessToken, required this.refreshToken});

  @override
  final AccessToken accessToken;
  @override
  final RefreshToken refreshToken;

  @override
  String toString() {
    return 'AuthenticationTokens(accessToken: $accessToken, refreshToken: $refreshToken)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthenticationTokensImpl &&
            (identical(other.accessToken, accessToken) ||
                other.accessToken == accessToken) &&
            (identical(other.refreshToken, refreshToken) ||
                other.refreshToken == refreshToken));
  }

  @override
  int get hashCode => Object.hash(runtimeType, accessToken, refreshToken);

  /// Create a copy of AuthenticationTokens
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthenticationTokensImplCopyWith<_$AuthenticationTokensImpl>
      get copyWith =>
          __$$AuthenticationTokensImplCopyWithImpl<_$AuthenticationTokensImpl>(
              this, _$identity);
}

abstract class _AuthenticationTokens implements AuthenticationTokens {
  const factory _AuthenticationTokens(
      {required final AccessToken accessToken,
      required final RefreshToken refreshToken}) = _$AuthenticationTokensImpl;

  @override
  AccessToken get accessToken;
  @override
  RefreshToken get refreshToken;

  /// Create a copy of AuthenticationTokens
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$AuthenticationTokensImplCopyWith<_$AuthenticationTokensImpl>
      get copyWith => throw _privateConstructorUsedError;
}

AuthenticationTokensNetwork _$AuthenticationTokensNetworkFromJson(
    Map<String, dynamic> json) {
  return _AuthenticationTokensNetwork.fromJson(json);
}

/// @nodoc
mixin _$AuthenticationTokensNetwork {
  @JsonKey(name: "access_token")
  String get accessToken => throw _privateConstructorUsedError;
  @JsonKey(name: "refresh_token")
  String get refreshToken => throw _privateConstructorUsedError;

  /// Serializes this AuthenticationTokensNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of AuthenticationTokensNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $AuthenticationTokensNetworkCopyWith<AuthenticationTokensNetwork>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthenticationTokensNetworkCopyWith<$Res> {
  factory $AuthenticationTokensNetworkCopyWith(
          AuthenticationTokensNetwork value,
          $Res Function(AuthenticationTokensNetwork) then) =
      _$AuthenticationTokensNetworkCopyWithImpl<$Res,
          AuthenticationTokensNetwork>;
  @useResult
  $Res call(
      {@JsonKey(name: "access_token") String accessToken,
      @JsonKey(name: "refresh_token") String refreshToken});
}

/// @nodoc
class _$AuthenticationTokensNetworkCopyWithImpl<$Res,
        $Val extends AuthenticationTokensNetwork>
    implements $AuthenticationTokensNetworkCopyWith<$Res> {
  _$AuthenticationTokensNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of AuthenticationTokensNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accessToken = null,
    Object? refreshToken = null,
  }) {
    return _then(_value.copyWith(
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AuthenticationTokensNetworkImplCopyWith<$Res>
    implements $AuthenticationTokensNetworkCopyWith<$Res> {
  factory _$$AuthenticationTokensNetworkImplCopyWith(
          _$AuthenticationTokensNetworkImpl value,
          $Res Function(_$AuthenticationTokensNetworkImpl) then) =
      __$$AuthenticationTokensNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "access_token") String accessToken,
      @JsonKey(name: "refresh_token") String refreshToken});
}

/// @nodoc
class __$$AuthenticationTokensNetworkImplCopyWithImpl<$Res>
    extends _$AuthenticationTokensNetworkCopyWithImpl<$Res,
        _$AuthenticationTokensNetworkImpl>
    implements _$$AuthenticationTokensNetworkImplCopyWith<$Res> {
  __$$AuthenticationTokensNetworkImplCopyWithImpl(
      _$AuthenticationTokensNetworkImpl _value,
      $Res Function(_$AuthenticationTokensNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of AuthenticationTokensNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? accessToken = null,
    Object? refreshToken = null,
  }) {
    return _then(_$AuthenticationTokensNetworkImpl(
      accessToken: null == accessToken
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String,
      refreshToken: null == refreshToken
          ? _value.refreshToken
          : refreshToken // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AuthenticationTokensNetworkImpl extends _AuthenticationTokensNetwork {
  const _$AuthenticationTokensNetworkImpl(
      {@JsonKey(name: "access_token") required this.accessToken,
      @JsonKey(name: "refresh_token") required this.refreshToken})
      : super._();

  factory _$AuthenticationTokensNetworkImpl.fromJson(
          Map<String, dynamic> json) =>
      _$$AuthenticationTokensNetworkImplFromJson(json);

  @override
  @JsonKey(name: "access_token")
  final String accessToken;
  @override
  @JsonKey(name: "refresh_token")
  final String refreshToken;

  @override
  String toString() {
    return 'AuthenticationTokensNetwork(accessToken: $accessToken, refreshToken: $refreshToken)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthenticationTokensNetworkImpl &&
            (identical(other.accessToken, accessToken) ||
                other.accessToken == accessToken) &&
            (identical(other.refreshToken, refreshToken) ||
                other.refreshToken == refreshToken));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, accessToken, refreshToken);

  /// Create a copy of AuthenticationTokensNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthenticationTokensNetworkImplCopyWith<_$AuthenticationTokensNetworkImpl>
      get copyWith => __$$AuthenticationTokensNetworkImplCopyWithImpl<
          _$AuthenticationTokensNetworkImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AuthenticationTokensNetworkImplToJson(
      this,
    );
  }
}

abstract class _AuthenticationTokensNetwork
    extends AuthenticationTokensNetwork {
  const factory _AuthenticationTokensNetwork(
          {@JsonKey(name: "access_token") required final String accessToken,
          @JsonKey(name: "refresh_token") required final String refreshToken}) =
      _$AuthenticationTokensNetworkImpl;
  const _AuthenticationTokensNetwork._() : super._();

  factory _AuthenticationTokensNetwork.fromJson(Map<String, dynamic> json) =
      _$AuthenticationTokensNetworkImpl.fromJson;

  @override
  @JsonKey(name: "access_token")
  String get accessToken;
  @override
  @JsonKey(name: "refresh_token")
  String get refreshToken;

  /// Create a copy of AuthenticationTokensNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$AuthenticationTokensNetworkImplCopyWith<_$AuthenticationTokensNetworkImpl>
      get copyWith => throw _privateConstructorUsedError;
}
