// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'sensor.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Sensor _$SensorFromJson(Map<String, dynamic> json) {
  return _Sensor.fromJson(json);
}

/// @nodoc
mixin _$Sensor {
  int get battery => throw _privateConstructorUsedError;
  String? get gateway => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  int get rssi => throw _privateConstructorUsedError;
  SensorStatus get status => throw _privateConstructorUsedError;

  /// Serializes this Sensor to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of Sensor
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SensorCopyWith<Sensor> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SensorCopyWith<$Res> {
  factory $SensorCopyWith(Sensor value, $Res Function(Sensor) then) =
      _$SensorCopyWithImpl<$Res, Sensor>;
  @useResult
  $Res call(
      {int battery,
      String? gateway,
      String mac,
      int rssi,
      SensorStatus status});
}

/// @nodoc
class _$SensorCopyWithImpl<$Res, $Val extends Sensor>
    implements $SensorCopyWith<$Res> {
  _$SensorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Sensor
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? gateway = freezed,
    Object? mac = null,
    Object? rssi = null,
    Object? status = null,
  }) {
    return _then(_value.copyWith(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      gateway: freezed == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as String?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SensorStatus,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SensorImplCopyWith<$Res> implements $SensorCopyWith<$Res> {
  factory _$$SensorImplCopyWith(
          _$SensorImpl value, $Res Function(_$SensorImpl) then) =
      __$$SensorImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int battery,
      String? gateway,
      String mac,
      int rssi,
      SensorStatus status});
}

/// @nodoc
class __$$SensorImplCopyWithImpl<$Res>
    extends _$SensorCopyWithImpl<$Res, _$SensorImpl>
    implements _$$SensorImplCopyWith<$Res> {
  __$$SensorImplCopyWithImpl(
      _$SensorImpl _value, $Res Function(_$SensorImpl) _then)
      : super(_value, _then);

  /// Create a copy of Sensor
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? gateway = freezed,
    Object? mac = null,
    Object? rssi = null,
    Object? status = null,
  }) {
    return _then(_$SensorImpl(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      gateway: freezed == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as String?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SensorStatus,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SensorImpl extends _Sensor with DiagnosticableTreeMixin {
  _$SensorImpl(
      {required this.battery,
      required this.gateway,
      required this.mac,
      required this.rssi,
      required this.status})
      : super._();

  factory _$SensorImpl.fromJson(Map<String, dynamic> json) =>
      _$$SensorImplFromJson(json);

  @override
  final int battery;
  @override
  final String? gateway;
  @override
  final String mac;
  @override
  final int rssi;
  @override
  final SensorStatus status;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Sensor(battery: $battery, gateway: $gateway, mac: $mac, rssi: $rssi, status: $status)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Sensor'))
      ..add(DiagnosticsProperty('battery', battery))
      ..add(DiagnosticsProperty('gateway', gateway))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('rssi', rssi))
      ..add(DiagnosticsProperty('status', status));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SensorImpl &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.gateway, gateway) || other.gateway == gateway) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.rssi, rssi) || other.rssi == rssi) &&
            (identical(other.status, status) || other.status == status));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, battery, gateway, mac, rssi, status);

  /// Create a copy of Sensor
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SensorImplCopyWith<_$SensorImpl> get copyWith =>
      __$$SensorImplCopyWithImpl<_$SensorImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SensorImplToJson(
      this,
    );
  }
}

abstract class _Sensor extends Sensor {
  factory _Sensor(
      {required final int battery,
      required final String? gateway,
      required final String mac,
      required final int rssi,
      required final SensorStatus status}) = _$SensorImpl;
  _Sensor._() : super._();

  factory _Sensor.fromJson(Map<String, dynamic> json) = _$SensorImpl.fromJson;

  @override
  int get battery;
  @override
  String? get gateway;
  @override
  String get mac;
  @override
  int get rssi;
  @override
  SensorStatus get status;

  /// Create a copy of Sensor
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SensorImplCopyWith<_$SensorImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

SensorNetwork _$SensorNetworkFromJson(Map<String, dynamic> json) {
  return _SensorNetwork.fromJson(json);
}

/// @nodoc
mixin _$SensorNetwork {
  int get battery => throw _privateConstructorUsedError;
  String? get gateway => throw _privateConstructorUsedError;
  int get rssi => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  @JsonKey(name: "status")
  SensorStatus get status => throw _privateConstructorUsedError;

  /// Serializes this SensorNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of SensorNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SensorNetworkCopyWith<SensorNetwork> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SensorNetworkCopyWith<$Res> {
  factory $SensorNetworkCopyWith(
          SensorNetwork value, $Res Function(SensorNetwork) then) =
      _$SensorNetworkCopyWithImpl<$Res, SensorNetwork>;
  @useResult
  $Res call(
      {int battery,
      String? gateway,
      int rssi,
      String mac,
      @JsonKey(name: "status") SensorStatus status});
}

/// @nodoc
class _$SensorNetworkCopyWithImpl<$Res, $Val extends SensorNetwork>
    implements $SensorNetworkCopyWith<$Res> {
  _$SensorNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of SensorNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? gateway = freezed,
    Object? rssi = null,
    Object? mac = null,
    Object? status = null,
  }) {
    return _then(_value.copyWith(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      gateway: freezed == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as String?,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SensorStatus,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SensorNetworkImplCopyWith<$Res>
    implements $SensorNetworkCopyWith<$Res> {
  factory _$$SensorNetworkImplCopyWith(
          _$SensorNetworkImpl value, $Res Function(_$SensorNetworkImpl) then) =
      __$$SensorNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int battery,
      String? gateway,
      int rssi,
      String mac,
      @JsonKey(name: "status") SensorStatus status});
}

/// @nodoc
class __$$SensorNetworkImplCopyWithImpl<$Res>
    extends _$SensorNetworkCopyWithImpl<$Res, _$SensorNetworkImpl>
    implements _$$SensorNetworkImplCopyWith<$Res> {
  __$$SensorNetworkImplCopyWithImpl(
      _$SensorNetworkImpl _value, $Res Function(_$SensorNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of SensorNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? gateway = freezed,
    Object? rssi = null,
    Object? mac = null,
    Object? status = null,
  }) {
    return _then(_$SensorNetworkImpl(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      gateway: freezed == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as String?,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SensorStatus,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SensorNetworkImpl extends _SensorNetwork with DiagnosticableTreeMixin {
  const _$SensorNetworkImpl(
      {required this.battery,
      required this.gateway,
      required this.rssi,
      required this.mac,
      @JsonKey(name: "status") required this.status})
      : super._();

  factory _$SensorNetworkImpl.fromJson(Map<String, dynamic> json) =>
      _$$SensorNetworkImplFromJson(json);

  @override
  final int battery;
  @override
  final String? gateway;
  @override
  final int rssi;
  @override
  final String mac;
  @override
  @JsonKey(name: "status")
  final SensorStatus status;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SensorNetwork(battery: $battery, gateway: $gateway, rssi: $rssi, mac: $mac, status: $status)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SensorNetwork'))
      ..add(DiagnosticsProperty('battery', battery))
      ..add(DiagnosticsProperty('gateway', gateway))
      ..add(DiagnosticsProperty('rssi', rssi))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('status', status));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SensorNetworkImpl &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.gateway, gateway) || other.gateway == gateway) &&
            (identical(other.rssi, rssi) || other.rssi == rssi) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.status, status) || other.status == status));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, battery, gateway, rssi, mac, status);

  /// Create a copy of SensorNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SensorNetworkImplCopyWith<_$SensorNetworkImpl> get copyWith =>
      __$$SensorNetworkImplCopyWithImpl<_$SensorNetworkImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SensorNetworkImplToJson(
      this,
    );
  }
}

abstract class _SensorNetwork extends SensorNetwork {
  const factory _SensorNetwork(
          {required final int battery,
          required final String? gateway,
          required final int rssi,
          required final String mac,
          @JsonKey(name: "status") required final SensorStatus status}) =
      _$SensorNetworkImpl;
  const _SensorNetwork._() : super._();

  factory _SensorNetwork.fromJson(Map<String, dynamic> json) =
      _$SensorNetworkImpl.fromJson;

  @override
  int get battery;
  @override
  String? get gateway;
  @override
  int get rssi;
  @override
  String get mac;
  @override
  @JsonKey(name: "status")
  SensorStatus get status;

  /// Create a copy of SensorNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SensorNetworkImplCopyWith<_$SensorNetworkImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

SensorStorage _$SensorStorageFromJson(Map<String, dynamic> json) {
  return _SensorStorage.fromJson(json);
}

/// @nodoc
mixin _$SensorStorage {
  int get battery => throw _privateConstructorUsedError;
  String? get gateway => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  int get rssi => throw _privateConstructorUsedError;
  @JsonKey(name: "status")
  SensorStatus get status => throw _privateConstructorUsedError;

  /// Serializes this SensorStorage to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of SensorStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SensorStorageCopyWith<SensorStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SensorStorageCopyWith<$Res> {
  factory $SensorStorageCopyWith(
          SensorStorage value, $Res Function(SensorStorage) then) =
      _$SensorStorageCopyWithImpl<$Res, SensorStorage>;
  @useResult
  $Res call(
      {int battery,
      String? gateway,
      String mac,
      int rssi,
      @JsonKey(name: "status") SensorStatus status});
}

/// @nodoc
class _$SensorStorageCopyWithImpl<$Res, $Val extends SensorStorage>
    implements $SensorStorageCopyWith<$Res> {
  _$SensorStorageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of SensorStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? gateway = freezed,
    Object? mac = null,
    Object? rssi = null,
    Object? status = null,
  }) {
    return _then(_value.copyWith(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      gateway: freezed == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as String?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SensorStatus,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SensorStorageImplCopyWith<$Res>
    implements $SensorStorageCopyWith<$Res> {
  factory _$$SensorStorageImplCopyWith(
          _$SensorStorageImpl value, $Res Function(_$SensorStorageImpl) then) =
      __$$SensorStorageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int battery,
      String? gateway,
      String mac,
      int rssi,
      @JsonKey(name: "status") SensorStatus status});
}

/// @nodoc
class __$$SensorStorageImplCopyWithImpl<$Res>
    extends _$SensorStorageCopyWithImpl<$Res, _$SensorStorageImpl>
    implements _$$SensorStorageImplCopyWith<$Res> {
  __$$SensorStorageImplCopyWithImpl(
      _$SensorStorageImpl _value, $Res Function(_$SensorStorageImpl) _then)
      : super(_value, _then);

  /// Create a copy of SensorStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? battery = null,
    Object? gateway = freezed,
    Object? mac = null,
    Object? rssi = null,
    Object? status = null,
  }) {
    return _then(_$SensorStorageImpl(
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      gateway: freezed == gateway
          ? _value.gateway
          : gateway // ignore: cast_nullable_to_non_nullable
              as String?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as SensorStatus,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$SensorStorageImpl extends _SensorStorage with DiagnosticableTreeMixin {
  _$SensorStorageImpl(
      {required this.battery,
      required this.gateway,
      required this.mac,
      required this.rssi,
      @JsonKey(name: "status") required this.status})
      : super._();

  factory _$SensorStorageImpl.fromJson(Map<String, dynamic> json) =>
      _$$SensorStorageImplFromJson(json);

  @override
  final int battery;
  @override
  final String? gateway;
  @override
  final String mac;
  @override
  final int rssi;
  @override
  @JsonKey(name: "status")
  final SensorStatus status;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SensorStorage(battery: $battery, gateway: $gateway, mac: $mac, rssi: $rssi, status: $status)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SensorStorage'))
      ..add(DiagnosticsProperty('battery', battery))
      ..add(DiagnosticsProperty('gateway', gateway))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('rssi', rssi))
      ..add(DiagnosticsProperty('status', status));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SensorStorageImpl &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.gateway, gateway) || other.gateway == gateway) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.rssi, rssi) || other.rssi == rssi) &&
            (identical(other.status, status) || other.status == status));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, battery, gateway, mac, rssi, status);

  /// Create a copy of SensorStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SensorStorageImplCopyWith<_$SensorStorageImpl> get copyWith =>
      __$$SensorStorageImplCopyWithImpl<_$SensorStorageImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$SensorStorageImplToJson(
      this,
    );
  }
}

abstract class _SensorStorage extends SensorStorage {
  factory _SensorStorage(
          {required final int battery,
          required final String? gateway,
          required final String mac,
          required final int rssi,
          @JsonKey(name: "status") required final SensorStatus status}) =
      _$SensorStorageImpl;
  _SensorStorage._() : super._();

  factory _SensorStorage.fromJson(Map<String, dynamic> json) =
      _$SensorStorageImpl.fromJson;

  @override
  int get battery;
  @override
  String? get gateway;
  @override
  String get mac;
  @override
  int get rssi;
  @override
  @JsonKey(name: "status")
  SensorStatus get status;

  /// Create a copy of SensorStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SensorStorageImplCopyWith<_$SensorStorageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SensorGateway {
  int get number => throw _privateConstructorUsedError;
  int get battery => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  int get rssi => throw _privateConstructorUsedError;

  /// Create a copy of SensorGateway
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SensorGatewayCopyWith<SensorGateway> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SensorGatewayCopyWith<$Res> {
  factory $SensorGatewayCopyWith(
          SensorGateway value, $Res Function(SensorGateway) then) =
      _$SensorGatewayCopyWithImpl<$Res, SensorGateway>;
  @useResult
  $Res call({int number, int battery, String mac, int rssi});
}

/// @nodoc
class _$SensorGatewayCopyWithImpl<$Res, $Val extends SensorGateway>
    implements $SensorGatewayCopyWith<$Res> {
  _$SensorGatewayCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of SensorGateway
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? number = null,
    Object? battery = null,
    Object? mac = null,
    Object? rssi = null,
  }) {
    return _then(_value.copyWith(
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SensorGatewayImplCopyWith<$Res>
    implements $SensorGatewayCopyWith<$Res> {
  factory _$$SensorGatewayImplCopyWith(
          _$SensorGatewayImpl value, $Res Function(_$SensorGatewayImpl) then) =
      __$$SensorGatewayImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int number, int battery, String mac, int rssi});
}

/// @nodoc
class __$$SensorGatewayImplCopyWithImpl<$Res>
    extends _$SensorGatewayCopyWithImpl<$Res, _$SensorGatewayImpl>
    implements _$$SensorGatewayImplCopyWith<$Res> {
  __$$SensorGatewayImplCopyWithImpl(
      _$SensorGatewayImpl _value, $Res Function(_$SensorGatewayImpl) _then)
      : super(_value, _then);

  /// Create a copy of SensorGateway
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? number = null,
    Object? battery = null,
    Object? mac = null,
    Object? rssi = null,
  }) {
    return _then(_$SensorGatewayImpl(
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as int,
      battery: null == battery
          ? _value.battery
          : battery // ignore: cast_nullable_to_non_nullable
              as int,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$SensorGatewayImpl extends _SensorGateway with DiagnosticableTreeMixin {
  _$SensorGatewayImpl(
      {required this.number,
      required this.battery,
      required this.mac,
      required this.rssi})
      : super._();

  @override
  final int number;
  @override
  final int battery;
  @override
  final String mac;
  @override
  final int rssi;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SensorGateway(number: $number, battery: $battery, mac: $mac, rssi: $rssi)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SensorGateway'))
      ..add(DiagnosticsProperty('number', number))
      ..add(DiagnosticsProperty('battery', battery))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('rssi', rssi));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SensorGatewayImpl &&
            (identical(other.number, number) || other.number == number) &&
            (identical(other.battery, battery) || other.battery == battery) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.rssi, rssi) || other.rssi == rssi));
  }

  @override
  int get hashCode => Object.hash(runtimeType, number, battery, mac, rssi);

  /// Create a copy of SensorGateway
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SensorGatewayImplCopyWith<_$SensorGatewayImpl> get copyWith =>
      __$$SensorGatewayImplCopyWithImpl<_$SensorGatewayImpl>(this, _$identity);
}

abstract class _SensorGateway extends SensorGateway {
  factory _SensorGateway(
      {required final int number,
      required final int battery,
      required final String mac,
      required final int rssi}) = _$SensorGatewayImpl;
  _SensorGateway._() : super._();

  @override
  int get number;
  @override
  int get battery;
  @override
  String get mac;
  @override
  int get rssi;

  /// Create a copy of SensorGateway
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SensorGatewayImplCopyWith<_$SensorGatewayImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
