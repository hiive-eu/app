// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UserImpl _$$UserImplFromJson(Map<String, dynamic> json) => _$UserImpl(
      id: json['id'] as String,
      displayName: json['displayName'] as String,
      email: json['email'] as String,
    );

Map<String, dynamic> _$$UserImplToJson(_$UserImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'displayName': instance.displayName,
      'email': instance.email,
    };

_$UserNetworkImpl _$$UserNetworkImplFromJson(Map<String, dynamic> json) =>
    _$UserNetworkImpl(
      id: json['id'] as String,
      displayName: json['displayname'] as String,
      email: json['email'] as String,
    );

Map<String, dynamic> _$$UserNetworkImplToJson(_$UserNetworkImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'displayname': instance.displayName,
      'email': instance.email,
    };
