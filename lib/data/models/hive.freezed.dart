// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'hive.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Hive _$HiveFromJson(Map<String, dynamic> json) {
  return _Hive.fromJson(json);
}

/// @nodoc
mixin _$Hive {
  String get id => throw _privateConstructorUsedError;
  bool get isHiive => throw _privateConstructorUsedError;
  Reading? get lastReading => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get sensor => throw _privateConstructorUsedError;

  /// Serializes this Hive to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of Hive
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $HiveCopyWith<Hive> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HiveCopyWith<$Res> {
  factory $HiveCopyWith(Hive value, $Res Function(Hive) then) =
      _$HiveCopyWithImpl<$Res, Hive>;
  @useResult
  $Res call(
      {String id,
      bool isHiive,
      Reading? lastReading,
      String name,
      String? sensor});

  $ReadingCopyWith<$Res>? get lastReading;
}

/// @nodoc
class _$HiveCopyWithImpl<$Res, $Val extends Hive>
    implements $HiveCopyWith<$Res> {
  _$HiveCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Hive
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? isHiive = null,
    Object? lastReading = freezed,
    Object? name = null,
    Object? sensor = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isHiive: null == isHiive
          ? _value.isHiive
          : isHiive // ignore: cast_nullable_to_non_nullable
              as bool,
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as Reading?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sensor: freezed == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  /// Create a copy of Hive
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $ReadingCopyWith<$Res>? get lastReading {
    if (_value.lastReading == null) {
      return null;
    }

    return $ReadingCopyWith<$Res>(_value.lastReading!, (value) {
      return _then(_value.copyWith(lastReading: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$HiveImplCopyWith<$Res> implements $HiveCopyWith<$Res> {
  factory _$$HiveImplCopyWith(
          _$HiveImpl value, $Res Function(_$HiveImpl) then) =
      __$$HiveImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      bool isHiive,
      Reading? lastReading,
      String name,
      String? sensor});

  @override
  $ReadingCopyWith<$Res>? get lastReading;
}

/// @nodoc
class __$$HiveImplCopyWithImpl<$Res>
    extends _$HiveCopyWithImpl<$Res, _$HiveImpl>
    implements _$$HiveImplCopyWith<$Res> {
  __$$HiveImplCopyWithImpl(_$HiveImpl _value, $Res Function(_$HiveImpl) _then)
      : super(_value, _then);

  /// Create a copy of Hive
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? isHiive = null,
    Object? lastReading = freezed,
    Object? name = null,
    Object? sensor = freezed,
  }) {
    return _then(_$HiveImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isHiive: null == isHiive
          ? _value.isHiive
          : isHiive // ignore: cast_nullable_to_non_nullable
              as bool,
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as Reading?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sensor: freezed == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HiveImpl extends _Hive {
  _$HiveImpl(
      {required this.id,
      required this.isHiive,
      required this.lastReading,
      required this.name,
      required this.sensor})
      : super._();

  factory _$HiveImpl.fromJson(Map<String, dynamic> json) =>
      _$$HiveImplFromJson(json);

  @override
  final String id;
  @override
  final bool isHiive;
  @override
  final Reading? lastReading;
  @override
  final String name;
  @override
  final String? sensor;

  @override
  String toString() {
    return 'Hive(id: $id, isHiive: $isHiive, lastReading: $lastReading, name: $name, sensor: $sensor)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HiveImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.isHiive, isHiive) || other.isHiive == isHiive) &&
            (identical(other.lastReading, lastReading) ||
                other.lastReading == lastReading) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sensor, sensor) || other.sensor == sensor));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, isHiive, lastReading, name, sensor);

  /// Create a copy of Hive
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$HiveImplCopyWith<_$HiveImpl> get copyWith =>
      __$$HiveImplCopyWithImpl<_$HiveImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HiveImplToJson(
      this,
    );
  }
}

abstract class _Hive extends Hive {
  factory _Hive(
      {required final String id,
      required final bool isHiive,
      required final Reading? lastReading,
      required final String name,
      required final String? sensor}) = _$HiveImpl;
  _Hive._() : super._();

  factory _Hive.fromJson(Map<String, dynamic> json) = _$HiveImpl.fromJson;

  @override
  String get id;
  @override
  bool get isHiive;
  @override
  Reading? get lastReading;
  @override
  String get name;
  @override
  String? get sensor;

  /// Create a copy of Hive
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$HiveImplCopyWith<_$HiveImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

HiveNetwork _$HiveNetworkFromJson(Map<String, dynamic> json) {
  return _HiveNetwork.fromJson(json);
}

/// @nodoc
mixin _$HiveNetwork {
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: "is_hiive")
  bool get isHiive => throw _privateConstructorUsedError;
  @JsonKey(name: "last_reading")
  ReadingNetwork? get lastReading => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get sensor => throw _privateConstructorUsedError;

  /// Serializes this HiveNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of HiveNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $HiveNetworkCopyWith<HiveNetwork> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HiveNetworkCopyWith<$Res> {
  factory $HiveNetworkCopyWith(
          HiveNetwork value, $Res Function(HiveNetwork) then) =
      _$HiveNetworkCopyWithImpl<$Res, HiveNetwork>;
  @useResult
  $Res call(
      {String id,
      @JsonKey(name: "is_hiive") bool isHiive,
      @JsonKey(name: "last_reading") ReadingNetwork? lastReading,
      String name,
      String? sensor});

  $ReadingNetworkCopyWith<$Res>? get lastReading;
}

/// @nodoc
class _$HiveNetworkCopyWithImpl<$Res, $Val extends HiveNetwork>
    implements $HiveNetworkCopyWith<$Res> {
  _$HiveNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of HiveNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? isHiive = null,
    Object? lastReading = freezed,
    Object? name = null,
    Object? sensor = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isHiive: null == isHiive
          ? _value.isHiive
          : isHiive // ignore: cast_nullable_to_non_nullable
              as bool,
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as ReadingNetwork?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sensor: freezed == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  /// Create a copy of HiveNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $ReadingNetworkCopyWith<$Res>? get lastReading {
    if (_value.lastReading == null) {
      return null;
    }

    return $ReadingNetworkCopyWith<$Res>(_value.lastReading!, (value) {
      return _then(_value.copyWith(lastReading: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$HiveNetworkImplCopyWith<$Res>
    implements $HiveNetworkCopyWith<$Res> {
  factory _$$HiveNetworkImplCopyWith(
          _$HiveNetworkImpl value, $Res Function(_$HiveNetworkImpl) then) =
      __$$HiveNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      @JsonKey(name: "is_hiive") bool isHiive,
      @JsonKey(name: "last_reading") ReadingNetwork? lastReading,
      String name,
      String? sensor});

  @override
  $ReadingNetworkCopyWith<$Res>? get lastReading;
}

/// @nodoc
class __$$HiveNetworkImplCopyWithImpl<$Res>
    extends _$HiveNetworkCopyWithImpl<$Res, _$HiveNetworkImpl>
    implements _$$HiveNetworkImplCopyWith<$Res> {
  __$$HiveNetworkImplCopyWithImpl(
      _$HiveNetworkImpl _value, $Res Function(_$HiveNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of HiveNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? isHiive = null,
    Object? lastReading = freezed,
    Object? name = null,
    Object? sensor = freezed,
  }) {
    return _then(_$HiveNetworkImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isHiive: null == isHiive
          ? _value.isHiive
          : isHiive // ignore: cast_nullable_to_non_nullable
              as bool,
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as ReadingNetwork?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sensor: freezed == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HiveNetworkImpl extends _HiveNetwork {
  const _$HiveNetworkImpl(
      {required this.id,
      @JsonKey(name: "is_hiive") required this.isHiive,
      @JsonKey(name: "last_reading") required this.lastReading,
      required this.name,
      required this.sensor})
      : super._();

  factory _$HiveNetworkImpl.fromJson(Map<String, dynamic> json) =>
      _$$HiveNetworkImplFromJson(json);

  @override
  final String id;
  @override
  @JsonKey(name: "is_hiive")
  final bool isHiive;
  @override
  @JsonKey(name: "last_reading")
  final ReadingNetwork? lastReading;
  @override
  final String name;
  @override
  final String? sensor;

  @override
  String toString() {
    return 'HiveNetwork(id: $id, isHiive: $isHiive, lastReading: $lastReading, name: $name, sensor: $sensor)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HiveNetworkImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.isHiive, isHiive) || other.isHiive == isHiive) &&
            (identical(other.lastReading, lastReading) ||
                other.lastReading == lastReading) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sensor, sensor) || other.sensor == sensor));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, isHiive, lastReading, name, sensor);

  /// Create a copy of HiveNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$HiveNetworkImplCopyWith<_$HiveNetworkImpl> get copyWith =>
      __$$HiveNetworkImplCopyWithImpl<_$HiveNetworkImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HiveNetworkImplToJson(
      this,
    );
  }
}

abstract class _HiveNetwork extends HiveNetwork {
  const factory _HiveNetwork(
      {required final String id,
      @JsonKey(name: "is_hiive") required final bool isHiive,
      @JsonKey(name: "last_reading") required final ReadingNetwork? lastReading,
      required final String name,
      required final String? sensor}) = _$HiveNetworkImpl;
  const _HiveNetwork._() : super._();

  factory _HiveNetwork.fromJson(Map<String, dynamic> json) =
      _$HiveNetworkImpl.fromJson;

  @override
  String get id;
  @override
  @JsonKey(name: "is_hiive")
  bool get isHiive;
  @override
  @JsonKey(name: "last_reading")
  ReadingNetwork? get lastReading;
  @override
  String get name;
  @override
  String? get sensor;

  /// Create a copy of HiveNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$HiveNetworkImplCopyWith<_$HiveNetworkImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

HiveStorage _$HiveStorageFromJson(Map<String, dynamic> json) {
  return _HiveStorage.fromJson(json);
}

/// @nodoc
mixin _$HiveStorage {
  String get id => throw _privateConstructorUsedError;
  @JsonKey(name: "is_hiive")
  bool get isHiive => throw _privateConstructorUsedError;
  @JsonKey(name: "last_reading")
  ReadingStorage? get lastReading => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String? get sensor => throw _privateConstructorUsedError;

  /// Serializes this HiveStorage to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of HiveStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $HiveStorageCopyWith<HiveStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HiveStorageCopyWith<$Res> {
  factory $HiveStorageCopyWith(
          HiveStorage value, $Res Function(HiveStorage) then) =
      _$HiveStorageCopyWithImpl<$Res, HiveStorage>;
  @useResult
  $Res call(
      {String id,
      @JsonKey(name: "is_hiive") bool isHiive,
      @JsonKey(name: "last_reading") ReadingStorage? lastReading,
      String name,
      String? sensor});

  $ReadingStorageCopyWith<$Res>? get lastReading;
}

/// @nodoc
class _$HiveStorageCopyWithImpl<$Res, $Val extends HiveStorage>
    implements $HiveStorageCopyWith<$Res> {
  _$HiveStorageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of HiveStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? isHiive = null,
    Object? lastReading = freezed,
    Object? name = null,
    Object? sensor = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isHiive: null == isHiive
          ? _value.isHiive
          : isHiive // ignore: cast_nullable_to_non_nullable
              as bool,
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as ReadingStorage?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sensor: freezed == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  /// Create a copy of HiveStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $ReadingStorageCopyWith<$Res>? get lastReading {
    if (_value.lastReading == null) {
      return null;
    }

    return $ReadingStorageCopyWith<$Res>(_value.lastReading!, (value) {
      return _then(_value.copyWith(lastReading: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$HiveStorageImplCopyWith<$Res>
    implements $HiveStorageCopyWith<$Res> {
  factory _$$HiveStorageImplCopyWith(
          _$HiveStorageImpl value, $Res Function(_$HiveStorageImpl) then) =
      __$$HiveStorageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      @JsonKey(name: "is_hiive") bool isHiive,
      @JsonKey(name: "last_reading") ReadingStorage? lastReading,
      String name,
      String? sensor});

  @override
  $ReadingStorageCopyWith<$Res>? get lastReading;
}

/// @nodoc
class __$$HiveStorageImplCopyWithImpl<$Res>
    extends _$HiveStorageCopyWithImpl<$Res, _$HiveStorageImpl>
    implements _$$HiveStorageImplCopyWith<$Res> {
  __$$HiveStorageImplCopyWithImpl(
      _$HiveStorageImpl _value, $Res Function(_$HiveStorageImpl) _then)
      : super(_value, _then);

  /// Create a copy of HiveStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? isHiive = null,
    Object? lastReading = freezed,
    Object? name = null,
    Object? sensor = freezed,
  }) {
    return _then(_$HiveStorageImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      isHiive: null == isHiive
          ? _value.isHiive
          : isHiive // ignore: cast_nullable_to_non_nullable
              as bool,
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as ReadingStorage?,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sensor: freezed == sensor
          ? _value.sensor
          : sensor // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$HiveStorageImpl extends _HiveStorage {
  _$HiveStorageImpl(
      {required this.id,
      @JsonKey(name: "is_hiive") required this.isHiive,
      @JsonKey(name: "last_reading") required this.lastReading,
      required this.name,
      required this.sensor})
      : super._();

  factory _$HiveStorageImpl.fromJson(Map<String, dynamic> json) =>
      _$$HiveStorageImplFromJson(json);

  @override
  final String id;
  @override
  @JsonKey(name: "is_hiive")
  final bool isHiive;
  @override
  @JsonKey(name: "last_reading")
  final ReadingStorage? lastReading;
  @override
  final String name;
  @override
  final String? sensor;

  @override
  String toString() {
    return 'HiveStorage(id: $id, isHiive: $isHiive, lastReading: $lastReading, name: $name, sensor: $sensor)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$HiveStorageImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.isHiive, isHiive) || other.isHiive == isHiive) &&
            (identical(other.lastReading, lastReading) ||
                other.lastReading == lastReading) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sensor, sensor) || other.sensor == sensor));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, isHiive, lastReading, name, sensor);

  /// Create a copy of HiveStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$HiveStorageImplCopyWith<_$HiveStorageImpl> get copyWith =>
      __$$HiveStorageImplCopyWithImpl<_$HiveStorageImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$HiveStorageImplToJson(
      this,
    );
  }
}

abstract class _HiveStorage extends HiveStorage {
  factory _HiveStorage(
      {required final String id,
      @JsonKey(name: "is_hiive") required final bool isHiive,
      @JsonKey(name: "last_reading") required final ReadingStorage? lastReading,
      required final String name,
      required final String? sensor}) = _$HiveStorageImpl;
  _HiveStorage._() : super._();

  factory _HiveStorage.fromJson(Map<String, dynamic> json) =
      _$HiveStorageImpl.fromJson;

  @override
  String get id;
  @override
  @JsonKey(name: "is_hiive")
  bool get isHiive;
  @override
  @JsonKey(name: "last_reading")
  ReadingStorage? get lastReading;
  @override
  String get name;
  @override
  String? get sensor;

  /// Create a copy of HiveStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$HiveStorageImplCopyWith<_$HiveStorageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
