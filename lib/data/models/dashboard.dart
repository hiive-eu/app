import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/models/sensor.dart';
import 'package:hiive/data/models/user.dart';

part "dashboard.freezed.dart";
part "dashboard.g.dart";

@freezed
class Dashboard with _$Dashboard {
  const factory Dashboard({
    required List<Gateway> gateways,
    required List<Hive> hives,
    required List<Sensor> sensors,
    required User? user,
  }) = _Dashboard;
}

@freezed
class DashboardNetwork with _$DashboardNetwork {
  const DashboardNetwork._();

  const factory DashboardNetwork({
    required List<GatewayNetwork> gateways,
    required List<HiveNetwork> hives,
    required List<SensorNetwork> sensors,
    required UserNetwork user,
  }) = _DashboardNetwork;

  Dashboard toDashboard() => Dashboard(
        gateways: this.gateways.map((i) => i.toGateway()).toList(),
        hives: this.hives.map((i) => i.toHive()).toList(),
        sensors: this.sensors.map((i) => i.toSensor()).toList(),
        user: this.user.toUser(),
      );

  factory DashboardNetwork.fromJson(Map<String, dynamic> json) =>
      _$DashboardNetworkFromJson(json);
}
