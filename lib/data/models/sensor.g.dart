// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SensorImpl _$$SensorImplFromJson(Map<String, dynamic> json) => _$SensorImpl(
      battery: (json['battery'] as num).toInt(),
      gateway: json['gateway'] as String?,
      mac: json['mac'] as String,
      rssi: (json['rssi'] as num).toInt(),
      status: $enumDecode(_$SensorStatusEnumMap, json['status']),
    );

Map<String, dynamic> _$$SensorImplToJson(_$SensorImpl instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'gateway': instance.gateway,
      'mac': instance.mac,
      'rssi': instance.rssi,
      'status': _$SensorStatusEnumMap[instance.status]!,
    };

const _$SensorStatusEnumMap = {
  SensorStatus.active: 'active',
  SensorStatus.inactive: 'inactive',
};

_$SensorNetworkImpl _$$SensorNetworkImplFromJson(Map<String, dynamic> json) =>
    _$SensorNetworkImpl(
      battery: (json['battery'] as num).toInt(),
      gateway: json['gateway'] as String?,
      rssi: (json['rssi'] as num).toInt(),
      mac: json['mac'] as String,
      status: $enumDecode(_$SensorStatusEnumMap, json['status']),
    );

Map<String, dynamic> _$$SensorNetworkImplToJson(_$SensorNetworkImpl instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'gateway': instance.gateway,
      'rssi': instance.rssi,
      'mac': instance.mac,
      'status': _$SensorStatusEnumMap[instance.status]!,
    };

_$SensorStorageImpl _$$SensorStorageImplFromJson(Map<String, dynamic> json) =>
    _$SensorStorageImpl(
      battery: (json['battery'] as num).toInt(),
      gateway: json['gateway'] as String?,
      mac: json['mac'] as String,
      rssi: (json['rssi'] as num).toInt(),
      status: $enumDecode(_$SensorStatusEnumMap, json['status']),
    );

Map<String, dynamic> _$$SensorStorageImplToJson(_$SensorStorageImpl instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'gateway': instance.gateway,
      'mac': instance.mac,
      'rssi': instance.rssi,
      'status': _$SensorStatusEnumMap[instance.status]!,
    };
