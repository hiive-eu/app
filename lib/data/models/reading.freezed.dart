// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'reading.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Reading _$ReadingFromJson(Map<String, dynamic> json) {
  return _Reading.fromJson(json);
}

/// @nodoc
mixin _$Reading {
  List<int> get frequencyBins => throw _privateConstructorUsedError;
  int get temperature => throw _privateConstructorUsedError;
  int get humidity => throw _privateConstructorUsedError;
  int get soundLevel => throw _privateConstructorUsedError;
  DateTime get time => throw _privateConstructorUsedError;

  /// Serializes this Reading to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of Reading
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadingCopyWith<Reading> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadingCopyWith<$Res> {
  factory $ReadingCopyWith(Reading value, $Res Function(Reading) then) =
      _$ReadingCopyWithImpl<$Res, Reading>;
  @useResult
  $Res call(
      {List<int> frequencyBins,
      int temperature,
      int humidity,
      int soundLevel,
      DateTime time});
}

/// @nodoc
class _$ReadingCopyWithImpl<$Res, $Val extends Reading>
    implements $ReadingCopyWith<$Res> {
  _$ReadingCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Reading
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? frequencyBins = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? soundLevel = null,
    Object? time = null,
  }) {
    return _then(_value.copyWith(
      frequencyBins: null == frequencyBins
          ? _value.frequencyBins
          : frequencyBins // ignore: cast_nullable_to_non_nullable
              as List<int>,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      soundLevel: null == soundLevel
          ? _value.soundLevel
          : soundLevel // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadingImplCopyWith<$Res> implements $ReadingCopyWith<$Res> {
  factory _$$ReadingImplCopyWith(
          _$ReadingImpl value, $Res Function(_$ReadingImpl) then) =
      __$$ReadingImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {List<int> frequencyBins,
      int temperature,
      int humidity,
      int soundLevel,
      DateTime time});
}

/// @nodoc
class __$$ReadingImplCopyWithImpl<$Res>
    extends _$ReadingCopyWithImpl<$Res, _$ReadingImpl>
    implements _$$ReadingImplCopyWith<$Res> {
  __$$ReadingImplCopyWithImpl(
      _$ReadingImpl _value, $Res Function(_$ReadingImpl) _then)
      : super(_value, _then);

  /// Create a copy of Reading
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? frequencyBins = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? soundLevel = null,
    Object? time = null,
  }) {
    return _then(_$ReadingImpl(
      frequencyBins: null == frequencyBins
          ? _value._frequencyBins
          : frequencyBins // ignore: cast_nullable_to_non_nullable
              as List<int>,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      soundLevel: null == soundLevel
          ? _value.soundLevel
          : soundLevel // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReadingImpl extends _Reading with DiagnosticableTreeMixin {
  _$ReadingImpl(
      {required final List<int> frequencyBins,
      required this.temperature,
      required this.humidity,
      required this.soundLevel,
      required this.time})
      : _frequencyBins = frequencyBins,
        super._();

  factory _$ReadingImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadingImplFromJson(json);

  final List<int> _frequencyBins;
  @override
  List<int> get frequencyBins {
    if (_frequencyBins is EqualUnmodifiableListView) return _frequencyBins;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_frequencyBins);
  }

  @override
  final int temperature;
  @override
  final int humidity;
  @override
  final int soundLevel;
  @override
  final DateTime time;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Reading(frequencyBins: $frequencyBins, temperature: $temperature, humidity: $humidity, soundLevel: $soundLevel, time: $time)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Reading'))
      ..add(DiagnosticsProperty('frequencyBins', frequencyBins))
      ..add(DiagnosticsProperty('temperature', temperature))
      ..add(DiagnosticsProperty('humidity', humidity))
      ..add(DiagnosticsProperty('soundLevel', soundLevel))
      ..add(DiagnosticsProperty('time', time));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadingImpl &&
            const DeepCollectionEquality()
                .equals(other._frequencyBins, _frequencyBins) &&
            (identical(other.temperature, temperature) ||
                other.temperature == temperature) &&
            (identical(other.humidity, humidity) ||
                other.humidity == humidity) &&
            (identical(other.soundLevel, soundLevel) ||
                other.soundLevel == soundLevel) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_frequencyBins),
      temperature,
      humidity,
      soundLevel,
      time);

  /// Create a copy of Reading
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadingImplCopyWith<_$ReadingImpl> get copyWith =>
      __$$ReadingImplCopyWithImpl<_$ReadingImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadingImplToJson(
      this,
    );
  }
}

abstract class _Reading extends Reading {
  factory _Reading(
      {required final List<int> frequencyBins,
      required final int temperature,
      required final int humidity,
      required final int soundLevel,
      required final DateTime time}) = _$ReadingImpl;
  _Reading._() : super._();

  factory _Reading.fromJson(Map<String, dynamic> json) = _$ReadingImpl.fromJson;

  @override
  List<int> get frequencyBins;
  @override
  int get temperature;
  @override
  int get humidity;
  @override
  int get soundLevel;
  @override
  DateTime get time;

  /// Create a copy of Reading
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadingImplCopyWith<_$ReadingImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

ReadingNetwork _$ReadingNetworkFromJson(Map<String, dynamic> json) {
  return _ReadingNetwork.fromJson(json);
}

/// @nodoc
mixin _$ReadingNetwork {
  @JsonKey(name: 'frequency_bins')
  List<int> get frequencyBins => throw _privateConstructorUsedError;
  int get temperature => throw _privateConstructorUsedError;
  int get humidity => throw _privateConstructorUsedError;
  @JsonKey(name: 'sound_level')
  int get soundLevel => throw _privateConstructorUsedError;
  DateTime get time => throw _privateConstructorUsedError;

  /// Serializes this ReadingNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of ReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadingNetworkCopyWith<ReadingNetwork> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadingNetworkCopyWith<$Res> {
  factory $ReadingNetworkCopyWith(
          ReadingNetwork value, $Res Function(ReadingNetwork) then) =
      _$ReadingNetworkCopyWithImpl<$Res, ReadingNetwork>;
  @useResult
  $Res call(
      {@JsonKey(name: 'frequency_bins') List<int> frequencyBins,
      int temperature,
      int humidity,
      @JsonKey(name: 'sound_level') int soundLevel,
      DateTime time});
}

/// @nodoc
class _$ReadingNetworkCopyWithImpl<$Res, $Val extends ReadingNetwork>
    implements $ReadingNetworkCopyWith<$Res> {
  _$ReadingNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? frequencyBins = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? soundLevel = null,
    Object? time = null,
  }) {
    return _then(_value.copyWith(
      frequencyBins: null == frequencyBins
          ? _value.frequencyBins
          : frequencyBins // ignore: cast_nullable_to_non_nullable
              as List<int>,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      soundLevel: null == soundLevel
          ? _value.soundLevel
          : soundLevel // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadingNetworkImplCopyWith<$Res>
    implements $ReadingNetworkCopyWith<$Res> {
  factory _$$ReadingNetworkImplCopyWith(_$ReadingNetworkImpl value,
          $Res Function(_$ReadingNetworkImpl) then) =
      __$$ReadingNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'frequency_bins') List<int> frequencyBins,
      int temperature,
      int humidity,
      @JsonKey(name: 'sound_level') int soundLevel,
      DateTime time});
}

/// @nodoc
class __$$ReadingNetworkImplCopyWithImpl<$Res>
    extends _$ReadingNetworkCopyWithImpl<$Res, _$ReadingNetworkImpl>
    implements _$$ReadingNetworkImplCopyWith<$Res> {
  __$$ReadingNetworkImplCopyWithImpl(
      _$ReadingNetworkImpl _value, $Res Function(_$ReadingNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of ReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? frequencyBins = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? soundLevel = null,
    Object? time = null,
  }) {
    return _then(_$ReadingNetworkImpl(
      frequencyBins: null == frequencyBins
          ? _value._frequencyBins
          : frequencyBins // ignore: cast_nullable_to_non_nullable
              as List<int>,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      soundLevel: null == soundLevel
          ? _value.soundLevel
          : soundLevel // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReadingNetworkImpl extends _ReadingNetwork
    with DiagnosticableTreeMixin {
  _$ReadingNetworkImpl(
      {@JsonKey(name: 'frequency_bins') required final List<int> frequencyBins,
      required this.temperature,
      required this.humidity,
      @JsonKey(name: 'sound_level') required this.soundLevel,
      required this.time})
      : _frequencyBins = frequencyBins,
        super._();

  factory _$ReadingNetworkImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadingNetworkImplFromJson(json);

  final List<int> _frequencyBins;
  @override
  @JsonKey(name: 'frequency_bins')
  List<int> get frequencyBins {
    if (_frequencyBins is EqualUnmodifiableListView) return _frequencyBins;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_frequencyBins);
  }

  @override
  final int temperature;
  @override
  final int humidity;
  @override
  @JsonKey(name: 'sound_level')
  final int soundLevel;
  @override
  final DateTime time;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ReadingNetwork(frequencyBins: $frequencyBins, temperature: $temperature, humidity: $humidity, soundLevel: $soundLevel, time: $time)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ReadingNetwork'))
      ..add(DiagnosticsProperty('frequencyBins', frequencyBins))
      ..add(DiagnosticsProperty('temperature', temperature))
      ..add(DiagnosticsProperty('humidity', humidity))
      ..add(DiagnosticsProperty('soundLevel', soundLevel))
      ..add(DiagnosticsProperty('time', time));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadingNetworkImpl &&
            const DeepCollectionEquality()
                .equals(other._frequencyBins, _frequencyBins) &&
            (identical(other.temperature, temperature) ||
                other.temperature == temperature) &&
            (identical(other.humidity, humidity) ||
                other.humidity == humidity) &&
            (identical(other.soundLevel, soundLevel) ||
                other.soundLevel == soundLevel) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_frequencyBins),
      temperature,
      humidity,
      soundLevel,
      time);

  /// Create a copy of ReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadingNetworkImplCopyWith<_$ReadingNetworkImpl> get copyWith =>
      __$$ReadingNetworkImplCopyWithImpl<_$ReadingNetworkImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadingNetworkImplToJson(
      this,
    );
  }
}

abstract class _ReadingNetwork extends ReadingNetwork {
  factory _ReadingNetwork(
      {@JsonKey(name: 'frequency_bins') required final List<int> frequencyBins,
      required final int temperature,
      required final int humidity,
      @JsonKey(name: 'sound_level') required final int soundLevel,
      required final DateTime time}) = _$ReadingNetworkImpl;
  _ReadingNetwork._() : super._();

  factory _ReadingNetwork.fromJson(Map<String, dynamic> json) =
      _$ReadingNetworkImpl.fromJson;

  @override
  @JsonKey(name: 'frequency_bins')
  List<int> get frequencyBins;
  @override
  int get temperature;
  @override
  int get humidity;
  @override
  @JsonKey(name: 'sound_level')
  int get soundLevel;
  @override
  DateTime get time;

  /// Create a copy of ReadingNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadingNetworkImplCopyWith<_$ReadingNetworkImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

ReadingStorage _$ReadingStorageFromJson(Map<String, dynamic> json) {
  return _ReadingStorage.fromJson(json);
}

/// @nodoc
mixin _$ReadingStorage {
  @JsonKey(name: 'frequency_bins')
  List<int> get frequencyBins => throw _privateConstructorUsedError;
  int get temperature => throw _privateConstructorUsedError;
  int get humidity => throw _privateConstructorUsedError;
  @JsonKey(name: 'sound_level')
  int get soundLevel => throw _privateConstructorUsedError;
  DateTime get time => throw _privateConstructorUsedError;

  /// Serializes this ReadingStorage to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of ReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $ReadingStorageCopyWith<ReadingStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReadingStorageCopyWith<$Res> {
  factory $ReadingStorageCopyWith(
          ReadingStorage value, $Res Function(ReadingStorage) then) =
      _$ReadingStorageCopyWithImpl<$Res, ReadingStorage>;
  @useResult
  $Res call(
      {@JsonKey(name: 'frequency_bins') List<int> frequencyBins,
      int temperature,
      int humidity,
      @JsonKey(name: 'sound_level') int soundLevel,
      DateTime time});
}

/// @nodoc
class _$ReadingStorageCopyWithImpl<$Res, $Val extends ReadingStorage>
    implements $ReadingStorageCopyWith<$Res> {
  _$ReadingStorageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of ReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? frequencyBins = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? soundLevel = null,
    Object? time = null,
  }) {
    return _then(_value.copyWith(
      frequencyBins: null == frequencyBins
          ? _value.frequencyBins
          : frequencyBins // ignore: cast_nullable_to_non_nullable
              as List<int>,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      soundLevel: null == soundLevel
          ? _value.soundLevel
          : soundLevel // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReadingStorageImplCopyWith<$Res>
    implements $ReadingStorageCopyWith<$Res> {
  factory _$$ReadingStorageImplCopyWith(_$ReadingStorageImpl value,
          $Res Function(_$ReadingStorageImpl) then) =
      __$$ReadingStorageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: 'frequency_bins') List<int> frequencyBins,
      int temperature,
      int humidity,
      @JsonKey(name: 'sound_level') int soundLevel,
      DateTime time});
}

/// @nodoc
class __$$ReadingStorageImplCopyWithImpl<$Res>
    extends _$ReadingStorageCopyWithImpl<$Res, _$ReadingStorageImpl>
    implements _$$ReadingStorageImplCopyWith<$Res> {
  __$$ReadingStorageImplCopyWithImpl(
      _$ReadingStorageImpl _value, $Res Function(_$ReadingStorageImpl) _then)
      : super(_value, _then);

  /// Create a copy of ReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? frequencyBins = null,
    Object? temperature = null,
    Object? humidity = null,
    Object? soundLevel = null,
    Object? time = null,
  }) {
    return _then(_$ReadingStorageImpl(
      frequencyBins: null == frequencyBins
          ? _value._frequencyBins
          : frequencyBins // ignore: cast_nullable_to_non_nullable
              as List<int>,
      temperature: null == temperature
          ? _value.temperature
          : temperature // ignore: cast_nullable_to_non_nullable
              as int,
      humidity: null == humidity
          ? _value.humidity
          : humidity // ignore: cast_nullable_to_non_nullable
              as int,
      soundLevel: null == soundLevel
          ? _value.soundLevel
          : soundLevel // ignore: cast_nullable_to_non_nullable
              as int,
      time: null == time
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReadingStorageImpl extends _ReadingStorage
    with DiagnosticableTreeMixin {
  _$ReadingStorageImpl(
      {@JsonKey(name: 'frequency_bins') required final List<int> frequencyBins,
      required this.temperature,
      required this.humidity,
      @JsonKey(name: 'sound_level') required this.soundLevel,
      required this.time})
      : _frequencyBins = frequencyBins,
        super._();

  factory _$ReadingStorageImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReadingStorageImplFromJson(json);

  final List<int> _frequencyBins;
  @override
  @JsonKey(name: 'frequency_bins')
  List<int> get frequencyBins {
    if (_frequencyBins is EqualUnmodifiableListView) return _frequencyBins;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_frequencyBins);
  }

  @override
  final int temperature;
  @override
  final int humidity;
  @override
  @JsonKey(name: 'sound_level')
  final int soundLevel;
  @override
  final DateTime time;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ReadingStorage(frequencyBins: $frequencyBins, temperature: $temperature, humidity: $humidity, soundLevel: $soundLevel, time: $time)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ReadingStorage'))
      ..add(DiagnosticsProperty('frequencyBins', frequencyBins))
      ..add(DiagnosticsProperty('temperature', temperature))
      ..add(DiagnosticsProperty('humidity', humidity))
      ..add(DiagnosticsProperty('soundLevel', soundLevel))
      ..add(DiagnosticsProperty('time', time));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadingStorageImpl &&
            const DeepCollectionEquality()
                .equals(other._frequencyBins, _frequencyBins) &&
            (identical(other.temperature, temperature) ||
                other.temperature == temperature) &&
            (identical(other.humidity, humidity) ||
                other.humidity == humidity) &&
            (identical(other.soundLevel, soundLevel) ||
                other.soundLevel == soundLevel) &&
            (identical(other.time, time) || other.time == time));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_frequencyBins),
      temperature,
      humidity,
      soundLevel,
      time);

  /// Create a copy of ReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadingStorageImplCopyWith<_$ReadingStorageImpl> get copyWith =>
      __$$ReadingStorageImplCopyWithImpl<_$ReadingStorageImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReadingStorageImplToJson(
      this,
    );
  }
}

abstract class _ReadingStorage extends ReadingStorage {
  factory _ReadingStorage(
      {@JsonKey(name: 'frequency_bins') required final List<int> frequencyBins,
      required final int temperature,
      required final int humidity,
      @JsonKey(name: 'sound_level') required final int soundLevel,
      required final DateTime time}) = _$ReadingStorageImpl;
  _ReadingStorage._() : super._();

  factory _ReadingStorage.fromJson(Map<String, dynamic> json) =
      _$ReadingStorageImpl.fromJson;

  @override
  @JsonKey(name: 'frequency_bins')
  List<int> get frequencyBins;
  @override
  int get temperature;
  @override
  int get humidity;
  @override
  @JsonKey(name: 'sound_level')
  int get soundLevel;
  @override
  DateTime get time;

  /// Create a copy of ReadingStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadingStorageImplCopyWith<_$ReadingStorageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
