// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway_reading.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$GatewayReadingImpl _$$GatewayReadingImplFromJson(Map<String, dynamic> json) =>
    _$GatewayReadingImpl(
      battery: (json['battery'] as num).toInt(),
      temperature: (json['temperature'] as num).toInt(),
      humidity: (json['humidity'] as num).toInt(),
      time: DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$$GatewayReadingImplToJson(
        _$GatewayReadingImpl instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
      'time': instance.time.toIso8601String(),
    };

_$GatewayReadingNetworkImpl _$$GatewayReadingNetworkImplFromJson(
        Map<String, dynamic> json) =>
    _$GatewayReadingNetworkImpl(
      battery: (json['battery'] as num).toInt(),
      temperature: (json['temperature'] as num).toInt(),
      humidity: (json['humidity'] as num).toInt(),
      time: DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$$GatewayReadingNetworkImplToJson(
        _$GatewayReadingNetworkImpl instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
      'time': instance.time.toIso8601String(),
    };

_$GatewayReadingStorageImpl _$$GatewayReadingStorageImplFromJson(
        Map<String, dynamic> json) =>
    _$GatewayReadingStorageImpl(
      battery: (json['battery'] as num).toInt(),
      temperature: (json['temperature'] as num).toInt(),
      humidity: (json['humidity'] as num).toInt(),
      time: DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$$GatewayReadingStorageImplToJson(
        _$GatewayReadingStorageImpl instance) =>
    <String, dynamic>{
      'battery': instance.battery,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
      'time': instance.time.toIso8601String(),
    };
