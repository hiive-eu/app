// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'gateway.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

Gateway _$GatewayFromJson(Map<String, dynamic> json) {
  return _Gateway.fromJson(json);
}

/// @nodoc
mixin _$Gateway {
  GatewayReading? get lastReading => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  int? get sdSpaceUsed => throw _privateConstructorUsedError;
  int? get sdStatus => throw _privateConstructorUsedError;
  GatewayStatus get status => throw _privateConstructorUsedError;
  String? get token => throw _privateConstructorUsedError;
  int? get wifiRssi => throw _privateConstructorUsedError;

  /// Serializes this Gateway to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of Gateway
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayCopyWith<Gateway> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayCopyWith<$Res> {
  factory $GatewayCopyWith(Gateway value, $Res Function(Gateway) then) =
      _$GatewayCopyWithImpl<$Res, Gateway>;
  @useResult
  $Res call(
      {GatewayReading? lastReading,
      String mac,
      String name,
      int? sdSpaceUsed,
      int? sdStatus,
      GatewayStatus status,
      String? token,
      int? wifiRssi});

  $GatewayReadingCopyWith<$Res>? get lastReading;
}

/// @nodoc
class _$GatewayCopyWithImpl<$Res, $Val extends Gateway>
    implements $GatewayCopyWith<$Res> {
  _$GatewayCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Gateway
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lastReading = freezed,
    Object? mac = null,
    Object? name = null,
    Object? sdSpaceUsed = freezed,
    Object? sdStatus = freezed,
    Object? status = null,
    Object? token = freezed,
    Object? wifiRssi = freezed,
  }) {
    return _then(_value.copyWith(
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as GatewayReading?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sdSpaceUsed: freezed == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int?,
      sdStatus: freezed == sdStatus
          ? _value.sdStatus
          : sdStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GatewayStatus,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      wifiRssi: freezed == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }

  /// Create a copy of Gateway
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayReadingCopyWith<$Res>? get lastReading {
    if (_value.lastReading == null) {
      return null;
    }

    return $GatewayReadingCopyWith<$Res>(_value.lastReading!, (value) {
      return _then(_value.copyWith(lastReading: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$GatewayImplCopyWith<$Res> implements $GatewayCopyWith<$Res> {
  factory _$$GatewayImplCopyWith(
          _$GatewayImpl value, $Res Function(_$GatewayImpl) then) =
      __$$GatewayImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {GatewayReading? lastReading,
      String mac,
      String name,
      int? sdSpaceUsed,
      int? sdStatus,
      GatewayStatus status,
      String? token,
      int? wifiRssi});

  @override
  $GatewayReadingCopyWith<$Res>? get lastReading;
}

/// @nodoc
class __$$GatewayImplCopyWithImpl<$Res>
    extends _$GatewayCopyWithImpl<$Res, _$GatewayImpl>
    implements _$$GatewayImplCopyWith<$Res> {
  __$$GatewayImplCopyWithImpl(
      _$GatewayImpl _value, $Res Function(_$GatewayImpl) _then)
      : super(_value, _then);

  /// Create a copy of Gateway
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lastReading = freezed,
    Object? mac = null,
    Object? name = null,
    Object? sdSpaceUsed = freezed,
    Object? sdStatus = freezed,
    Object? status = null,
    Object? token = freezed,
    Object? wifiRssi = freezed,
  }) {
    return _then(_$GatewayImpl(
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as GatewayReading?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sdSpaceUsed: freezed == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int?,
      sdStatus: freezed == sdStatus
          ? _value.sdStatus
          : sdStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GatewayStatus,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      wifiRssi: freezed == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayImpl extends _Gateway with DiagnosticableTreeMixin {
  _$GatewayImpl(
      {required this.lastReading,
      required this.mac,
      required this.name,
      required this.sdSpaceUsed,
      required this.sdStatus,
      required this.status,
      required this.token,
      required this.wifiRssi})
      : super._();

  factory _$GatewayImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayImplFromJson(json);

  @override
  final GatewayReading? lastReading;
  @override
  final String mac;
  @override
  final String name;
  @override
  final int? sdSpaceUsed;
  @override
  final int? sdStatus;
  @override
  final GatewayStatus status;
  @override
  final String? token;
  @override
  final int? wifiRssi;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Gateway(lastReading: $lastReading, mac: $mac, name: $name, sdSpaceUsed: $sdSpaceUsed, sdStatus: $sdStatus, status: $status, token: $token, wifiRssi: $wifiRssi)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Gateway'))
      ..add(DiagnosticsProperty('lastReading', lastReading))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('sdSpaceUsed', sdSpaceUsed))
      ..add(DiagnosticsProperty('sdStatus', sdStatus))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('token', token))
      ..add(DiagnosticsProperty('wifiRssi', wifiRssi));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayImpl &&
            (identical(other.lastReading, lastReading) ||
                other.lastReading == lastReading) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sdSpaceUsed, sdSpaceUsed) ||
                other.sdSpaceUsed == sdSpaceUsed) &&
            (identical(other.sdStatus, sdStatus) ||
                other.sdStatus == sdStatus) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.wifiRssi, wifiRssi) ||
                other.wifiRssi == wifiRssi));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, lastReading, mac, name,
      sdSpaceUsed, sdStatus, status, token, wifiRssi);

  /// Create a copy of Gateway
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayImplCopyWith<_$GatewayImpl> get copyWith =>
      __$$GatewayImplCopyWithImpl<_$GatewayImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayImplToJson(
      this,
    );
  }
}

abstract class _Gateway extends Gateway {
  factory _Gateway(
      {required final GatewayReading? lastReading,
      required final String mac,
      required final String name,
      required final int? sdSpaceUsed,
      required final int? sdStatus,
      required final GatewayStatus status,
      required final String? token,
      required final int? wifiRssi}) = _$GatewayImpl;
  _Gateway._() : super._();

  factory _Gateway.fromJson(Map<String, dynamic> json) = _$GatewayImpl.fromJson;

  @override
  GatewayReading? get lastReading;
  @override
  String get mac;
  @override
  String get name;
  @override
  int? get sdSpaceUsed;
  @override
  int? get sdStatus;
  @override
  GatewayStatus get status;
  @override
  String? get token;
  @override
  int? get wifiRssi;

  /// Create a copy of Gateway
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayImplCopyWith<_$GatewayImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

GatewayNetwork _$GatewayNetworkFromJson(Map<String, dynamic> json) {
  return _GatewayNetwork.fromJson(json);
}

/// @nodoc
mixin _$GatewayNetwork {
  @JsonKey(name: "last_reading")
  GatewayReading? get lastReading => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  @JsonKey(name: "sd_space_used")
  int? get sdSpaceUsed => throw _privateConstructorUsedError;
  @JsonKey(name: "sd_status")
  int? get sdStatus => throw _privateConstructorUsedError;
  GatewayStatus get status => throw _privateConstructorUsedError;
  String get token => throw _privateConstructorUsedError;
  @JsonKey(name: "wifi_rssi")
  int? get wifiRssi => throw _privateConstructorUsedError;

  /// Serializes this GatewayNetwork to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayNetworkCopyWith<GatewayNetwork> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayNetworkCopyWith<$Res> {
  factory $GatewayNetworkCopyWith(
          GatewayNetwork value, $Res Function(GatewayNetwork) then) =
      _$GatewayNetworkCopyWithImpl<$Res, GatewayNetwork>;
  @useResult
  $Res call(
      {@JsonKey(name: "last_reading") GatewayReading? lastReading,
      String mac,
      String name,
      @JsonKey(name: "sd_space_used") int? sdSpaceUsed,
      @JsonKey(name: "sd_status") int? sdStatus,
      GatewayStatus status,
      String token,
      @JsonKey(name: "wifi_rssi") int? wifiRssi});

  $GatewayReadingCopyWith<$Res>? get lastReading;
}

/// @nodoc
class _$GatewayNetworkCopyWithImpl<$Res, $Val extends GatewayNetwork>
    implements $GatewayNetworkCopyWith<$Res> {
  _$GatewayNetworkCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lastReading = freezed,
    Object? mac = null,
    Object? name = null,
    Object? sdSpaceUsed = freezed,
    Object? sdStatus = freezed,
    Object? status = null,
    Object? token = null,
    Object? wifiRssi = freezed,
  }) {
    return _then(_value.copyWith(
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as GatewayReading?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sdSpaceUsed: freezed == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int?,
      sdStatus: freezed == sdStatus
          ? _value.sdStatus
          : sdStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GatewayStatus,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      wifiRssi: freezed == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }

  /// Create a copy of GatewayNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayReadingCopyWith<$Res>? get lastReading {
    if (_value.lastReading == null) {
      return null;
    }

    return $GatewayReadingCopyWith<$Res>(_value.lastReading!, (value) {
      return _then(_value.copyWith(lastReading: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$GatewayNetworkImplCopyWith<$Res>
    implements $GatewayNetworkCopyWith<$Res> {
  factory _$$GatewayNetworkImplCopyWith(_$GatewayNetworkImpl value,
          $Res Function(_$GatewayNetworkImpl) then) =
      __$$GatewayNetworkImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "last_reading") GatewayReading? lastReading,
      String mac,
      String name,
      @JsonKey(name: "sd_space_used") int? sdSpaceUsed,
      @JsonKey(name: "sd_status") int? sdStatus,
      GatewayStatus status,
      String token,
      @JsonKey(name: "wifi_rssi") int? wifiRssi});

  @override
  $GatewayReadingCopyWith<$Res>? get lastReading;
}

/// @nodoc
class __$$GatewayNetworkImplCopyWithImpl<$Res>
    extends _$GatewayNetworkCopyWithImpl<$Res, _$GatewayNetworkImpl>
    implements _$$GatewayNetworkImplCopyWith<$Res> {
  __$$GatewayNetworkImplCopyWithImpl(
      _$GatewayNetworkImpl _value, $Res Function(_$GatewayNetworkImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayNetwork
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lastReading = freezed,
    Object? mac = null,
    Object? name = null,
    Object? sdSpaceUsed = freezed,
    Object? sdStatus = freezed,
    Object? status = null,
    Object? token = null,
    Object? wifiRssi = freezed,
  }) {
    return _then(_$GatewayNetworkImpl(
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as GatewayReading?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sdSpaceUsed: freezed == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int?,
      sdStatus: freezed == sdStatus
          ? _value.sdStatus
          : sdStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GatewayStatus,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      wifiRssi: freezed == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayNetworkImpl extends _GatewayNetwork
    with DiagnosticableTreeMixin {
  const _$GatewayNetworkImpl(
      {@JsonKey(name: "last_reading") required this.lastReading,
      required this.mac,
      required this.name,
      @JsonKey(name: "sd_space_used") required this.sdSpaceUsed,
      @JsonKey(name: "sd_status") required this.sdStatus,
      required this.status,
      required this.token,
      @JsonKey(name: "wifi_rssi") required this.wifiRssi})
      : super._();

  factory _$GatewayNetworkImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayNetworkImplFromJson(json);

  @override
  @JsonKey(name: "last_reading")
  final GatewayReading? lastReading;
  @override
  final String mac;
  @override
  final String name;
  @override
  @JsonKey(name: "sd_space_used")
  final int? sdSpaceUsed;
  @override
  @JsonKey(name: "sd_status")
  final int? sdStatus;
  @override
  final GatewayStatus status;
  @override
  final String token;
  @override
  @JsonKey(name: "wifi_rssi")
  final int? wifiRssi;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GatewayNetwork(lastReading: $lastReading, mac: $mac, name: $name, sdSpaceUsed: $sdSpaceUsed, sdStatus: $sdStatus, status: $status, token: $token, wifiRssi: $wifiRssi)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'GatewayNetwork'))
      ..add(DiagnosticsProperty('lastReading', lastReading))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('sdSpaceUsed', sdSpaceUsed))
      ..add(DiagnosticsProperty('sdStatus', sdStatus))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('token', token))
      ..add(DiagnosticsProperty('wifiRssi', wifiRssi));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayNetworkImpl &&
            (identical(other.lastReading, lastReading) ||
                other.lastReading == lastReading) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sdSpaceUsed, sdSpaceUsed) ||
                other.sdSpaceUsed == sdSpaceUsed) &&
            (identical(other.sdStatus, sdStatus) ||
                other.sdStatus == sdStatus) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.wifiRssi, wifiRssi) ||
                other.wifiRssi == wifiRssi));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, lastReading, mac, name,
      sdSpaceUsed, sdStatus, status, token, wifiRssi);

  /// Create a copy of GatewayNetwork
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayNetworkImplCopyWith<_$GatewayNetworkImpl> get copyWith =>
      __$$GatewayNetworkImplCopyWithImpl<_$GatewayNetworkImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayNetworkImplToJson(
      this,
    );
  }
}

abstract class _GatewayNetwork extends GatewayNetwork {
  const factory _GatewayNetwork(
          {@JsonKey(name: "last_reading")
          required final GatewayReading? lastReading,
          required final String mac,
          required final String name,
          @JsonKey(name: "sd_space_used") required final int? sdSpaceUsed,
          @JsonKey(name: "sd_status") required final int? sdStatus,
          required final GatewayStatus status,
          required final String token,
          @JsonKey(name: "wifi_rssi") required final int? wifiRssi}) =
      _$GatewayNetworkImpl;
  const _GatewayNetwork._() : super._();

  factory _GatewayNetwork.fromJson(Map<String, dynamic> json) =
      _$GatewayNetworkImpl.fromJson;

  @override
  @JsonKey(name: "last_reading")
  GatewayReading? get lastReading;
  @override
  String get mac;
  @override
  String get name;
  @override
  @JsonKey(name: "sd_space_used")
  int? get sdSpaceUsed;
  @override
  @JsonKey(name: "sd_status")
  int? get sdStatus;
  @override
  GatewayStatus get status;
  @override
  String get token;
  @override
  @JsonKey(name: "wifi_rssi")
  int? get wifiRssi;

  /// Create a copy of GatewayNetwork
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayNetworkImplCopyWith<_$GatewayNetworkImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

GatewayStorage _$GatewayStorageFromJson(Map<String, dynamic> json) {
  return _GatewayStorage.fromJson(json);
}

/// @nodoc
mixin _$GatewayStorage {
  @JsonKey(name: "last_reading")
  GatewayReadingStorage? get lastReading => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  @JsonKey(name: "sd_space_used")
  int? get sdSpaceUsed => throw _privateConstructorUsedError;
  @JsonKey(name: "sd_status")
  int? get sdStatus => throw _privateConstructorUsedError;
  GatewayStatus get status => throw _privateConstructorUsedError;
  String? get token => throw _privateConstructorUsedError;
  @JsonKey(name: "wifi_rssi")
  int? get wifiRssi => throw _privateConstructorUsedError;

  /// Serializes this GatewayStorage to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of GatewayStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayStorageCopyWith<GatewayStorage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayStorageCopyWith<$Res> {
  factory $GatewayStorageCopyWith(
          GatewayStorage value, $Res Function(GatewayStorage) then) =
      _$GatewayStorageCopyWithImpl<$Res, GatewayStorage>;
  @useResult
  $Res call(
      {@JsonKey(name: "last_reading") GatewayReadingStorage? lastReading,
      String mac,
      String name,
      @JsonKey(name: "sd_space_used") int? sdSpaceUsed,
      @JsonKey(name: "sd_status") int? sdStatus,
      GatewayStatus status,
      String? token,
      @JsonKey(name: "wifi_rssi") int? wifiRssi});

  $GatewayReadingStorageCopyWith<$Res>? get lastReading;
}

/// @nodoc
class _$GatewayStorageCopyWithImpl<$Res, $Val extends GatewayStorage>
    implements $GatewayStorageCopyWith<$Res> {
  _$GatewayStorageCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lastReading = freezed,
    Object? mac = null,
    Object? name = null,
    Object? sdSpaceUsed = freezed,
    Object? sdStatus = freezed,
    Object? status = null,
    Object? token = freezed,
    Object? wifiRssi = freezed,
  }) {
    return _then(_value.copyWith(
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as GatewayReadingStorage?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sdSpaceUsed: freezed == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int?,
      sdStatus: freezed == sdStatus
          ? _value.sdStatus
          : sdStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GatewayStatus,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      wifiRssi: freezed == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }

  /// Create a copy of GatewayStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayReadingStorageCopyWith<$Res>? get lastReading {
    if (_value.lastReading == null) {
      return null;
    }

    return $GatewayReadingStorageCopyWith<$Res>(_value.lastReading!, (value) {
      return _then(_value.copyWith(lastReading: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$GatewayStorageImplCopyWith<$Res>
    implements $GatewayStorageCopyWith<$Res> {
  factory _$$GatewayStorageImplCopyWith(_$GatewayStorageImpl value,
          $Res Function(_$GatewayStorageImpl) then) =
      __$$GatewayStorageImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@JsonKey(name: "last_reading") GatewayReadingStorage? lastReading,
      String mac,
      String name,
      @JsonKey(name: "sd_space_used") int? sdSpaceUsed,
      @JsonKey(name: "sd_status") int? sdStatus,
      GatewayStatus status,
      String? token,
      @JsonKey(name: "wifi_rssi") int? wifiRssi});

  @override
  $GatewayReadingStorageCopyWith<$Res>? get lastReading;
}

/// @nodoc
class __$$GatewayStorageImplCopyWithImpl<$Res>
    extends _$GatewayStorageCopyWithImpl<$Res, _$GatewayStorageImpl>
    implements _$$GatewayStorageImplCopyWith<$Res> {
  __$$GatewayStorageImplCopyWithImpl(
      _$GatewayStorageImpl _value, $Res Function(_$GatewayStorageImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayStorage
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? lastReading = freezed,
    Object? mac = null,
    Object? name = null,
    Object? sdSpaceUsed = freezed,
    Object? sdStatus = freezed,
    Object? status = null,
    Object? token = freezed,
    Object? wifiRssi = freezed,
  }) {
    return _then(_$GatewayStorageImpl(
      lastReading: freezed == lastReading
          ? _value.lastReading
          : lastReading // ignore: cast_nullable_to_non_nullable
              as GatewayReadingStorage?,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      sdSpaceUsed: freezed == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int?,
      sdStatus: freezed == sdStatus
          ? _value.sdStatus
          : sdStatus // ignore: cast_nullable_to_non_nullable
              as int?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as GatewayStatus,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      wifiRssi: freezed == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$GatewayStorageImpl extends _GatewayStorage
    with DiagnosticableTreeMixin {
  _$GatewayStorageImpl(
      {@JsonKey(name: "last_reading") required this.lastReading,
      required this.mac,
      required this.name,
      @JsonKey(name: "sd_space_used") required this.sdSpaceUsed,
      @JsonKey(name: "sd_status") required this.sdStatus,
      required this.status,
      required this.token,
      @JsonKey(name: "wifi_rssi") required this.wifiRssi})
      : super._();

  factory _$GatewayStorageImpl.fromJson(Map<String, dynamic> json) =>
      _$$GatewayStorageImplFromJson(json);

  @override
  @JsonKey(name: "last_reading")
  final GatewayReadingStorage? lastReading;
  @override
  final String mac;
  @override
  final String name;
  @override
  @JsonKey(name: "sd_space_used")
  final int? sdSpaceUsed;
  @override
  @JsonKey(name: "sd_status")
  final int? sdStatus;
  @override
  final GatewayStatus status;
  @override
  final String? token;
  @override
  @JsonKey(name: "wifi_rssi")
  final int? wifiRssi;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'GatewayStorage(lastReading: $lastReading, mac: $mac, name: $name, sdSpaceUsed: $sdSpaceUsed, sdStatus: $sdStatus, status: $status, token: $token, wifiRssi: $wifiRssi)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'GatewayStorage'))
      ..add(DiagnosticsProperty('lastReading', lastReading))
      ..add(DiagnosticsProperty('mac', mac))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('sdSpaceUsed', sdSpaceUsed))
      ..add(DiagnosticsProperty('sdStatus', sdStatus))
      ..add(DiagnosticsProperty('status', status))
      ..add(DiagnosticsProperty('token', token))
      ..add(DiagnosticsProperty('wifiRssi', wifiRssi));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayStorageImpl &&
            (identical(other.lastReading, lastReading) ||
                other.lastReading == lastReading) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.sdSpaceUsed, sdSpaceUsed) ||
                other.sdSpaceUsed == sdSpaceUsed) &&
            (identical(other.sdStatus, sdStatus) ||
                other.sdStatus == sdStatus) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.wifiRssi, wifiRssi) ||
                other.wifiRssi == wifiRssi));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, lastReading, mac, name,
      sdSpaceUsed, sdStatus, status, token, wifiRssi);

  /// Create a copy of GatewayStorage
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayStorageImplCopyWith<_$GatewayStorageImpl> get copyWith =>
      __$$GatewayStorageImplCopyWithImpl<_$GatewayStorageImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$GatewayStorageImplToJson(
      this,
    );
  }
}

abstract class _GatewayStorage extends GatewayStorage {
  factory _GatewayStorage(
          {@JsonKey(name: "last_reading")
          required final GatewayReadingStorage? lastReading,
          required final String mac,
          required final String name,
          @JsonKey(name: "sd_space_used") required final int? sdSpaceUsed,
          @JsonKey(name: "sd_status") required final int? sdStatus,
          required final GatewayStatus status,
          required final String? token,
          @JsonKey(name: "wifi_rssi") required final int? wifiRssi}) =
      _$GatewayStorageImpl;
  _GatewayStorage._() : super._();

  factory _GatewayStorage.fromJson(Map<String, dynamic> json) =
      _$GatewayStorageImpl.fromJson;

  @override
  @JsonKey(name: "last_reading")
  GatewayReadingStorage? get lastReading;
  @override
  String get mac;
  @override
  String get name;
  @override
  @JsonKey(name: "sd_space_used")
  int? get sdSpaceUsed;
  @override
  @JsonKey(name: "sd_status")
  int? get sdStatus;
  @override
  GatewayStatus get status;
  @override
  String? get token;
  @override
  @JsonKey(name: "wifi_rssi")
  int? get wifiRssi;

  /// Create a copy of GatewayStorage
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayStorageImplCopyWith<_$GatewayStorageImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
