// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'reading.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ReadingImpl _$$ReadingImplFromJson(Map<String, dynamic> json) =>
    _$ReadingImpl(
      frequencyBins: (json['frequencyBins'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
      temperature: (json['temperature'] as num).toInt(),
      humidity: (json['humidity'] as num).toInt(),
      soundLevel: (json['soundLevel'] as num).toInt(),
      time: DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$$ReadingImplToJson(_$ReadingImpl instance) =>
    <String, dynamic>{
      'frequencyBins': instance.frequencyBins,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
      'soundLevel': instance.soundLevel,
      'time': instance.time.toIso8601String(),
    };

_$ReadingNetworkImpl _$$ReadingNetworkImplFromJson(Map<String, dynamic> json) =>
    _$ReadingNetworkImpl(
      frequencyBins: (json['frequency_bins'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
      temperature: (json['temperature'] as num).toInt(),
      humidity: (json['humidity'] as num).toInt(),
      soundLevel: (json['sound_level'] as num).toInt(),
      time: DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$$ReadingNetworkImplToJson(
        _$ReadingNetworkImpl instance) =>
    <String, dynamic>{
      'frequency_bins': instance.frequencyBins,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
      'sound_level': instance.soundLevel,
      'time': instance.time.toIso8601String(),
    };

_$ReadingStorageImpl _$$ReadingStorageImplFromJson(Map<String, dynamic> json) =>
    _$ReadingStorageImpl(
      frequencyBins: (json['frequency_bins'] as List<dynamic>)
          .map((e) => (e as num).toInt())
          .toList(),
      temperature: (json['temperature'] as num).toInt(),
      humidity: (json['humidity'] as num).toInt(),
      soundLevel: (json['sound_level'] as num).toInt(),
      time: DateTime.parse(json['time'] as String),
    );

Map<String, dynamic> _$$ReadingStorageImplToJson(
        _$ReadingStorageImpl instance) =>
    <String, dynamic>{
      'frequency_bins': instance.frequencyBins,
      'temperature': instance.temperature,
      'humidity': instance.humidity,
      'sound_level': instance.soundLevel,
      'time': instance.time.toIso8601String(),
    };
