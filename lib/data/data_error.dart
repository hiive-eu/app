import 'package:freezed_annotation/freezed_annotation.dart';

part 'data_error.freezed.dart';
part 'data_error.g.dart';

@Freezed(unionKey: "cause", unionValueCase: FreezedUnionCase.pascal)
sealed class ApiError with _$ApiError implements Exception {
  // Server errors
  const factory ApiError.invalidCredentials() = InvalidCredentials;
  const factory ApiError.missingJWTToken() = MissingJWTToken;
  const factory ApiError.tokenValidationFailure() = TokenValidationFailure;

  const factory ApiError.invalidDisplayname() = InvalidDisplayname;
  const factory ApiError.invalidEmail() = InvalidEmail;
  const factory ApiError.invalidPassword() = InvalidPassword;
  const factory ApiError.unconfirmedUser() = UnconfirmedUser;

  const factory ApiError.unknownGateway() = UnknownGateway;

  const factory ApiError.unknownSensor() = UnknownSensor;

  const factory ApiError.unexpectedResponseData() = UnexpectedResponseData;

  const factory ApiError.internalServerError() = InternalServerError;
  const factory ApiError.notFound() = NotFound;
  const factory ApiError.badRequest() = BadRequest;

  // Client errors
  const factory ApiError.noConnection() = NoConnection;
  const factory ApiError.notLoggedIn() = NotLoggedIn;
  const factory ApiError.unknown() = Unknown;

  factory ApiError.fromJson(Map<String, dynamic> json) =>
      _$ApiErrorFromJson(json);
}

@freezed
sealed class DbError with _$DbError implements Exception {
  const factory DbError.createTableError(
    String tableName,
    Exception error,
  ) = CreateTableFailed;
  const factory DbError.deleteError(
    Exception error,
  ) = DeleteError;
  const factory DbError.getSingleError(
    Exception error,
  ) = GetSingleError;
  const factory DbError.getAllError(
    Exception error,
  ) = GetAllError;
  const factory DbError.insertError(
    Exception error,
  ) = InsertError;
  const factory DbError.updateError(
    Exception error,
  ) = UpdatetError;
  const factory DbError.transactionError(
    Exception error,
  ) = TransactionError;
  const factory DbError.initError(String error) = InitError;
  const factory DbError.noDatabase() = NoDatabase;
  const factory DbError.noReferenceToDatabase() = NoReferenceToDatabase;
  const factory DbError.unknownDbTaskError(String error) = UnknownDbTaskError;
  const factory DbError.unknownEntityError(String error) = UnknownEntityError;
}

@freezed
sealed class DataError with _$DataError implements Exception {
  const factory DataError.api(ApiError apiError) = Api;
  const factory DataError.db(DbError dbError) = Db;
}
