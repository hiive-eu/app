import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';

part "gateway_readings_repository.g.dart";

@riverpod
GatewayReadingsRepository gatewayReadingsRepository(Ref ref) =>
    GatewayReadingsRepository(ref);

class GatewayReadingsRepository {
  late final Ref _ref;
  GatewayReadingsRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<DataError, List<GatewayReading>> getGatewayReadings(
          String mac) =>
      _ref
          .read(apiProvider)
          .getGatewayReadings(mac)
          .mapLeft((apiError) => DataError.api(apiError))
          .map((response) =>
              response.readings.map((i) => i.toGatewayReading()).toList());
}
