import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/data_error.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';
import 'package:hiive/data/models/dashboard.dart';

part "dashboard_repository.g.dart";

@riverpod
DashboardRepository dashboardRepository(Ref ref) => DashboardRepository(ref);

class DashboardRepository {
  late final Ref _ref;
  DashboardRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<DataError, Dashboard> getDashboard() => _ref
      .read(apiProvider)
      .getDashboard()
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.dashboard.toDashboard());
}
