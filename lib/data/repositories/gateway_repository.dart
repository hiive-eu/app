import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/gateway/claim_gateway_endpoint.dart';
import 'package:hiive/data/api/endpoints/gateway/update_gateway_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/sensor.dart';
import 'package:hiive/data/state/logged_in_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';
import 'package:hiive/data/models/gateway.dart';

part "gateway_repository.g.dart";

@riverpod
GatewayRepository gatewayRepository(Ref ref) => GatewayRepository(ref);

class GatewayRepository {
  late final Ref _ref;
  GatewayRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<ApiError, Unit> checkGateway(String mac) {
    switch (_ref.read(loggedInNotifierProvider)) {
      case (true):
        return _ref.read(apiProvider).checkGateway(mac);
      case (false):
        return TaskEither.right(unit);
    }
  }

  TaskEither<DataError, Gateway> claimGateway(
    ClaimGatewayData claimGatewayData,
  ) =>
      _ref
          .read(apiProvider)
          .claimGateway(claimGatewayData)
          .mapLeft((apiError) => DataError.api(apiError))
          .map((response) => response.gateway.toGateway());

  TaskEither<DataError, Gateway> getGateway(String mac) => _ref
      .read(apiProvider)
      .getGateway(mac)
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.gateway.toGateway());

  TaskEither<DataError, List<Gateway>> getGateways() => _ref
      .read(apiProvider)
      .getGateways()
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.gateways
          .map((gatewayNetwork) => gatewayNetwork.toGateway())
          .toList());

  TaskEither<DataError, List<Sensor>> removeGateway(String mac) => _ref
      .read(apiProvider)
      .removeGateway(mac)
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.sensors
          .map((sensorNetwork) => sensorNetwork.toSensor())
          .toList());

  TaskEither<DataError, Gateway> updateGateway(
    String mac,
    UpdateGatewayData updateGatewayData,
  ) =>
      _ref
          .read(apiProvider)
          .updateGateway(mac, updateGatewayData)
          .mapLeft((apiError) => DataError.api(apiError))
          .map((response) => response.gateway.toGateway());
}
