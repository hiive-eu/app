// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewayRepositoryHash() => r'6adbbd17df6754dc7b45d4d89f88bf5508b1c215';

/// See also [gatewayRepository].
@ProviderFor(gatewayRepository)
final gatewayRepositoryProvider =
    AutoDisposeProvider<GatewayRepository>.internal(
  gatewayRepository,
  name: r'gatewayRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewayRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef GatewayRepositoryRef = AutoDisposeProviderRef<GatewayRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
