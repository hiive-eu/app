import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/user/update_password_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';

part "user_repository.g.dart";

@riverpod
UserRepository userRepository(Ref ref) => UserRepository(ref);

class UserRepository {
  late final Ref _ref;
  UserRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<ApiError, Unit> deleteUser() =>
      _ref.read(apiProvider).deleteUser();

  TaskEither<ApiError, Unit> updatePassword(
    UpdatePasswordData updatePassowrdData,
  ) =>
      _ref.read(apiProvider).updatePassword(updatePassowrdData);
}
