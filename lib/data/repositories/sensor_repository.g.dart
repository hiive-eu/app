// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sensor_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sensorRepositoryHash() => r'2bd5303e7c078035e713511231bf18aefcbd8d30';

/// See also [sensorRepository].
@ProviderFor(sensorRepository)
final sensorRepositoryProvider = AutoDisposeProvider<SensorRepository>.internal(
  sensorRepository,
  name: r'sensorRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sensorRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SensorRepositoryRef = AutoDisposeProviderRef<SensorRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
