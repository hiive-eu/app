import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/reading.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';

part "readings_repository.g.dart";

@riverpod
ReadingsRepository readingsRepository(Ref ref) => ReadingsRepository(ref);

class ReadingsRepository {
  late final Ref _ref;
  ReadingsRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<DataError, List<Reading>> getReadings(String id) => _ref
      .read(apiProvider)
      .getReadings(id)
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.readings.map((i) => i.toReading()).toList());
}
