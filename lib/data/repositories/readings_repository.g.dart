// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'readings_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$readingsRepositoryHash() =>
    r'b17e42a38949b13ef28ab804eda3659205eee1fa';

/// See also [readingsRepository].
@ProviderFor(readingsRepository)
final readingsRepositoryProvider =
    AutoDisposeProvider<ReadingsRepository>.internal(
  readingsRepository,
  name: r'readingsRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$readingsRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef ReadingsRepositoryRef = AutoDisposeProviderRef<ReadingsRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
