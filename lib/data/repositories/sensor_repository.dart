import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/sensor/claim_sensor_endpoint.dart';
import 'package:hiive/data/api/endpoints/sensor/update_sensor_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/models/sensor.dart';
import 'package:hiive/data/state/logged_in_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';

part "sensor_repository.g.dart";

@riverpod
SensorRepository sensorRepository(Ref ref) => SensorRepository(ref);

class SensorRepository {
  late final Ref _ref;
  SensorRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<ApiError, Unit> checkSensor(String mac) {
    switch (_ref.read(loggedInNotifierProvider)) {
      case (true):
        return _ref.read(apiProvider).checkSensor(mac);
      case (false):
        return TaskEither.right(unit);
    }
  }

  TaskEither<DataError, (Hive, Sensor)> claimSensor(
    ClaimSensorData claimSensorData,
  ) =>
      _ref
          .read(apiProvider)
          .claimSensor(claimSensorData)
          .mapLeft((apiError) => DataError.api(apiError))
          .map(
        (response) {
          Hive hive = response.hive.toHive();
          Sensor sensor = response.sensor.toSensor();
          return (hive, sensor);
        },
      );

  TaskEither<DataError, List<Sensor>> getSensors() => _ref
      .read(apiProvider)
      .getSensors()
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.sensors
          .map((sensorNetwork) => sensorNetwork.toSensor())
          .toList());

  TaskEither<DataError, Sensor> updateSensor(
    String mac,
    UpdateSensorData updateSensorData,
  ) =>
      _ref
          .read(apiProvider)
          .updateSensor(mac, updateSensorData)
          .mapLeft((apiError) => DataError.api(apiError))
          .map((response) => response.sensor.toSensor());
}
