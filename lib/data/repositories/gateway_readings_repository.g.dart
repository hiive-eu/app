// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gateway_readings_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$gatewayReadingsRepositoryHash() =>
    r'46564df4e457e471511ae9735377803d6ff439ce';

/// See also [gatewayReadingsRepository].
@ProviderFor(gatewayReadingsRepository)
final gatewayReadingsRepositoryProvider =
    AutoDisposeProvider<GatewayReadingsRepository>.internal(
  gatewayReadingsRepository,
  name: r'gatewayReadingsRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$gatewayReadingsRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef GatewayReadingsRepositoryRef
    = AutoDisposeProviderRef<GatewayReadingsRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
