import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/api.dart';
import 'package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart';
import 'package:hiive/data/api/endpoints/hive/update_hive_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "hive_repository.g.dart";

@riverpod
HiveRepository hiveRepository(Ref ref) => HiveRepository(ref);

class HiveRepository {
  late final Ref _ref;
  HiveRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<DataError, Hive> createHive(
    CreateHiveData createHiveData,
  ) =>
      _ref
          .read(apiProvider)
          .createHive(createHiveData)
          .mapLeft((apiError) => DataError.api(apiError))
          .map((response) => response.hive.toHive());

  TaskEither<DataError, Hive> getHive(String id) => _ref
      .read(apiProvider)
      .getHive(id)
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.hive.toHive());

  TaskEither<DataError, List<Hive>> getHives() => _ref
      .read(apiProvider)
      .getHives()
      .mapLeft((apiError) => DataError.api(apiError))
      .map((response) => response.hives.map((hive) => hive.toHive()).toList());

  TaskEither<DataError, Unit> removeHive(String id) => _ref
      .read(apiProvider)
      .removeHive(id)
      .mapLeft((apiError) => DataError.api(apiError))
      .map((_) => unit);

  TaskEither<DataError, List<Hive>> updateHive(
    String id,
    UpdateHiveData updateHiveData,
  ) =>
      _ref
          .read(apiProvider)
          .updateHive(id, updateHiveData)
          .mapLeft((apiError) => DataError.api(apiError))
          .map((response) => response.hives
              .map((hiveNetwork) => hiveNetwork.toHive())
              .toList());
}
