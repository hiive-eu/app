import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/authentication/forgot_password_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/login_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/refresh_tokens_endpoint.dart';
import "package:hiive/data/api/endpoints/authentication/reset_password_endpoint.dart";
import 'package:hiive/data/api/endpoints/authentication/signup_confirm_endpoint.dart';
import 'package:hiive/data/api/endpoints/authentication/signup_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/authentication_tokens.dart';
import 'package:hiive/data/state/authentication_notifier.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:hiive/data/api/api.dart';

part "authentication_repository.g.dart";

@riverpod
AuthenticationRepository authenticationRepository(Ref ref) =>
    AuthenticationRepository(ref);

class AuthenticationRepository {
  late final Ref _ref;
  AuthenticationRepository(Ref ref) {
    this._ref = ref;
  }

  TaskEither<ApiError, Unit> forgotPassword(
    ForgotPasswordData forgotPasswordData,
  ) {
    switch (true /* isConnected */) {
      case (true):
        return _ref.read(apiProvider).forgotPassword(forgotPasswordData);
      case (false):
        throw ApiError.noConnection();
    }
  }

  TaskEither<ApiError, AuthenticationTokens> login(LoginData loginData) {
    switch (true /* isConnected */) {
      case (true):
        return _ref.read(apiProvider).login(loginData).map(
              (response) =>
                  response.authenticationTokens.toAuthenticationTokens(),
            );
      case (false):
        throw ApiError.noConnection();
    }
  }

  TaskEither<ApiError, Unit> logout() {
    switch (_ref.read(authenticationNotifierProvider).value != null) {
      case (true):
        return _ref.read(apiProvider).logout();
      case (false):
        throw ApiError.notLoggedIn();
    }
  }

  TaskEither<ApiError, Unit> signup(SignupData signupData) {
    switch (true /* isConnected */) {
      case (true):
        return _ref.read(apiProvider).signup(signupData);
      case (false):
        throw ApiError.noConnection();
    }
  }

  TaskEither<ApiError, AuthenticationTokens> refreshTokens(
    RefreshTokensData refreshTokensData,
  ) {
    //switch (_ref.read(authenticationNotifierProvider).value != null) {
    switch (true /* isConnected */) {
      case (true):
        return _ref.read(apiProvider).refreshTokens(refreshTokensData).map(
              (response) =>
                  response.authenticationTokens.toAuthenticationTokens(),
            );
      case (false):
        throw ApiError.noConnection();
    }
  }

  TaskEither<ApiError, Unit> resetPassword(
    ResetPasswordData resetpasswordData,
  ) {
    switch (true /* isConnected */) {
      case (true):
        return _ref.read(apiProvider).resetPassword(resetpasswordData);
      case (false):
        throw ApiError.noConnection();
    }
  }

  TaskEither<ApiError, Unit> signupConfirm(
    SignupConfirmData signupCOnfirmData,
  ) {
    switch (true /* isConnected */) {
      case (true):
        return _ref.read(apiProvider).signupConfirm(signupCOnfirmData);
      case (false):
        throw ApiError.noConnection();
    }
  }
}
