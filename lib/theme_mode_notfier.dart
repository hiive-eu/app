import 'package:flutter/material.dart';
import 'package:hiive/constants/defaults.dart';
import 'package:hiive/utils/extensions.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'theme_mode_notfier.g.dart';

@Riverpod(keepAlive: true)
class ThemeModeNotifier extends _$ThemeModeNotifier {
  @override
  ThemeMode build() => ThemeMode.light; // Real value is set in main.dart

  void setThemeMode(ThemeMode v) async {
    print("[THEME] Theme set to ${v.toString()}");
    final preferences = await SharedPreferences.getInstance();
    preferences.setInt(cPreferencesThemeModeKey, v.toInt());
    state = v;
  }
}
