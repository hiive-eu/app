import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import "package:hiive/router/app_router.dart";
import 'package:hiive/services/ble_service.dart';

//
//class GatewayEditScreen extends ConsumerStatefulWidget {
//  final String mac;
//  const GatewayEditScreen({super.key, required this.mac});
//
//  @override
//  ConsumerState<GatewayEditScreen> createState() => _GatewayEditScreenState();
//}
//
//class _GatewayEditScreenState extends ConsumerState<GatewayEditScreen> {
//  late final BleService _bleService;
//
//  bool _wifiLoading = false;
//  late BleConStatus _bleConStatus;
//  final List<WiFiNetwork> _wifiNetworks = [];
//
//  @override
//  void initState() {
//    super.initState();
//    _bleService = ref.read(bleServiceProvider);
//    WidgetsBinding.instance.addPostFrameCallback((_) {
//      _bleService
//          .connect(null, widget.mac)
//          .run()
//          .then((value) => print("[GATEWAY EDIT] Connect done: $value"));
//    });
//  }
//
//  @override
//  void didChangeDependencies() {
//    super.didChangeDependencies();
//  }
//
//  @override
//  void dispose() {
//    _bleService.disconnect().run();
//    super.dispose();
//  }
//
//  String? _wifiPasswordValidator(String? v) {
//    if (v == null || v.isEmpty) {
//      return "password";
//    }
//    if (v.length > 63) {
//      return "password too long";
//    }
//    return null;
//  }
//
//  void _onError(String error) {
//    setState(() {
//      _wifiLoading = false;
//    });
//    print("[ERROR | EDIT GATEWAY] $error");
//    ScaffoldMessenger.of(context).showSnackBar(
//      SnackBar(
//        content: Text(
//          error,
//          textAlign: TextAlign.center,
//        ),
//      ),
//    );
//  }
//
//  Future<String?> _showWifiPasswordDialog(String ssid) {
//    return showDialog(
//        context: context,
//        builder: (BuildContext context) {
//          TextEditingController passwordEditingController =
//              TextEditingController();
//          return AlertDialog(
//            title: Text(ssid),
//            content: TextFormField(
//              decoration: const InputDecoration(
//                labelText: "Password",
//              ),
//              obscureText: true,
//              validator: _wifiPasswordValidator,
//              controller: passwordEditingController,
//            ),
//            actions: [
//              ElevatedButton(
//                child: const Text('Cancel'),
//                onPressed: () {
//                  Navigator.of(context).pop(null);
//                },
//              ),
//              ElevatedButton(
//                child: const Text('Submit'),
//                onPressed: () {
//                  Navigator.of(context).pop(passwordEditingController.text);
//                },
//              ),
//            ],
//          );
//        });
//  }
//
//  void _onWifiNetworkTapped(WiFiNetwork network) async {
//    String? password;
//    if (network.secure) {
//      password = await _showWifiPasswordDialog(network.ssid);
//    } else {
//      password = "";
//    }
//
//    if (password != null) {
//      setState(() => _wifiLoading = true);
//      _bleService.setWifiCredentials(network.ssid, password).match(
//        (e) {
//          _onError(e);
//        },
//        (r) {
//          _bleService
//              .testWifi()
//              .match(
//                (e) {
//                  _onError(e);
//                },
//                (_) {},
//              )
//              .run()
//              .then((_) {
//                _bleService.refreshGatewayInfo().match((e) {
//                  _onError(e);
//                }, (_) {
//                  setState(() => _wifiLoading = false);
//                }).run();
//              });
//        },
//      ).run();
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    _bleConStatus = ref.watch(bleConnectedNotifierProvider);
//    return Scaffold(
//      resizeToAvoidBottomInset: false,
//      appBar: AppBar(
//        title: const Text("Setup gateway"),
//        backgroundColor: _bleConStatus.maybeWhen(
//            connected: (_) => Theme.of(context).colorScheme.primary,
//            orElse: () => Colors.black12),
//        actions: [
//          IconButton(
//            onPressed: () async {
//              setState(() {
//                _wifiLoading = true;
//                _wifiNetworks.clear();
//              });
//              _bleService.scanWifi().match(
//                (e) {
//                  _onError(e);
//                },
//                (wifiScanStream) {
//                  wifiScanStream.listen(
//                    (wifi) {
//                      print("${wifi.ssid} - ${wifi.rssi}");
//                      setState(() => _wifiNetworks.add(wifi));
//                    },
//                    onError: (error) {
//                      print("WIFI ERROR");
//                      setState(() => _wifiLoading = false);
//                    },
//                    onDone: () {
//                      print("WIFI DONE");
//                      setState(() => _wifiLoading = false);
//                    },
//                  );
//                },
//              ).run();
//            },
//            icon: const Icon(Icons.wifi_find),
//          ),
//          IconButton(
//            onPressed: () async {
//              setState(() {
//                _wifiLoading = true;
//              });
//              _bleService.getReadingsFilenames().match(
//                (e) {
//                  _onError(e);
//                },
//                (readingFilenamesStream) {
//                  readingFilenamesStream.listen(
//                    (filename) {
//                      print(filename);
//                    },
//                    onError: (error) {
//                      print("FILENAMES ERROR");
//                      setState(() => _wifiLoading = false);
//                    },
//                    onDone: () {
//                      print("FILENAMSS DONE");
//                      setState(() => _wifiLoading = false);
//                    },
//                  );
//                },
//              ).run();
//            },
//            icon: const Icon(Icons.list),
//          ),
//          IconButton(
//            onPressed: () async {
//              setState(() {
//                _wifiLoading = true;
//              });
//              _bleService.getReadingsFile("2024-07").match(
//                (e) {
//                  _onError(e);
//                },
//                (readingFilenamesStream) {
//                  List<int> readingsFile = [];
//                  readingFilenamesStream.listen(
//                    (data) {
//                      readingsFile = readingsFile + data.data;
//                      print(
//                        "Readings download progress: ${(data.received / data.filesize) * 100} (${data.received}/${data.filesize})",
//                      );
//                    },
//                    onError: (error) {
//                      setState(() => _wifiLoading = false);
//                    },
//                    onDone: () {
//                      setState(() => _wifiLoading = false);
//                    },
//                  );
//                },
//              ).run();
//            },
//            icon: const Icon(Icons.data_array),
//          ),
//          IconButton(
//            onPressed: () async {
//              setState(() {
//                _wifiLoading = true;
//              });
//              _bleService.deleteReadingsFile("2024-10").match(
//                (e) {
//                  _onError(e);
//                },
//                (retVal) {
//                  retVal ? print("File deleted") : print("Error deleting file");
//                  setState(() => _wifiLoading = false);
//                },
//              ).run();
//            },
//            icon: const Icon(Icons.delete),
//          ),
//          IconButton(
//            onPressed: () async {
//              setState(() {
//                _wifiLoading = true;
//              });
//              _bleService.updateFirmware("blink").match(
//                (e) {
//                  _onError(e);
//                },
//                (updateProgressStream) {
//                  updateProgressStream.listen(
//                    (data) {
//                      print("Update download progress: $data/100%");
//                    },
//                    cancelOnError: true,
//                    onError: (error) {
//                      print("FIRMWARE ERROR");
//                      setState(() => _wifiLoading = false);
//                    },
//                    onDone: () {
//                      print("FIRMWARE DONE");
//                      setState(() => _wifiLoading = false);
//                    },
//                  );
//                },
//              ).run();
//            },
//            icon: const Icon(Icons.update),
//          ),
//        ],
//      ),
//      body: Padding(
//        padding: const EdgeInsets.all(16.0),
//        child: Column(
//          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//          crossAxisAlignment: CrossAxisAlignment.center,
//          children: <Widget>[
//            _bleConStatus.when(
//              connected: (connectedGateway) {
//                if (connectedGateway.battery != null) {
//                  return Expanded(
//                    child: Column(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      crossAxisAlignment: CrossAxisAlignment.center,
//                      children: <Widget>[
//                        Column(
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: <Widget>[
//                            Text("Battery: ${connectedGateway.battery}"),
//                            Text("Rssi: ${connectedGateway.rssi}"),
//                            Text("Ssid: ${connectedGateway.ssid}"),
//                            Row(
//                              children: [
//                                const Text("WiFi status: "),
//                                connectedGateway.wifiTest == 1
//                                    ? const Icon(
//                                        Icons.check,
//                                        color: Colors.green,
//                                      )
//                                    : const Icon(
//                                        Icons.remove,
//                                        color: Colors.red,
//                                      ),
//                              ],
//                            ),
//                            Row(
//                              children: [
//                                const Text("SD status: "),
//                                connectedGateway.sdTest == 1
//                                    ? const Icon(
//                                        Icons.check,
//                                        color: Colors.green,
//                                      )
//                                    : const Icon(
//                                        Icons.remove,
//                                        color: Colors.red,
//                                      ),
//                              ],
//                            ),
//                            Text(
//                                "SD space used: ${connectedGateway.sdSpaceUsed}%"),
//                          ],
//                        ),
//                        //ElevatedButton(
//                        //    onPressed: () {
//                        //      print("Transfer readings");
//                        //    },
//                        //    child: const Text("Transfer readings")),
//                        //ElevatedButton(
//                        //    onPressed: () {
//                        //      print("Change interval");
//                        //    },
//                        //    child: const Text("Set reading interval")),
//                      ],
//                    ),
//                  );
//                } else {
//                  return const Expanded(
//                      child: Center(child: CircularProgressIndicator()));
//                }
//              },
//              disconnected: () => Expanded(
//                child: Column(
//                  children: [
//                    const Center(
//                      child: Text("Gateway not found"),
//                    ),
//                    ElevatedButton(
//                        onPressed: () {
//                          _bleService.connect(null, widget.mac).run().then(
//                                (res) => res.match(
//                                  (error) => print(error),
//                                  (r) => {},
//                                ),
//                              );
//                        },
//                        child: const Text("Retry"))
//                  ],
//                ),
//              ),
//              connecting: (_) => const Expanded(
//                  child: Center(child: CircularProgressIndicator())),
//            ),
//            Expanded(
//              child: Stack(
//                children: [
//                  Visibility(
//                    visible: _wifiLoading,
//                    child: const Center(child: CircularProgressIndicator()),
//                  ),
//                  ListView.builder(
//                      itemCount: _wifiNetworks.length,
//                      itemBuilder: (context, index) {
//                        return GestureDetector(
//                          onTap: () =>
//                              _onWifiNetworkTapped(_wifiNetworks[index]),
//                          child: Container(
//                            padding: const EdgeInsets.all(10),
//                            margin: const EdgeInsets.symmetric(horizontal: 24),
//                            child: Row(
//                              children: [
//                                Expanded(
//                                    child: Text(_wifiNetworks[index].ssid)),
//                                Icon(wifiIconFromRssi(
//                                    _wifiNetworks[index].rssi)),
//                              ],
//                            ),
//                          ),
//                        );
//                      }),
//                ],
//              ),
//            ),
//            Visibility(
//              visible: _bleConStatus.maybeWhen(
//                  connected: (_) => true, orElse: () => false),
//              child: Center(
//                child: ElevatedButton(
//                  onPressed: () {
//                    _bleService.startSynchronization().run().then((_) {
//                      if (context.mounted) {
//                        context.goNamed(ScreenRoute.gatewayPair.name,
//                            pathParameters: {"mac": widget.mac});
//                      }
//                    });
//                  },
//                  child: const Text("Start sensor pairing"),
//                ),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//}
//
class ReadingsFileProgress {
  int filesize;
  int received;
  Uint8List data;

  ReadingsFileProgress(
      {required this.filesize, required this.received, required this.data});
}
