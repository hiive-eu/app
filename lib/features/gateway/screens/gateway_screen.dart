import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:hiive/constants/defaults.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:hiive/data/models/wifi_network.dart';
import 'package:hiive/data/state/gateway_readings_notifier.dart';
import 'package:hiive/data/state/gateways_notifier.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/features/gateway/ble_gateway_notifier.dart';
import 'package:hiive/features/gateway/widgets/sensor_grid.dart';
import 'package:hiive/features/gateway_setup/widgets/ble_connection_indicator.dart';
import 'package:hiive/router/app_router.dart';
import 'package:hiive/widgets/charts/battery_chart.dart';
import 'package:hiive/widgets/charts/humidity_chart.dart';
import 'package:hiive/widgets/charts/temperature_chart.dart';

class GatewayScreen extends ConsumerStatefulWidget {
  final String mac;
  const GatewayScreen({super.key, required this.mac});

  @override
  ConsumerState<GatewayScreen> createState() => _GatewayScreenState();
}

class _GatewayScreenState extends ConsumerState<GatewayScreen> {
  bool loadData = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Refresh data on screen open
    if (loadData) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref
            .read(gatewayReadingsNotifierProvider.notifier)
            .getGatewayReadings(widget.mac);
        ref.read(gatewaysNotifierProvider.notifier).getGateway(widget.mac);
        ref.read(hivesNotifierProvider.notifier).getHives();
      });
      loadData = false;
    }

    final AsyncValue<List<Gateway>> gatewaysProviderState =
        ref.watch(gatewaysNotifierProvider);
    final AsyncValue<Map<String, List<GatewayReading>>>
        gatewayReadingsProviderState =
        ref.watch(gatewayReadingsNotifierProvider);
    final AsyncValue<List<Hive>> hivesProviderState =
        ref.watch(hivesNotifierProvider);

    final Gateway? gateway =
        ref.watch(gatewaysNotifierProvider).getByMac(widget.mac);
    final List<GatewayReading> gatewayReadings =
        ref.watch(gatewayReadingsNotifierProvider).getByMac(widget.mac);

    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    bool gatewayConnected = switch (bleGatewayGuard.value) {
      null => false,
      GatewayUnknown() => false,
      GatewayKnown(:final bleGateway) =>
        bleGateway.connectionState == DeviceConnectionState.connected
    };

    int gatewayInterval = switch (bleGatewayGuard.value) {
      null => cMaxIntervalDuration,
      GatewayUnknown() => cMaxIntervalDuration,
      GatewayKnown(:final bleGateway) => bleGateway.interval
    };

    final gatewayName = gateway?.name ?? "Unknown";
    final gatewayRssi = gateway?.wifiRssi ?? 0;
    final wifiIcon = wifiIconFromRssi(gatewayRssi) == null
        ? Stack(
            alignment: AlignmentDirectional.center,
            children: [
              Icon(
                size: 46,
                Icons.wifi,
                color: Color(0x80c19c6e),
              ),
              Icon(
                weight: 1,
                size: 28,
                Icons.question_mark_outlined,
                color: Colors.grey[400],
              )
            ],
          )
        : Icon(wifiIconFromRssi(gatewayRssi), size: gatewayConnected ? 28 : 36);

    return Scaffold(
      appBar: AppBar(
        title: Text(gatewayName),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          gatewayReadingsProviderState.isLoading
              ? Expanded(child: Center(child: CircularProgressIndicator()))
              : gatewayReadings.length <= 1
                  ? Center(child: Text("Nothing here yet"))
                  : Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 8.0),
                              child: TemperatureChart(
                                temperaturesArg: gatewayReadings
                                    .map((r) => MapEntry(r.time, r.temperature))
                                    .toList(),
                                height: 150,
                                width: 400,
                              ),
                            ),
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 8.0),
                              child: HumidityChart(
                                humiditiesArg: gatewayReadings
                                    .map((r) => MapEntry(r.time, r.humidity))
                                    .toList(),
                                height: 150,
                                width: 400,
                              ),
                            ),
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 8.0, horizontal: 8.0),
                              child: BatteryChart(
                                batteryLevelsArg: gatewayReadings
                                    .map((r) => MapEntry(r.time, r.battery))
                                    .toList(),
                                height: 150,
                                width: 400,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
          SizedBox(
            width: double.infinity,
            height: 180,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                gatewaysProviderState.isLoading
                    ? Expanded(
                        child: Center(child: CircularProgressIndicator()))
                    : Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(16, 8, 8, 18),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "WiFi",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                      decoration: TextDecoration.underline,
                                    ),
                              ),
                              Expanded(
                                child: Container(
                                    child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: switch (bleGatewayGuard.value) {
                                    GatewayKnown(:final bleGateway) => [
                                        Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: wifiIcon,
                                        ),
                                        if (gatewayConnected &&
                                            bleGateway.ssid.isNotEmpty)
                                          FittedBox(
                                            fit: BoxFit.fitWidth,
                                            child: Text(
                                              maxLines: 2,
                                              bleGateway.ssid,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .titleLarge,
                                            ),
                                          )
                                      ],
                                    _ => [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(bottom: 8),
                                          child: Center(child: wifiIcon),
                                        ),
                                      ],
                                  },
                                )),
                              ),
                              FilledButton(
                                style: FilledButton.styleFrom(
                                  elevation: 3,
                                ),
                                onPressed: gatewayConnected
                                    ? () => context.pushNamed(
                                          ScreenRoute.setupGatewayWifi.name,
                                          queryParameters: {
                                            "shouldPop": "true"
                                          },
                                        )
                                    : null,
                                child: Text(
                                  "Change",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(fontWeight: FontWeight.w600),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                BleConnectionIndicator(mac: widget.mac, token: gateway?.token),
                hivesProviderState.isLoading
                    ? Expanded(
                        child: Center(child: CircularProgressIndicator()))
                    : Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8, 8, 18, 18),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Hives",
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(
                                      decoration: TextDecoration.underline,
                                    ),
                              ),
                              Expanded(
                                child: Center(
                                  child: SensorGrid(
                                    mac: widget.mac,
                                    interval: gatewayInterval,
                                  ),
                                ),
                              ),
                              FilledButton(
                                style: FilledButton.styleFrom(elevation: 3),
                                onPressed: gatewayConnected
                                    ? () => context.pushNamed(
                                          ScreenRoute.setupGatewayPair.name,
                                          queryParameters: {
                                            "shouldPop": "true"
                                          },
                                        )
                                    : null,
                                child: Text(
                                  "Pair",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(fontWeight: FontWeight.w600),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
