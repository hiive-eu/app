// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ble_gateway_notifier.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BleGatewayGuard {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() gatewayUnknown,
    required TResult Function(BleGateway bleGateway) gatewayKnown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? gatewayUnknown,
    TResult? Function(BleGateway bleGateway)? gatewayKnown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? gatewayUnknown,
    TResult Function(BleGateway bleGateway)? gatewayKnown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GatewayUnknown value) gatewayUnknown,
    required TResult Function(GatewayKnown value) gatewayKnown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GatewayUnknown value)? gatewayUnknown,
    TResult? Function(GatewayKnown value)? gatewayKnown,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GatewayUnknown value)? gatewayUnknown,
    TResult Function(GatewayKnown value)? gatewayKnown,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BleGatewayGuardCopyWith<$Res> {
  factory $BleGatewayGuardCopyWith(
          BleGatewayGuard value, $Res Function(BleGatewayGuard) then) =
      _$BleGatewayGuardCopyWithImpl<$Res, BleGatewayGuard>;
}

/// @nodoc
class _$BleGatewayGuardCopyWithImpl<$Res, $Val extends BleGatewayGuard>
    implements $BleGatewayGuardCopyWith<$Res> {
  _$BleGatewayGuardCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of BleGatewayGuard
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
abstract class _$$GatewayUnknownImplCopyWith<$Res> {
  factory _$$GatewayUnknownImplCopyWith(_$GatewayUnknownImpl value,
          $Res Function(_$GatewayUnknownImpl) then) =
      __$$GatewayUnknownImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GatewayUnknownImplCopyWithImpl<$Res>
    extends _$BleGatewayGuardCopyWithImpl<$Res, _$GatewayUnknownImpl>
    implements _$$GatewayUnknownImplCopyWith<$Res> {
  __$$GatewayUnknownImplCopyWithImpl(
      _$GatewayUnknownImpl _value, $Res Function(_$GatewayUnknownImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleGatewayGuard
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$GatewayUnknownImpl implements GatewayUnknown {
  const _$GatewayUnknownImpl();

  @override
  String toString() {
    return 'BleGatewayGuard.gatewayUnknown()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GatewayUnknownImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() gatewayUnknown,
    required TResult Function(BleGateway bleGateway) gatewayKnown,
  }) {
    return gatewayUnknown();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? gatewayUnknown,
    TResult? Function(BleGateway bleGateway)? gatewayKnown,
  }) {
    return gatewayUnknown?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? gatewayUnknown,
    TResult Function(BleGateway bleGateway)? gatewayKnown,
    required TResult orElse(),
  }) {
    if (gatewayUnknown != null) {
      return gatewayUnknown();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GatewayUnknown value) gatewayUnknown,
    required TResult Function(GatewayKnown value) gatewayKnown,
  }) {
    return gatewayUnknown(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GatewayUnknown value)? gatewayUnknown,
    TResult? Function(GatewayKnown value)? gatewayKnown,
  }) {
    return gatewayUnknown?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GatewayUnknown value)? gatewayUnknown,
    TResult Function(GatewayKnown value)? gatewayKnown,
    required TResult orElse(),
  }) {
    if (gatewayUnknown != null) {
      return gatewayUnknown(this);
    }
    return orElse();
  }
}

abstract class GatewayUnknown implements BleGatewayGuard {
  const factory GatewayUnknown() = _$GatewayUnknownImpl;
}

/// @nodoc
abstract class _$$GatewayKnownImplCopyWith<$Res> {
  factory _$$GatewayKnownImplCopyWith(
          _$GatewayKnownImpl value, $Res Function(_$GatewayKnownImpl) then) =
      __$$GatewayKnownImplCopyWithImpl<$Res>;
  @useResult
  $Res call({BleGateway bleGateway});

  $BleGatewayCopyWith<$Res> get bleGateway;
}

/// @nodoc
class __$$GatewayKnownImplCopyWithImpl<$Res>
    extends _$BleGatewayGuardCopyWithImpl<$Res, _$GatewayKnownImpl>
    implements _$$GatewayKnownImplCopyWith<$Res> {
  __$$GatewayKnownImplCopyWithImpl(
      _$GatewayKnownImpl _value, $Res Function(_$GatewayKnownImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleGatewayGuard
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? bleGateway = null,
  }) {
    return _then(_$GatewayKnownImpl(
      null == bleGateway
          ? _value.bleGateway
          : bleGateway // ignore: cast_nullable_to_non_nullable
              as BleGateway,
    ));
  }

  /// Create a copy of BleGatewayGuard
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $BleGatewayCopyWith<$Res> get bleGateway {
    return $BleGatewayCopyWith<$Res>(_value.bleGateway, (value) {
      return _then(_value.copyWith(bleGateway: value));
    });
  }
}

/// @nodoc

class _$GatewayKnownImpl implements GatewayKnown {
  const _$GatewayKnownImpl(this.bleGateway);

  @override
  final BleGateway bleGateway;

  @override
  String toString() {
    return 'BleGatewayGuard.gatewayKnown(bleGateway: $bleGateway)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayKnownImpl &&
            (identical(other.bleGateway, bleGateway) ||
                other.bleGateway == bleGateway));
  }

  @override
  int get hashCode => Object.hash(runtimeType, bleGateway);

  /// Create a copy of BleGatewayGuard
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayKnownImplCopyWith<_$GatewayKnownImpl> get copyWith =>
      __$$GatewayKnownImplCopyWithImpl<_$GatewayKnownImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() gatewayUnknown,
    required TResult Function(BleGateway bleGateway) gatewayKnown,
  }) {
    return gatewayKnown(bleGateway);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? gatewayUnknown,
    TResult? Function(BleGateway bleGateway)? gatewayKnown,
  }) {
    return gatewayKnown?.call(bleGateway);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? gatewayUnknown,
    TResult Function(BleGateway bleGateway)? gatewayKnown,
    required TResult orElse(),
  }) {
    if (gatewayKnown != null) {
      return gatewayKnown(bleGateway);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GatewayUnknown value) gatewayUnknown,
    required TResult Function(GatewayKnown value) gatewayKnown,
  }) {
    return gatewayKnown(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GatewayUnknown value)? gatewayUnknown,
    TResult? Function(GatewayKnown value)? gatewayKnown,
  }) {
    return gatewayKnown?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GatewayUnknown value)? gatewayUnknown,
    TResult Function(GatewayKnown value)? gatewayKnown,
    required TResult orElse(),
  }) {
    if (gatewayKnown != null) {
      return gatewayKnown(this);
    }
    return orElse();
  }
}

abstract class GatewayKnown implements BleGatewayGuard {
  const factory GatewayKnown(final BleGateway bleGateway) = _$GatewayKnownImpl;

  BleGateway get bleGateway;

  /// Create a copy of BleGatewayGuard
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayKnownImplCopyWith<_$GatewayKnownImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$BleGateway {
  String get id => throw _privateConstructorUsedError;
  String get mac => throw _privateConstructorUsedError;
  DeviceConnectionState get connectionState =>
      throw _privateConstructorUsedError;
  int get interval => throw _privateConstructorUsedError;
  String get ssid => throw _privateConstructorUsedError;
  GatewayTestResult get testResults => throw _privateConstructorUsedError;

  /// Create a copy of BleGateway
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $BleGatewayCopyWith<BleGateway> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BleGatewayCopyWith<$Res> {
  factory $BleGatewayCopyWith(
          BleGateway value, $Res Function(BleGateway) then) =
      _$BleGatewayCopyWithImpl<$Res, BleGateway>;
  @useResult
  $Res call(
      {String id,
      String mac,
      DeviceConnectionState connectionState,
      int interval,
      String ssid,
      GatewayTestResult testResults});

  $GatewayTestResultCopyWith<$Res> get testResults;
}

/// @nodoc
class _$BleGatewayCopyWithImpl<$Res, $Val extends BleGateway>
    implements $BleGatewayCopyWith<$Res> {
  _$BleGatewayCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of BleGateway
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? mac = null,
    Object? connectionState = null,
    Object? interval = null,
    Object? ssid = null,
    Object? testResults = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      connectionState: null == connectionState
          ? _value.connectionState
          : connectionState // ignore: cast_nullable_to_non_nullable
              as DeviceConnectionState,
      interval: null == interval
          ? _value.interval
          : interval // ignore: cast_nullable_to_non_nullable
              as int,
      ssid: null == ssid
          ? _value.ssid
          : ssid // ignore: cast_nullable_to_non_nullable
              as String,
      testResults: null == testResults
          ? _value.testResults
          : testResults // ignore: cast_nullable_to_non_nullable
              as GatewayTestResult,
    ) as $Val);
  }

  /// Create a copy of BleGateway
  /// with the given fields replaced by the non-null parameter values.
  @override
  @pragma('vm:prefer-inline')
  $GatewayTestResultCopyWith<$Res> get testResults {
    return $GatewayTestResultCopyWith<$Res>(_value.testResults, (value) {
      return _then(_value.copyWith(testResults: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$BleGatewayImplCopyWith<$Res>
    implements $BleGatewayCopyWith<$Res> {
  factory _$$BleGatewayImplCopyWith(
          _$BleGatewayImpl value, $Res Function(_$BleGatewayImpl) then) =
      __$$BleGatewayImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String id,
      String mac,
      DeviceConnectionState connectionState,
      int interval,
      String ssid,
      GatewayTestResult testResults});

  @override
  $GatewayTestResultCopyWith<$Res> get testResults;
}

/// @nodoc
class __$$BleGatewayImplCopyWithImpl<$Res>
    extends _$BleGatewayCopyWithImpl<$Res, _$BleGatewayImpl>
    implements _$$BleGatewayImplCopyWith<$Res> {
  __$$BleGatewayImplCopyWithImpl(
      _$BleGatewayImpl _value, $Res Function(_$BleGatewayImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleGateway
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? mac = null,
    Object? connectionState = null,
    Object? interval = null,
    Object? ssid = null,
    Object? testResults = null,
  }) {
    return _then(_$BleGatewayImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      mac: null == mac
          ? _value.mac
          : mac // ignore: cast_nullable_to_non_nullable
              as String,
      connectionState: null == connectionState
          ? _value.connectionState
          : connectionState // ignore: cast_nullable_to_non_nullable
              as DeviceConnectionState,
      interval: null == interval
          ? _value.interval
          : interval // ignore: cast_nullable_to_non_nullable
              as int,
      ssid: null == ssid
          ? _value.ssid
          : ssid // ignore: cast_nullable_to_non_nullable
              as String,
      testResults: null == testResults
          ? _value.testResults
          : testResults // ignore: cast_nullable_to_non_nullable
              as GatewayTestResult,
    ));
  }
}

/// @nodoc

class _$BleGatewayImpl extends _BleGateway {
  const _$BleGatewayImpl(
      {required this.id,
      required this.mac,
      required this.connectionState,
      required this.interval,
      required this.ssid,
      required this.testResults})
      : super._();

  @override
  final String id;
  @override
  final String mac;
  @override
  final DeviceConnectionState connectionState;
  @override
  final int interval;
  @override
  final String ssid;
  @override
  final GatewayTestResult testResults;

  @override
  String toString() {
    return 'BleGateway(id: $id, mac: $mac, connectionState: $connectionState, interval: $interval, ssid: $ssid, testResults: $testResults)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$BleGatewayImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.mac, mac) || other.mac == mac) &&
            (identical(other.connectionState, connectionState) ||
                other.connectionState == connectionState) &&
            (identical(other.interval, interval) ||
                other.interval == interval) &&
            (identical(other.ssid, ssid) || other.ssid == ssid) &&
            (identical(other.testResults, testResults) ||
                other.testResults == testResults));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, id, mac, connectionState, interval, ssid, testResults);

  /// Create a copy of BleGateway
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$BleGatewayImplCopyWith<_$BleGatewayImpl> get copyWith =>
      __$$BleGatewayImplCopyWithImpl<_$BleGatewayImpl>(this, _$identity);
}

abstract class _BleGateway extends BleGateway {
  const factory _BleGateway(
      {required final String id,
      required final String mac,
      required final DeviceConnectionState connectionState,
      required final int interval,
      required final String ssid,
      required final GatewayTestResult testResults}) = _$BleGatewayImpl;
  const _BleGateway._() : super._();

  @override
  String get id;
  @override
  String get mac;
  @override
  DeviceConnectionState get connectionState;
  @override
  int get interval;
  @override
  String get ssid;
  @override
  GatewayTestResult get testResults;

  /// Create a copy of BleGateway
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$BleGatewayImplCopyWith<_$BleGatewayImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$GatewayTestResult {
  GatewayWifiTestResult get wifiTestResult =>
      throw _privateConstructorUsedError;
  int get rssi => throw _privateConstructorUsedError;
  GatewaySdTestResult get sdTestResult => throw _privateConstructorUsedError;
  int get sdSpaceUsed => throw _privateConstructorUsedError;

  /// Create a copy of GatewayTestResult
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayTestResultCopyWith<GatewayTestResult> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayTestResultCopyWith<$Res> {
  factory $GatewayTestResultCopyWith(
          GatewayTestResult value, $Res Function(GatewayTestResult) then) =
      _$GatewayTestResultCopyWithImpl<$Res, GatewayTestResult>;
  @useResult
  $Res call(
      {GatewayWifiTestResult wifiTestResult,
      int rssi,
      GatewaySdTestResult sdTestResult,
      int sdSpaceUsed});
}

/// @nodoc
class _$GatewayTestResultCopyWithImpl<$Res, $Val extends GatewayTestResult>
    implements $GatewayTestResultCopyWith<$Res> {
  _$GatewayTestResultCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayTestResult
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? wifiTestResult = null,
    Object? rssi = null,
    Object? sdTestResult = null,
    Object? sdSpaceUsed = null,
  }) {
    return _then(_value.copyWith(
      wifiTestResult: null == wifiTestResult
          ? _value.wifiTestResult
          : wifiTestResult // ignore: cast_nullable_to_non_nullable
              as GatewayWifiTestResult,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      sdTestResult: null == sdTestResult
          ? _value.sdTestResult
          : sdTestResult // ignore: cast_nullable_to_non_nullable
              as GatewaySdTestResult,
      sdSpaceUsed: null == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewayTestResultImplCopyWith<$Res>
    implements $GatewayTestResultCopyWith<$Res> {
  factory _$$GatewayTestResultImplCopyWith(_$GatewayTestResultImpl value,
          $Res Function(_$GatewayTestResultImpl) then) =
      __$$GatewayTestResultImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {GatewayWifiTestResult wifiTestResult,
      int rssi,
      GatewaySdTestResult sdTestResult,
      int sdSpaceUsed});
}

/// @nodoc
class __$$GatewayTestResultImplCopyWithImpl<$Res>
    extends _$GatewayTestResultCopyWithImpl<$Res, _$GatewayTestResultImpl>
    implements _$$GatewayTestResultImplCopyWith<$Res> {
  __$$GatewayTestResultImplCopyWithImpl(_$GatewayTestResultImpl _value,
      $Res Function(_$GatewayTestResultImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayTestResult
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? wifiTestResult = null,
    Object? rssi = null,
    Object? sdTestResult = null,
    Object? sdSpaceUsed = null,
  }) {
    return _then(_$GatewayTestResultImpl(
      wifiTestResult: null == wifiTestResult
          ? _value.wifiTestResult
          : wifiTestResult // ignore: cast_nullable_to_non_nullable
              as GatewayWifiTestResult,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      sdTestResult: null == sdTestResult
          ? _value.sdTestResult
          : sdTestResult // ignore: cast_nullable_to_non_nullable
              as GatewaySdTestResult,
      sdSpaceUsed: null == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$GatewayTestResultImpl implements _GatewayTestResult {
  const _$GatewayTestResultImpl(
      {required this.wifiTestResult,
      required this.rssi,
      required this.sdTestResult,
      required this.sdSpaceUsed});

  @override
  final GatewayWifiTestResult wifiTestResult;
  @override
  final int rssi;
  @override
  final GatewaySdTestResult sdTestResult;
  @override
  final int sdSpaceUsed;

  @override
  String toString() {
    return 'GatewayTestResult(wifiTestResult: $wifiTestResult, rssi: $rssi, sdTestResult: $sdTestResult, sdSpaceUsed: $sdSpaceUsed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayTestResultImpl &&
            (identical(other.wifiTestResult, wifiTestResult) ||
                other.wifiTestResult == wifiTestResult) &&
            (identical(other.rssi, rssi) || other.rssi == rssi) &&
            (identical(other.sdTestResult, sdTestResult) ||
                other.sdTestResult == sdTestResult) &&
            (identical(other.sdSpaceUsed, sdSpaceUsed) ||
                other.sdSpaceUsed == sdSpaceUsed));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, wifiTestResult, rssi, sdTestResult, sdSpaceUsed);

  /// Create a copy of GatewayTestResult
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayTestResultImplCopyWith<_$GatewayTestResultImpl> get copyWith =>
      __$$GatewayTestResultImplCopyWithImpl<_$GatewayTestResultImpl>(
          this, _$identity);
}

abstract class _GatewayTestResult implements GatewayTestResult {
  const factory _GatewayTestResult(
      {required final GatewayWifiTestResult wifiTestResult,
      required final int rssi,
      required final GatewaySdTestResult sdTestResult,
      required final int sdSpaceUsed}) = _$GatewayTestResultImpl;

  @override
  GatewayWifiTestResult get wifiTestResult;
  @override
  int get rssi;
  @override
  GatewaySdTestResult get sdTestResult;
  @override
  int get sdSpaceUsed;

  /// Create a copy of GatewayTestResult
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayTestResultImplCopyWith<_$GatewayTestResultImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
