import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/data/state/sensors_notifier.dart';

class SensorGrid extends ConsumerWidget {
  final String mac;
  final int interval;

  const SensorGrid({super.key, required this.mac, required this.interval});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final List<String> sensorMacs =
        (ref.read(sensorsNotifierProvider).value ?? [])
            .where((sensor) => sensor.gateway == mac)
            .map((sensor) => sensor.mac)
            .toList();
    final List<DateTime?> hiveLastReadings =
        (ref.read(hivesNotifierProvider).value ?? [])
            .where((hive) => sensorMacs.contains(hive.sensor))
            .map((hive) => hive.lastReading?.time)
            .toList();

    return GridView.builder(
      shrinkWrap: true,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 5,
        crossAxisSpacing: 4,
        mainAxisSpacing: 4,
        childAspectRatio: 1,
      ),
      itemCount: 10,
      itemBuilder: (context, index) {
        late Color color;
        if (index < hiveLastReadings.length) {
          Duration? sinceLastReading = hiveLastReadings[index] == null
              ? null
              : DateTime.now().toUtc().difference(hiveLastReadings[index]!);
          if (sinceLastReading == null) {
            color = Colors.black;
          } else if (sinceLastReading.inMinutes > interval * 4) {
            color = Colors.red.withAlpha(164);
          } else if (sinceLastReading.inMinutes > interval * 1) {
            color = Colors.yellow.withAlpha(164);
          } else {
            color = Colors.green.withAlpha(164);
          }
        } else {
          color = Color(0x40c19c6e);
        }
        return Container(
          color: color,
        );
      },
    );
  }
}
