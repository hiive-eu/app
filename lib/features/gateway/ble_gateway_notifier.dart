import 'dart:async';
import 'dart:convert';

import 'package:flutter_reactive_ble/flutter_reactive_ble.dart' hide Unit;
import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hiive/data/models/sensor.dart';
import 'package:hiive/data/models/wifi_network.dart';
import 'package:hiive/services/ble.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "ble_gateway_notifier.g.dart";
part "ble_gateway_notifier.freezed.dart";

@Riverpod(keepAlive: true)
class BleGatewayNotifier extends _$BleGatewayNotifier {
  StreamSubscription<ConnectionStateUpdate>? _connectionStateUpdateSubscription;
  @override
  Future<BleGatewayGuard> build() async {
    return BleGatewayGuard.gatewayUnknown();
  }

  Future<Unit> connect({String? mac, String? token}) async {
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () async {
        (
          String, // ble id
          String, // mac
          Stream<ConnectionStateUpdate>,
          int, // interval
          GatewayTestResultBle,
          String, //ssid
        ) gatewayConnectData = await ref
            .read(bleServiceProvider)
            .connect(mac, token)
            .match(
              (bleError) => throw bleError,
              (gateway) => gateway,
            )
            .run();
        _connectionStateUpdateSubscription = gatewayConnectData.$3.listen(
          (connectionState) =>
              _updateDeviceConnectionState(connectionState.connectionState),
        );

        return BleGatewayGuard.gatewayKnown(
          BleGateway(
              connectionState:
                  DeviceConnectionState.connected, // TODO check if works
              id: gatewayConnectData.$1,
              interval: gatewayConnectData.$4,
              mac: gatewayConnectData.$2,
              ssid: gatewayConnectData.$6,
              testResults: gatewayConnectData.$5.toGatewayTestResult()),
        );
      },
    );
    return unit;
  }

  Future<Unit> disconnect() async {
    _updateDeviceConnectionState(DeviceConnectionState.disconnected);
    _connectionStateUpdateSubscription?.cancel();
    return unit;
  }

  Future<Either<BleError, Unit>> setInterval(int interval) async {
    BleGatewayGuard prevState = state.value!;
    BleGatewayGuard newState = prevState;
    Either<BleError, Unit>? maybeResponse;
    state = AsyncLoading();
    state = await AsyncValue.guard(() async {
      maybeResponse = switch (state.value!) {
        GatewayUnknown() => Either.left(BleError.gatewayNotConnected()),
        GatewayKnown(:final bleGateway) => await ref
              .read(bleServiceProvider)
              .writeCharacteristic(
                Uuid.parse(systemService),
                Uuid.parse(systemIntervalChar),
                [interval],
                bleGateway.id,
              )
              .map(
            (_) {
              newState = GatewayKnown(bleGateway.copyWith(interval: interval));
              return unit;
            },
          ).run()
      };
      return newState;
    });

    return maybeResponse!;
  }

  Future<Either<BleError, GatewayWifiTestResult>> setWifiCredentials(
    String ssid,
    String password,
  ) async {
    BleGatewayGuard prevState = state.value!;
    Either<BleError, GatewayWifiTestResult>? maybeResponse;
    state = AsyncLoading();
    state = await AsyncValue.guard(() async {
      maybeResponse = switch (state.value!) {
        GatewayUnknown() => Either.left(BleError.gatewayNotConnected()),
        GatewayKnown(:final bleGateway) => await TaskEither.sequenceListSeq([
            _setWifiSsid(ssid, bleGateway.id),
            _setWifiPassword(password, bleGateway.id),
            _testWifiConnection(bleGateway.id)
          ]).map((r) => r[2] as GatewayWifiTestResult).run()
      };
      return prevState;
    });

    return maybeResponse!;
  }

  Future<Either<BleError, Unit>> setToken(String token) async {
    BleGatewayGuard prevState = state.value!;
    Either<BleError, Unit>? maybeResponse;
    state = AsyncLoading();
    state = await AsyncValue.guard(() async {
      maybeResponse = switch (state.value!) {
        GatewayUnknown() => Either.left(BleError.gatewayNotConnected()),
        GatewayKnown(:final bleGateway) => await ref
            .read(bleServiceProvider)
            .setToken(bleGateway.id, token)
            .run(),
      };
      return prevState;
    });

    return maybeResponse!;
  }

  Future<Either<BleError, Stream<WiFiNetwork>>> scanWifi() async {
    BleGatewayGuard prevState = state.value!;
    Either<BleError, Stream<WiFiNetwork>>? maybeWifiStream;

    state = AsyncLoading();
    state = await AsyncValue.guard(
      () async {
        maybeWifiStream = switch (state.value!) {
          GatewayUnknown() => Either.left(BleError.gatewayNotConnected()),
          GatewayKnown(:final bleGateway) => await ref
              .read(bleServiceProvider)
              .triggerAndStreamString(
                Uuid.parse(wifiService),
                Uuid.parse(wifiScanStartChar),
                Uuid.parse(wifiService),
                Uuid.parse(wifiScanResultsChar),
                bleGateway.id,
              )
              .map(
                (stream) => stream.map(
                  (wifiNetworkData) =>
                      WiFiNetwork.fromGatewayString(wifiNetworkData),
                ),
              )
              .run(),
        };
        return prevState;
      },
    );
    return maybeWifiStream!;
  }

  Future<Either<BleError, Stream<SensorGateway>>> startPairing() async {
    BleGatewayGuard prevState = state.value!;
    Either<BleError, Stream<SensorGateway>>? maybeSensorStream;
    state = AsyncLoading();
    state = await AsyncValue.guard(
      () async {
        maybeSensorStream = switch (state.value!) {
          GatewayUnknown() => Either.left(BleError.gatewayNotConnected()),
          GatewayKnown(:final bleGateway) => await ref
              .read(bleServiceProvider)
              .triggerAndStreamString(
                Uuid.parse(pairService),
                Uuid.parse(pairBeginChar),
                Uuid.parse(pairService),
                Uuid.parse(pairSensorInfoChar),
                bleGateway.id,
              )
              .map(
                (stream) => stream.map(
                  (sensorData) => SensorGateway.fromGatewayString(sensorData),
                ),
              )
              .run(),
        };
        return prevState;
      },
    );
    return maybeSensorStream!;
  }

  Future<Either<BleError, Unit>> endPairing() async {
    Either<BleError, Unit>? maybeResponse;
    BleGatewayGuard prevState = state.value!;
    state = AsyncLoading();
    state = await AsyncValue.guard(
      () async {
        maybeResponse = switch (state.value!) {
          GatewayUnknown() => Either.left(BleError.gatewayNotConnected()),
          GatewayKnown(:final bleGateway) => await ref
              .read(bleServiceProvider)
              .writeCharacteristic(
                response: false,
                Uuid.parse(pairService),
                Uuid.parse(pairStartCycleChar),
                [1],
                bleGateway.id,
              )
              .run(),
        };
        return prevState;
      },
    );

    return maybeResponse!;
  }

  Future<Unit> getReadingsFilenames() async {
    return unit;
  }

  Future<Unit> getReadingsFile(String filename) async {
    return unit;
  }

  Future<Unit> updateFirmware(String version) async {
    return unit;
  }

  Future<Unit> deleteReadingsFile(String filename) async {
    return unit;
  }

  TaskEither<BleError, Unit> _setWifiSsid(
    String ssid,
    String deviceId,
  ) {
    print("[BLE] Set wifi SSID: $ssid");
    return ref.read(bleServiceProvider).writeCharacteristic(
        Uuid.parse(wifiService),
        Uuid.parse(wifiSsidChar),
        ascii.encode(ssid),
        deviceId);
  }

  TaskEither<BleError, Unit> _setWifiPassword(
    String password,
    String deviceId,
  ) {
    print("[BLE] Set wifi password: $password");
    return ref.read(bleServiceProvider).writeCharacteristic(
        Uuid.parse(wifiService),
        Uuid.parse(wifiPasswordChar),
        ascii.encode(password),
        deviceId);
  }

  TaskEither<BleError, GatewayWifiTestResult> _testWifiConnection(
    String deviceId,
  ) {
    print("[BLE] Test wifi connection");
    return ref
        .read(bleServiceProvider)
        .triggerAndGatewayTestResult(
          Uuid.parse(testService),
          Uuid.parse(testCommandChar),
          Uuid.parse(testService),
          Uuid.parse(testResultChar),
          deviceId,
        )
        .map(
      (r) {
        print("testWifiConnection: ${r.wifiRes}");
        return GatewayWifiTestResult.createFromValue(r.wifiRes);
      },
    );
  }

  Future<Unit> _updateDeviceConnectionState(
    DeviceConnectionState deviceConnectionState,
  ) async {
    BleGatewayGuard prevState = state.value!;
    state = AsyncValue.loading();
    state = await AsyncValue.guard(
      () async {
        switch (prevState) {
          case GatewayUnknown():
            return prevState;
          case GatewayKnown():
            return prevState.copyWith
                .bleGateway(connectionState: deviceConnectionState);
        }
      },
    );
    return unit;
  }
}

@freezed
sealed class BleGatewayGuard with _$BleGatewayGuard {
  const factory BleGatewayGuard.gatewayUnknown() = GatewayUnknown;
  const factory BleGatewayGuard.gatewayKnown(BleGateway bleGateway) =
      GatewayKnown;
}

@freezed
class BleGateway with _$BleGateway {
  const BleGateway._();
  const factory BleGateway({
    required String id,
    required String mac,
    required DeviceConnectionState connectionState,
    required int interval,
    required String ssid,
    required GatewayTestResult testResults,
  }) = _BleGateway;

  bool isConnected() {
    return connectionState == DeviceConnectionState.connected;
  }
}

@freezed
class GatewayTestResult with _$GatewayTestResult {
  const factory GatewayTestResult({
    required GatewayWifiTestResult wifiTestResult,
    required int rssi,
    required GatewaySdTestResult sdTestResult,
    required int sdSpaceUsed,
  }) = _GatewayTestResult;
}

enum GatewayWifiTestResult {
  ok(1),
  unexpectedError(0),
  wifiEnableError(-1),
  wifiConnectTimeout(-2),
  fetchCAError(-3),
  apiRequestError(-4);

  const GatewayWifiTestResult(this.responseCode);
  final int responseCode;

  static GatewayWifiTestResult createFromValue(int i) {
    return GatewayWifiTestResult.values.firstWhere((x) => x.responseCode == i,
        orElse: () => GatewayWifiTestResult.unexpectedError);
  }
}

enum GatewaySdTestResult {
  ok(1),
  unexpectedError(0),
  mountError(-1),
  createDirError(-2),
  createFileError(-3),
  writeError(-4),
  readError(-5),
  removeFileError(-6),
  writtenAndReadNotMatchingError(-7);

  const GatewaySdTestResult(this.responseCode);
  final int responseCode;

  static GatewaySdTestResult createFromValue(int i) {
    return GatewaySdTestResult.values.firstWhere((x) => x.responseCode == i,
        orElse: () => GatewaySdTestResult.unexpectedError);
  }
}
