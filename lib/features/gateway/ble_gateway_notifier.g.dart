// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_gateway_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleGatewayNotifierHash() =>
    r'02a16225319dbed6968de17a7964938f83166d0c';

/// See also [BleGatewayNotifier].
@ProviderFor(BleGatewayNotifier)
final bleGatewayNotifierProvider =
    AsyncNotifierProvider<BleGatewayNotifier, BleGatewayGuard>.internal(
  BleGatewayNotifier.new,
  name: r'bleGatewayNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleGatewayNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BleGatewayNotifier = AsyncNotifier<BleGatewayGuard>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
