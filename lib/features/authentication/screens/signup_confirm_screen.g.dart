// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'signup_confirm_screen.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$signupConfirmNotifierHash() =>
    r'5cd3a16e8a7a45af69b44e9418b3cc094061fc03';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$SignupConfirmNotifier
    extends BuildlessAutoDisposeAsyncNotifier<void> {
  late final String token;

  FutureOr<void> build(
    String token,
  );
}

/// See also [SignupConfirmNotifier].
@ProviderFor(SignupConfirmNotifier)
const signupConfirmNotifierProvider = SignupConfirmNotifierFamily();

/// See also [SignupConfirmNotifier].
class SignupConfirmNotifierFamily extends Family<AsyncValue<void>> {
  /// See also [SignupConfirmNotifier].
  const SignupConfirmNotifierFamily();

  /// See also [SignupConfirmNotifier].
  SignupConfirmNotifierProvider call(
    String token,
  ) {
    return SignupConfirmNotifierProvider(
      token,
    );
  }

  @override
  SignupConfirmNotifierProvider getProviderOverride(
    covariant SignupConfirmNotifierProvider provider,
  ) {
    return call(
      provider.token,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'signupConfirmNotifierProvider';
}

/// See also [SignupConfirmNotifier].
class SignupConfirmNotifierProvider
    extends AutoDisposeAsyncNotifierProviderImpl<SignupConfirmNotifier, void> {
  /// See also [SignupConfirmNotifier].
  SignupConfirmNotifierProvider(
    String token,
  ) : this._internal(
          () => SignupConfirmNotifier()..token = token,
          from: signupConfirmNotifierProvider,
          name: r'signupConfirmNotifierProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$signupConfirmNotifierHash,
          dependencies: SignupConfirmNotifierFamily._dependencies,
          allTransitiveDependencies:
              SignupConfirmNotifierFamily._allTransitiveDependencies,
          token: token,
        );

  SignupConfirmNotifierProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.token,
  }) : super.internal();

  final String token;

  @override
  FutureOr<void> runNotifierBuild(
    covariant SignupConfirmNotifier notifier,
  ) {
    return notifier.build(
      token,
    );
  }

  @override
  Override overrideWith(SignupConfirmNotifier Function() create) {
    return ProviderOverride(
      origin: this,
      override: SignupConfirmNotifierProvider._internal(
        () => create()..token = token,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        token: token,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<SignupConfirmNotifier, void>
      createElement() {
    return _SignupConfirmNotifierProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is SignupConfirmNotifierProvider && other.token == token;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, token.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin SignupConfirmNotifierRef on AutoDisposeAsyncNotifierProviderRef<void> {
  /// The parameter `token` of this provider.
  String get token;
}

class _SignupConfirmNotifierProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<SignupConfirmNotifier, void>
    with SignupConfirmNotifierRef {
  _SignupConfirmNotifierProviderElement(super.provider);

  @override
  String get token => (origin as SignupConfirmNotifierProvider).token;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
