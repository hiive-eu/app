import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/data/api/endpoints/authentication/forgot_password_endpoint.dart";
import "package:hiive/data/api/endpoints/authentication/login_endpoint.dart";
import "package:hiive/data/api/endpoints/authentication/signup_endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/repositories/authentication_repository.dart";
import "package:hiive/data/state/authentication_notifier.dart";
import "package:hiive/data/state/onboarded_notifier.dart";
import "package:hiive/main.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/utils/extensions.dart";
import "package:hiive/utils/misc.dart";
import 'package:go_router/go_router.dart';

sealed class AuthScreenMode {}

class LoginMode extends AuthScreenMode {}

class SignupMode extends AuthScreenMode {}

class ForgotPasswordMode extends AuthScreenMode {}

class AuthScreen extends ConsumerStatefulWidget {
  final String? canPop;
  final String? nextScreen;
  const AuthScreen({super.key, required this.nextScreen, required this.canPop});

  @override
  AuthScreenState createState() => AuthScreenState();
}

class AuthScreenState extends ConsumerState<AuthScreen> {
  final _form = GlobalKey<FormState>();
  AuthScreenMode _mode = LoginMode();
  Future<void>? _pendingNetworkRequest;

  String _email = "";
  String _displayName = "";
  String _password = "";
  bool _remember = false;

  bool _shouldShowEmailField() => true;
  bool _shouldShowDisplayNameField() => switch (this._mode) {
        SignupMode() => true,
        _ => false,
      };
  bool _shouldShowPasswordField() => switch (this._mode) {
        ForgotPasswordMode() => false,
        _ => true,
      };

  bool _shouldShowRememberCheckbox() => switch (this._mode) {
        LoginMode() => true,
        _ => false,
      };

  bool _shouldShowSignupButton() => switch (this._mode) {
        SignupMode() => false,
        _ => true,
      };

  String? _emailValidator(String? v) {
    if (v == null || v.isEmpty) {
      return "Email is required";
    }
    return null;
  }

  String? _displayNameValidator(String? v) => switch (this._mode) {
        SignupMode() =>
          (v == null || v.isEmpty) ? "Display name is required" : null,
        _ => null,
      };

  String? _passwordValidator(String? v) => switch (this._mode) {
        SignupMode() =>
          (v == null || v.isEmpty) ? "Password is required" : null,
        _ => null,
      };

  void _onError(ApiError apiError) {
    if (!context.mounted) return;
    showSnackbar(context, apiError.toString());
  }

  void _onLoginSuccess() {
    bool onboarded = ref.read(onboardedNotifierProvider);
    print("[ONBOARDTEST - auth_screen] onboarded: $onboarded");
    print("[ONBOARDTEST - auth_screen] canPop: ${widget.canPop}");
    print("[ONBOARDTEST - auth_screen] nextScreen: ${widget.nextScreen}");
    if (widget.canPop == null && widget.nextScreen == null) {
      print("[ONBOARDTEST] 1");
      if (onboarded && context.mounted) {
        print("[ONBOARDTEST] 2");
        context.goNamed(ScreenRoute.dashboard.name);
        return;
      } else {
        print("[ONBOARDTEST] 3");
        ref.read(onboardedNotifierProvider.notifier).setOnboarded(true);
        context.goNamed(ScreenRoute.onboardWelcome.name);
        return;
      }
    }
    if (widget.nextScreen == null && context.mounted) {
      print("[ONBOARDTEST] 4");
      context.pop();
      return;
    } else {
      print("[ONBOARDTEST] 5");
      context.goNamed(widget.nextScreen!);
      return;
    }
  }

  void _onSignupSuccess() {
    if (!context.mounted) return;
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text(
          "Confirmation email sent",
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  void _onForgotPasswordSuccess() {
    if (!context.mounted) return;
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text(
          "Password reset email sent",
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _submitButton() {
    return switch (this._mode) {
      LoginMode() => FilledButton(
          onPressed: () => _submitForm(),
          style: FilledButton.styleFrom(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 130.0),
            minimumSize: const Size(140, 60),
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
          child: Text(
            "Login",
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ),
      SignupMode() => FilledButton(
          onPressed: () => _submitForm(),
          style: FilledButton.styleFrom(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 130.0),
            minimumSize: const Size(140, 60),
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
          child: Text(
            "Sign Up",
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ),
      ForgotPasswordMode() => FilledButton(
          onPressed: () => _submitForm(),
          style: FilledButton.styleFrom(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 130.0),
            minimumSize: const Size(140, 60),
            elevation: 3,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
          child: Text(
            "Reset Password",
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ),
    };
  }

  Widget _changeModeButton() => switch (this._mode) {
        LoginMode() => TextButton(
            onPressed: () {
              setState(() => this._mode = ForgotPasswordMode());
            },
            child: const Text(
              "Forgot Password?",
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
          ),
        SignupMode() => TextButton(
            onPressed: () {
              setState(() => this._mode = LoginMode());
            },
            child: const Text(
              "Login",
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
          ),
        ForgotPasswordMode() => TextButton(
            onPressed: () {
              setState(() => this._mode = LoginMode());
            },
            child: const Text(
              "Login",
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
            ),
          ),
      };

  void _submitForm() async {
    if (_form.currentState!.validate()) {
      _form.currentState!.save();

      switch (this._mode) {
        case LoginMode():
          {
            final loginData =
                LoginData(email: this._email, password: this._password);
            final future = ref
                // TODO Handle error and result
                .read(authenticationNotifierProvider.notifier)
                .login(loginData, this._remember)
                .then(
              (res) {
                res.match(
                  (apiError) => _onError(apiError),
                  (_) => _onLoginSuccess(),
                );
              },
            );
            setState(() {
              _pendingNetworkRequest = future;
            });
          }
        case SignupMode():
          {
            final signupData = SignupData(
                email: this._email,
                displayName: this._displayName,
                password: this._password);
            await ref
                .read(authenticationRepositoryProvider)
                .signup(signupData)
                .run()
                .then(
                  (result) => result.match(
                    _onError,
                    (_) => _onSignupSuccess(),
                  ),
                );
          }
        case ForgotPasswordMode():
          {
            final forgotPasswordData = ForgotPasswordData(
              email: this._email,
            );
            await ref
                .read(authenticationRepositoryProvider)
                .forgotPassword(forgotPasswordData)
                .run()
                .then(
                  (result) => result.match(
                    _onError,
                    (_) => _onForgotPasswordSuccess(),
                  ),
                );
          }
      }
    }
  }

  PreferredSizeWidget appBarCloseable() {
    return AppBar(
      title: appBarTitle(),
      actions: [
        IconButton(
          onPressed: () => context.goNamed(ScreenRoute.dashboard.name),
          icon: Icon(Icons.close),
        )
      ],
    );
  }

  PreferredSizeWidget appBarUncloseable() {
    return AppBar(
      title: appBarTitle(),
    );
  }

  @override
  Widget build(BuildContext context) {
    print("[ONBOARDTEST] canPop: ${widget.canPop}");
    print("[ONBOARDTEST] nextScreen: ${widget.nextScreen}");
    bool screenCloseable = widget.canPop?.toBool() ?? false;
    return FutureBuilder(
      future: _pendingNetworkRequest,
      builder: (context, snapshot) {
        return GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Scaffold(
            appBar: screenCloseable ? appBarCloseable() : appBarUncloseable(),
            body: snapshot.connectionState == ConnectionState.waiting
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SingleChildScrollView(
                    child: Column(
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(top: 30.0, bottom: 20.0),
                          child: Center(
                            child: SizedBox(
                                width: 200,
                                child: Image(
                                    image:
                                        AssetImage('assets/images/logo.webp'))),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 24.0),
                          child: Form(
                            key: _form,
                            child: Column(
                              children: <Widget>[
                                Visibility(
                                  visible: _shouldShowEmailField(),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: TextFormField(
                                      decoration: const InputDecoration(
                                        prefixIcon: Icon(Icons.email),
                                        border: OutlineInputBorder(),
                                        labelText: "Email",
                                      ),
                                      initialValue: this._email,
                                      keyboardType: TextInputType.emailAddress,
                                      textInputAction: TextInputAction.next,
                                      validator: _emailValidator,
                                      onSaved: (value) =>
                                          this._email = value ?? "",
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: _shouldShowDisplayNameField(),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: TextFormField(
                                      decoration: const InputDecoration(
                                        prefixIcon: Icon(Icons.face),
                                        border: OutlineInputBorder(),
                                        labelText: "Display Name",
                                      ),
                                      initialValue: this._displayName,
                                      textInputAction: TextInputAction.next,
                                      validator: _displayNameValidator,
                                      onSaved: (value) =>
                                          this._displayName = value ?? "",
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: _shouldShowPasswordField(),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 8.0),
                                    child: TextFormField(
                                      decoration: const InputDecoration(
                                        prefixIcon: Icon(Icons.lock),
                                        border: OutlineInputBorder(),
                                        labelText: "Password",
                                      ),
                                      initialValue: this._password,
                                      obscureText: true,
                                      textInputAction: TextInputAction.done,
                                      validator: _passwordValidator,
                                      onSaved: (value) =>
                                          this._password = value ?? "",
                                      onFieldSubmitted: (_) => _submitForm(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 10.0,
                        ),
                        Visibility(
                          visible: _shouldShowRememberCheckbox(),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text("remember credentials:"),
                              Checkbox(
                                value: this._remember,
                                onChanged: (bool? b) {
                                  setState(() {
                                    this._remember = b!;
                                  });
                                },
                              ),
                            ],
                          ),
                        ),
                        _changeModeButton(),
                        _submitButton(),
                        Visibility(
                          visible: _shouldShowSignupButton(),
                          child: Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: TextButton(
                              onPressed: () {
                                setState(() => this._mode = SignupMode());
                              },
                              child: const Text(
                                "Sign Up",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
          ),
        );
      },
    );
  }

  Widget appBarTitle() => switch (this._mode) {
        LoginMode() => const Text("Login"),
        SignupMode() => const Text("Sign Up"),
        ForgotPasswordMode() => const Text("Forgot Password"),
      };
}
