import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hiive/data/api/endpoints/authentication/signup_confirm_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/repositories/authentication_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "signup_confirm_screen.g.dart";

class SignupConfirmScreen extends ConsumerStatefulWidget {
  final String token;
  const SignupConfirmScreen({super.key, required this.token});

  @override
  ConsumerState<SignupConfirmScreen> createState() =>
      _SignupConfirmScreenState();
}

class _SignupConfirmScreenState extends ConsumerState<SignupConfirmScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(""),
      ),
      body: ref.watch(signupConfirmNotifierProvider(widget.token)).map(
            data: (result) => ok(),
            error: (error) => err(error.error as ApiError),
            loading: (_) => loading(),
          ),
    );
  }

  Widget ok() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "Account confirmed!",
            style: Theme.of(context).textTheme.headlineLarge,
          ),
          TextButton(
            onPressed: () =>
                !context.mounted ? {} : Navigator.of(context).pop(),
            child: const Text("Login"),
          )
        ],
      ),
    );
  }

  Widget loading() {
    return const Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          CircularProgressIndicator(),
        ],
      ),
    );
  }

  Widget err(ApiError error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "Error: ${error.toString()}",
            style: Theme.of(context).textTheme.titleLarge,
          ),
        ],
      ),
    );
  }
}

@riverpod
class SignupConfirmNotifier extends _$SignupConfirmNotifier {
  @override
  Future<void> build(
    String token,
  ) async {
    return fetchSignupConfirm(SignupConfirmData(token: token));
  }

  Future<void> fetchSignupConfirm(SignupConfirmData signupConfirmData) async {
    return ref
        .read(authenticationRepositoryProvider)
        .signupConfirm(signupConfirmData)
        .match(
          (apiError) => throw apiError,
          (_) {},
        )
        .run();
  }
}
