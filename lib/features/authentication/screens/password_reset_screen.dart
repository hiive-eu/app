import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hiive/data/api/endpoints/authentication/reset_password_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/repositories/authentication_repository.dart';
import 'package:hiive/utils/misc.dart';

class PasswordResetScreen extends ConsumerStatefulWidget {
  final String token;
  const PasswordResetScreen({super.key, required this.token});

  @override
  ConsumerState<PasswordResetScreen> createState() =>
      _PasswordResetScreenState();
}

class _PasswordResetScreenState extends ConsumerState<PasswordResetScreen> {
  final _form = GlobalKey<FormState>();
  var _isLoading = false;

  String _newPassword = "";
  String _confirmPassword = "";

  String? _passwordValidator(String? v) =>
      (v == null || v.isEmpty) ? "Password is required" : null;

  void _onError(ApiError apiError) {
    if (!context.mounted) return;
    setState(() => this._isLoading = false);
    showSnackbar(context, apiError.toString());
  }

  void _onResetPasswordSuccess() {
    if (!context.mounted) return;
    showSnackbar(context, "Password changed!");

    setState(() => this._isLoading = false);
    Navigator.of(context).pop();
  }

  void _submitForm() async {
    if (_form.currentState!.validate()) {
      setState(() => this._isLoading = true);
      _form.currentState!.save();

      final resetPasswordData = ResetPasswordData(
          token: widget.token,
          newPassword: this._newPassword,
          newPasswordConfirm: this._confirmPassword);
      await ref
          // TODO replace with authentication notifier
          .read(authenticationRepositoryProvider)
          .resetPassword(resetPasswordData)
          .match(
            _onError,
            (_) => _onResetPasswordSuccess(),
          )
          .run();
    } else {
      setState(() => this._isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reset password"),
      ),
      body: this._isLoading
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24.0),
                child: Form(
                  key: _form,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            prefixIcon: Icon(Icons.lock),
                            border: OutlineInputBorder(),
                            labelText: "New password",
                          ),
                          initialValue: this._newPassword,
                          obscureText: true,
                          textInputAction: TextInputAction.next,
                          validator: _passwordValidator,
                          onSaved: (value) => this._newPassword = value ?? "",
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: TextFormField(
                          decoration: const InputDecoration(
                            prefixIcon: Icon(Icons.lock),
                            border: OutlineInputBorder(),
                            labelText: "Confirm password",
                          ),
                          initialValue: this._confirmPassword,
                          obscureText: true,
                          textInputAction: TextInputAction.done,
                          validator: _passwordValidator,
                          onSaved: (value) =>
                              this._confirmPassword = value ?? "",
                          onFieldSubmitted: (_) => _submitForm(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              FilledButton(
                onPressed: _submitForm,
                style: FilledButton.styleFrom(
                  padding: const EdgeInsets.all(12.0),
                  minimumSize: const Size(140, 60),
                  elevation: 3,
                ),
                child: Text(
                  "Reset password",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
            ]),
    );
  }
}
