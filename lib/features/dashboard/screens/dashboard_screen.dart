import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import "package:hiive/app.dart";
import "package:hiive/app_config.dart";
import "package:hiive/constants/app_theme.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/dashboard.dart";
import "package:hiive/data/state/authentication_notifier.dart";
import "package:hiive/data/state/dashboard_notifier.dart";
import "package:hiive/data/state/logged_in_notifier.dart";
import "package:hiive/features/dashboard/widgets/dashboard_gateways.dart";
import "package:hiive/features/dashboard/widgets/dashboard_hives.dart";
import 'package:go_router/go_router.dart';
import "package:hiive/router/app_router.dart";
import "package:hiive/utils/misc.dart";

class DashboardScreen extends ConsumerStatefulWidget {
  const DashboardScreen({super.key});

  @override
  ConsumerState<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends ConsumerState<DashboardScreen> {
  Future<void>? _pendingNetworkRequest;

  void _onError(ApiError apiError) {
    if (mounted) {
      showSnackbar(context, apiError.toString());
    }
  }

  Future<void> showLogoutDialog(BuildContext context, WidgetRef ref) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog.adaptive(
            title: const Text("Log Out"),
            content: const Text("Are you sure you want to log out?"),
            actions: [
              TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text("Cancel")),
              FilledButton(
                  onPressed: () {
                    if (!context.mounted) return;
                    Navigator.of(context).pop();

                    final future = ref
                        .read(authenticationNotifierProvider.notifier)
                        .logout()
                        .then(
                      (res) {
                        res.match(
                          (apiError) => _onError(apiError),
                          (_) => (),
                        );
                      },
                    );
                    setState(() {
                      _pendingNetworkRequest = future;
                    });
                  },
                  child: const Text("Log out"))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    //Widget build(BuildContext context, WidgetRef ref) {
    bool loggedIn = ref.watch(loggedInNotifierProvider);
    final AsyncValue<Unit> dashboardRes = ref.watch(dashboardNotifierProvider);

    List<Widget> devActions = [
      IconButton(
        onPressed: () => throw Exception('This is test exception'),
        icon: Icon(Icons.hub_rounded),
      ),
      IconButton(
        onPressed: () => context.pushNamed(ScreenRoute.dev.name),
        icon: Icon(Icons.settings),
      ),
      PopupMenuButton(
        itemBuilder: (context) => [
          const PopupMenuItem(value: 0, child: Text("Settings")),
          PopupMenuItem(
              value: 1, child: loggedIn ? Text("Logout") : Text("Login")),
        ],
        onSelected: (value) {
          switch (value) {
            case 0:
              context.pushNamed(ScreenRoute.user.name);
              break;
            case 1:
              loggedIn
                  ? showLogoutDialog(context, ref)
                  : context.pushNamed(ScreenRoute.auth.name);
              break;
            default:
          }
        },
      ),
    ];

    List<Widget> prodActions = [
      PopupMenuButton(
        itemBuilder: (context) => [
          const PopupMenuItem(value: 0, child: Text("Settings")),
          PopupMenuItem(
              value: 1, child: loggedIn ? Text("Logout") : Text("Login")),
        ],
        onSelected: (value) {
          switch (value) {
            case 0:
              context.pushNamed(ScreenRoute.user.name);
              break;
            case 1:
              loggedIn
                  ? showLogoutDialog(context, ref)
                  : context.goNamed(ScreenRoute.auth.name);
              break;
            default:
          }
        },
      )
    ];

    return FutureBuilder(
      future: _pendingNetworkRequest,
      builder: (context, snapshot) {
        return DefaultTabController(
            initialIndex: 0,
            length: 2,
            child: Scaffold(
              appBar: AppBar(
                title: const Text("Dashboard"),
                bottom: TabBar(
                  indicatorColor: Theme.of(context).colorScheme.onPrimary,
                  tabs: [
                    Tab(
                      child: Text(
                        "HIVES",
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.w600),
                      ),
                    ),
                    Tab(
                      child: Text(
                        "GATEWAYS",
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontWeight: FontWeight.w600),
                      ),
                    ),
                  ],
                ),
                actions:
                    ref.read(appConfigProvider).flavor == AppFlavor.development
                        ? devActions
                        : prodActions,
              ),
              body: DecoratedBox(
                decoration: BoxDecoration(
                  image: DecorationImage(
                      opacity: 0.5,
                      image: AssetImage("assets/images/gradient_bg.webp"),
                      fit: BoxFit.cover),
                ),
                child: snapshot.connectionState == ConnectionState.waiting
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : dashboardRes.maybeWhen(
                        loading: () => const Center(
                          child: CircularProgressIndicator(),
                        ),
                        orElse: () => TabBarView(
                            children: [DashboardHives(), DashboardGateways()]),
                      ),
              ),
            ));
      },
    );
  }
}
