// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_gateways.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$dashboardGatewaysPageControllerHash() =>
    r'a58d6ec3c0652517a4e8a6926e689834d6d0a530';

/// See also [dashboardGatewaysPageController].
@ProviderFor(dashboardGatewaysPageController)
final dashboardGatewaysPageControllerProvider =
    AutoDisposeProvider<PageController>.internal(
  dashboardGatewaysPageController,
  name: r'dashboardGatewaysPageControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$dashboardGatewaysPageControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef DashboardGatewaysPageControllerRef
    = AutoDisposeProviderRef<PageController>;
String _$dashboardGatewayPageIndexNotifierHash() =>
    r'c1f7882e0a68211c93e1bd06cce4295c9e087d22';

/// See also [DashboardGatewayPageIndexNotifier].
@ProviderFor(DashboardGatewayPageIndexNotifier)
final dashboardGatewayPageIndexNotifierProvider = AutoDisposeNotifierProvider<
    DashboardGatewayPageIndexNotifier, int>.internal(
  DashboardGatewayPageIndexNotifier.new,
  name: r'dashboardGatewayPageIndexNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$dashboardGatewayPageIndexNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$DashboardGatewayPageIndexNotifier = AutoDisposeNotifier<int>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
