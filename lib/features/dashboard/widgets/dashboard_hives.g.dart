// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'dashboard_hives.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$dashboardHivesPageControllerHash() =>
    r'aa893752999782ba97d6cbf921213819d90370c7';

/// See also [dashboardHivesPageController].
@ProviderFor(dashboardHivesPageController)
final dashboardHivesPageControllerProvider =
    AutoDisposeProvider<PageController>.internal(
  dashboardHivesPageController,
  name: r'dashboardHivesPageControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$dashboardHivesPageControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef DashboardHivesPageControllerRef
    = AutoDisposeProviderRef<PageController>;
String _$dashboardHivePageIndexNotifierHash() =>
    r'b3a3095e23a79ea4941574fd0fcfea7a90bdf825';

/// See also [DashboardHivePageIndexNotifier].
@ProviderFor(DashboardHivePageIndexNotifier)
final dashboardHivePageIndexNotifierProvider =
    AutoDisposeNotifierProvider<DashboardHivePageIndexNotifier, int>.internal(
  DashboardHivePageIndexNotifier.new,
  name: r'dashboardHivePageIndexNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$dashboardHivePageIndexNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$DashboardHivePageIndexNotifier = AutoDisposeNotifier<int>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
