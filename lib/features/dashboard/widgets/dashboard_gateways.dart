import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:hiive/data/state/gateways_notifier.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/router/app_router.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "dashboard_gateways.g.dart";

class DashboardGateways extends ConsumerWidget {
  const DashboardGateways({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final AsyncValue<List<Gateway>> gatewaysProviderState =
        ref.watch(gatewaysNotifierProvider);
    final List<Gateway> gateways =
        ref.watch(gatewaysNotifierProvider).value ?? [];
    return Container(
      margin: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: switch (gatewaysProviderState) {
        AsyncLoading _ => Center(
            child: CircularProgressIndicator(),
          ),
        _ => Stack(
            alignment: AlignmentDirectional.bottomEnd,
            children: [
              Column(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        final gateway = gateways[ref
                            .read(dashboardGatewaysPageControllerProvider)
                            .page!
                            .toInt()];
                        context.pushNamed(
                          ScreenRoute.gateway.name,
                          pathParameters: {"mac": gateway.mac},
                        );
                      },
                      child: PageView(
                        controller:
                            ref.watch(dashboardGatewaysPageControllerProvider),
                        children: gateways.isNotEmpty
                            ? gateways
                                .map((h) => GatewayPageViewItem(gateway: h))
                                .toList()
                            : [const Center(child: Text("No gateways"))],
                        onPageChanged: (index) {
                          ref
                              .read(dashboardGatewayPageIndexNotifierProvider
                                  .notifier)
                              .setIndex(index);
                        },
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      gateways.length,
                      (index) => Container(
                        margin: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 16),
                        width: 8,
                        height: 8,
                        decoration: BoxDecoration(
                            color: index ==
                                    ref.watch(
                                        dashboardGatewayPageIndexNotifierProvider)
                                ? Theme.of(context).colorScheme.secondary
                                : Colors.black26,
                            shape: BoxShape.circle),
                      ),
                    ),
                  ),
                ],
              ),
              FloatingActionButton(
                onPressed: () =>
                    context.pushNamed(ScreenRoute.setupGatewayConnect.name),
                child: Icon(
                  Icons.add,
                ),
              ),
            ],
          ),
      },
    );
  }
}

class GatewayPageViewItem extends StatelessWidget {
  final Gateway gateway;

  const GatewayPageViewItem({super.key, required this.gateway});

  @override
  Widget build(BuildContext context) {
    Duration? sinceLastReading = gateway.lastReading?.time == null
        ? null
        : DateTime.now().toUtc().difference(gateway.lastReading!.time);
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            size: 32,
                            Icons.thermostat,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: gateway.lastReading == null
                                ? Text("-")
                                : Text(
                                    "${gateway.lastReading!.temperature}°C",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            size: 32,
                            Icons.water_drop,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: gateway.lastReading == null
                                ? Text("-")
                                : Text(
                                    "${gateway.lastReading!.humidity}%",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                          )
                        ],
                      ),
                    ],
                  ),
                  if (sinceLastReading != null)
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "${sinceLastReading.inMinutes} minutes ago",
                        style: Theme.of(context).textTheme.bodySmall,
                      ),
                    ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(32, 32, 32, 32),
                child: Image.asset("assets/images/gateway.webp"),
              ),
              Center(
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    gateway.name.toUpperCase(),
                    style: Theme.of(context).textTheme.displayLarge,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

@riverpod
PageController dashboardGatewaysPageController(
    DashboardGatewaysPageControllerRef ref) {
  final pageController = PageController(
    initialPage: ref.read(dashboardGatewayPageIndexNotifierProvider),
  );
  return pageController;
}

@riverpod
class DashboardGatewayPageIndexNotifier
    extends _$DashboardGatewayPageIndexNotifier {
  @override
  int build() {
    return 0;
  }

  void setIndex(int index) {
    ref.read(dashboardGatewaysPageControllerProvider).jumpToPage(index);
    state = index;
  }
}
