import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/state/sensors_notifier.dart';
import 'package:hiive/router/app_router.dart';
import 'package:hiive/utils/misc.dart';
import 'package:hiive/widgets/dialogs/hive.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "dashboard_hives.g.dart";

class DashboardHives extends ConsumerWidget {
  const DashboardHives({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final AsyncValue<List<Hive>> hivesProviderState =
        ref.watch(hivesNotifierProvider);
    final List<Hive> hives = ref.watch(hivesNotifierProvider).value ?? [];
    return Container(
      margin: const EdgeInsets.fromLTRB(16, 0, 16, 16),
      child: switch (hivesProviderState) {
        AsyncLoading _ => Center(
            child: CircularProgressIndicator(),
          ),
        _ => Stack(
            alignment: AlignmentDirectional.bottomEnd,
            children: [
              Column(
                children: [
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        final hive = hives[ref
                            .read(dashboardHivesPageControllerProvider)
                            .page!
                            .toInt()];
                        context.pushNamed(
                          ScreenRoute.hive.name,
                          pathParameters: {"id": hive.id},
                        );
                      },
                      child: PageView(
                        controller:
                            ref.watch(dashboardHivesPageControllerProvider),
                        children: hives.isNotEmpty
                            ? hives
                                .map((h) => HivePageViewItem(
                                      hive: h,
                                      battery: ref
                                          .watch(sensorsNotifierProvider)
                                          .getByMac(h.sensor)
                                          ?.battery,
                                    ))
                                .toList()
                            : [const Center(child: Text("No hives"))],
                        onPageChanged: (index) {
                          ref
                              .read(dashboardHivePageIndexNotifierProvider
                                  .notifier)
                              .setIndex(index);
                        },
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List.generate(
                      hives.length,
                      (index) => Container(
                        margin: const EdgeInsets.fromLTRB(8.0, 8, 8.0, 8),
                        width: 8,
                        height: 8,
                        decoration: BoxDecoration(
                            color: index ==
                                    ref.watch(
                                        dashboardHivePageIndexNotifierProvider)
                                ? Theme.of(context).colorScheme.secondary
                                : Colors.black26,
                            shape: BoxShape.circle),
                      ),
                    ),
                  ),
                ],
              ),
              FloatingActionButton(
                onPressed: () {
                  createHiveDialog(context)
                      .match((r) => print(r), (l) => print(l))
                      .run();
                },
                child: Icon(
                  Icons.add,
                ),
              ),
            ],
          ),
      },
    );
  }
}

class HivePageViewItem extends StatelessWidget {
  final Hive hive;
  final int? battery;

  const HivePageViewItem(
      {super.key, required this.hive, required this.battery});

  @override
  Widget build(BuildContext context) {
    Duration? sinceLastReading = hive.lastReading?.time == null
        ? null
        : DateTime.now().toUtc().difference(hive.lastReading!.time);
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            size: 32,
                            Icons.thermostat,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: hive.lastReading == null
                                ? Text("-")
                                : Text(
                                    "${hive.lastReading!.temperature}°C",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            size: 32,
                            Icons.mic,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: hive.lastReading == null
                                ? Text("-")
                                : Text(
                                    "${hive.lastReading!.soundLevel} dB SPL",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                          )
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            size: 32,
                            Icons.water_drop,
                            color: Theme.of(context).colorScheme.primary,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: hive.lastReading == null
                                ? Text("-")
                                : Text(
                                    "${hive.lastReading!.humidity}%",
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        size: 26,
                        batteryIconFromBattery(battery ?? 100),
                        color: Theme.of(context).colorScheme.primary,
                      ),
                      hive.lastReading == null
                          ? Text("-")
                          : Text(
                              "$battery%",
                              style: Theme.of(context).textTheme.bodySmall,
                            ),
                    ],
                  ),
                  if (sinceLastReading != null)
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "${sinceLastReading.inMinutes} minutes ago",
                        style: Theme.of(context).textTheme.bodySmall,
                      ),
                    ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(32, 32, 32, 32),
                child: Image.asset(hive.isHiive
                    ? "assets/images/hiive.webp"
                    : "assets/images/boxhive.webp"),
              ),
              Center(
                child: FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    hive.name.toUpperCase(),
                    style: Theme.of(context).textTheme.displayLarge,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

@riverpod
PageController dashboardHivesPageController(
    DashboardHivesPageControllerRef ref) {
  final pageController = PageController(
    initialPage: ref.read(dashboardHivePageIndexNotifierProvider),
  );
  return pageController;
}

@riverpod
class DashboardHivePageIndexNotifier extends _$DashboardHivePageIndexNotifier {
  @override
  int build() {
    return 0;
  }

  void setIndex(int index) {
    ref.read(dashboardHivesPageControllerProvider).jumpToPage(index);
    state = index;
  }
}
