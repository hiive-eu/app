import "package:go_router/go_router.dart";

import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/constants/app_theme.dart";
import "package:hiive/data/state/onboarded_notifier.dart";
import "package:hiive/main.dart";
import "package:hiive/router/app_router.dart";

class OnboardWelcomeScreen extends ConsumerStatefulWidget {
  const OnboardWelcomeScreen({super.key});

  @override
  ConsumerState<OnboardWelcomeScreen> createState() =>
      _OnboardWelcomeScreenState();
}

class _OnboardWelcomeScreenState extends ConsumerState<OnboardWelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    completeOnboarding();
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () => context.goNamed(ScreenRoute.dashboard.name),
            icon: Icon(Icons.close),
          )
        ],
      ),
      body: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: <Color>[colorNewHighlightOrange, colorNewBaseBrown],
            stops: [0.0, 0.4],
          ),
          image: DecorationImage(
            image: AssetImage("assets/images/onboard_hives_bg.webp"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 46, horizontal: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text("WELCOME", style: Theme.of(context).textTheme.displayMedium),
              SizedBox(height: 32),
              Text(
                "Which type of hive do you want to set up?",
                style: Theme.of(context).textTheme.titleLarge,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  FilledButton.tonal(
                    style: FilledButton.styleFrom(elevation: 3),
                    onPressed: () => context.goNamed(
                      ScreenRoute.onboardHive.name,
                      queryParameters: {"isHiive": "false"},
                    ),
                    child: Text(
                      "Other",
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(fontWeight: FontWeight.w600),
                    ),
                  ),
                  SizedBox(width: 32),
                  FilledButton(
                    style: FilledButton.styleFrom(elevation: 3),
                    onPressed: () => context.goNamed(
                        ScreenRoute.onboardHive.name,
                        queryParameters: {"isHiive": "true"}),
                    child: Text(
                      "HIIVE",
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(fontWeight: FontWeight.w600),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
