import "package:go_router/go_router.dart";

import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/constants/app_theme.dart";
import "package:hiive/constants/defaults.dart";
import "package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart";
import "package:hiive/data/state/hives_notifier.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/utils/misc.dart";

class OnboardHiveScreen extends ConsumerStatefulWidget {
  final String isHiiveParam;
  const OnboardHiveScreen({super.key, required this.isHiiveParam});

  @override
  ConsumerState<OnboardHiveScreen> createState() => _OnboardHiveScreenState();
}

class _OnboardHiveScreenState extends ConsumerState<OnboardHiveScreen> {
  Future<void>? loadingFuture;
  final _form = GlobalKey<FormState>();
  String _name = "";

  String? _nameValidator(String? v) {
    String illegalChar = "";
    if (v == null || v.isEmpty || v.length > 255) {
      return "Name must be between 1 and 255 characters";
    }
    if (forbiddenNameChars.any(
      (e) {
        if (v.contains(e)) {
          illegalChar = e;
          return true;
        } else {
          return false;
        }
      },
    )) {
      return "Name must not contain: $illegalChar ";
    }
    return null;
  }

  void _onNameSubmitted(bool isHiive) {
    if (_form.currentState!.validate()) {
      _form.currentState!.save();
      final future = ref
          .read(hivesNotifierProvider.notifier)
          .createHive(
            CreateHiveData(name: this._name, isHiive: isHiive),
          )
          .then((maybeClaimError) {
        maybeClaimError.match(
          (dataError) => _onDataError(context, dataError),
          (hive) {
            context.goNamed(
              ScreenRoute.onboardGateway.name,
            );
          },
        );
      });
      setState(() {
        loadingFuture = future;
      });
    }
  }

  void _onDataError(context, dataError) {
    if (mounted) {
      showSnackbar(context, dataError.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isHiive = widget.isHiiveParam == "true" ? true : false;
    return FutureBuilder(
      future: loadingFuture,
      builder: (context, snapshot) {
        return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () => context.goNamed(ScreenRoute.dashboard.name),
                icon: Icon(Icons.close),
              )
            ],
          ),
          body: DecoratedBox(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: <Color>[colorNewHighlightOrange, colorNewBaseBrown],
                stops: [0.0, 0.4],
              ),
              image: DecorationImage(
                  image: AssetImage(
                      "assets/images/${isHiive ? "onboard_hiive_bg.webp" : "onboard_boxhive_bg.webp"}"),
                  fit: BoxFit.cover),
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 46, horizontal: 32),
              child: snapshot.connectionState == ConnectionState.waiting
                  ? Center(child: CircularProgressIndicator())
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "What would you like to name the colony?",
                          style: Theme.of(context).textTheme.titleLarge,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 32),
                        Form(
                          key: _form,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: TextFormField(
                                  decoration: const InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: "Name",
                                  ),
                                  initialValue: this._name,
                                  keyboardType: TextInputType.text,
                                  textInputAction: TextInputAction.done,
                                  validator: _nameValidator,
                                  onSaved: (value) => this._name = value ?? "",
                                  onFieldSubmitted: (_) =>
                                      _onNameSubmitted(isHiive),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 32),
                        FilledButton(
                            style: FilledButton.styleFrom(elevation: 3),
                            child: Text(
                              "Continue",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(fontWeight: FontWeight.w600),
                            ),
                            onPressed: () => _onNameSubmitted(isHiive)),
                      ],
                    ),
            ),
          ),
        );
      },
    );
  }
}
