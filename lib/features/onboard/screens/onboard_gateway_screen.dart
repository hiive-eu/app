import "package:go_router/go_router.dart";

import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/constants/app_theme.dart";
import "package:hiive/router/app_router.dart";

class OnboardGatewayScreen extends ConsumerStatefulWidget {
  const OnboardGatewayScreen({super.key});

  @override
  ConsumerState<OnboardGatewayScreen> createState() =>
      _OnboardGatewayScreenState();
}

class _OnboardGatewayScreenState extends ConsumerState<OnboardGatewayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () => context.goNamed(ScreenRoute.dashboard.name),
            icon: Icon(Icons.close),
          )
        ],
      ),
      body: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: <Color>[colorNewHighlightOrange, colorNewBaseBrown],
            stops: [0.0, 0.4],
          ),
          image: DecorationImage(
              image: AssetImage("assets/images/onboard_gateway_bg.webp"),
              fit: BoxFit.cover),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 46, horizontal: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Want to set up a gateway?",
                style: Theme.of(context).textTheme.titleLarge,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 32),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                FilledButton.tonal(
                  onPressed: () => context.goNamed(
                    ScreenRoute.dashboard.name,
                  ),
                  style: FilledButton.styleFrom(elevation: 3),
                  child: Text(
                    "Later",
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w600),
                  ),
                ),
                SizedBox(width: 32),
                FilledButton(
                  onPressed: () =>
                      context.goNamed(ScreenRoute.setupGatewayConnect.name),
                  style: FilledButton.styleFrom(elevation: 3),
                  child: Text(
                    "Yes",
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w600),
                  ),
                ),
              ])
            ],
          ),
        ),
      ),
    );
  }
}
