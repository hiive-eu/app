import 'package:flutter/material.dart';

class TextStylesScreen extends StatelessWidget {
  const TextStylesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TextTheme textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: AppBar(
        title: Text('TextTheme Showcase'),
      ),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: [
          Text('Display Large', style: textTheme.displayLarge),
          Text('Display Medium', style: textTheme.displayMedium),
          Text('Display Small', style: textTheme.displaySmall),
          SizedBox(height: 16),
          Text('Headline Large', style: textTheme.headlineLarge),
          Text('Headline Medium', style: textTheme.headlineMedium),
          Text('Headline Small', style: textTheme.headlineSmall),
          SizedBox(height: 16),
          Text('Title Large', style: textTheme.titleLarge),
          Text('Title Medium', style: textTheme.titleMedium),
          Text('Title Small', style: textTheme.titleSmall),
          SizedBox(height: 16),
          Text('Body Large', style: textTheme.bodyLarge),
          Text('Body Medium', style: textTheme.bodyMedium),
          Text('Body Small', style: textTheme.bodySmall),
          SizedBox(height: 16),
          Text('Label Large', style: textTheme.labelLarge),
          Text('Label Medium', style: textTheme.labelMedium),
          Text('Label Small', style: textTheme.labelSmall),
        ],
      ),
    );
  }
}
