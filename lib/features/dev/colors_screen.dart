import 'package:flutter/material.dart';

class ColorsScreen extends StatelessWidget {
  const ColorsScreen({super.key});

  Widget _buildColorBox(String label, Color color, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 50,
            color: color,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    label,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(color: Colors.white),
                  ),
                  Text(
                    label,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium!
                        .copyWith(color: Colors.black),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 4.0),
          Text(
            'Hex: ${color.value.toRadixString(16).padLeft(8, '0').toUpperCase()}',
            style: TextStyle(color: Colors.black54, fontSize: 12),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Colors Showcase'),
      ),
      body: ListView(
        padding: EdgeInsets.all(16),
        children: [
          _buildColorBox(
              'Primary', Theme.of(context).colorScheme.primary, context),
          _buildColorBox(
              'On Primary', Theme.of(context).colorScheme.onPrimary, context),
          _buildColorBox(
              'Secondary', Theme.of(context).colorScheme.secondary, context),
          _buildColorBox('On Secondary',
              Theme.of(context).colorScheme.onSecondary, context),
          _buildColorBox(
              'Surface', Theme.of(context).colorScheme.surface, context),
          _buildColorBox(
              'On Surface', Theme.of(context).colorScheme.onSurface, context),
          _buildColorBox('Error', Theme.of(context).colorScheme.error, context),
          _buildColorBox(
              'On Error', Theme.of(context).colorScheme.onError, context),
        ],
      ),
    );
  }
}
