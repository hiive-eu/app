import "package:go_router/go_router.dart";
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hiive/constants/defaults.dart';
import 'package:hiive/data/state/onboarded_notifier.dart';
import 'package:hiive/router/app_router.dart';
import 'package:hiive/theme_mode_notfier.dart';
import 'package:hiive/utils/extensions.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

class DevScreen extends ConsumerStatefulWidget {
  const DevScreen({super.key});

  @override
  ConsumerState<DevScreen> createState() => _DevScreenState();
}

class _DevScreenState extends ConsumerState<DevScreen> {
  @override
  Widget build(BuildContext context) {
    ThemeMode themeMode = ref.watch(themeModeNotifierProvider);

    return Scaffold(
      appBar: AppBar(
        title: Text('Widget Showcase'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          children: [
            FilledButton(
              onPressed: () => context.goNamed(ScreenRoute.textStyles.name),
              child: Text("Text styles"),
            ),
            SizedBox(height: 16),
            FilledButton(
              onPressed: () => context.goNamed(ScreenRoute.widgetStyles.name),
              child: Text("Widget styles"),
            ),
            SizedBox(height: 16),
            FilledButton(
              onPressed: () => context.goNamed(ScreenRoute.colors.name),
              child: Text("Colors"),
            ),
            SizedBox(height: 16),
            Divider(),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Theme mode",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                DropdownButton<ThemeMode>(
                  value: themeMode,
                  onChanged: (themeMode) {
                    if (themeMode != null) {
                      ref
                          .read(themeModeNotifierProvider.notifier)
                          .setThemeMode(themeMode);
                    }
                  },
                  items: [ThemeMode.light, ThemeMode.dark, ThemeMode.system]
                      .map<DropdownMenuItem<ThemeMode>>((ThemeMode value) {
                    return DropdownMenuItem<ThemeMode>(
                      value: value,
                      child: Text(value.toPresentationString()),
                    );
                  }).toList(),
                ),
              ],
            ),
            SizedBox(height: 16),
            Divider(),
            SizedBox(height: 16),
            FilledButton(
              onPressed: () async {
                print("[PREFERENCES] Removing preferences");
                final preferences = await SharedPreferences.getInstance();
                for (var key in cPreferencesAllKeys) {
                  print("[PREFERENCES] Removing $key");
                  preferences.remove(key);
                }
                ref
                    .read(onboardedNotifierProvider.notifier)
                    .setOnboarded(false);
                print("[PREFERENCES] Preferences removed");
              },
              child: Text("Remove preferences"),
            ),
            SizedBox(height: 16),
            FilledButton(
              onPressed: () async {
                print("[DATABASE] Deleting database");
                final dbPath = await getDatabasesPath();
                final path = join(dbPath, cDatabaseName);
                await deleteDatabase(path);
                print("[DATABASE] Database deleted");
              },
              child: Text("Delete database"),
            ),
          ],
        ),
      ),
    );
  }
}
