import 'package:flutter/material.dart';
import 'package:hiive/constants/defaults.dart';

class WidgetStylesScreen extends StatefulWidget {
  const WidgetStylesScreen({super.key});

  @override
  State<WidgetStylesScreen> createState() => _WidgetStylesScreenState();
}

class _WidgetStylesScreenState extends State<WidgetStylesScreen> {
  bool checkboxState = false;
  String textFormFieldText = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Widget Showcase'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: Column(
          children: [
            ElevatedButton(
              onPressed: () {},
              child: Text("Elevated Button"),
            ),
            SizedBox(height: 16),
            TextButton(
              onPressed: () {},
              child: Text("Text Button"),
            ),
            SizedBox(height: 16),
            SizedBox(height: 16),
            FilledButton(
              onPressed: () {},
              child: Text("Filled Button"),
            ),
            SizedBox(height: 16),
            FilledButton.tonal(
              onPressed: () {},
              child: Text("Filled Button tonal"),
            ),
            SizedBox(height: 16),
            OutlinedButton(
              onPressed: () {},
              child: Text("Outlined button"),
            ),
            SizedBox(height: 16),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text("Checbox:"),
                Checkbox(
                  value: this.checkboxState,
                  onChanged: (bool? b) {
                    setState(() {
                      this.checkboxState = b!;
                    });
                  },
                ),
              ],
            ),
            TextFormField(
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.baby_changing_station),
                border: OutlineInputBorder(),
                labelText: "TextFormField",
              ),
              initialValue: this.textFormFieldText,
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              onSaved: (value) => this.textFormFieldText = value ?? "",
            ),
            SizedBox(height: 16),
            FloatingActionButton(
              onPressed: () {},
              child: Icon(
                Icons.add,
              ),
            ),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: cSensorColors
                  .map(
                    (color) => Padding(
                      padding: EdgeInsets.symmetric(horizontal: 4, vertical: 0),
                      child: Container(
                        width: 50,
                        height: 50,
                        color: color,
                      ),
                    ),
                  )
                  .toList(),
            )
          ],
        ),
      ),
    );
  }
}
