import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/api.dart';
import 'package:hiive/data/api/endpoints/user/update_password_endpoint.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/state/user_notifier.dart';

class UserScreen extends ConsumerStatefulWidget {
  const UserScreen({super.key});

  @override
  ConsumerState<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends ConsumerState<UserScreen> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _newPasswordController;
  late TextEditingController _newPasswordConfirmController;

  @override
  void initState() {
    this._emailController = TextEditingController();
    this._passwordController = TextEditingController();
    this._newPasswordController = TextEditingController();
    this._newPasswordConfirmController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _newPasswordController.dispose();
    _newPasswordConfirmController.dispose();
    super.dispose();
  }

  TaskEither<ApiError, void> _onChangePassword() {
    final updatePasswordData = UpdatePasswordData(
      currentPassword: this._passwordController.text,
      newPassword: this._newPasswordController.text,
      newPasswordConfirm: this._newPasswordConfirmController.text,
    );
    return ref.read(apiProvider).updatePassword(updatePasswordData);
  }

  // TODO dont pop bool
  Future<bool?> _showDeleteUserDialog() {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog.adaptive(
            title: const Text("Delete user"),
            content: const Text("Do you really want to delete your user?"),
            actions: [
              TextButton(
                child: const Text('Cancel'),
                onPressed: () {
                  // TODO dont pop bool
                  Navigator.of(context).pop(false);
                },
              ),
              ElevatedButton(
                child: const Text('Remove'),
                onPressed: () {
                  ref.read(userNotifierProvider.notifier).removeUser();
                  // TODO dont pop bool
                  Navigator.of(context).pop(true);
                },
              ),
            ],
          );
        });
  }

  void _onError(String error) {
    print("[ERROR | HIVE] $error");
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(
          error,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> appBarActions = [
      IconButton(
        icon: const Icon(Icons.delete),
        tooltip: "Remove user",
        onPressed: () {
          _showDeleteUserDialog().then(
            (shouldPopScreen) {
              if (context.mounted) {
                if (shouldPopScreen!) Navigator.of(context).pop();
              }
            },
          );
        },
      )
    ];

    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Settings"),
          actions: appBarActions,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Current password: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                ),
                Text(
                  "New password: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                TextField(
                  controller: _newPasswordController,
                  obscureText: true,
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                ),
                Text(
                  "New password confirmation: ",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                TextField(
                  controller: _newPasswordConfirmController,
                  obscureText: true,
                  decoration:
                      const InputDecoration(border: OutlineInputBorder()),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      onPressed: () => _onChangePassword()
                          .match((apiError) => _onError(apiError.toString()),
                              (r) => _onError("Password succesfully changed"))
                          .run(),
                      child: const Text("Update password"),
                    ),
                    TextButton(
                      onPressed: () => print("Forgot password"),
                      child: const Text("Forgot password"),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
