import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hiive/constants/app_theme.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/models/gateway_reading.dart';
import 'package:hiive/data/models/reading.dart';
import 'package:hiive/data/state/gateway_readings_notifier.dart';
import 'package:hiive/data/state/gateways_notifier.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/state/readings_notifier.dart';
import 'package:hiive/data/state/sensors_notifier.dart';
import 'package:hiive/utils/misc.dart';
import 'package:hiive/widgets/charts/hive_gateway_chart.dart';
import 'package:hiive/widgets/charts/sound_level_chart.dart';
import 'package:hiive/widgets/charts/temperature_humidity_chart.dart';

class HiveScreen extends ConsumerStatefulWidget {
  final String id;

  const HiveScreen({super.key, required this.id});

  @override
  ConsumerState<HiveScreen> createState() => _HiveScreenState();
}

class _HiveScreenState extends ConsumerState<HiveScreen> {
  bool loadHiveData = true;
  bool loadGatewayData = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void onApiError(BuildContext context, ApiError apiError) {
    if (mounted) {
      showSnackbar(context, apiError.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    if (loadHiveData) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref.read(hivesNotifierProvider.notifier).getHive(widget.id);
        ref.read(readingsNotifierProvider.notifier).getReadings(widget.id);
      });
      loadHiveData = false;
    }

    final AsyncValue<List<Hive>> hivesProviderState =
        ref.watch(hivesNotifierProvider);
    final AsyncValue<Map<String, List<Reading>>> readingsProviderState =
        ref.watch(readingsNotifierProvider);
    final AsyncValue<Map<String, List<GatewayReading>>>
        gatewayReadingsProviderState =
        ref.watch(gatewayReadingsNotifierProvider);

    final Hive? hive = ref.watch(hivesNotifierProvider).getById(widget.id);
    final List<Reading> readings =
        ref.watch(readingsNotifierProvider).getByid(widget.id);

    String? gatewayMac;
    if (hive?.sensor != null) {
      gatewayMac =
          ref.watch(sensorsNotifierProvider).getByMac(hive!.sensor!)?.gateway;
    }

    final List<GatewayReading> gatewayReadings = gatewayMac == null
        ? []
        : ref.watch(gatewayReadingsNotifierProvider).getByMac(gatewayMac);

    if (gatewayMac != null && loadGatewayData) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        ref
            .read(gatewayReadingsNotifierProvider.notifier)
            .getGatewayReadings(gatewayMac!);
        loadGatewayData = false;
      });
    }

    final hiveName = hive?.name ?? "Unknown";

    return Scaffold(
      appBar: AppBar(
        title: Text(hiveName),
      ),
      body: hivesProviderState.isLoading
          ? Center(child: CircularProgressIndicator())
          : Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                readingsProviderState.isLoading ||
                        gatewayReadingsProviderState.isLoading
                    ? Center(child: CircularProgressIndicator())
                    : readings.length <= 1
                        ? Center(child: Text("Nothing here yet"))
                        : Center(
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 8.0),
                                  child: HiveGatewayChart(
                                    hiveReadingsArg: readings
                                        .map((r) =>
                                            MapEntry(r.time, r.temperature))
                                        .toList(),
                                    gatewayReadingsArg: gatewayReadings
                                        .map((r) =>
                                            MapEntry(r.time, r.temperature))
                                        .toList(),
                                    hiveColor: colorNewHighlightOrange,
                                    gatewayColor: colorNewHighlightBrown,
                                    title: "Temperature",
                                    unit: "°C",
                                    height: 200,
                                    width: 400,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 8.0),
                                  child: HiveGatewayChart(
                                    hiveReadingsArg: readings
                                        .map(
                                            (r) => MapEntry(r.time, r.humidity))
                                        .toList(),
                                    gatewayReadingsArg: gatewayReadings
                                        .map(
                                            (r) => MapEntry(r.time, r.humidity))
                                        .toList(),
                                    hiveColor: colorNewHighlightOrange,
                                    gatewayColor: colorNewHighlightBrown,
                                    title: "Humidity",
                                    unit: "%",
                                    height: 200,
                                    width: 400,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 8.0),
                                  child: SoundLevelChart(
                                    soundLevelsArg: readings
                                        .map((r) =>
                                            MapEntry(r.time, r.soundLevel))
                                        .toList(),
                                    height: 200,
                                    width: 400,
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "Queen status: ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
              ],
            ),
    );
  }
}
