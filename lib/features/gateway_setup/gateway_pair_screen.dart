import "dart:async";

import 'package:collection/collection.dart';
import "package:flutter/material.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:fpdart/fpdart.dart";
import 'package:go_router/go_router.dart';
import "package:hiive/constants/app_theme.dart";
import "package:hiive/constants/defaults.dart";

import "package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart";
import "package:hiive/data/api/endpoints/hive/update_hive_endpoint.dart";
import "package:hiive/data/api/endpoints/sensor/claim_sensor_endpoint.dart";
import "package:hiive/data/api/endpoints/sensor/update_sensor_endpoint.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/hive.dart";
import "package:hiive/data/models/sensor.dart";
import "package:hiive/data/state/dashboard_notifier.dart";
import "package:hiive/data/state/hives_notifier.dart";
import "package:hiive/data/state/sensors_notifier.dart";
import "package:hiive/features/gateway/ble_gateway_notifier.dart";
import "package:hiive/features/gateway_setup/widgets/ble_connection_indicator.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/services/ble.dart";
import "package:hiive/utils/extensions.dart";
import "package:hiive/utils/misc.dart";
import "package:hiive/widgets/dialogs/hive.dart";

class SetupGatewayPairScreen extends ConsumerStatefulWidget {
  final String? shouldPop;
  const SetupGatewayPairScreen({super.key, this.shouldPop});

  @override
  ConsumerState<SetupGatewayPairScreen> createState() =>
      _SetupGatewayPairScreenState();
}

class _SetupGatewayPairScreenState
    extends ConsumerState<SetupGatewayPairScreen> {
  Future<Either<BleError, Stream<SensorGateway>>>? _pendingPairInit;
  Future<void>? _pendingSetInterval;
  StreamSubscription<SensorGateway>? _sensorStreamSubscription;
  List<String> ignoredSensors = [];
  Map<String, Color> sensorColors = {};

  void onDataError(BuildContext context, DataError dataError) {
    if (mounted) {
      showSnackbar(context, dataError.toString());
    }
  }

  void onBleError(BleError bleError) {
    if (mounted) {
      showSnackbar(context, bleError.toString());
    }
  }

  Widget loading() {
    return Center(child: CircularProgressIndicator());
  }

  Widget showStartPairing() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        FilledButton(
          style: FilledButton.styleFrom(elevation: 3),
          child: Text(
            "Start pairing",
            style: Theme.of(context)
                .textTheme
                .bodyMedium!
                .copyWith(fontWeight: FontWeight.w600),
          ),
          onPressed: () {
            _pendingPairInit =
                ref.read(bleGatewayNotifierProvider.notifier).startPairing();
          },
        ),
      ],
    );
  }

  void setSensorRssiAndBattery(String mac, int battery, int rssi) {
    print("[DEBUG] setSensorRssiAndBattery");
    Sensor? prevSensor = ref.read(sensorsNotifierProvider).getByMac(mac);
    if (prevSensor != null) {
      ref.read(sensorsNotifierProvider.notifier).manuallyAddOrUpdateSensors(
        [
          prevSensor.copyWith(
            battery: battery,
            rssi: rssi,
          )
        ],
      );
    }
  }

  void onSensorTileTapped(Sensor sensor, String gatewayMac) async {
    Hive? hive;
    Either<String, Hive?> createOrChooseHiveRes =
        await createOrChooseHiveDialog(context).run();
    createOrChooseHiveRes.match(
      (error) => print(error),
      (maybeHive) async {
        if (maybeHive != null) {
          hive = maybeHive;
        }
      },
    );
    if (hive != null) {
      Either<DataError, Unit> updateHiveResult =
          await ref.read(hivesNotifierProvider.notifier).updateHive(
                hive!.id,
                UpdateHiveData(name: null, sensor: sensor.mac),
              );
      updateHiveResult.mapLeft((dataError) => onDataError(context, dataError));
      print("[CLAIMTEST] updateHiveResult: $updateHiveResult");

      Either<DataError, Unit> updateSensorResult =
          await ref.read(sensorsNotifierProvider.notifier).updateSensor(
                sensor.mac,
                UpdateSensorData(gateway: gatewayMac),
              );
      updateSensorResult
          .mapLeft((dataError) => onDataError(context, dataError));
      print("[CLAIMTEST] updateSensorResult: $updateSensorResult");
    }
  }

  void handleNewSensor(Sensor sensorFromGateway, String gatewayMac) async {
    print("[CLAIMTEST] handleNewSensor");
    Either<String, Hive?> createOrChooseHiveRes =
        await createOrChooseHiveDialog(context).run();
    createOrChooseHiveRes.match(
      (error) => print(error),
      (maybeHive) async {
        if (maybeHive != null) {
          Either<DataError, Sensor> claimSensorResult =
              await ref.read(sensorsNotifierProvider.notifier).claimSensor(
                    ClaimSensorData(
                      battery: sensorFromGateway.battery,
                      gateway: gatewayMac,
                      hive: maybeHive.id,
                      mac: sensorFromGateway.mac,
                      rssi: sensorFromGateway.rssi,
                    ),
                  );
          claimSensorResult
              .mapLeft((dataError) => onDataError(context, dataError));
        }
      },
    );
    print("[CLAIMTEST] createOrChooseHiveResult: $createOrChooseHiveRes");
  }

  void handleClaimedSensor(Sensor sensorFromGateway, String gatewayMac) async {
    print("[CLAIMTEST] handleClaimedSensor");
    Hive? hive;

    // Check if sensor is already assigned to a hive
    Hive? maybeAssignedHive =
        ref.read(hivesNotifierProvider).getBySensor(sensorFromGateway.mac);
    print("[CLAIMTEST] maybeAssignedHive: $maybeAssignedHive");

    // Choose or create a hive for the sensor, if it is not already assigned one
    if (maybeAssignedHive == null) {
      Either<String, Hive?> createOrChooseHiveRes =
          await createOrChooseHiveDialog(context).run();
      createOrChooseHiveRes.match(
        (error) => print(error),
        (maybeHive) async {
          if (maybeHive != null) {
            hive = maybeHive;
          }
        },
      );
      print("[CLAIMTEST] createOrChooseHiveResult $createOrChooseHiveRes");
    } else {
      hive = maybeAssignedHive;
    }

    print("[CLAIMTEST] hive: $hive");
    // Update hive and sensor
    if (hive != null) {
      Either<DataError, Unit> updateHiveResult =
          await ref.read(hivesNotifierProvider.notifier).updateHive(
                hive!.id,
                UpdateHiveData(name: null, sensor: sensorFromGateway.mac),
              );
      updateHiveResult.mapLeft((dataError) => onDataError(context, dataError));
      print("[CLAIMTEST] updateHiveResult: $updateHiveResult");

      Either<DataError, Unit> updateSensorResult =
          await ref.read(sensorsNotifierProvider.notifier).updateSensor(
                sensorFromGateway.mac,
                UpdateSensorData(gateway: gatewayMac),
              );
      updateSensorResult
          .mapLeft((dataError) => onDataError(context, dataError));
      print("[CLAIMTEST] updateSensorResult: $updateSensorResult");
    }
  }

  void onSensorInfoFromGateway(
    SensorGateway sensorGateway,
    String gatewayMac,
  ) async {
    if (ignoredSensors.contains(sensorGateway.mac)) {
      return;
    } else {
      ignoredSensors.add(sensorGateway.mac);
    }

    Sensor? sensor =
        ref.read(sensorsNotifierProvider).getByMac(sensorGateway.mac);

    sensorColors
        .addAll({sensorGateway.mac: cSensorColors[sensorGateway.number % 5]});
    // NEW SENSOR
    if (sensor == null) {
      print("[PAIRING] New sensor");
      handleNewSensor(sensorGateway.toSensor(), gatewayMac);
    } else {
      // CLAIMED SENSOR
      print("[PAIRING] Claimed sensor");
      handleClaimedSensor(sensorGateway.toSensor(), gatewayMac);
    }
  }

  Widget showSensors(
    List<Sensor> sensors,
    List<Hive> hives,
    int currentInterval,
    String gatewayMac,
  ) {
    return Column(
      children: [
        Expanded(
          child: sensors.isEmpty
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(32, 32, 32, 32),
                    child: Image.asset("assets/images/sensor_box_press.webp"),
                  ),
                )
              : ListView.builder(
                  shrinkWrap: true,
                  itemCount: sensors.length,
                  itemBuilder: (context, index) {
                    String hiveName = hives
                            .firstWhereOrNull(
                                (hive) => hive.sensor == sensors[index].mac)
                            ?.name ??
                        "<CHOOSE HIVE>";
                    return GestureDetector(
                      child: ListTile(
                        leading: Container(
                          width: 24,
                          height: 24,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: sensorColors[sensors[index].mac] ??
                                  Colors.transparent),
                        ),
                        title: Text(hiveName,
                            style: Theme.of(context).textTheme.bodyMedium),
                        trailing: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Icon(
                                batteryIconFromBattery(sensors[index].battery)),
                            Icon(loraIconFromRssi(sensors[index].rssi)),
                          ],
                        ),
                        onTap: () {
                          onSensorTileTapped(sensors[index], gatewayMac);
                        },
                      ),
                    );
                  },
                ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Choose interval (minutes):"),
            LayoutBuilder(
              builder: (context, constraints) {
                double circleSize =
                    (constraints.maxWidth - (cIntervalChoices.length) * 16) /
                        cIntervalChoices.length;
                double fontSize = circleSize / 3;
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: cIntervalChoices
                        .map(
                          (interval) => InkWell(
                            onTap: () async {
                              _pendingSetInterval = ref
                                  .read(bleGatewayNotifierProvider.notifier)
                                  .setInterval(interval)
                                  .then(
                                    (res) => res.match(
                                        (bleError) => onBleError(bleError),
                                        (_) => unit),
                                  );
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 8),
                              width:
                                  circleSize, // Equal width and height for perfect circle
                              height: circleSize,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: interval == currentInterval
                                    ? Theme.of(context).colorScheme.primary
                                    : Colors.grey[300],
                              ),
                              child: Center(
                                child: Text(
                                  interval.toString(),
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: fontSize,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                );
              },
            ),
          ],
        ),
        FutureBuilder(
          future: _pendingSetInterval,
          builder: (context, snapshot) => switch (snapshot.connectionState) {
            ConnectionState.waiting => FilledButton(
                style: FilledButton.styleFrom(elevation: 3),
                onPressed: null,
                child: CircularProgressIndicator(color: colorNewBaseWhite),
              ),
            _ => FilledButton(
                onPressed: () async {
                  if (currentInterval == 0) {
                    if (mounted) {
                      showSnackbar(context, "Choose an interval");
                    }
                  } else {
                    await ref
                        .read(bleGatewayNotifierProvider.notifier)
                        .endPairing()
                        .then(
                          (res) => res.match(
                            (bleError) => onBleError(bleError),
                            (_) {
                              Future.delayed(
                                  Duration(seconds: 30),
                                  ref
                                      .read(dashboardNotifierProvider.notifier)
                                      .getDashboard);
                              if (mounted) {
                                widget.shouldPop?.toBool() ?? false
                                    ? context.pop()
                                    : context.goNamed(
                                        ScreenRoute.dashboard.name,
                                      );
                              }
                            },
                          ),
                        );
                  }
                },
                child: Text(
                  "Start cycle",
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
              ),
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Sensor> sensors = ref.watch(sensorsNotifierProvider).value ?? [];
    List<Hive> hives = ref.watch(hivesNotifierProvider).value ?? [];
    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    String connectedGatewayMac =
        switch (bleGatewayGuard.value ?? GatewayUnknown()) {
      GatewayUnknown() => "",
      GatewayKnown(:final bleGateway) => bleGateway.mac,
    };
    int connectedGatewayInterval =
        switch (bleGatewayGuard.value ?? GatewayUnknown()) {
      GatewayUnknown() => 0,
      GatewayKnown(:final bleGateway) => bleGateway.interval,
    };
    List<Sensor> pairedSensors = sensors
        .where((sensor) => sensor.gateway == connectedGatewayMac)
        .toList();
    return FutureBuilder(
      future: _pendingPairInit,
      builder: (context, snapshot) {
        return Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: AppBar(
            title: const Text("Gateway setup"),
          ),
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(32.0, 32.0, 32.0, 32.0),
                  child: switch (snapshot.connectionState) {
                    ConnectionState.done => Text(
                        "Turn on your sensors by pressing both buttons at the same time. Repeat for each sensor.",
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.headlineMedium),
                    _ => Column(
                        children: [
                          Text("Have your sensors ready",
                              textAlign: TextAlign.center,
                              style:
                                  Theme.of(context).textTheme.headlineMedium),
                          Image.asset("assets/images/sensor_box.webp"),
                        ],
                      ),
                  },
                ),
                Expanded(
                  child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 32),
                      child: switch (snapshot.connectionState) {
                        ConnectionState.none => showStartPairing(),
                        ConnectionState.done => snapshot.data!.match(
                            (bleError) =>
                                Center(child: Text(bleError.toString())),
                            (sensorStream) {
                              // Subscribe to sensor stream if not done already
                              _sensorStreamSubscription ??= sensorStream.listen(
                                  (sensorInfo) => onSensorInfoFromGateway(
                                        sensorInfo,
                                        connectedGatewayMac,
                                      ));
                              return showSensors(
                                pairedSensors,
                                hives,
                                connectedGatewayInterval,
                                connectedGatewayMac,
                              );
                            },
                          ),
                        _ => loading(),
                      }),
                ),
                BleConnectionIndicator(),
              ],
            ),
          ),
        );
      },
    );
  }
}
