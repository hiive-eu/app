import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hiive/data/models/gateway.dart';
import 'package:hiive/data/state/gateways_notifier.dart';
import 'package:hiive/features/gateway/ble_gateway_notifier.dart';

class BleConnectionIndicator extends ConsumerWidget {
  final String? mac;
  final String? token;
  const BleConnectionIndicator({super.key, this.mac, this.token});

  Widget building() {
    return bleConnectionIndicatorWidget(
      icon: Icons.bluetooth_disabled,
      iconColor: Colors.black87,
      textOrButton: Text(
        "",
      ),
      loading: true,
    );
  }

  Widget hasData(
    BuildContext context,
    AsyncValue<BleGatewayGuard> bleGatewayGuard,
    AsyncValue<List<Gateway>> gatewaysAsyncValue,
    WidgetRef ref,
  ) {
    bool loading = bleGatewayGuard.isLoading;
    IconData icon = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => Icons.bluetooth_disabled,
      GatewayKnown(:final bleGateway) => switch (bleGateway.connectionState) {
          DeviceConnectionState.connecting => Icons.bluetooth_searching,
          DeviceConnectionState.connected => Icons.bluetooth_connected,
          _ => Icons.bluetooth_disabled,
        },
    };
    Color iconColor = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => Colors.black87,
      GatewayKnown(:final bleGateway) => switch (bleGateway.connectionState) {
          DeviceConnectionState.connecting => Colors.lightBlue,
          DeviceConnectionState.connected => Colors.blue,
          _ => Colors.black87,
        },
    };
    Widget textOrButton = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => this.mac == null
          ? FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                "",
                style: Theme.of(context).textTheme.bodySmall,
              ),
            )
          : Column(
              children: [
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    gatewaysAsyncValue.getByMac(this.mac!)?.name ?? this.mac!,
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ),
                OutlinedButton(
                  child: Text(
                    "Connect",
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  onPressed: () => ref
                      .read(bleGatewayNotifierProvider.notifier)
                      .connect(mac: this.mac, token: this.token),
                ),
              ],
            ),
      GatewayKnown(:final bleGateway) => switch (bleGateway.connectionState) {
          DeviceConnectionState.connecting => Text(
              gatewaysAsyncValue.getByMac(bleGateway.mac)?.name ??
                  bleGateway.mac,
              style: Theme.of(context).textTheme.bodySmall,
            ),
          DeviceConnectionState.connected => Text(
              gatewaysAsyncValue.getByMac(bleGateway.mac)?.name ??
                  bleGateway.mac,
              style: Theme.of(context).textTheme.bodySmall,
            ),
          _ => Column(
              children: [
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    gatewaysAsyncValue.getByMac(bleGateway.mac)?.name ??
                        bleGateway.mac,
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                ),
                OutlinedButton(
                  child: Text(
                    "Connect",
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                  onPressed: () => ref
                      .read(bleGatewayNotifierProvider.notifier)
                      .connect(mac: bleGateway.mac, token: this.token),
                ),
              ],
            ),
        },
    };
    return bleConnectionIndicatorWidget(
      icon: icon,
      iconColor: iconColor,
      textOrButton: textOrButton,
      loading: loading,
    );
  }

  Widget bleConnectionIndicatorWidget(
      {required IconData icon,
      required Color iconColor,
      required Widget textOrButton,
      required bool loading}) {
    return SizedBox(
      width: 140,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8, 8, 8, 18),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Icon(
                  size: 32,
                  icon,
                  color: iconColor,
                ),
                Visibility(
                  visible: loading,
                  child: SizedBox(
                    child: CircularProgressIndicator(
                      color: Colors.blue,
                    ),
                  ),
                )
              ],
            ),
            textOrButton,
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    AsyncValue<List<Gateway>> gateways = ref.watch(gatewaysNotifierProvider);
    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    return bleGatewayGuard.hasValue
        ? hasData(context, bleGatewayGuard, gateways, ref)
        : building();
  }
}
