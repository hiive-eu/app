import "package:flutter/material.dart";
import "package:flutter_reactive_ble/flutter_reactive_ble.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/constants/defaults.dart";
import "package:hiive/features/gateway/ble_gateway_notifier.dart";
import 'package:go_router/go_router.dart';
import "package:hiive/features/gateway_setup/widgets/ble_connection_indicator.dart";
import "package:hiive/router/app_router.dart";

class SetupGatewayNameScreen extends ConsumerStatefulWidget {
  const SetupGatewayNameScreen({super.key});

  @override
  SetupGatewayNameScreenState createState() => SetupGatewayNameScreenState();
}

class SetupGatewayNameScreenState
    extends ConsumerState<SetupGatewayNameScreen> {
  final _form = GlobalKey<FormState>();
  String _name = "";

  String? _nameValidator(String? v) {
    String illegalChar = "";
    if (v == null || v.isEmpty || v.length > 255) {
      return "Name must be between 1 and 255 characters";
    }
    if (forbiddenNameChars.any(
      (e) {
        if (v.contains(e)) {
          illegalChar = e;
          return true;
        } else {
          return false;
        }
      },
    )) {
      return "Name must not contain: $illegalChar ";
    }
    return null;
  }

  void _onNameSubmitted() {
    if (_form.currentState!.validate()) {
      _form.currentState!.save();

      context.goNamed(ScreenRoute.setupGatewayMode.name,
          queryParameters: {"name": this._name});
    }
  }

  Widget loading() {
    return Center(child: CircularProgressIndicator());
  }

  Widget bleGatewayData(
    BuildContext context,
    AsyncValue<BleGatewayGuard> bleGatewayGuard,
    WidgetRef ref,
  ) {
    bool gatewayConnected = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => false,
      GatewayKnown(:final bleGateway) =>
        bleGateway.connectionState == DeviceConnectionState.connected,
    };
    return Form(
      key: _form,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: TextFormField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Name",
              ),
              textInputAction: TextInputAction.done,
              validator: _nameValidator,
              onSaved: (value) => this._name = value ?? "",
              onFieldSubmitted: (_) =>
                  gatewayConnected ? _onNameSubmitted() : null,
            ),
          ),
          FilledButton.icon(
            style: FilledButton.styleFrom(elevation: 3),
            iconAlignment: IconAlignment.end,
            label: Text(
              "CONTINUE",
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.w600),
            ),
            onPressed: () => gatewayConnected ? _onNameSubmitted() : null,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Text("Gateway setup"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(32.0, 32.0, 32.0, 32.0),
              child: Text(
                "Name gateway",
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: bleGatewayData(context, bleGatewayGuard, ref),
              ),
            ),
            BleConnectionIndicator(),
          ],
        ),
      ),
    );
  }
}
