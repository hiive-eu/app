import "dart:async";

import "package:flutter/material.dart";
import "package:flutter_reactive_ble/flutter_reactive_ble.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import 'package:go_router/go_router.dart';
import "package:hiive/constants/app_theme.dart";
import "package:hiive/data/models/wifi_network.dart";
import "package:hiive/features/gateway/ble_gateway_notifier.dart";
import "package:hiive/features/gateway_setup/widgets/ble_connection_indicator.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/services/ble.dart";
import "package:hiive/utils/extensions.dart";
import "package:hiive/utils/misc.dart";

class SetupGatewayWifiScreen extends ConsumerStatefulWidget {
  final String? shouldPop;
  const SetupGatewayWifiScreen({super.key, this.shouldPop});

  @override
  ConsumerState<SetupGatewayWifiScreen> createState() =>
      _SetupGatewayWifiScreenState();
}

class _SetupGatewayWifiScreenState
    extends ConsumerState<SetupGatewayWifiScreen> {
  final List<WiFiNetwork> _wifiNetworks = [];
  StreamSubscription<WiFiNetwork>? _wifiNetworkStreamSubscription;
  Future<void>? _pendingBleTask;

  @override
  void dispose() {
    _wifiNetworkStreamSubscription?.cancel();
    super.dispose();
  }

  void _onBleError(BleError bleError) {
    if (!context.mounted) return;
    showSnackbar(context, bleError.toString());
  }

  void _onWifiTestError(GatewayWifiTestResult response) {
    if (!context.mounted) return;
    showSnackbar(context, response.toString());
  }

  void _onWifiTestSuccess() {
    if (!context.mounted) return;
    widget.shouldPop?.toBool() ?? false
        ? context.pop()
        : context.goNamed(
            ScreenRoute.setupGatewayPair.name,
          );
  }

  String? _wifiPasswordValidator(String? v) {
    if (v == null || v.isEmpty) {
      return "password";
    }
    if (v.length > 63) {
      return "password too long";
    }
    return null;
  }

  Future<String?> _showWifiPasswordDialog(String ssid) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          TextEditingController passwordEditingController =
              TextEditingController();
          return AlertDialog(
            title: Text(
              ssid,
              style: Theme.of(context).textTheme.titleSmall,
            ),
            content: TextFormField(
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                labelText: "Password",
              ),
              obscureText: true,
              validator: _wifiPasswordValidator,
              controller: passwordEditingController,
            ),
            actions: [
              FilledButton.tonal(
                child: Text(
                  'Cancel',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
                onPressed: () {
                  Navigator.of(context).pop(null);
                },
              ),
              FilledButton(
                child: Text(
                  'Submit',
                  style: Theme.of(context)
                      .textTheme
                      .bodyMedium!
                      .copyWith(fontWeight: FontWeight.w600),
                ),
                onPressed: () {
                  Navigator.of(context).pop(passwordEditingController.text);
                },
              ),
            ],
          );
        });
  }

  void _scanWifi() {
    _wifiNetworks.clear();
    ref.read(bleGatewayNotifierProvider.notifier).scanWifi().then(
          (value) => value.match(
            (bleError) => showSnackbar(context, bleError.toString()),
            (wifiNetworkStream) => setState(() {
              _wifiNetworkStreamSubscription = wifiNetworkStream.listen(
                (wifiNetwork) {
                  setState(() {
                    _wifiNetworks.add(wifiNetwork);
                  });
                },
                onDone: () {
                  _wifiNetworkStreamSubscription?.cancel().then((_) {
                    setState(() {
                      _wifiNetworkStreamSubscription = null;
                    });
                  });
                },
                onError: (error, stacktrace) {
                  if (!context.mounted) return;
                  showSnackbar(context, "wifistream error: $error");
                },
              );
            }),
          ),
        );
  }

  void _onWifiNetworkTapped(WiFiNetwork network, WidgetRef ref) async {
    String? password;
    if (network.secure) {
      password = await _showWifiPasswordDialog(network.ssid);
    } else {
      password = "";
    }

    if (password != null) {
      setState(() {
        _pendingBleTask = ref
            .read(bleGatewayNotifierProvider.notifier)
            .setWifiCredentials(network.ssid, password!)
            .then(
          (res) {
            res.match(
              (bleError) => _onBleError(bleError),
              (response) => switch (response) {
                GatewayWifiTestResult.ok => _onWifiTestSuccess(),
                _ => _onWifiTestError(response),
              },
            );
          },
        );
      });
    }
  }

  Widget loading() {
    return Center(child: CircularProgressIndicator());
  }

  Widget bleGatewayData(
    BuildContext context,
    AsyncValue<BleGatewayGuard> bleGatewayGuard,
    AsyncSnapshot<void> snapshot,
    WidgetRef ref,
  ) {
    bool gatewayConnected = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => false,
      GatewayKnown(:final bleGateway) =>
        bleGateway.connectionState == DeviceConnectionState.connected,
    };
    bool buttonsDisabled = !gatewayConnected ||
        bleGatewayGuard.isLoading ||
        _wifiNetworkStreamSubscription != null;

    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: _wifiNetworks.length,
              itemBuilder: (context, index) => GestureDetector(
                child: InkWell(
                  onTap: () => buttonsDisabled
                      ? null
                      : _onWifiNetworkTapped(_wifiNetworks[index], ref),
                  child: ListTile(
                    title: Text(_wifiNetworks[index].ssid),
                    trailing: Icon(wifiIconFromRssi(_wifiNetworks[index].rssi)),
                  ),
                ),
              ),
            ),
          ),
          FilledButton(
            style: FilledButton.styleFrom(elevation: 3),
            onPressed: _scanWifi,
            child: _wifiNetworkStreamSubscription != null
                ? CircularProgressIndicator(
                    color: colorNewBaseWhite,
                  )
                : Text(
                    "SCAN",
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium!
                        .copyWith(fontWeight: FontWeight.w600),
                  ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    return FutureBuilder(
      future: _pendingBleTask,
      builder: (context, snapshot) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Gateway setup"),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(32.0, 32.0, 32.0, 32.0),
                child: Text(
                  "Connect WiFi",
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
              Expanded(
                child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: bleGatewayGuard.hasValue &&
                            snapshot.connectionState != ConnectionState.waiting
                        ? bleGatewayData(
                            context, bleGatewayGuard, snapshot, ref)
                        : loading()),
              ),
              BleConnectionIndicator(),
            ],
          ),
        );
      },
    );
  }
}
