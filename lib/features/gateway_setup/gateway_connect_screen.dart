import "package:flutter/material.dart";
import "package:flutter_reactive_ble/flutter_reactive_ble.dart" hide Unit;
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/gateway.dart";
import "package:hiive/data/state/gateways_notifier.dart";
import "package:hiive/features/gateway/ble_gateway_notifier.dart";
import 'package:go_router/go_router.dart';
import "package:hiive/features/gateway_setup/widgets/ble_connection_indicator.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/utils/misc.dart";

class SetupGatewayConnectScreen extends ConsumerStatefulWidget {
  const SetupGatewayConnectScreen({super.key});

  @override
  ConsumerState<SetupGatewayConnectScreen> createState() =>
      _SetupGatewayConnectScreenState();
}

class _SetupGatewayConnectScreenState
    extends ConsumerState<SetupGatewayConnectScreen> {
  Future<void>? _pendingNetworkRequest;

  void _onDataError(BuildContext context, DataError dataError) {
    if (!context.mounted) return;
    showSnackbar(context, dataError.toString());
  }

  Future<bool?> _showRemoveGatewayDialog(
    Gateway gateway,
    BuildContext context,
  ) {
    return showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog.adaptive(
          title: const Text("WARNING"),
          icon: Icon(Icons.warning),
          content: const Text(
              "Proceeding will reset your gateway. All readings will be lost"),
          actions: [
            TextButton(
              child: const Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop(null);
              },
            ),
            TextButton(
              child: const Text(
                "Continue",
                style: TextStyle(color: Colors.red),
              ),
              onPressed: () {
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    );
  }

  Widget loading() {
    return Center(child: CircularProgressIndicator());
  }

  Widget bleGatewayData(
    BuildContext context,
    AsyncValue<BleGatewayGuard> bleGatewayGuard,
    AsyncValue<List<Gateway>> gatewaysAsyncValue,
    bool gatewayConnected,
    WidgetRef ref,
  ) {
    bool gatewayKnown = bleGatewayGuard.value is GatewayKnown;
    String connectedGatewayMac = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => "",
      GatewayKnown(:final bleGateway) => bleGateway.mac,
    };
    String buttonText = gatewayKnown ? "CONTINUE" : "SCAN";

    void Function()? buttonFunction = bleGatewayGuard.isLoading
        ? null
        : () async {
            if (gatewayConnected) {
              Gateway? alreadyClaimedGateway =
                  gatewaysAsyncValue.getByMac(connectedGatewayMac);
              // NEW GATEWAY
              if (alreadyClaimedGateway == null) {
                if (!context.mounted) return;
                context.goNamed(
                  ScreenRoute.setupGatewayName.name,
                );
              } else {
                // ALREADY CLAIMED GATEWAY
                bool removeGateway = await _showRemoveGatewayDialog(
                      alreadyClaimedGateway,
                      context,
                    ) ??
                    false;
                if (removeGateway) {
                  final future = ref
                      .read(gatewaysNotifierProvider.notifier)
                      .removeGateway(alreadyClaimedGateway.mac)
                      .then(
                    (maybeError) {
                      maybeError.match(
                        (dataError) => _onDataError(context, dataError),
                        (_) {
                          if (!context.mounted) return;
                          context.goNamed(
                            ScreenRoute.setupGatewayName.name,
                          );
                        },
                      );
                    },
                  );
                  setState(() {
                    _pendingNetworkRequest = future;
                  });
                }
              }
            } else {
              ref.read(bleGatewayNotifierProvider.notifier).connect();
            }
          };

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          FilledButton(
            onPressed: buttonFunction,
            style: FilledButton.styleFrom(elevation: 3),
            child: Text(
              buttonText,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.w600),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    AsyncValue<List<Gateway>> gateways = ref.watch(gatewaysNotifierProvider);
    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    bool gatewayConnected = switch (bleGatewayGuard.value) {
      null => false,
      GatewayUnknown() => false,
      GatewayKnown(:final bleGateway) =>
        bleGateway.connectionState == DeviceConnectionState.connected,
    };
    return FutureBuilder(
      future: _pendingNetworkRequest,
      builder: (context, snapshot) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Gateway setup"),
          ),
          body: DecoratedBox(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(gatewayConnected
                      ? "assets/images/gateway_no_solar_bg.webp"
                      : "assets/images/gateway_no_solar_touch_bg.webp"),
                  fit: BoxFit.cover),
            ),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(32.0, 32.0, 32.0, 32.0),
                          child: Text(
                            textAlign: TextAlign.center,
                            gatewayConnected
                                ? "Connected!"
                                : "Hold touch button for 3 seconds and wait for it to glow blue",
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        Container(
                          child: bleGatewayGuard.hasValue &&
                                  snapshot.connectionState !=
                                      ConnectionState.waiting
                              ? bleGatewayData(context, bleGatewayGuard,
                                  gateways, gatewayConnected, ref)
                              : loading(),
                        ),
                      ],
                    ),
                  ),
                  BleConnectionIndicator(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
