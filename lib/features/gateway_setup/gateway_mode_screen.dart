import "package:flutter/material.dart";
import "package:flutter_reactive_ble/flutter_reactive_ble.dart" hide Unit;
import "package:flutter_riverpod/flutter_riverpod.dart";
import 'package:go_router/go_router.dart';
import "package:hiive/data/api/endpoints/gateway/claim_gateway_endpoint.dart";
import "package:hiive/data/state/gateways_notifier.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/state/logged_in_notifier.dart";
import "package:hiive/features/gateway/ble_gateway_notifier.dart";
import "package:hiive/features/gateway_setup/widgets/ble_connection_indicator.dart";
import "package:hiive/router/app_router.dart";
import "package:hiive/services/ble.dart";
import "package:hiive/utils/misc.dart";

List<String> onlineBullets = [
  "WiFi connection",
  "Data is automatically stored online",
  "Live data is always available",
  "Gateway is automatically updated",
];
List<String> offlineBullets = [
  "No internet connection",
  "Data is stored on the Gateway itself",
  "Data must be transfered using phone",
  "Gateway updates are handled manually",
];

class SetupGatewayModeScreen extends ConsumerStatefulWidget {
  final String name;
  const SetupGatewayModeScreen({super.key, required this.name});

  @override
  ConsumerState<SetupGatewayModeScreen> createState() =>
      _SetupGatewayModeScreenState();
}

class _SetupGatewayModeScreenState
    extends ConsumerState<SetupGatewayModeScreen> {
  Future<void>? loadingFuture;

  void _onDataError(BuildContext context, DataError dataError) {
    if (mounted) {
      showSnackbar(context, dataError.toString());
    }
  }

  void _onBleError(BleError bleError) {
    if (mounted) {
      showSnackbar(context, bleError.toString());
    }
  }

  void _onClaimSuccess() {
    if (mounted) {
      context.goNamed(
        ScreenRoute.setupGatewayWifi.name,
        queryParameters: {"shouldPop": "false"},
      );
    }
  }

  Widget loading() {
    return Center(child: CircularProgressIndicator());
  }

  Widget bleGatewayData(
    BuildContext context,
    AsyncValue<BleGatewayGuard> bleGatewayGuard,
    AsyncSnapshot<void> snapshot,
    WidgetRef ref,
  ) {
    bool gatewayConnected = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => false,
      GatewayKnown(:final bleGateway) =>
        bleGateway.connectionState == DeviceConnectionState.connected,
    };
    String connectedGatewayMac = switch (bleGatewayGuard.value!) {
      GatewayUnknown() => "",
      GatewayKnown(:final bleGateway) => bleGateway.mac,
    };

    bool buttonsDisabled = !gatewayConnected ||
        bleGatewayGuard.isLoading ||
        snapshot.connectionState == ConnectionState.waiting;

    void claimGatewayFunction() {
      final future = ref
          .read(gatewaysNotifierProvider.notifier)
          .claimGateway(
            ClaimGatewayData(mac: connectedGatewayMac, name: widget.name),
          )
          .then((maybeClaimError) {
        maybeClaimError.match(
          (dataError) => _onDataError(context, dataError),
          (maybeToken) {
            if (maybeToken != null) {
              ref
                  .read(bleGatewayNotifierProvider.notifier)
                  .setToken(maybeToken)
                  .then(
                (maybeSetTokenError) {
                  maybeSetTokenError.match(
                    (bleError) => _onBleError(bleError),
                    (_) => _onClaimSuccess(),
                  );
                },
              );
            } else {
              _onClaimSuccess();
            }
          },
        );
      });
      setState(() {
        loadingFuture = future;
      });
    }

    void Function()? onlineButtonFunction =
        buttonsDisabled || !ref.read(loggedInNotifierProvider)
            ? () => context.pushNamed(ScreenRoute.auth.name, queryParameters: {
                  "canPop": "true",
                })
            : () => claimGatewayFunction();

    // void Function()? offlineButtonFunction;

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: onlineButtonFunction,
          child: Container(
            margin: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: Theme.of(context).colorScheme.primary,
                width: 4,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Center(
                      child: Text(
                        "ONLINE",
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  fontWeight: FontWeight.w600,
                                ),
                      ),
                    ),
                  ),
                  ...onlineBullets.map((line) => Text(
                        "• $line",
                        style: Theme.of(context).textTheme.bodyMedium,
                      )),
                ],
              ),
            ),
          ),
        ),
        InkWell(
          onTap: () => showSnackbar(context, "NOT IMPLEMENTED"),
          child: Container(
            margin: EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              //color: Colors.transparent,
              border: Border.all(
                color: Theme.of(context).colorScheme.primary,
                width: 4,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        "OFFLINE",
                        style:
                            Theme.of(context).textTheme.titleMedium!.copyWith(
                                  fontWeight: FontWeight.w600,
                                ),
                      ),
                    ),
                  ),
                  ...offlineBullets.map((line) => Text(
                        "• $line",
                        style: Theme.of(context).textTheme.bodyMedium,
                      )),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    AsyncValue<BleGatewayGuard> bleGatewayGuard =
        ref.watch(bleGatewayNotifierProvider);
    return FutureBuilder(
      future: loadingFuture,
      builder: (context, snapshot) {
        return Scaffold(
          appBar: AppBar(
            title: const Text("Gateway setup"),
          ),
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(32.0, 32.0, 32.0, 32.0),
                  child: Text(
                    "Choose gateway mode",
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ),
                Expanded(
                  child: Container(
                      padding: EdgeInsets.all(8.0),
                      child: bleGatewayGuard.hasValue &&
                              snapshot.connectionState !=
                                  ConnectionState.waiting
                          ? bleGatewayData(
                              context, bleGatewayGuard, snapshot, ref)
                          : loading()),
                ),
                BleConnectionIndicator(),
              ],
            ),
          ),
        );
      },
    );
  }
}
