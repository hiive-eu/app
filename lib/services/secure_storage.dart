import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fpdart/fpdart.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part "secure_storage.g.dart";

@riverpod
SecureStorage secureStorage(SecureStorageRef ref) => SecureStorage();

class SecureStorage {
  late final FlutterSecureStorage _storage;

  SecureStorage() {
    this._storage = const FlutterSecureStorage();
  }

  TaskEither<String, Unit> write(StorageKey key, String value) {
    return TaskEither.tryCatch(
      () => this._storage.write(key: key.key, value: value).then(
        (_) {
          print("[STORAGE] Wrote $key : $value");
          return unit;
        },
      ),
      (error, stackTrace) => error.toString(),
    );
  }

  TaskEither<String, String> read(StorageKey key) {
    return TaskEither.tryCatch(
      () => this._storage.read(key: key.key).then((value) {
        if (value == null) {
          throw SecureStorageReadError(key.key);
        } else {
          return value;
        }
      }),
      (error, stackTrace) => error.toString(),
    );
  }

  TaskEither<String, Unit> delete(StorageKey key) {
    return TaskEither.tryCatch(
      () => this._storage.delete(key: key.key).then(
        (_) {
          print("[STORAGE] Deleted $key");
          return unit;
        },
      ),
      (error, stackTrace) => error.toString(),
    );
  }
}

enum StorageKey {
  accessToken("ACCESS-TOKEN"),
  refreshToken("REFRESH-TOKEN"),
  email("EMAIL"),
  password("PASSWORD"),
  ;

  final String key;

  const StorageKey(this.key);
}

class SecureStorageReadError implements Exception {
  String key;
  SecureStorageReadError(this.key);
}

class SecureStorageWriteError implements Exception {
  String key, value;
  SecureStorageWriteError(this.key, this.value);
}

class SecureStorageDeleteError implements Exception {
  String key;
  SecureStorageDeleteError(this.key);
}
