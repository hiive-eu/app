import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/constants/defaults.dart';
import 'package:hiive/data/data_error.dart';
import 'package:hiive/data/db/db.dart';
import 'package:hiive/data/db/tables.dart';
import 'package:path/path.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:sqflite/sqflite.dart';

part "sqlite.g.dart";

@Riverpod(keepAlive: true)
SqliteService sqliteService(Ref ref) => SqliteService(ref);

class SqliteService {
  late final Ref _ref;
  Database? _database;

  //DatabaseService(DatabaseServiceRef ref) : this._ref = ref;
  SqliteService(Ref ref) : this._ref = ref;

  TaskEither<DbError, Unit> init() => TaskEither.tryCatch(
        () async {
          print("[DB] Initializing...");
          final dbPath = await getDatabasesPath();
          final path = join(dbPath, cDatabaseName);
          print("[DB] Location: $path");
          _database = await openDatabase(
            path,
            version: cDatabaseVersion,
            onConfigure: _onConfigure,
            onCreate: _onCreate,
          );
          return unit;
        },
        (error, stackTrace) {
          print("[ERROR:DB] init ${error.toString()}");
          return DbError.initError(error.toString());
        },
      );

  Future _onCreate(Database db, int version) async {
    print("[DB] Creating database");
    await TaskEither.sequenceList([
      createTable(db, gatewaysTable),
      createTable(db, hivesTable),
      createTable(db, sensorsTable),
      createTable(db, readingsTable),
      createTable(db, gwReadingsTable),
    ]).match(
      (error) {
        throw error;
      },
      (_) {
        return unit;
      },
    ).run();
  }

  Future _onConfigure(Database db) async {
    print("[DB] Configuring database");
    await db.execute('PRAGMA foreign_keys = ON');
  }

  TaskEither<DbError, Unit> createTable(
    Database db,
    DbTable table,
  ) {
    print("[DB] Creating table: ${table.tableName}");
    return TaskEither.tryCatch(
      () async {
        String createTableExpression = '''
            CREATE TABLE ${table.tableName} (${table.columnDefinitions})
          ''';
        await db.execute(createTableExpression);
        return unit;
      },
      (error, stackTrace) {
        print("[ERROR:DB] createTable ${error.toString()}");
        return DbError.createTableError(
          table.tableName,
          error as Exception,
        );
      },
    );
  }

  TaskEither<DbError, Unit> delete<T extends DbEntity>(
    DbTable table,
    String primaryKeyValue,
  ) {
    print(
      "[DB] Deleting from table ${table.tableName.toUpperCase()} where primary key = $primaryKeyValue",
    );
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      await this._database?.delete(
        table.tableName,
        where: "${table.primaryKey} = ?",
        whereArgs: [primaryKeyValue],
      );
      return unit;
    }, (error, stackTrace) {
      print("[ERROR:DB] deleteError ${error.toString()}");
      return DbError.deleteError(error as Exception);
    });
  }

  TaskEither<DbError, T?> getSingle<T extends DbEntity>(
    DbTable table,
    String primaryKeyValue,
  ) {
    print(
      "[DB] Get from table${table.tableName.toUpperCase()} where primary key = $primaryKeyValue",
    );
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      final queryRes = await this._database?.rawQuery(
        "SELECT * FROM ${table.tableName} WHERE ${table.primaryKey} = ?",
        [primaryKeyValue],
      );
      if (queryRes != null) {
        return queryRes.isEmpty
            ? null
            : table.entityConstructor(queryRes.first) as T;
      } else {
        return null;
      }
    }, (error, stackTrace) {
      print("[ERROR:DB] getSingleError ${error.toString()}");
      return DbError.getSingleError(error as Exception);
    });
  }

  TaskEither<DbError, List<T>> getAll<T extends DbEntity>(
    DbTable table,
  ) {
    print(
      "[DB] Get all: ${table.tableName.toUpperCase()}",
    );
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      final List<Map<String, Object?>> objectsJson =
          await this._database!.query(table.tableName);
      print(objectsJson);
      return objectsJson
          .map((objectJson) => table.entityConstructor(objectJson) as T)
          .toList();
    }, (error, stackTrace) {
      print("[ERROR:DB] get all error ${error.toString()}");
      return DbError.getAllError(error as Exception);
    });
  }

  TaskEither<DbError, T> insert<T extends DbEntity>(
    DbTable table,
    T object,
  ) {
    print(
      "[DB] Inserting into ${table.tableName.toUpperCase()}:\n${object.toJson()}",
    );
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      await this._database?.insert(
            table.tableName,
            object.toJson(),
            conflictAlgorithm: ConflictAlgorithm.abort,
          );
      return object;
    }, (error, stackTrace) {
      print("[ERROR:DB] insertError ${error.toString()}");
      return DbError.insertError(error as Exception);
    });
  }

  TaskEither<DbError, T> update<T extends DbEntity>(
    DbTable table,
    T object,
  ) {
    print(
      "[DB] Updating ${table.tableName.toUpperCase()}:\n${object.toJson()}",
    );
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      Map<String, dynamic> hiveStorageJson = object.toJson();
      await this._database?.update(
        table.tableName,
        hiveStorageJson,
        where: "${table.primaryKey} = ?",
        whereArgs: [hiveStorageJson[table.primaryKey]],
      );
      return object;
    }, (error, stackTrace) {
      print("[ERROR:DB] updateError ${error.toString()}");
      return DbError.updateError(error as Exception);
    });
  }

  TaskEither<DbError, Map<String, List<T>>> transaction<T extends DbEntity>(
    List<TransactionOperation> txnOps,
  ) {
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      Map<String, List<T>> result = {};
      await this._database!.transaction((txn) async {
        for (final txnOp in txnOps) {
          Map<String, dynamic> objectJson = txnOp.object.toJson();
          int operationRes;
          switch (txnOp.operationType) {
            case TransactionOperationType.insert:
              operationRes = await txn.insert(
                txnOp.table.tableName,
                objectJson,
              );
            case TransactionOperationType.update:
              operationRes = await txn.update(
                txnOp.table.tableName,
                objectJson,
                where: "${txnOp.table.primaryKey} = ?",
                whereArgs: [objectJson[txnOp.table.primaryKey]],
              );
            case TransactionOperationType.delete:
              operationRes = await txn.delete(
                txnOp.table.tableName,
                where: "${txnOp.table.primaryKey} = ?",
                whereArgs: [objectJson[txnOp.table.primaryKey]],
              );
          }
          if (operationRes == 1) {
            if (!result.keys.contains(txnOp.table.tableName)) {
              result[txnOp.table.tableName] = [];
            }
            result[txnOp.table.tableName]?.add(txnOp.object as T);
          }
        }
      });
      return result;
    }, (error, stackTrace) {
      print("[ERROR:DB] transactionError ${error.toString()}");
      return DbError.transactionError(error as Exception);
    });
  }

  TaskEither<DbError, List<T>> updateTransaction<T extends DbEntity>(
    List<(DbTable table, DbEntity object)> updateStatementInfos,
  ) {
    return TaskEither.tryCatch(() async {
      if (this._database == null) {
        throw DbError.noReferenceToDatabase();
      }
      List<T> results = [];
      await this._database!.transaction((txn) async {
        for (final updateStatementInfo in updateStatementInfos) {
          Map<String, dynamic> objectJson = updateStatementInfo.$2.toJson();
          int updateRes = await txn.update(
            updateStatementInfo.$1.tableName,
            objectJson,
            where: "${updateStatementInfo.$1.primaryKey} = ?",
            whereArgs: [objectJson[updateStatementInfo.$1.primaryKey]],
          );
          if (updateRes == 1) {
            results.add(updateStatementInfo.$2 as T);
          } else {
            throw DbError.unknownEntityError(
                "Update error: 0 rows affected trying to update\n$objectJson");
          }
        }
      });
      return results;
    }, (error, stackTrace) {
      print("[ERROR:DB] updateError ${error.toString()}");
      return DbError.updateError(error as Exception);
    });
  }
}

class TransactionOperation {
  final TransactionOperationType operationType;
  final DbTable table;
  final DbEntity object;

  TransactionOperation(
      {required this.operationType, required this.table, required this.object});
}

enum TransactionOperationType {
  insert,
  update,
  delete;
}
