import "dart:io";

import 'package:connectivity_plus/connectivity_plus.dart';
import "package:dio/io.dart";
import "package:flutter/foundation.dart";
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/data/data_error.dart";
import "package:hiive/data/models/authentication_tokens.dart";
import "package:hiive/data/state/authentication_notifier.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:fpdart/fpdart.dart';
import "package:dio/dio.dart";
import "package:hiive/app_config.dart";
import "package:pretty_dio_logger/pretty_dio_logger.dart";

import "package:hiive/utils/loggers.dart";

part 'http.freezed.dart';
part 'http.g.dart';

@riverpod
HttpService httpService(Ref ref) => HttpService(ref);

class HttpService {
  late final Ref _ref;
  late final Dio _dio;

  HttpService(Ref ref) : this._ref = ref {
    //get baseurl from config
    final appConf = this._ref.read(appConfigProvider);
    final baseUrl = "https://${appConf.domain}";
    final httpAuth = appConf.httpAuth;
    final proxyAddr = appConf.proxyAddr;
    final options = BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: const Duration(milliseconds: 5000),
      sendTimeout: const Duration(milliseconds: 5000),
      receiveTimeout: const Duration(milliseconds: 5000),
      headers: httpAuth != null ? {"authorization": "Basic $httpAuth"} : {},
      contentType: Headers.jsonContentType,
      responseType: ResponseType.json,
      validateStatus: (status) {
        return status! < 500;
      },
    );

    this._dio = Dio(options);

    if (proxyAddr != null) {
      this._dio.httpClientAdapter = IOHttpClientAdapter(createHttpClient: () {
        final HttpClient client = HttpClient();
        client.findProxy = (url) {
          return "PROXY $proxyAddr";
        };
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
        return client;
      });
    }

    this._dio.interceptors.add(this._checkConnectionInterceptor);
    this._dio.interceptors.add(this._checkAuthenticationInterceptor);
    if (kDebugMode && DioLogger.showLogs) {
      _dio.interceptors.add(this._loggerInterceptor);
    }
  }

  TaskEither<ApiError, Map<String, dynamic>?> delete(
      String path, bool authRequired) {
    return TaskEither.tryCatch(() {
      return _dio
          .delete(path, options: Options(headers: {"auth-req": authRequired}))
          .then((response) => ApiResponse.fromJson(response.data));
    }, (error, _) {
      return parseRequestError(error);
    }).flatMap(parseApiResponse);
  }

  TaskEither<ApiError, Map<String, dynamic>?> get(
      String path, bool authRequired) {
    return TaskEither.tryCatch(() {
      return _dio
          .get(path, options: Options(headers: {"auth-req": authRequired}))
          .then((response) => ApiResponse.fromJson(response.data));
    }, (error, _) {
      return parseRequestError(error);
    }).flatMap(parseApiResponse);
  }

  TaskEither<ApiError, Map<String, dynamic>?> patch(
      String path, bool authRequired, Object data) {
    return TaskEither.tryCatch(() {
      return _dio
          .patch(path,
              options: Options(headers: {"auth-req": authRequired}), data: data)
          .then((response) => ApiResponse.fromJson(response.data));
    }, (error, _) {
      return parseRequestError(error);
    }).flatMap(parseApiResponse);
  }

  TaskEither<ApiError, Map<String, dynamic>?> post(
      String path, bool authRequired, Object data) {
    return TaskEither.tryCatch(() {
      return _dio
          .post(path,
              options: Options(headers: {"auth-req": authRequired}), data: data)
          .then((response) => ApiResponse.fromJson(response.data));
    }, (error, _) {
      return parseRequestError(error);
    }).flatMap(parseApiResponse);
  }

  final _checkConnectionInterceptor = InterceptorsWrapper(
    onRequest: (options, handler) async {
      if (!await ConnectionStatus.isConnected().run()) {
        handler.reject(
          DioException(
            requestOptions: options,
            message: "no-connection",
          ),
        );
      }

      handler.next(options);
    },
    onResponse: (response, handler) async {
      handler.next(response);
    },
    onError: (error, handler) async {
      handler.next(error);
    },
  );

  late final _checkAuthenticationInterceptor = InterceptorsWrapper(
    onRequest: (options, handler) async {
      if (options.headers["auth-req"] as bool) {
        final AuthenticationTokens? authenticationTokens =
            this._ref.read(authenticationNotifierProvider).value;
        if (authenticationTokens == null) {
          handler.reject(
            DioException(
              requestOptions: options,
              message: "not-logged-in",
            ),
          );
        } else {
          options.headers["Cookie"] =
              "Authorization=Bearer ${authenticationTokens.accessToken.token}";
          options.headers.remove("auth-req");
        }
      }
      handler.next(options);
    },
    onResponse: (response, handler) async {
      handler.next(response);
    },
    onError: (error, handler) async {
      handler.next(error);
    },
  );

  final _loggerInterceptor = PrettyDioLogger(
    requestHeader: true,
    requestBody: true,
    responseHeader: true,
    responseBody: true,
  );
}

@Freezed(unionKey: "status")
sealed class ApiResponse with _$ApiResponse {
  const factory ApiResponse.ok(Map<String, dynamic>? data) = Ok;
  const factory ApiResponse.error(ApiError error) = Error;

  factory ApiResponse.fromJson(Map<String, dynamic> json) =>
      _$ApiResponseFromJson(json);
}

TaskEither<ApiError, Map<String, dynamic>?> parseApiResponse(
    ApiResponse apiResponse) {
  return apiResponse.when(
    ok: (Map<String, dynamic>? data) {
      return TaskEither.right(data);
    },
    error: (ApiError error) {
      return TaskEither.left(error);
    },
  );
}

ApiError parseRequestError(Object error) {
  if (error is DioException) {
    switch (error.type) {
      case DioExceptionType.badResponse:
        return const ApiError.internalServerError();
      case DioExceptionType.connectionTimeout:
        return const ApiError.unknown();
      case DioExceptionType.sendTimeout:
        return const ApiError.unknown();
      case DioExceptionType.receiveTimeout:
        return const ApiError.unknown();
      case DioExceptionType.cancel:
        return const ApiError.unknown();
      case DioExceptionType.badCertificate:
        return const ApiError.unknown();
      case DioExceptionType.connectionError:
        return const ApiError.unknown();
      case DioExceptionType.unknown:
        {
          if (error.message == "no-connection") {
            return const ApiError.noConnection();
          } else if (error.message == "not-logged-in") {
            return const ApiError.notLoggedIn();
          } else {
            print("[HTTP] Error: $error");
            return const ApiError.unknown();
          }
        }
      default:
        print("[HTTP] Error: $error");
        return const ApiError.unknown();
    }
  } else {
    print("[HTTP] Error: $error");
    return const ApiError.unknown();
  }
}

class ConnectionStatus {
  static Task<bool> isConnected() {
    return Task(() async {
      List<ConnectivityResult> connectionsResult =
          await (Connectivity().checkConnectivity());

      if (connectionsResult.contains(ConnectivityResult.wifi) ||
          connectionsResult.contains(ConnectivityResult.mobile)) {
        return true;
      } else {
        return false;
      }
    });
  }
}
