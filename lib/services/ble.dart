import "dart:async";
import "dart:convert";
import "dart:typed_data";

import "package:flutter_reactive_ble/flutter_reactive_ble.dart" hide Unit;
import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/constants/defaults.dart";
import "package:hiive/features/gateway/ble_gateway_notifier.dart";
import "package:riverpod_annotation/riverpod_annotation.dart";
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:fpdart/fpdart.dart';

part "ble.freezed.dart";
part "ble.g.dart";

const systemService = "00000001-d913-4e77-844e-26088d4fef37";
const systemTimeChar = "00000002-276e-45a0-98c8-5c957b1102b9";
const systemBatteryChar = "00000003-b12e-469a-8d88-cfa3724bee90";
const systemIntervalChar = "00000004-7392-4c6f-994b-f826b36f707e";
const systemTokenChar = "00000005-fc0b-4e0b-bed4-c2ff8dde785c";
const systemResetChar = "00000006-da3b-4184-8fbf-6bed2ff99adb";
const systemFirmwareVersionChar = "00000007-832c-40bc-aa3c-2b7734ded777";
const systemFirmwareUpdateChar = "00000008-11f1-4dcd-ae04-c73ed89e0c9d";

const pairService = "00000001-1750-4be4-9b7b-ca810732ec93";
const pairBeginChar = "00000002-1223-43f9-8933-e67ebf03cad7";
const pairSensorInfoChar = "00000003-65a2-426b-ad5c-96c8c5d9c18c";
const pairStartCycleChar = "00000004-1af1-4ae1-86d8-7bc15839cdbc";

const wifiService = "00000001-78b6-49a7-8176-f4b5f7531a8b";
const wifiSsidChar = "00000002-0176-4a78-aca0-a0975e5bf760";
const wifiPasswordChar = "00000004-dccb-4871-8f6d-b7fcbeb16890";
const wifiScanStartChar = "00000005-e348-409b-9c40-f8dd89967c7d";
const wifiScanResultsChar = "00000006-89e0-40bf-9a3d-3dbe0c2bc306";

const serverService = "00000001-d44e-4975-ae6b-e6acffbc5a58";
const serverDomainChar = "00000002-73aa-4252-a4b7-5ee9e7e38b7b";
const serverStaticDomainChar = "00000003-c78d-4b56-ab1d-ede67e495855";

const testService = "00000001-2bb9-4751-a21b-544174e97c31";
const testCommandChar = "00000002-e43f-4b55-99a0-91e3af48fb81";
const testResultChar = "00000003-257b-4578-87f2-da88387a7e91";

const readingsService = "00000001-396e-4177-b9f1-324da78413b9";
const readingsCommandChar = "00000002-20f0-4819-ac37-0dfc4f76555b";
const readingsDataChar = "00000003-d9b8-419d-832c-b1262683e7c0";

const bleTaskOk = 1;
const bleTaskError = -1;

@Riverpod(keepAlive: true)
BleService bleService(Ref ref) => BleService(ref);

class BleService {
  late final Ref _ref;
  final _ble = FlutterReactiveBle();
  late final List<Uuid> _services;
  late final Map<Uuid, List<Uuid>> _servicesWithCharacteristics;

  StreamSubscription<ConnectionStateUpdate>? _connectedDeviceStreamSubscription;
  StreamSubscription<List<int>>? _firmwareUpdateProgressStreamSubscription;
  StreamSubscription<List<int>>? _pairedSensorsStreamSubscription;
  StreamSubscription<List<int>>? _wifiScanStreamSubscription;
  StreamSubscription<List<int>>? _readingsFilenamesStreamSubscription;
  StreamSubscription<List<int>>? _readingsFileStreamSubscription;

  BleService(Ref ref) : this._ref = ref {
    final systemServiceUuid = Uuid.parse(systemService);
    final pairServiceUuid = Uuid.parse(pairService);
    final wifiServiceUuid = Uuid.parse(wifiService);
    final serverServiceUuid = Uuid.parse(serverService);
    final testServiceUuid = Uuid.parse(testService);
    final readingsServiceUuid = Uuid.parse(readingsService);

    _services = [
      systemServiceUuid,
      pairServiceUuid,
      wifiServiceUuid,
      serverServiceUuid,
      testServiceUuid,
      readingsServiceUuid,
    ];
    _servicesWithCharacteristics = {
      systemServiceUuid: [
        Uuid.parse(systemTimeChar),
        Uuid.parse(systemBatteryChar),
        Uuid.parse(systemIntervalChar),
        Uuid.parse(systemTokenChar),
        Uuid.parse(systemResetChar),
        Uuid.parse(systemFirmwareVersionChar),
        Uuid.parse(systemFirmwareUpdateChar),
      ],
      pairServiceUuid: [
        Uuid.parse(pairBeginChar),
        Uuid.parse(pairSensorInfoChar),
        Uuid.parse(pairStartCycleChar),
      ],
      wifiServiceUuid: [
        Uuid.parse(wifiSsidChar),
        Uuid.parse(wifiPasswordChar),
        Uuid.parse(wifiScanStartChar),
        Uuid.parse(wifiScanResultsChar),
      ],
      serverServiceUuid: [
        Uuid.parse(serverDomainChar),
        Uuid.parse(serverStaticDomainChar),
      ],
      testServiceUuid: [
        Uuid.parse(testCommandChar),
        Uuid.parse(testResultChar),
      ],
      readingsServiceUuid: [
        Uuid.parse(readingsCommandChar),
        Uuid.parse(readingsDataChar),
      ],
    };
  }

  Stream<BleStatus> status() {
    return _ble.statusStream;
  }

  TaskEither<BleError, (String, String)> scan() {
    return TaskEither.tryCatch(
      () async {
        DiscoveredDevice gateway = await _ble
            .scanForDevices(
                withServices: _services, scanMode: ScanMode.balanced)
            .firstWhere(
                (device) => device.name.startsWith(cGatewayBleNamePrefix))
            .timeout(Duration(seconds: cBleScanTimeoutSeconds));
        String gatewayMac = gateway.name.split(":").last;
        return (gateway.id, gatewayMac);
      },
      (error, stackTrace) => BleError.noScanResult(),
    );
    //Future<DiscoveredDevice> device = _ble
    //    .scanForDevices(withServices: _services, scanMode: ScanMode.balanced)
    //    .firstWhere(
    //  (device) {
    //    return device.name.startsWith("HIIVE-GW");
    //  },
    //).timeout(Duration(seconds: cBleScanTimeoutSeconds));
  }

  //TaskEither<BleError, String> _getGatewayIdFromMac(String mac) {
  //  return TaskEither.tryCatch(
  //    () async {
  //      print("[BLE] Scanning for gateway: $mac");
  //      late final StreamSubscription<DiscoveredDevice> scannedSubscription;
  //      final completer = Completer<String>();
  //      final timeoutTimer = Timer(const Duration(seconds: 10), () {
  //        scannedSubscription.cancel();
  //        if (!completer.isCompleted) {
  //          completer
  //              .completeError("[ERROR][BLE] Could not find gateway: $mac");
  //        }
  //      });
  //      scannedSubscription = scan().listen(
  //        (device) {
  //          if (device.name.split(':').last == mac) {
  //            completer.complete(device.id);
  //            scannedSubscription.cancel();
  //            timeoutTimer.cancel();
  //          }
  //        },
  //        cancelOnError: true,
  //        onError: (e, st) {
  //          return e.toString();
  //        },
  //        onDone: () =>
  //            print("[BLE] Scan (getGatewayIdFromMac) subscription done"),
  //      );
  //      return completer.future;
  //    },
  //    (error, stackTrace) => BleError.noScanResult(),
  //  );
  //}

  TaskEither<
      BleError,
      (
        String, // ble id
        String, // mac
        Stream<ConnectionStateUpdate>,
        int, // interval
        GatewayTestResultBle,
        String, // ssid
      )> connect(
    String? mac,
    String? token,
  ) {
    String? gatewayId;
    String? gatewayMac = mac;

    return TaskEither.tryCatch(() async {
      // scan
      await this.scan().match(
        (bleError) => throw bleError,
        (bleGateway) {
          if (gatewayMac != null && gatewayMac != bleGateway.$2) {
            throw BleError.gatewayNotFound();
          }
          gatewayId = bleGateway.$1;
          gatewayMac = bleGateway.$2;
        },
      ).run();

      // connect
      Stream<ConnectionStateUpdate> connectionStateUpdateStream =
          _ble.connectToAdvertisingDevice(
        id: gatewayId!,
        withServices: _services,
        prescanDuration: const Duration(seconds: cPrescanSeconds),
        servicesWithCharacteristicsToDiscover: _servicesWithCharacteristics,
        connectionTimeout: const Duration(seconds: cConnectionTimeout),
      );
      print("[BLE] Requesting MTU: $cMtu");
      final negotiatedMtu =
          await _ble.requestMtu(deviceId: gatewayId!, mtu: cMtu);
      print("[BLE] Negotiated MTU: $negotiatedMtu");
      final setTimeStatus = await _setTime(gatewayId!).run();
      setTimeStatus.match((bleError) => throw bleError, (_) {});
      if (token != null) {
        final setTokenStatus = await setToken(gatewayId!, token).run();
        setTokenStatus.match((bleError) => throw bleError, (_) {});
      }
      final getIntervalStatus = await _getInterval(gatewayId!).run();
      int interval = getIntervalStatus.match(
          (bleError) => throw bleError, (interval) => interval);

      final getTestResultStatus = await _getTestResult(gatewayId!).run();
      GatewayTestResultBle testResult = getTestResultStatus.match(
          (bleError) => throw bleError, (testResult) => testResult);

      final getSsidStatus = await _getSsid(gatewayId!).run();
      String ssid =
          getSsidStatus.match((bleError) => throw bleError, (ssid) => ssid);

      return (
        gatewayId!,
        gatewayMac!,
        connectionStateUpdateStream,
        interval,
        testResult,
        ssid,
      );
    }, (error, stacktrace) => error as BleError);
  }

  //TaskEither<BleError, (String, String)> connect(String? id, String mac) {
  //TaskEither<BleError, (String, String)> connect(String? mac) {
  //  String? gatewayId;
  //  String? gatewayMac = mac;

  //  final completer = Completer<Unit>();

  //  // Complete if already connected
  //  _ref.read(bleConnectedNotifierProvider).whenOrNull(
  //      connected: (connectedGateway) {
  //    if (connectedGateway.mac == mac) {
  //      print("[BLE] Already connected to: $mac");
  //      completer.complete(unit);
  //    }
  //  });

  //  if (completer.isCompleted) {
  //    return TaskEither.right(unit);
  //  }

  //  _ref.read(bleConnectedNotifierProvider.notifier).connecting(
  //        ConnectingGateway(
  //          id: null,
  //          mac: mac,
  //        ),
  //      );

  //  // Get gatewayId if not provided
  //  return TaskEither.tryCatch(
  //    () async {
  //      if (_gatewayId == null) {
  //        await _getGatewayIdFromMac(mac).match(
  //          (bleError) {
  //            _ref.read(bleConnectedNotifierProvider.notifier).disconnected();
  //            throw bleError;
  //          },
  //          (discoveredGatewayId) {
  //            gatewayId = discoveredGatewayId;
  //          },
  //        ).run();
  //      }
  //      print("[BLE] Connecting to $gatewayId");
  //      _connectedDeviceStreamSubscription = _ble
  //          .connectToAdvertisingDevice(
  //              id: gatewayId,
  //              withServices: _services,
  //              prescanDuration: const Duration(seconds: 1),
  //              servicesWithCharacteristicsToDiscover:
  //                  _servicesWithCharacteristics,
  //              connectionTimeout: const Duration(seconds: 1))
  //          .listen(
  //        (connectionState) {
  //          if (connectionState.failure != null) {
  //            print("[BLE] Connection state error: ${connectionState.failure}");
  //            _ref.read(bleConnectedNotifierProvider.notifier).disconnected();
  //          } else {
  //            switch (connectionState.connectionState) {
  //              case DeviceConnectionState.connecting:
  //                _ref.read(bleConnectedNotifierProvider.notifier).connecting(
  //                      ConnectingGateway(
  //                        id: gatewayId,
  //                        mac: mac,
  //                      ),
  //                    );

  //                break;
  //              case DeviceConnectionState.connected:
  //                _ble.requestMtu(deviceId: gatewayId, mtu: cMtu);
  //                _ref
  //                    .read(bleConnectedNotifierProvider.notifier)
  //                    .connected(ConnectedGateway(id: gatewayId, mac: mac));
  //                _getGatewayInfo().match(
  //                  (e) {
  //                    _ref
  //                        .read(bleConnectedNotifierProvider.notifier)
  //                        .disconnected();
  //                    if (!completer.isCompleted) {
  //                      completer.completeError(e);
  //                    }
  //                  },
  //                  (connectedGateway) {
  //                    _ref
  //                        .read(bleConnectedNotifierProvider.notifier)
  //                        .connected(connectedGateway);
  //                    if (!completer.isCompleted) {
  //                      completer.complete(unit);
  //                    }
  //                  },
  //                ).run();
  //                break;
  //              case DeviceConnectionState.disconnecting:
  //                break;
  //              case DeviceConnectionState.disconnected:
  //                _ref
  //                    .read(bleConnectedNotifierProvider.notifier)
  //                    .disconnected();
  //                break;
  //              default:
  //                break;
  //            }
  //          }
  //        },
  //        cancelOnError: true,
  //        onError: (e, st) => e.toString(),
  //        onDone: () => print("[BLE] Connect subscription done"),
  //      );
  //      return completer.future;
  //    },
  //    (error, stackTrace) => error.toString(),
  //  );
  //}

  //Task<void> disconnect() {
  //  return Task(() async {
  //    await _connectedDeviceStreamSubscription?.cancel();
  //    await _firmwareUpdateProgressStreamSubscription?.cancel();
  //    await _pairedSensorsStreamSubscription?.cancel();
  //    await _wifiScanStreamSubscription?.cancel();
  //    await _readingsFilenamesStreamSubscription?.cancel();
  //    await _readingsFileStreamSubscription?.cancel();
  //    _ref.read(bleConnectedNotifierProvider.notifier).disconnected();
  //  });
  //}

  //TaskEither<String, void> initializeGateway(String token, Duration interval) {
  //  final tasks = [
  //    _setToken(token),
  //  ];
  //  return tasks.sequenceTaskEitherSeq();
  //}

  //TaskEither<String, void> setWifiCredentials(String ssid, String password) {
  //  return TaskEither.sequenceList(
  //      [_setWifiSsid(ssid), _setWifiPassword(password)]).map((results) {});
  //}

  //TaskEither<String, void> testWifi() {
  //  print("[BLE] Wifi test begin");
  //  final testServiceUuid = Uuid.parse(testService);
  //  final testCommandCharUuid = Uuid.parse(testCommandChar);
  //  final testResultCharUuid = Uuid.parse(testResultChar);
  //  return _triggerAndListenSingleValue(testServiceUuid, testCommandCharUuid,
  //          testServiceUuid, testResultCharUuid, [1], 10)
  //      .map((r) {});
  //}

  //TaskEither<String, void> testSd() {
  //  print("[BLE] SD test Begin");
  //  final testServiceUuid = Uuid.parse(testService);
  //  final testCommandCharUuid = Uuid.parse(testCommandChar);
  //  final testResultCharUuid = Uuid.parse(testResultChar);
  //  return _triggerAndListenSingleValue(testServiceUuid, testCommandCharUuid,
  //          testServiceUuid, testResultCharUuid, [2], 10)
  //      .map((r) {});
  //}

  //TaskEither<String, Stream<WiFiNetwork>> scanWifi() {
  //  print("[BLE] Scan wifi begin");
  //  final wifiServiceUuid = Uuid.parse(wifiService);
  //  final wifiScanCharUuid = Uuid.parse(wifiScanStartChar);
  //  final wifiScanResultsCharUuid = Uuid.parse(wifiScanResultsChar);

  //  final exposedStreamController = StreamController<WiFiNetwork>();

  //  return TaskEither.tryCatch(
  //    () {
  //      return _triggerAndStream(wifiServiceUuid, wifiScanCharUuid,
  //              wifiServiceUuid, wifiScanResultsCharUuid, [1])
  //          .map(
  //            (rawStream) {
  //              _wifiScanStreamSubscription = rawStream.listen(
  //                (event) {
  //                  print(event);
  //                  if (event.isNotEmpty) {
  //                    if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskOk) {
  //                      print("WiFi scan done");
  //                      // GW DONE
  //                      _wifiScanStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskError) {
  //                      // GW ERROR
  //                      print("[BLE][ERROR] WiFi scan ");
  //                      print("[BLE][ERROR] WiFi scan ");
  //                      exposedStreamController.addError(
  //                        Exception("scanWifi Gateway error"),
  //                      );
  //                      _wifiScanStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else {
  //                      final List<String> dataStrings =
  //                          String.fromCharCodes(event).split('#');
  //                      final String ssid = dataStrings[0];
  //                      final int rssi = int.parse(dataStrings[1]);
  //                      final bool secure =
  //                          int.parse(dataStrings[2]) == 0 ? false : true;
  //                      exposedStreamController.add(WiFiNetwork(
  //                          ssid: ssid, rssi: rssi, secure: secure));
  //                    }
  //                  }
  //                },
  //                onDone: () {
  //                  print("wifi inner done");
  //                  exposedStreamController.close();
  //                },
  //              );
  //            },
  //          )
  //          .run()
  //          .then((value) => exposedStreamController.stream);
  //    },
  //    (error, stackTrace) {
  //      print(error);
  //      print(stackTrace);
  //      return error.toString();
  //    },
  //  );
  //}

  //TaskEither<String, Stream<PairedSensor>> listenPairedSensors() {
  //  print("[BLE] Listen paired sensors");
  //  final pairServiceUuid = Uuid.parse(pairService);
  //  final pairSensorInfoCharUuid = Uuid.parse(pairSensorInfoChar);

  //  final exposedStreamController = StreamController<PairedSensor>();

  //  return TaskEither.tryCatch(
  //    () {
  //      return _listenCharacteristic(pairServiceUuid, pairSensorInfoCharUuid)
  //          .map(
  //            (rawStream) {
  //              _pairedSensorsStreamSubscription = rawStream.listen(
  //                (event) {
  //                  print(String.fromCharCodes(event));
  //                  final dataString = String.fromCharCodes(event);
  //                  final list = dataString.split(" ");
  //                  final mac = list[0];
  //                  final battery = int.parse(list[1]);
  //                  final rssi = int.parse(list[2]);
  //                  exposedStreamController.add(
  //                      PairedSensor(mac: mac, battery: battery, rssi: rssi));
  //                },
  //                onDone: () {
  //                  exposedStreamController.close();
  //                },
  //              );
  //            },
  //          )
  //          .run()
  //          .then((value) => exposedStreamController.stream);
  //    },
  //    (error, stackTrace) {
  //      return error.toString();
  //    },
  //  );
  //}

  //TaskEither<String, void> startSynchronization() {
  //  print("[BLE] Start pairing");
  //  final pairServiceUuid = Uuid.parse(pairService);
  //  final pairBeginCharUuid = Uuid.parse(pairBeginChar);

  //  return _writeCharacteristic(pairServiceUuid, pairBeginCharUuid, [1]);
  //}

  //TaskEither<String, void> startCycle() {
  //  print("[BLE] Begin cycle");
  //  final operationServiceUuid = Uuid.parse(pairService);
  //  final operationStartCycleCharUuid = Uuid.parse(pairStartCycleChar);

  //  return _writeCharacteristic(
  //      operationServiceUuid, operationStartCycleCharUuid, [1],
  //      response: false);
  //}

  //TaskEither<String, Stream<String>> getReadingsFilenames() {
  //  print("[BLE] Get reading filenames");
  //  final readingsServiceUuid = Uuid.parse(readingsService);
  //  final readingsCommandUuid = Uuid.parse(readingsCommandChar);
  //  final readingsDataUuid = Uuid.parse(readingsDataChar);

  //  final exposedStreamController = StreamController<String>();

  //  return TaskEither.tryCatch(
  //    () {
  //      return _triggerAndStream(readingsServiceUuid, readingsCommandUuid,
  //              readingsServiceUuid, readingsDataUuid, [1])
  //          .map(
  //            (rawStream) {
  //              _readingsFilenamesStreamSubscription = rawStream.listen(
  //                (event) {
  //                  print(event);
  //                  // End of stream
  //                  if (event.isNotEmpty) {
  //                    if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskOk) {
  //                      // GW DONE
  //                      _readingsFilenamesStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskError) {
  //                      // GW ERROR
  //                      print("[BLE][ERROR] Get reading filenames ");
  //                      exposedStreamController.addError(
  //                        Exception("getReadingsFilenames Gateway error"),
  //                      );
  //                      _readingsFilenamesStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else {
  //                      exposedStreamController
  //                          .add(String.fromCharCodes(event));
  //                    }
  //                  }
  //                },
  //                onDone: () {
  //                  print("filenames inner done");
  //                  exposedStreamController.close();
  //                },
  //              );
  //            },
  //          )
  //          .run()
  //          .then((value) => exposedStreamController.stream);
  //    },
  //    (error, stackTrace) {
  //      print(error);
  //      print(stackTrace);
  //      return error.toString();
  //    },
  //  );
  //}

  //TaskEither<String, Stream<ReadingsFileProgress>> getReadingsFile(
  //    String filename) {
  //  print("[BLE] Get reading file: $filename");
  //  final readingsServiceUuid = Uuid.parse(readingsService);
  //  final readingsCommandUuid = Uuid.parse(readingsCommandChar);
  //  final readingsDataUuid = Uuid.parse(readingsDataChar);

  //  final exposedStreamController = StreamController<ReadingsFileProgress>();

  //  int? filesize;
  //  int bytesReceived = 0;
  //  return TaskEither.tryCatch(
  //    () {
  //      return _triggerAndStream(
  //              readingsServiceUuid,
  //              readingsCommandUuid,
  //              readingsServiceUuid,
  //              readingsDataUuid,
  //              [2, ...filename.codeUnits])
  //          .map(
  //            (rawStream) {
  //              _readingsFileStreamSubscription = rawStream.listen(
  //                (event) {
  //                  // End of stream
  //                  if (event.isNotEmpty) {
  //                    if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskOk) {
  //                      // GW DONE
  //                      print("[BLE] Get readings done ");
  //                      _readingsFileStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskError) {
  //                      // GW ERROR
  //                      print("[BLE][ERROR] Get readings file ");
  //                      exposedStreamController.addError(
  //                        Exception("getReadingsFile Gateway error"),
  //                      );
  //                      _readingsFileStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else {
  //                      if (filesize == null) {
  //                        filesize = event[3] << 24 |
  //                            event[2] << 16 |
  //                            event[1] << 8 |
  //                            event[0];
  //                      } else {
  //                        bytesReceived += event.length;
  //                        exposedStreamController.add(
  //                          ReadingsFileProgress(
  //                            filesize: filesize ?? 0,
  //                            received: bytesReceived,
  //                            data: Uint8List.fromList(event),
  //                          ),
  //                        );
  //                      }
  //                      List<int> bytesReceivedBytes = [
  //                        (bytesReceived >> 24) & 0xFF,
  //                        (bytesReceived >> 16) & 0xFF,
  //                        (bytesReceived >> 8) & 0xFF,
  //                        bytesReceived & 0xFF
  //                      ];
  //                      _writeCharacteristic(
  //                        readingsServiceUuid,
  //                        readingsCommandUuid,
  //                        [4, ...bytesReceivedBytes],
  //                      ).run();
  //                    }
  //                  }
  //                },
  //                onDone: () {
  //                  print("readings inner done");
  //                  exposedStreamController.close();
  //                },
  //              );
  //            },
  //          )
  //          .run()
  //          .then((value) => exposedStreamController.stream);
  //    },
  //    (error, stackTrace) {
  //      print(error);
  //      print(stackTrace);
  //      return error.toString();
  //    },
  //  );
  //}

  //TaskEither<String, Stream<int>> updateFirmware(String version) {
  //  print("[BLE] Updating gateway version to: $version");
  //  final systemServiceUuid = Uuid.parse(systemService);
  //  final systemFirmwareUpdateCharUuid = Uuid.parse(systemFirmwareUpdateChar);

  //  final exposedStreamController = StreamController<int>();

  //  return TaskEither.tryCatch(
  //    () {
  //      return _triggerAndStream(
  //              systemServiceUuid,
  //              systemFirmwareUpdateCharUuid,
  //              systemServiceUuid,
  //              systemFirmwareUpdateCharUuid,
  //              [...version.codeUnits])
  //          .map(
  //            (rawStream) {
  //              _firmwareUpdateProgressStreamSubscription = rawStream.listen(
  //                (event) {
  //                  print(event);
  //                  if (event.isNotEmpty) {
  //                    if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskOk) {
  //                      // GW DONE
  //                      _firmwareUpdateProgressStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else if (event.length == 1 &&
  //                        Int8List.fromList([event[0]]).first == bleTaskError) {
  //                      // GW ERROR
  //                      print("[BLE][ERROR] Update firmware ");
  //                      exposedStreamController.addError(
  //                        Exception("updateFirmware Gateway error"),
  //                      );
  //                      _firmwareUpdateProgressStreamSubscription?.cancel();
  //                      exposedStreamController.close();
  //                    } else {
  //                      // First number is never used, but it distingueshes the actual data from control codes (1 success, -1 error)
  //                      int updateProgress = int.parse(
  //                          String.fromCharCodes(event).split(" ")[1]);
  //                      exposedStreamController.add(updateProgress);
  //                    }
  //                  }
  //                },
  //                onDone: () {
  //                  print("firmware inner done");
  //                  exposedStreamController.close();
  //                },
  //              );
  //            },
  //          )
  //          .run()
  //          .then((value) => exposedStreamController.stream);
  //    },
  //    (error, stackTrace) {
  //      print(error);
  //      print(stackTrace);
  //      return error.toString();
  //    },
  //  );
  //}

  //TaskEither<String, bool> deleteReadingsFile(String filename) {
  //  print("[BLE] Get reading file: $filename");
  //  final readingsServiceUuid = Uuid.parse(readingsService);
  //  final readingsCommandUuid = Uuid.parse(readingsCommandChar);
  //  final readingsDataUuid = Uuid.parse(readingsDataChar);

  //  return _triggerAndListenSingleValue(
  //          readingsServiceUuid,
  //          readingsCommandUuid,
  //          readingsServiceUuid,
  //          readingsDataUuid,
  //          [3, ...filename.codeUnits],
  //          10)
  //      .map((r) {
  //    return r[0] < 1 ? false : true;
  //  });
  //}

  //TaskEither<String, void> refreshGatewayInfo() {
  //  return _getGatewayInfo().flatMap((r) => TaskEither.right(
  //      _ref.read(bleConnectedNotifierProvider.notifier).connected(r)));
  //}

  //TaskEither<String, ConnectedGateway> _getGatewayInfo() {
  //  return _ref.read(bleConnectedNotifierProvider).maybeWhen(
  //        connected: (connectedGateway) {
  //          return TaskEither.sequenceListSeq([
  //            _getBattery(),
  //            _getWifiSsid(),
  //            _getTestResult(),
  //            _setTime().map((r) => [0]),
  //          ]).map((r) {
  //            final int battery = r[0][0];
  //            final String? ssid =
  //                r[1].isEmpty ? null : String.fromCharCodes(r[1]);

  //            final List<String> testResults =
  //                String.fromCharCodes(r[2]).split(" ");
  //            final List<String> wifiTestResult = testResults[0].split('#');
  //            final List<String> sdTestResult = testResults[1].split('#');

  //            final wifiTest = int.parse(wifiTestResult[0]);
  //            final rssi = int.parse(wifiTestResult[1]);

  //            final sdTest = int.parse(sdTestResult[0]);
  //            final sdSpaceUsed = int.parse(sdTestResult[1]);

  //            return ConnectedGateway(
  //              id: connectedGateway.id,
  //              mac: connectedGateway.mac,
  //              battery: battery,
  //              ssid: ssid,
  //              rssi: rssi,
  //              wifiTest: wifiTest,
  //              sdTest: sdTest,
  //              sdSpaceUsed: sdSpaceUsed,
  //            );
  //          });
  //        },
  //        orElse: () =>
  //            TaskEither.left("[ERROR][BLE] Not connected to gateway"),
  //      );
  //}

  //TaskEither<String, List<int>> _getBattery() {
  //  final operationServiceUuid = Uuid.parse(systemService);
  //  final operationBatteryCharUuid = Uuid.parse(systemBatteryChar);
  //  return _readCharacteristic(operationServiceUuid, operationBatteryCharUuid)
  //      .mapLeft((_) => "Failed to read battery");
  //}

  //TaskEither<String, List<int>> _getWifiSsid() {
  //  final wifiServiceUuid = Uuid.parse(wifiService);
  //  final wifiSsidCharUuid = Uuid.parse(wifiSsidChar);
  //  return _readCharacteristic(wifiServiceUuid, wifiSsidCharUuid)
  //      .mapLeft((_) => "Failed to read ssid");
  //}

  //TaskEither<String, List<int>> _getTestResult() {
  //  final testServiceUuid = Uuid.parse(testService);
  //  final testWifiResCharUuid = Uuid.parse(testResultChar);
  //  return _readCharacteristic(testServiceUuid, testWifiResCharUuid)
  //      .mapLeft((_) => "Failed to read WiFi test result");
  //}
  //
  TaskEither<BleError, int> _getInterval(String deviceId) {
    final operationServiceUuid = Uuid.parse(systemService);
    final operationIntervalCharUuid = Uuid.parse(systemIntervalChar);
    return readCharacteristic(
            operationServiceUuid, operationIntervalCharUuid, deviceId)
        .map((interval) => interval[0]);
  }

  TaskEither<BleError, GatewayTestResultBle> _getTestResult(String deviceId) {
    final testServiceUuid = Uuid.parse(testService);
    final testResultCharUuid = Uuid.parse(testResultChar);
    return readCharacteristic(testServiceUuid, testResultCharUuid, deviceId)
        .map((testResult) =>
            GatewayTestResultBle.fromString(String.fromCharCodes(testResult)));
  }

  TaskEither<BleError, String> _getSsid(String deviceId) {
    final wifiServiceUuid = Uuid.parse(wifiService);
    final wifiSsidCharUuid = Uuid.parse(wifiSsidChar);
    return readCharacteristic(wifiServiceUuid, wifiSsidCharUuid, deviceId)
        .map((ssid) => String.fromCharCodes(ssid));
  }

  TaskEither<BleError, Unit> _setTime(
    String deviceId,
  ) {
    final timeEpoch = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    final systemServiceUuid = Uuid.parse(systemService);
    final systemTimeCharUuid = Uuid.parse(systemTimeChar);
    final timeEpochEncoded = Uint8List(8)
      ..buffer.asByteData().setInt64(0, timeEpoch, Endian.little);
    print("[BLE] Setting gateway time to ${DateTime.now()}");
    return writeCharacteristic(
        systemServiceUuid, systemTimeCharUuid, timeEpochEncoded, deviceId);
  }

  TaskEither<BleError, Unit> setToken(
    String deviceId,
    String token,
  ) {
    final systemServiceUuid = Uuid.parse(systemService);
    final systemTokenCharUuid = Uuid.parse(systemTokenChar);
    print("[BLE] Setting gateway token");
    return writeCharacteristic(
      systemServiceUuid,
      systemTokenCharUuid,
      ascii.encode(token),
      deviceId,
    );
  }

  //TaskEither<String, void> _setDomain(String domain) {
  //  final serverServiceUuid = Uuid.parse(serverService);
  //  final serverDomainCharUuid = Uuid.parse(serverDomainChar);
  //  final List<int> domainEncoded = ascii.encode(domain);
  //  return _writeCharacteristic(
  //      serverServiceUuid, serverDomainCharUuid, domainEncoded);
  //}

  //TaskEither<String, void> _setStaticDomain(String staticDomain) {
  //  final serverServiceUuid = Uuid.parse(serverService);
  //  final serverStaticDomainCharUuid = Uuid.parse(serverStaticDomainChar);
  //  final List<int> domainEncoded = ascii.encode(staticDomain);
  //  return _writeCharacteristic(
  //      serverServiceUuid, serverStaticDomainCharUuid, domainEncoded);
  //}

  //TaskEither<String, void> _setToken(String token) {
  //  final systemServiceUuid = Uuid.parse(systemService);
  //  final systemTokenCharUuid = Uuid.parse(systemTokenChar);
  //  final List<int> tokenEncoded = ascii.encode(token);
  //  return _writeCharacteristic(
  //      systemServiceUuid, systemTokenCharUuid, tokenEncoded);
  //}

  //TaskEither<String, void> _setWifiSsid(String ssid) {
  //  final wifiServiceUuid = Uuid.parse(wifiService);
  //  final wifiSsidCharUuid = Uuid.parse(wifiSsidChar);
  //  final List<int> ssidEncoded = ascii.encode(ssid);
  //  return _writeCharacteristic(wifiServiceUuid, wifiSsidCharUuid, ssidEncoded);
  //}

  //TaskEither<String, void> _setWifiPassword(String password) {
  //  final wifiServiceUuid = Uuid.parse(wifiService);
  //  final wifiPasswordCharUuid = Uuid.parse(wifiPasswordChar);
  //  final List<int> ssidEncoded = ascii.encode(password);
  //  return _writeCharacteristic(
  //      wifiServiceUuid, wifiPasswordCharUuid, ssidEncoded);
  //}

  //TaskEither<String, List<int>> _listenSingleValue(
  //  Uuid serviceUuid,
  //  Uuid characteristicUuid,
  //  int timeout,
  //) {
  //  return _ref.read(bleConnectedNotifierProvider).maybeWhen(
  //        connected: (connectedGateway) {
  //          late final StreamSubscription<List<int>> charSubscription;
  //          final completer = Completer<List<int>>();
  //          final timeoutTimer = Timer(Duration(seconds: timeout), () {
  //            charSubscription.cancel();
  //            completer.completeError("[ERROR][BLE] listenSingleValue timeout");
  //          });

  //          return TaskEither.tryCatch(
  //            () async {
  //              print("[BLE] Listen $characteristicUuid single value");
  //              final characteristic = QualifiedCharacteristic(
  //                  serviceId: serviceUuid,
  //                  characteristicId: characteristicUuid,
  //                  deviceId: connectedGateway.id);
  //              charSubscription = _ble
  //                  .subscribeToCharacteristic(characteristic)
  //                  .take(1)
  //                  .listen(
  //                    (data) {
  //                      timeoutTimer.cancel();
  //                      completer.complete(data);
  //                    },
  //                    cancelOnError: true,
  //                    onError: (error) {
  //                      timeoutTimer.cancel();
  //                      completer.completeError(error);
  //                    },
  //                    onDone: () =>
  //                        print("[BLE] listenSingleValue subscription done"),
  //                  );

  //              return await completer.future;
  //            },
  //            (error, stackTrace) {
  //              timeoutTimer.cancel();
  //              return error.toString();
  //            },
  //          );
  //        },
  //        orElse: () =>
  //            TaskEither.left("[ERROR][BLE] Not connected to gateway"),
  //      );
  //}

  //TaskEither<String, List<int>> _triggerAndListenSingleValue(
  //  Uuid triggerServiceUuid,
  //  Uuid triggerCharacteristicUuid,
  //  Uuid? listenServiceUuidArg,
  //  Uuid? listenCharacteristicUuidArg,
  //  List<int> triggerValue,
  //  int timeout,
  //) {
  //  Uuid listenServiceUuid = listenServiceUuidArg ?? triggerServiceUuid;
  //  Uuid listenCharacteristicUuid =
  //      listenCharacteristicUuidArg ?? triggerCharacteristicUuid;
  //  return _ref.read(bleConnectedNotifierProvider).maybeWhen(
  //        connected: (connectedGateway) {
  //          late final StreamSubscription<List<int>> charSubscription;
  //          final completer = Completer<List<int>>();
  //          final timeoutTimer = Timer(Duration(seconds: timeout), () {
  //            charSubscription.cancel();
  //            completer.completeError(
  //                "[ERROR][BLE] triggerAndListenSingleValue timeout");
  //          });

  //          final listenTask = TaskEither.tryCatch(
  //            () async {
  //              print("[BLE] listen $listenCharacteristicUuid single value");
  //              final characteristic = QualifiedCharacteristic(
  //                  serviceId: listenServiceUuid,
  //                  characteristicId: listenCharacteristicUuid,
  //                  deviceId: connectedGateway.id);
  //              charSubscription = _ble
  //                  .subscribeToCharacteristic(characteristic)
  //                  .take(1)
  //                  .listen(
  //                    (data) {
  //                      timeoutTimer.cancel();
  //                      completer.complete(data);
  //                    },
  //                    cancelOnError: true,
  //                    onError: (error) {
  //                      timeoutTimer.cancel();
  //                      completer.completeError(error);
  //                    },
  //                    onDone: () => print(
  //                        "[BLE] triggerAndlistenSingleValue subscription done"),
  //                  );

  //              return await completer.future;
  //            },
  //            (error, stackTrace) {
  //              timeoutTimer.cancel();
  //              return error.toString();
  //            },
  //          );

  //          final triggerTask = _writeCharacteristic(
  //              triggerServiceUuid, triggerCharacteristicUuid, triggerValue);

  //          return TaskEither.sequenceList([
  //            listenTask,
  //            triggerTask.map((_) => [0])
  //          ]).map(
  //            (r) => r[0], // return result of listenTask (r[0])
  //          );
  //        },
  //        orElse: () =>
  //            TaskEither.left("[ERROR][BLE] Not connected to gateway"),
  //      );
  //}

  TaskEither<BleError, GatewayTestResultBle> triggerAndGatewayTestResult(
    Uuid triggerServiceUuid,
    Uuid triggerCharacteristicUuid,
    Uuid? subServiceUuidArg,
    Uuid? subCharacteristicUuidArg,
    String deviceId,
  ) {
    Uuid subServiceUuid = subServiceUuidArg ?? triggerServiceUuid;
    Uuid subCharacteristicUuid =
        subCharacteristicUuidArg ?? triggerCharacteristicUuid;
    final subCharacteristic = QualifiedCharacteristic(
        serviceId: subServiceUuid,
        characteristicId: subCharacteristicUuid,
        deviceId: deviceId);

    Future<GatewayTestResultBle> response = _ble
        .subscribeToCharacteristic(subCharacteristic)
        .map(
          (response) =>
              GatewayTestResultBle.fromString(String.fromCharCodes(response)),
        )
        .first;

    return TaskEither.tryCatch(() async {
      Either<BleError, Unit> gg = await writeCharacteristic(
        triggerServiceUuid,
        triggerCharacteristicUuid,
        [cTriggerBleTaskValue],
        deviceId,
      ).run();

      return gg.match(
        (bleError) => throw bleError,
        (r) async => await response,
      );
    }, (bleError, stackTrace) {
      return bleError as BleError;
    });
  }

  TaskEither<BleError, int> triggerAndInt(
    Uuid triggerServiceUuid,
    Uuid triggerCharacteristicUuid,
    Uuid? subServiceUuidArg,
    Uuid? subCharacteristicUuidArg,
    String deviceId,
  ) {
    Uuid subServiceUuid = subServiceUuidArg ?? triggerServiceUuid;
    Uuid subCharacteristicUuid =
        subCharacteristicUuidArg ?? triggerCharacteristicUuid;
    final subCharacteristic = QualifiedCharacteristic(
        serviceId: subServiceUuid,
        characteristicId: subCharacteristicUuid,
        deviceId: deviceId);

    Future<int> response = _ble
        .subscribeToCharacteristic(subCharacteristic)
        .map((response) => response[0])
        .first;

    return TaskEither.tryCatch(() async {
      Either<BleError, Unit> gg = await writeCharacteristic(
        triggerServiceUuid,
        triggerCharacteristicUuid,
        [cTriggerBleTaskValue],
        deviceId,
      ).run();

      return gg.match(
        (bleError) => throw bleError,
        (r) async => await response,
      );
    }, (bleError, stackTrace) {
      return bleError as BleError;
    });
  }

  TaskEither<BleError, Stream<List<int>>> triggerAndStream(
    Uuid triggerServiceUuid,
    Uuid triggerCharacteristicUuid,
    Uuid? subServiceUuidArg,
    Uuid? subCharacteristicUuidArg,
    String deviceId,
  ) {
    Uuid subServiceUuid = subServiceUuidArg ?? triggerServiceUuid;
    Uuid subCharacteristicUuid =
        subCharacteristicUuidArg ?? triggerCharacteristicUuid;
    final subCharacteristic = QualifiedCharacteristic(
        serviceId: subServiceUuid,
        characteristicId: subCharacteristicUuid,
        deviceId: deviceId);

    final exposedStreamController = StreamController<List<int>>();
    final Stream<List<int>> stream =
        _ble.subscribeToCharacteristic(subCharacteristic);

    StreamSubscription<List<int>>? streamSubscription;

    streamSubscription = stream.listen(
      (event) {
        if (event.length == 1) {
          if (event[0] == bleTaskOk) {
            print("[BLE] BleTaskOk received. Closing internal stream");
            streamSubscription?.cancel();
            exposedStreamController.close();
          } else if (event[0] == bleTaskError) {
            print(
                "[BLE] BleTaskError received. Sending error and closing internal stream");
            exposedStreamController.addError(BleError.unknownBleTaskError());
            streamSubscription?.cancel();
            exposedStreamController.close();
          }
        } else {
          exposedStreamController.add(event);
        }
      },
      onDone: () {
        print("[BLE] BleTask done, closing exposed stream");
        exposedStreamController.close();
      },
    );

    return writeCharacteristic(triggerServiceUuid, triggerCharacteristicUuid,
            [cTriggerBleTaskValue], deviceId)
        .map(
      (_) => exposedStreamController.stream,
    );
  }

  TaskEither<BleError, Stream<String>> triggerAndStreamString(
    Uuid triggerServiceUuid,
    Uuid triggerCharacteristicUuid,
    Uuid? subServiceUuidArg,
    Uuid? subCharacteristicUuidArg,
    String deviceId,
  ) {
    return triggerAndStream(triggerServiceUuid, triggerCharacteristicUuid,
            subServiceUuidArg, subCharacteristicUuidArg, deviceId)
        .map(
      (stream) => stream.map(
        (event) => String.fromCharCodes(event),
      ),
    );
    //Uuid subServiceUuid = subServiceUuidArg ?? triggerServiceUuid;
    //Uuid subCharacteristicUuid =
    //    subCharacteristicUuidArg ?? triggerCharacteristicUuid;
    //final subCharacteristic = QualifiedCharacteristic(
    //  serviceId: subServiceUuid,
    //  characteristicId: subCharacteristicUuid,
    //  deviceId: deviceId,
    //);
    //final Stream<String> stream = _ble
    //    .subscribeToCharacteristic(subCharacteristic)
    //    .map((event) => String.fromCharCodes(event));
    //return _writeCharacteristic(triggerServiceUuid, triggerCharacteristicUuid,
    //        [_cTriggerBleTaskValue], deviceId)
    //    .map((_) => stream);

    //        return _writeCharacteristic(
    //            triggerServiceUuid,
    //            triggerCharacteristicUuid,
    //            [_cTriggerBleTaskValue]).map((_) => stream);
    //      },
    //      orElse: () =>
    //          TaskEither.left("[ERROR][BLE] Not connected to gateway"),
    //    );
  }

  //TaskEither<String, List<List<int>>> _triggerAndListenUntil(
  //  Uuid triggerServiceUuid,
  //  Uuid triggerCharacteristicUuid,
  //  Uuid? subServiceUuidArg,
  //  Uuid? subCharacteristicUuidArg,
  //  List<int> triggerValue,
  //  List<int> stopValue,
  //  int timeout,
  //) {
  //  Uuid subServiceUuid = subServiceUuidArg ?? triggerServiceUuid;
  //  Uuid subCharacteristicUuid =
  //      subCharacteristicUuidArg ?? triggerCharacteristicUuid;
  //  return _ref.read(bleConnectedNotifierProvider).maybeWhen(
  //        connected: (connectedGateway) {
  //          final List<List<int>> result = [];
  //          late final StreamSubscription<List<int>> charSubscription;
  //          final completer = Completer<List<List<int>>>();
  //          final scanTimer = Timer(Duration(seconds: timeout), () {
  //            charSubscription.cancel();
  //            completer
  //                .completeError("[ERROR][BLE] triggerAndListenUntil timeout");
  //          });
  //          final listenTask = TaskEither.tryCatch(() async {
  //            print("[BLE] Listen $subCharacteristicUuidArg until $stopValue");
  //            final characteristic = QualifiedCharacteristic(
  //                serviceId: subServiceUuid,
  //                characteristicId: subCharacteristicUuid,
  //                deviceId: connectedGateway.id);
  //            charSubscription = _ble
  //                .subscribeToCharacteristic(characteristic)
  //                .listen(
  //                  (value) {
  //                    if (scanTimer.isActive) {
  //                      scanTimer.cancel();
  //                    }
  //                    print(String.fromCharCodes(value));
  //                    if (value[0] == stopValue[0]) {
  //                      charSubscription.cancel();
  //                      completer.complete(result);
  //                    } else {
  //                      result.add(value);
  //                    }
  //                  },
  //                  cancelOnError: true,
  //                  onError: (error) {
  //                    if (scanTimer.isActive) {
  //                      scanTimer.cancel();
  //                    }
  //                    completer.completeError(error);
  //                  },
  //                  onDone: () =>
  //                      print("[BLE] TriggerAndlistenUntil subscription done"),
  //                );

  //            return await completer.future;
  //          }, (error, stackTrace) {
  //            if (scanTimer.isActive) {
  //              scanTimer.cancel();
  //            }
  //            return error.toString();
  //          });

  //          final triggerTask = _writeCharacteristic(
  //              triggerServiceUuid, triggerCharacteristicUuid, triggerValue);

  //          return TaskEither.sequenceList([
  //            listenTask,
  //            triggerTask.map((_) => [
  //                  [0]
  //                ])
  //          ]).map((r) => r[0]); // return result of listenTask (r[0])
  //        },
  //        orElse: () =>
  //            TaskEither.left("[ERROR][BLE] Not connected to gateway"),
  //      );
  //}

  TaskEither<BleError, List<int>> readCharacteristic(
    Uuid serviceUuid,
    Uuid characteristicUuid,
    String deviceId,
  ) {
    final characteristic = QualifiedCharacteristic(
        serviceId: serviceUuid,
        characteristicId: characteristicUuid,
        deviceId: deviceId);
    return TaskEither.tryCatch(
      () {
        final val = _ble.readCharacteristic(characteristic);
        print("[BLE] Read $characteristicUuid");
        return val;
      },
      (reactiveBleError, stackTrace) =>
          BleError.readCharacteristicError(reactiveBleError.toString()),
    );
  }

  TaskEither<BleError, Unit> writeCharacteristic(Uuid serviceUuid,
      Uuid characteristicUuid, List<int> value, String deviceId,
      {bool response = true}) {
    print("[BLE] Write $characteristicUuid -> $value");
    final characteristic = QualifiedCharacteristic(
        serviceId: serviceUuid,
        characteristicId: characteristicUuid,
        deviceId: deviceId);
    return TaskEither.tryCatch(
      () {
        response
            ? _ble.writeCharacteristicWithResponse(characteristic, value: value)
            : _ble.writeCharacteristicWithoutResponse(characteristic,
                value: value);
        return Future.value(unit);
      },
      (reactiveBleError, stackTrace) =>
          BleError.writeCharacteristicError(reactiveBleError.toString()),
    );
  }

  TaskEither<BleError, Stream<List<int>>> listenCharacteristic(
    Uuid serviceUuid,
    Uuid characteristicUuid,
    String deviceId,
  ) {
    print("[BLE] Listen $characteristicUuid");
    final characteristic = QualifiedCharacteristic(
        serviceId: serviceUuid,
        characteristicId: characteristicUuid,
        deviceId: deviceId);
    return TaskEither.right(_ble.subscribeToCharacteristic(characteristic));
  }
}

//@Riverpod(keepAlive: true)
//class BleConnectedNotifier extends _$BleConnectedNotifier {
//  @override
//  BleConStatus build() {
//    return const BleConStatus.disconnected();
//  }
//
//  void disconnected() {
//    print("[BLE] Disconnected");
//    state = const BleConStatus.disconnected();
//  }
//
//  void connecting(ConnectingGateway connectingGateway) {
//    print("[BLE] Connecting...");
//    state = BleConStatus.connecting(connectingGateway);
//  }
//
//  void connected(ConnectedGateway connectedGateway) {
//    print("[BLE] Connected");
//    state = BleConStatus.connected(connectedGateway);
//  }
//}
//
//class ConnectedGateway {
//  String id;
//  String mac;
//  int? battery;
//  String? ssid;
//  int? rssi;
//  int? wifiTest;
//  int? sdTest;
//  int? sdSpaceUsed;
//
//  ConnectedGateway({
//    required this.id,
//    required this.mac,
//    this.battery,
//    this.ssid,
//    this.rssi,
//    this.wifiTest,
//    this.sdTest,
//    this.sdSpaceUsed,
//  });
//}
//
//class ConnectingGateway {
//  String? id;
//  String mac;
//
//  ConnectingGateway({
//    required this.id,
//    required this.mac,
//  });
//}
//
//@freezed
//sealed class BleConStatus with _$BleConStatus {
//  const factory BleConStatus.connected(ConnectedGateway connectedGateway) =
//      Connected;
//  const factory BleConStatus.disconnected() = Disconnected;
//  const factory BleConStatus.connecting(ConnectingGateway connectingGateway) =
//      Connecting;
//}
//
@freezed
sealed class BleError with _$BleError implements Exception {
  // Server errors
  const factory BleError.noScanResult() = NoScanResult;
  const factory BleError.gatewayNotConnected() = GatewayNotConnected;
  const factory BleError.gatewayNotFound() = GatewayNotFound;
  const factory BleError.connectionFailed() = ConnectionFailed;
  const factory BleError.unknownBleTaskError() = UnknownBleTaskError;
  const factory BleError.readCharacteristicError(String reactiveBleError) =
      ReadCharacteristicError;
  const factory BleError.writeCharacteristicError(String reactiveBleError) =
      WriteCharacteristicError;
}

@freezed
class GatewayTestResultBle with _$GatewayTestResultBle {
  const GatewayTestResultBle._();
  const factory GatewayTestResultBle({
    required int wifiRes,
    required int wifiRssi,
    required int sdRes,
    required int sdSpaceUsed,
  }) = _GatewayTestResultBle;

  factory GatewayTestResultBle.fromString(String s) {
    List<String> wifiAndSdSplit = s.split(" ");
    List<String> wifiSplit = wifiAndSdSplit[0].split("#");
    List<String> sdSplit = wifiAndSdSplit[1].split("#");
    int wifiRes = int.parse(wifiSplit[0]);
    int wifiRssi = int.parse(wifiSplit[1]);
    int sdRes = int.parse(sdSplit[0]);
    int sdSpaceUsed = int.parse(sdSplit[1]);
    return GatewayTestResultBle(
      wifiRes: wifiRes,
      wifiRssi: wifiRssi,
      sdRes: sdRes,
      sdSpaceUsed: sdSpaceUsed,
    );
  }

  GatewayTestResult toGatewayTestResult() => GatewayTestResult(
        wifiTestResult: GatewayWifiTestResult.createFromValue(this.wifiRes),
        rssi: this.wifiRssi,
        sdTestResult: GatewaySdTestResult.createFromValue(this.sdRes),
        sdSpaceUsed: this.sdSpaceUsed,
      );
}
