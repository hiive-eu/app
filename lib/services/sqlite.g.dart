// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sqlite.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$sqliteServiceHash() => r'4aef9eeb29ab103fc55b8095fc787d9b7ed7530f';

/// See also [sqliteService].
@ProviderFor(sqliteService)
final sqliteServiceProvider = Provider<SqliteService>.internal(
  sqliteService,
  name: r'sqliteServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$sqliteServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef SqliteServiceRef = ProviderRef<SqliteService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
