// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ble.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$BleError {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BleErrorCopyWith<$Res> {
  factory $BleErrorCopyWith(BleError value, $Res Function(BleError) then) =
      _$BleErrorCopyWithImpl<$Res, BleError>;
}

/// @nodoc
class _$BleErrorCopyWithImpl<$Res, $Val extends BleError>
    implements $BleErrorCopyWith<$Res> {
  _$BleErrorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc
abstract class _$$NoScanResultImplCopyWith<$Res> {
  factory _$$NoScanResultImplCopyWith(
          _$NoScanResultImpl value, $Res Function(_$NoScanResultImpl) then) =
      __$$NoScanResultImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NoScanResultImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$NoScanResultImpl>
    implements _$$NoScanResultImplCopyWith<$Res> {
  __$$NoScanResultImplCopyWithImpl(
      _$NoScanResultImpl _value, $Res Function(_$NoScanResultImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$NoScanResultImpl implements NoScanResult {
  const _$NoScanResultImpl();

  @override
  String toString() {
    return 'BleError.noScanResult()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$NoScanResultImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return noScanResult();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return noScanResult?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (noScanResult != null) {
      return noScanResult();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return noScanResult(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return noScanResult?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (noScanResult != null) {
      return noScanResult(this);
    }
    return orElse();
  }
}

abstract class NoScanResult implements BleError {
  const factory NoScanResult() = _$NoScanResultImpl;
}

/// @nodoc
abstract class _$$GatewayNotConnectedImplCopyWith<$Res> {
  factory _$$GatewayNotConnectedImplCopyWith(_$GatewayNotConnectedImpl value,
          $Res Function(_$GatewayNotConnectedImpl) then) =
      __$$GatewayNotConnectedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GatewayNotConnectedImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$GatewayNotConnectedImpl>
    implements _$$GatewayNotConnectedImplCopyWith<$Res> {
  __$$GatewayNotConnectedImplCopyWithImpl(_$GatewayNotConnectedImpl _value,
      $Res Function(_$GatewayNotConnectedImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$GatewayNotConnectedImpl implements GatewayNotConnected {
  const _$GatewayNotConnectedImpl();

  @override
  String toString() {
    return 'BleError.gatewayNotConnected()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayNotConnectedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return gatewayNotConnected();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return gatewayNotConnected?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (gatewayNotConnected != null) {
      return gatewayNotConnected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return gatewayNotConnected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return gatewayNotConnected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (gatewayNotConnected != null) {
      return gatewayNotConnected(this);
    }
    return orElse();
  }
}

abstract class GatewayNotConnected implements BleError {
  const factory GatewayNotConnected() = _$GatewayNotConnectedImpl;
}

/// @nodoc
abstract class _$$GatewayNotFoundImplCopyWith<$Res> {
  factory _$$GatewayNotFoundImplCopyWith(_$GatewayNotFoundImpl value,
          $Res Function(_$GatewayNotFoundImpl) then) =
      __$$GatewayNotFoundImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GatewayNotFoundImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$GatewayNotFoundImpl>
    implements _$$GatewayNotFoundImplCopyWith<$Res> {
  __$$GatewayNotFoundImplCopyWithImpl(
      _$GatewayNotFoundImpl _value, $Res Function(_$GatewayNotFoundImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$GatewayNotFoundImpl implements GatewayNotFound {
  const _$GatewayNotFoundImpl();

  @override
  String toString() {
    return 'BleError.gatewayNotFound()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GatewayNotFoundImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return gatewayNotFound();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return gatewayNotFound?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (gatewayNotFound != null) {
      return gatewayNotFound();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return gatewayNotFound(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return gatewayNotFound?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (gatewayNotFound != null) {
      return gatewayNotFound(this);
    }
    return orElse();
  }
}

abstract class GatewayNotFound implements BleError {
  const factory GatewayNotFound() = _$GatewayNotFoundImpl;
}

/// @nodoc
abstract class _$$ConnectionFailedImplCopyWith<$Res> {
  factory _$$ConnectionFailedImplCopyWith(_$ConnectionFailedImpl value,
          $Res Function(_$ConnectionFailedImpl) then) =
      __$$ConnectionFailedImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ConnectionFailedImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$ConnectionFailedImpl>
    implements _$$ConnectionFailedImplCopyWith<$Res> {
  __$$ConnectionFailedImplCopyWithImpl(_$ConnectionFailedImpl _value,
      $Res Function(_$ConnectionFailedImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$ConnectionFailedImpl implements ConnectionFailed {
  const _$ConnectionFailedImpl();

  @override
  String toString() {
    return 'BleError.connectionFailed()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ConnectionFailedImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return connectionFailed();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return connectionFailed?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (connectionFailed != null) {
      return connectionFailed();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return connectionFailed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return connectionFailed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (connectionFailed != null) {
      return connectionFailed(this);
    }
    return orElse();
  }
}

abstract class ConnectionFailed implements BleError {
  const factory ConnectionFailed() = _$ConnectionFailedImpl;
}

/// @nodoc
abstract class _$$UnknownBleTaskErrorImplCopyWith<$Res> {
  factory _$$UnknownBleTaskErrorImplCopyWith(_$UnknownBleTaskErrorImpl value,
          $Res Function(_$UnknownBleTaskErrorImpl) then) =
      __$$UnknownBleTaskErrorImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$UnknownBleTaskErrorImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$UnknownBleTaskErrorImpl>
    implements _$$UnknownBleTaskErrorImplCopyWith<$Res> {
  __$$UnknownBleTaskErrorImplCopyWithImpl(_$UnknownBleTaskErrorImpl _value,
      $Res Function(_$UnknownBleTaskErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
}

/// @nodoc

class _$UnknownBleTaskErrorImpl implements UnknownBleTaskError {
  const _$UnknownBleTaskErrorImpl();

  @override
  String toString() {
    return 'BleError.unknownBleTaskError()';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnknownBleTaskErrorImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return unknownBleTaskError();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return unknownBleTaskError?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (unknownBleTaskError != null) {
      return unknownBleTaskError();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return unknownBleTaskError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return unknownBleTaskError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (unknownBleTaskError != null) {
      return unknownBleTaskError(this);
    }
    return orElse();
  }
}

abstract class UnknownBleTaskError implements BleError {
  const factory UnknownBleTaskError() = _$UnknownBleTaskErrorImpl;
}

/// @nodoc
abstract class _$$ReadCharacteristicErrorImplCopyWith<$Res> {
  factory _$$ReadCharacteristicErrorImplCopyWith(
          _$ReadCharacteristicErrorImpl value,
          $Res Function(_$ReadCharacteristicErrorImpl) then) =
      __$$ReadCharacteristicErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String reactiveBleError});
}

/// @nodoc
class __$$ReadCharacteristicErrorImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$ReadCharacteristicErrorImpl>
    implements _$$ReadCharacteristicErrorImplCopyWith<$Res> {
  __$$ReadCharacteristicErrorImplCopyWithImpl(
      _$ReadCharacteristicErrorImpl _value,
      $Res Function(_$ReadCharacteristicErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? reactiveBleError = null,
  }) {
    return _then(_$ReadCharacteristicErrorImpl(
      null == reactiveBleError
          ? _value.reactiveBleError
          : reactiveBleError // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ReadCharacteristicErrorImpl implements ReadCharacteristicError {
  const _$ReadCharacteristicErrorImpl(this.reactiveBleError);

  @override
  final String reactiveBleError;

  @override
  String toString() {
    return 'BleError.readCharacteristicError(reactiveBleError: $reactiveBleError)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReadCharacteristicErrorImpl &&
            (identical(other.reactiveBleError, reactiveBleError) ||
                other.reactiveBleError == reactiveBleError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, reactiveBleError);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$ReadCharacteristicErrorImplCopyWith<_$ReadCharacteristicErrorImpl>
      get copyWith => __$$ReadCharacteristicErrorImplCopyWithImpl<
          _$ReadCharacteristicErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return readCharacteristicError(reactiveBleError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return readCharacteristicError?.call(reactiveBleError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (readCharacteristicError != null) {
      return readCharacteristicError(reactiveBleError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return readCharacteristicError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return readCharacteristicError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (readCharacteristicError != null) {
      return readCharacteristicError(this);
    }
    return orElse();
  }
}

abstract class ReadCharacteristicError implements BleError {
  const factory ReadCharacteristicError(final String reactiveBleError) =
      _$ReadCharacteristicErrorImpl;

  String get reactiveBleError;

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$ReadCharacteristicErrorImplCopyWith<_$ReadCharacteristicErrorImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$WriteCharacteristicErrorImplCopyWith<$Res> {
  factory _$$WriteCharacteristicErrorImplCopyWith(
          _$WriteCharacteristicErrorImpl value,
          $Res Function(_$WriteCharacteristicErrorImpl) then) =
      __$$WriteCharacteristicErrorImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String reactiveBleError});
}

/// @nodoc
class __$$WriteCharacteristicErrorImplCopyWithImpl<$Res>
    extends _$BleErrorCopyWithImpl<$Res, _$WriteCharacteristicErrorImpl>
    implements _$$WriteCharacteristicErrorImplCopyWith<$Res> {
  __$$WriteCharacteristicErrorImplCopyWithImpl(
      _$WriteCharacteristicErrorImpl _value,
      $Res Function(_$WriteCharacteristicErrorImpl) _then)
      : super(_value, _then);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? reactiveBleError = null,
  }) {
    return _then(_$WriteCharacteristicErrorImpl(
      null == reactiveBleError
          ? _value.reactiveBleError
          : reactiveBleError // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$WriteCharacteristicErrorImpl implements WriteCharacteristicError {
  const _$WriteCharacteristicErrorImpl(this.reactiveBleError);

  @override
  final String reactiveBleError;

  @override
  String toString() {
    return 'BleError.writeCharacteristicError(reactiveBleError: $reactiveBleError)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WriteCharacteristicErrorImpl &&
            (identical(other.reactiveBleError, reactiveBleError) ||
                other.reactiveBleError == reactiveBleError));
  }

  @override
  int get hashCode => Object.hash(runtimeType, reactiveBleError);

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$WriteCharacteristicErrorImplCopyWith<_$WriteCharacteristicErrorImpl>
      get copyWith => __$$WriteCharacteristicErrorImplCopyWithImpl<
          _$WriteCharacteristicErrorImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() noScanResult,
    required TResult Function() gatewayNotConnected,
    required TResult Function() gatewayNotFound,
    required TResult Function() connectionFailed,
    required TResult Function() unknownBleTaskError,
    required TResult Function(String reactiveBleError) readCharacteristicError,
    required TResult Function(String reactiveBleError) writeCharacteristicError,
  }) {
    return writeCharacteristicError(reactiveBleError);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? noScanResult,
    TResult? Function()? gatewayNotConnected,
    TResult? Function()? gatewayNotFound,
    TResult? Function()? connectionFailed,
    TResult? Function()? unknownBleTaskError,
    TResult? Function(String reactiveBleError)? readCharacteristicError,
    TResult? Function(String reactiveBleError)? writeCharacteristicError,
  }) {
    return writeCharacteristicError?.call(reactiveBleError);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? noScanResult,
    TResult Function()? gatewayNotConnected,
    TResult Function()? gatewayNotFound,
    TResult Function()? connectionFailed,
    TResult Function()? unknownBleTaskError,
    TResult Function(String reactiveBleError)? readCharacteristicError,
    TResult Function(String reactiveBleError)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (writeCharacteristicError != null) {
      return writeCharacteristicError(reactiveBleError);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoScanResult value) noScanResult,
    required TResult Function(GatewayNotConnected value) gatewayNotConnected,
    required TResult Function(GatewayNotFound value) gatewayNotFound,
    required TResult Function(ConnectionFailed value) connectionFailed,
    required TResult Function(UnknownBleTaskError value) unknownBleTaskError,
    required TResult Function(ReadCharacteristicError value)
        readCharacteristicError,
    required TResult Function(WriteCharacteristicError value)
        writeCharacteristicError,
  }) {
    return writeCharacteristicError(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(NoScanResult value)? noScanResult,
    TResult? Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult? Function(GatewayNotFound value)? gatewayNotFound,
    TResult? Function(ConnectionFailed value)? connectionFailed,
    TResult? Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult? Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult? Function(WriteCharacteristicError value)? writeCharacteristicError,
  }) {
    return writeCharacteristicError?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoScanResult value)? noScanResult,
    TResult Function(GatewayNotConnected value)? gatewayNotConnected,
    TResult Function(GatewayNotFound value)? gatewayNotFound,
    TResult Function(ConnectionFailed value)? connectionFailed,
    TResult Function(UnknownBleTaskError value)? unknownBleTaskError,
    TResult Function(ReadCharacteristicError value)? readCharacteristicError,
    TResult Function(WriteCharacteristicError value)? writeCharacteristicError,
    required TResult orElse(),
  }) {
    if (writeCharacteristicError != null) {
      return writeCharacteristicError(this);
    }
    return orElse();
  }
}

abstract class WriteCharacteristicError implements BleError {
  const factory WriteCharacteristicError(final String reactiveBleError) =
      _$WriteCharacteristicErrorImpl;

  String get reactiveBleError;

  /// Create a copy of BleError
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$WriteCharacteristicErrorImplCopyWith<_$WriteCharacteristicErrorImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$GatewayTestResultBle {
  int get wifiRes => throw _privateConstructorUsedError;
  int get wifiRssi => throw _privateConstructorUsedError;
  int get sdRes => throw _privateConstructorUsedError;
  int get sdSpaceUsed => throw _privateConstructorUsedError;

  /// Create a copy of GatewayTestResultBle
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $GatewayTestResultBleCopyWith<GatewayTestResultBle> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GatewayTestResultBleCopyWith<$Res> {
  factory $GatewayTestResultBleCopyWith(GatewayTestResultBle value,
          $Res Function(GatewayTestResultBle) then) =
      _$GatewayTestResultBleCopyWithImpl<$Res, GatewayTestResultBle>;
  @useResult
  $Res call({int wifiRes, int wifiRssi, int sdRes, int sdSpaceUsed});
}

/// @nodoc
class _$GatewayTestResultBleCopyWithImpl<$Res,
        $Val extends GatewayTestResultBle>
    implements $GatewayTestResultBleCopyWith<$Res> {
  _$GatewayTestResultBleCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of GatewayTestResultBle
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? wifiRes = null,
    Object? wifiRssi = null,
    Object? sdRes = null,
    Object? sdSpaceUsed = null,
  }) {
    return _then(_value.copyWith(
      wifiRes: null == wifiRes
          ? _value.wifiRes
          : wifiRes // ignore: cast_nullable_to_non_nullable
              as int,
      wifiRssi: null == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int,
      sdRes: null == sdRes
          ? _value.sdRes
          : sdRes // ignore: cast_nullable_to_non_nullable
              as int,
      sdSpaceUsed: null == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GatewayTestResultBleImplCopyWith<$Res>
    implements $GatewayTestResultBleCopyWith<$Res> {
  factory _$$GatewayTestResultBleImplCopyWith(_$GatewayTestResultBleImpl value,
          $Res Function(_$GatewayTestResultBleImpl) then) =
      __$$GatewayTestResultBleImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int wifiRes, int wifiRssi, int sdRes, int sdSpaceUsed});
}

/// @nodoc
class __$$GatewayTestResultBleImplCopyWithImpl<$Res>
    extends _$GatewayTestResultBleCopyWithImpl<$Res, _$GatewayTestResultBleImpl>
    implements _$$GatewayTestResultBleImplCopyWith<$Res> {
  __$$GatewayTestResultBleImplCopyWithImpl(_$GatewayTestResultBleImpl _value,
      $Res Function(_$GatewayTestResultBleImpl) _then)
      : super(_value, _then);

  /// Create a copy of GatewayTestResultBle
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? wifiRes = null,
    Object? wifiRssi = null,
    Object? sdRes = null,
    Object? sdSpaceUsed = null,
  }) {
    return _then(_$GatewayTestResultBleImpl(
      wifiRes: null == wifiRes
          ? _value.wifiRes
          : wifiRes // ignore: cast_nullable_to_non_nullable
              as int,
      wifiRssi: null == wifiRssi
          ? _value.wifiRssi
          : wifiRssi // ignore: cast_nullable_to_non_nullable
              as int,
      sdRes: null == sdRes
          ? _value.sdRes
          : sdRes // ignore: cast_nullable_to_non_nullable
              as int,
      sdSpaceUsed: null == sdSpaceUsed
          ? _value.sdSpaceUsed
          : sdSpaceUsed // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$GatewayTestResultBleImpl extends _GatewayTestResultBle {
  const _$GatewayTestResultBleImpl(
      {required this.wifiRes,
      required this.wifiRssi,
      required this.sdRes,
      required this.sdSpaceUsed})
      : super._();

  @override
  final int wifiRes;
  @override
  final int wifiRssi;
  @override
  final int sdRes;
  @override
  final int sdSpaceUsed;

  @override
  String toString() {
    return 'GatewayTestResultBle(wifiRes: $wifiRes, wifiRssi: $wifiRssi, sdRes: $sdRes, sdSpaceUsed: $sdSpaceUsed)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GatewayTestResultBleImpl &&
            (identical(other.wifiRes, wifiRes) || other.wifiRes == wifiRes) &&
            (identical(other.wifiRssi, wifiRssi) ||
                other.wifiRssi == wifiRssi) &&
            (identical(other.sdRes, sdRes) || other.sdRes == sdRes) &&
            (identical(other.sdSpaceUsed, sdSpaceUsed) ||
                other.sdSpaceUsed == sdSpaceUsed));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, wifiRes, wifiRssi, sdRes, sdSpaceUsed);

  /// Create a copy of GatewayTestResultBle
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$GatewayTestResultBleImplCopyWith<_$GatewayTestResultBleImpl>
      get copyWith =>
          __$$GatewayTestResultBleImplCopyWithImpl<_$GatewayTestResultBleImpl>(
              this, _$identity);
}

abstract class _GatewayTestResultBle extends GatewayTestResultBle {
  const factory _GatewayTestResultBle(
      {required final int wifiRes,
      required final int wifiRssi,
      required final int sdRes,
      required final int sdSpaceUsed}) = _$GatewayTestResultBleImpl;
  const _GatewayTestResultBle._() : super._();

  @override
  int get wifiRes;
  @override
  int get wifiRssi;
  @override
  int get sdRes;
  @override
  int get sdSpaceUsed;

  /// Create a copy of GatewayTestResultBle
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$GatewayTestResultBleImplCopyWith<_$GatewayTestResultBleImpl>
      get copyWith => throw _privateConstructorUsedError;
}
