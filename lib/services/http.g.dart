// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'http.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$OkImpl _$$OkImplFromJson(Map<String, dynamic> json) => _$OkImpl(
      json['data'] as Map<String, dynamic>?,
      $type: json['status'] as String?,
    );

Map<String, dynamic> _$$OkImplToJson(_$OkImpl instance) => <String, dynamic>{
      'data': instance.data,
      'status': instance.$type,
    };

_$ErrorImpl _$$ErrorImplFromJson(Map<String, dynamic> json) => _$ErrorImpl(
      ApiError.fromJson(json['error'] as Map<String, dynamic>),
      $type: json['status'] as String?,
    );

Map<String, dynamic> _$$ErrorImplToJson(_$ErrorImpl instance) =>
    <String, dynamic>{
      'error': instance.error,
      'status': instance.$type,
    };

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$httpServiceHash() => r'36a1be78367b06874418dcf96bde087a71fe8390';

/// See also [httpService].
@ProviderFor(httpService)
final httpServiceProvider = AutoDisposeProvider<HttpService>.internal(
  httpService,
  name: r'httpServiceProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$httpServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef HttpServiceRef = AutoDisposeProviderRef<HttpService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
