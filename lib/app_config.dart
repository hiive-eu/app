import "dart:convert";
import "package:flutter/services.dart";

import "package:flutter_riverpod/flutter_riverpod.dart";
import "package:hiive/app.dart";

final appConfigProvider = Provider((ref) => AppConfig());

class AppConfig {
  late final AppFlavor flavor;
  late final String domain;
  late final String readingsDomain;
  late final String staticDomain;
  late final int port;
  String? httpAuth;
  String? proxyAddr;

  AppConfig() {
    this.flavor = appFlavor == "production"
        ? AppFlavor.production
        : AppFlavor.development;
    switch (this.flavor) {
      case AppFlavor.production:
        this.domain = "api.hiive.buzz";
        this.readingsDomain = "readings.hiive.buzz";
        this.staticDomain = "releases.hiive.buzz";
        this.port = 443;
        break;
      case AppFlavor.development:
        this.domain = "dev.api.hiive.buzz";
        this.readingsDomain = "dev.readings.hiive.buzz";
        this.staticDomain = "dev.releases.hiive.buzz";
        this.port = 443;
        const httpAuthUser = String.fromEnvironment('HIIVE_HTTP_AUTH_USER');
        const httpAuthPass = String.fromEnvironment('HIIVE_HTTP_AUTH_PASS');
        const proxyIP = String.fromEnvironment('PROXY_IP');
        const proxyPort = String.fromEnvironment('PROXY_PORT');
        if (proxyIP.isNotEmpty && proxyPort.isNotEmpty) {
          this.proxyAddr = "$proxyIP:$proxyPort";
        }
        this.httpAuth =
            base64Encode(utf8.encode("$httpAuthUser:$httpAuthPass"));
        break;
      default:
    }
  }
}
