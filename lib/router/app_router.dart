import "package:go_router/go_router.dart";

import "package:hiive/features/authentication/screens/auth_screen.dart";
import "package:hiive/features/authentication/screens/password_reset_screen.dart";
import "package:hiive/features/authentication/screens/signup_confirm_screen.dart";
import "package:hiive/features/dashboard/screens/dashboard_screen.dart";
import "package:hiive/features/dev/dev_screen.dart";
import "package:hiive/features/dev/text_styles_screen.dart";
import "package:hiive/features/dev/widget_styles_screen.dart";
import "package:hiive/features/dev/colors_screen.dart";
import "package:hiive/features/gateway/screens/gateway_screen.dart";
import "package:hiive/features/gateway_setup/gateway_connect_screen.dart";
import "package:hiive/features/gateway_setup/gateway_name_screen.dart";
import "package:hiive/features/gateway_setup/gateway_mode_screen.dart";
import "package:hiive/features/gateway_setup/gateway_wifi_screen.dart";
import "package:hiive/features/gateway_setup/gateway_pair_screen.dart";
import "package:hiive/features/hive/screens/hive_screen.dart";
import "package:hiive/features/onboard/screens/onboard_gateway_screen.dart";
import "package:hiive/features/onboard/screens/onboard_hive_screen.dart";
import "package:hiive/features/onboard/screens/onboard_welcome_screen.dart";
import "package:hiive/features/user/screens/user_screen.dart";
import "package:hiive/utils/router_observer.dart";

GoRouter genRouter(ScreenRoute initialLocation) {
  return GoRouter(
    debugLogDiagnostics: true,
    initialLocation: initialLocation.path,
    routes: routes,
    observers: [RouterObserver()],
  );
}

final routes = [
  GoRoute(
    path: ScreenRoute.onboardWelcome.path,
    name: ScreenRoute.onboardWelcome.name,
    builder: (context, state) => const OnboardWelcomeScreen(),
  ),
  GoRoute(
    path: ScreenRoute.onboardHive.path,
    name: ScreenRoute.onboardHive.name,
    builder: (context, state) => OnboardHiveScreen(
      isHiiveParam: state.uri.queryParameters["isHiive"]!,
    ),
  ),
  GoRoute(
    path: ScreenRoute.onboardGateway.path,
    name: ScreenRoute.onboardGateway.name,
    builder: (context, state) => OnboardGatewayScreen(),
  ),
  GoRoute(
      path: ScreenRoute.auth.path,
      name: ScreenRoute.auth.name,
      builder: (context, state) => AuthScreen(
            canPop: state.uri.queryParameters["canPop"],
            nextScreen: state.uri.queryParameters["nextScreen"],
          ),
      routes: [
        GoRoute(
          path: ScreenRoute.signupConfirm.path,
          name: ScreenRoute.signupConfirm.name,
          builder: (context, state) =>
              SignupConfirmScreen(token: state.pathParameters["token"]!),
        ),
        GoRoute(
          path: ScreenRoute.passwordReset.path,
          name: ScreenRoute.passwordReset.name,
          builder: (context, state) =>
              PasswordResetScreen(token: state.pathParameters["token"]!),
        ),
      ]),
  GoRoute(
    path: ScreenRoute.dashboard.path,
    name: ScreenRoute.dashboard.name,
    builder: (context, state) => const DashboardScreen(),
    routes: [
      GoRoute(
          path: ScreenRoute.gateway.path,
          name: ScreenRoute.gateway.name,
          builder: (context, state) =>
              GatewayScreen(mac: state.pathParameters["mac"]!),
          routes: [
            GoRoute(
              path: ScreenRoute.gatewayEdit.path,
              name: ScreenRoute.gatewayEdit.name,
              builder: (context, state) =>
                  //GatewayEditScreen(mac: state.pathParameters["mac"]!),
                  DashboardScreen(),
              routes: [
                GoRoute(
                  path: ScreenRoute.gatewayPair.path,
                  name: ScreenRoute.gatewayPair.name,
                  builder: (context, state) =>
                      //GatewayPairScreen(mac: state.pathParameters["mac"]!),
                      DashboardScreen(),
                ),
              ],
            ),
          ]),
      GoRoute(
        path: ScreenRoute.dev.path,
        name: ScreenRoute.dev.name,
        builder: (context, state) => const DevScreen(),
        routes: [
          GoRoute(
            path: ScreenRoute.textStyles.path,
            name: ScreenRoute.textStyles.name,
            builder: (context, state) => const TextStylesScreen(),
          ),
          GoRoute(
            path: ScreenRoute.widgetStyles.path,
            name: ScreenRoute.widgetStyles.name,
            builder: (context, state) => const WidgetStylesScreen(),
          ),
          GoRoute(
            path: ScreenRoute.colors.path,
            name: ScreenRoute.colors.name,
            builder: (context, state) => const ColorsScreen(),
          ),
        ],
      ),
      GoRoute(
        path: ScreenRoute.setupGatewayConnect.path,
        name: ScreenRoute.setupGatewayConnect.name,
        builder: (context, state) => const SetupGatewayConnectScreen(),
      ),
      GoRoute(
        path: ScreenRoute.setupGatewayName.path,
        name: ScreenRoute.setupGatewayName.name,
        builder: (context, state) => const SetupGatewayNameScreen(),
      ),
      GoRoute(
        path: ScreenRoute.setupGatewayMode.path,
        name: ScreenRoute.setupGatewayMode.name,
        builder: (context, state) => SetupGatewayModeScreen(
          name: state.uri.queryParameters["name"]!,
        ),
      ),
      GoRoute(
        path: ScreenRoute.setupGatewayWifi.path,
        name: ScreenRoute.setupGatewayWifi.name,
        builder: (context, state) => SetupGatewayWifiScreen(
          shouldPop: state.uri.queryParameters["shouldPop"],
        ),
      ),
      GoRoute(
        path: ScreenRoute.setupGatewayPair.path,
        name: ScreenRoute.setupGatewayPair.name,
        builder: (context, state) => SetupGatewayPairScreen(
          shouldPop: state.uri.queryParameters["shouldPop"],
        ),
      ),
      GoRoute(
        path: ScreenRoute.hive.path,
        name: ScreenRoute.hive.name,
        builder: (context, state) => HiveScreen(
          id: state.pathParameters["id"]!,
        ),
      ),
      GoRoute(
        path: ScreenRoute.user.path,
        name: ScreenRoute.user.name,
        builder: (context, state) => const UserScreen(),
      ),
    ],
  ),
];

enum ScreenRoute {
  // ONBOARDING
  onboardWelcome(
    "/onboard-welcome",
    "/onboard-weclone",
    "onboard-welcome",
  ),
  onboardHive(
    "/onboard-hive",
    "/onboard-hive",
    "onboard-hive",
  ),
  onboardGateway(
    "/onboard-gateway",
    "/onboard-gateway",
    "onboard-gateway",
  ),

  // AUTHENTICATION
  auth("/auth", "/auth", "auth"),
  passwordReset("reset/:token", "/auth/reset/:token", "password-reset"),
  signupConfirm("confirm/:token", "/auth/confirm/:token", "signup-confirm"),

  // DASHBOARD
  dashboard("/", "/", "dashoard"),

  // GATEWAY
  gateway("gateway/:mac", "/gateway/:mac", "gateway"),
  gatewayEdit("edit", "/gateway/:mac/edit", "gateway-edit"),
  gatewayPair("pair", "/gateway/:mac/edit/pair", "gateway-pair"),

  gatewayScan("gateway-scan", "/gateway-scan", "gateway-scan"),
  setupGatewayConnect(
    "setup-gateway-connect",
    "/setup-gateway-connect",
    "setup-gateway-connect",
  ),
  setupGatewayName(
    "setup-gateway-name",
    "/setup-gateway-name",
    "setup-gateway-name",
  ),
  setupGatewayMode(
    "setup-gateway-mode",
    "/setup-gateway-mode",
    "setup-gateway-mode",
  ),
  setupGatewayWifi(
    "setup-gateway-wifi",
    "/setup-gateway-wifi",
    "setup-gateway-wifi",
  ),

  setupGatewayPair(
    "setup-gateway-pair",
    "/setup-gateway-pair",
    "setup-gateway-pair",
  ),

  // Hive
  hive("hive/:id", "/hive/:id", "hive"),

  // USER
  user("user", "/user", "user"),

  // DEV
  dev("dev", "/dev", "dev"),
  textStyles("text-styles", "/text-styles", "text-styles"),
  widgetStyles("widget-styles", "/widget-styles", "widget-styles"),
  colors("colors", "/colors", "colors"),
  ;

  final String path, fullPath, name;

  const ScreenRoute(this.path, this.fullPath, this.name);
}
