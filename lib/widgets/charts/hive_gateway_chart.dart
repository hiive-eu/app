import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HiveGatewayChart extends StatelessWidget {
  final List<MapEntry<DateTime, int>> hiveReadingsArg;
  final List<MapEntry<DateTime, int>> gatewayReadingsArg;
  final Color hiveColor;
  final Color gatewayColor;
  final String title;
  final String unit;
  final double height;
  final double width;

  const HiveGatewayChart({
    super.key,
    required this.hiveReadingsArg,
    required this.gatewayReadingsArg,
    required this.hiveColor,
    required this.gatewayColor,
    required this.title,
    required this.unit,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    hiveReadingsArg.sort((a, b) => a.key.compareTo(b.key));
    gatewayReadingsArg.sort((a, b) => a.key.compareTo(b.key));
    final hiveReadings = hiveReadingsArg;
    final gatewayReadings = gatewayReadingsArg;
    final int minYSpan = 10;
    final Set<DateTime> allDates = {
      ...hiveReadings.map((e) => e.key),
      ...gatewayReadings.map((e) => e.key),
    };
    final Set<int> allValues = {
      ...hiveReadings.map((e) => e.value),
      ...gatewayReadings.map((e) => e.value),
    };
    final List<DateTime> allDatesSorted = allDates.toList();
    allDatesSorted.sort();

    DateTime earliestDate = allDates.reduce((a, b) => a.isBefore(b) ? a : b);

    int shortestIntervalMinutes = 666666;
    for (int i = 1; i < allDatesSorted.length; i++) {
      Duration interval = allDatesSorted[i].difference(allDatesSorted[i - 1]);
      if (interval.inMinutes < shortestIntervalMinutes) {
        shortestIntervalMinutes = (interval.inSeconds / 60).round();
      }
    }

    final lowestValue = allValues.reduce((a, b) => a < b ? a : b);
    final highestValue = allValues.reduce((a, b) => a > b ? a : b);
    final int minY = ((lowestValue - highestValue).abs() < minYSpan)
        ? lowestValue -
            ((minYSpan - (lowestValue - highestValue).abs()) / 2).round()
        : lowestValue;
    final int maxY = ((lowestValue - highestValue).abs() < minYSpan)
        ? highestValue +
            ((minYSpan - (lowestValue - highestValue).abs()) / 2).round()
        : highestValue;

    final hiveSpots = hiveReadings
        .map(
          (e) => FlSpot(
            (e.key.difference(earliestDate).inMinutes / shortestIntervalMinutes)
                .round()
                .toDouble(),
            e.value.toDouble(),
          ),
        )
        .toList();

    final gatewaySpots = gatewayReadings
        .map(
          (e) => FlSpot(
            (e.key.difference(earliestDate).inMinutes / shortestIntervalMinutes)
                .round()
                .toDouble(),
            e.value.toDouble(),
          ),
        )
        .toList();

    final maxX = hiveSpots.reduce((a, b) => a.x > b.x ? a : b).x;
    double intervalX = maxX <= 5 * 2 ? 2 : (maxX / 5).floor().toDouble() - 1;
    double intervalY = (highestValue - lowestValue) <= 3 * 2
        ? 2
        : ((maxY) / 3).floor().toDouble() - 1;

    return SizedBox(
      width: this.width,
      height: this.height,
      child: LineChart(
        LineChartData(
          minY: minY.toDouble(),
          maxY: maxY.toDouble(),
          lineBarsData: [
            LineChartBarData(
              spots: hiveSpots,
              color: this.hiveColor,
              dotData: FlDotData(
                show: true,
                getDotPainter: (a, b, c, d) {
                  return FlDotCirclePainter(
                    radius: 1,
                    color: this.gatewayColor,
                  );
                },
                checkToShowDot: (spot, barData) {
                  return hiveSpots.map((e) => e.x).contains(spot.x);
                },
              ),
            ),
            LineChartBarData(
              spots: gatewaySpots,
              color: this.gatewayColor,
              dotData: FlDotData(
                show: true,
                getDotPainter: (a, b, c, d) {
                  return FlDotCirclePainter(
                    radius: 1,
                    color: this.hiveColor,
                  );
                },
                checkToShowDot: (spot, barData) {
                  return hiveSpots.map((e) => e.x).contains(spot.x);
                },
              ),
            )
          ],
          titlesData: FlTitlesData(
            topTitles: AxisTitles(
              axisNameSize: 32,
              axisNameWidget: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 34),
                    child: RichText(
                      text: TextSpan(
                        text: this.title,
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(color: Colors.black),
                        children: [
                          TextSpan(
                            text: " (${this.unit})",
                            style: Theme.of(context)
                                .textTheme
                                .bodySmall!
                                .copyWith(color: Colors.black),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 34.0),
                    child: RichText(
                      text: TextSpan(
                        text: "Hive",
                        style: TextStyle(
                            color: this.hiveColor, fontWeight: FontWeight.w600),
                        children: [
                          TextSpan(
                              text: " / ",
                              style: TextStyle(color: Colors.black)),
                          TextSpan(
                            text: "Gateway",
                            style: TextStyle(
                                color: this.gatewayColor,
                                fontWeight: FontWeight.w600),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              sideTitles: SideTitles(showTitles: false),
            ),
            rightTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                getTitlesWidget: (v, m) {
                  return SideTitleWidget(
                    meta: m,
                    child: Text(
                      "",
                    ),
                  );
                },
              ),
            ),
            leftTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                maxIncluded: false,
                interval: intervalY,
                getTitlesWidget: (val, meta) {
                  return SideTitleWidget(
                    meta: meta,
                    child: Text(
                      meta.formattedValue,
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(fontSize: 8),
                    ),
                  );
                },
              ),
            ),
            bottomTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                maxIncluded: false,
                reservedSize: 35,
                interval: intervalX,
                getTitlesWidget: (val, meta) {
                  DateTime date = earliestDate.add(Duration(
                      minutes: (shortestIntervalMinutes * val).toInt()));
                  return SideTitleWidget(
                    meta: meta,
                    child: Column(
                      children: [
                        Text(
                          DateFormat("HH:mm").format(date),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(fontSize: 8),
                        ),
                        Text(
                          DateFormat("d MMM").format(date),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(fontSize: 6),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ), //
          ),
        ),
      ),
    );
  }
}
