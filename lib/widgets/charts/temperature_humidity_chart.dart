import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/utils/misc.dart';

class TemperatureHumidityChart extends StatelessWidget {
  final List<MapEntry<DateTime, int>> temperatureReadings;
  final List<MapEntry<DateTime, int>> humidityReadings;
  final double height;
  final double width;

  const TemperatureHumidityChart({
    super.key,
    required this.temperatureReadings,
    required this.humidityReadings,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    final Set<DateTime> allDates = {
      ...temperatureReadings.map((e) => e.key),
      ...humidityReadings.map((e) => e.key),
    };
    final List<DateTime> allDatesSorted = allDates.toList();
    allDatesSorted.sort();

    DateTime earliestDate = allDates.reduce((a, b) => a.isBefore(b) ? a : b);

    int shortestIntervalMinutes = 666;
    for (int i = 1; i < allDatesSorted.length; i++) {
      Duration interval = allDatesSorted[i].difference(allDatesSorted[i - 1]);
      if (interval.inMinutes < shortestIntervalMinutes) {
        shortestIntervalMinutes = (interval.inSeconds / 60).round();
      }
    }

    final lowestTemperature =
        temperatureReadings.reduce((a, b) => a.value < b.value ? a : b).value;
    final highestTemperature =
        temperatureReadings.reduce((a, b) => a.value > b.value ? a : b).value;
    final int minY = ((lowestTemperature - highestTemperature).abs() < 5)
        ? lowestTemperature -
            ((5 - (lowestTemperature - highestTemperature).abs()) / 2).round()
        : lowestTemperature;
    final int maxY = ((lowestTemperature - highestTemperature).abs() < 5)
        ? highestTemperature +
            ((5 - (lowestTemperature - highestTemperature).abs()) / 2).round()
        : highestTemperature;

    final temperatureSpots = temperatureReadings
        .map((e) => FlSpot(
            (((e.key.difference(earliestDate).inSeconds /
                        (shortestIntervalMinutes * 60))) /
                    60)
                .round()
                .toDouble(),
            e.value.toDouble()))
        .toList();

    final humiditySpots = humidityReadings
        .map((e) => FlSpot(
            (e.key.difference(earliestDate).inMinutes / shortestIntervalMinutes)
                .round()
                .toDouble(),
            mapRange(e.value.toDouble(), 0, 100, minY.toDouble(),
                    maxY.toDouble())!
                .toDouble()))
        .toList();

    print(humiditySpots);

    return SizedBox(
      width: this.width,
      height: this.height,
      child: LineChart(
        LineChartData(
          minY: minY.toDouble(),
          maxY: maxY.toDouble(),
          lineBarsData: [
            LineChartBarData(
              spots: temperatureSpots,
              color: Colors.red,
              dotData: FlDotData(show: false),
            ),
            LineChartBarData(
              spots: humiditySpots,
              color: Colors.blue,
              dotData: FlDotData(show: false),
            )
          ],
          titlesData: FlTitlesData(
            topTitles: AxisTitles(
              sideTitles: SideTitles(showTitles: false),
            ),
            leftTitles: AxisTitles(
              axisNameSize: 24,
              axisNameWidget: Text("Temperature (°C)",
                  style: Theme.of(context).textTheme.titleMedium),
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                getTitlesWidget: (v, m) {
                  return SideTitleWidget(
                    meta: m,
                    child: Text(
                      m.formattedValue,
                      style: Theme.of(context).textTheme.titleSmall!.copyWith(
                            color: Colors.red,
                            fontWeight: FontWeight.w600,
                          ),
                    ),
                  );
                },
              ),
            ),
            rightTitles: AxisTitles(
              axisNameSize: 24,
              axisNameWidget: Text("Humidity (%)",
                  style: Theme.of(context).textTheme.titleMedium),
              sideTitles: SideTitles(
                  showTitles: true,
                  reservedSize: 35,
                  getTitlesWidget: (v, m) {
                    return SideTitleWidget(
                      meta: m,
                      child: Text(
                        mapRange(v, minY.toDouble(), maxY.toDouble(), 0, 100)!
                            .toInt()
                            .toString(),
                        style: Theme.of(context).textTheme.titleSmall!.copyWith(
                              color: Colors.blue,
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                    );
                  }),
            ),
          ),
        ),
      ),
    );
  }
}
