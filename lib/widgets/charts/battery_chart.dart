import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:hiive/constants/app_theme.dart';
import 'package:intl/intl.dart';

class BatteryChart extends StatelessWidget {
  final List<MapEntry<DateTime, int>> batteryLevelsArg;
  final double height;
  final double width;

  const BatteryChart({
    super.key,
    required this.batteryLevelsArg,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    batteryLevelsArg.sort((a, b) => a.key.compareTo(b.key));
    final batteryLevels = batteryLevelsArg; //.take(10).toList();
    final Set<DateTime> allDates = {
      ...batteryLevels.map((e) => e.key),
    };
    final List<DateTime> allDatesSorted = allDates.toList();
    allDatesSorted.sort();

    DateTime earliestDate = allDates.reduce((a, b) => a.isBefore(b) ? a : b);

    int shortestIntervalMinutes = 666666;
    for (int i = 1; i < allDatesSorted.length; i++) {
      Duration interval = allDatesSorted[i].difference(allDatesSorted[i - 1]);
      if (interval.inMinutes < shortestIntervalMinutes) {
        shortestIntervalMinutes = (interval.inSeconds / 60).round();
      }
    }

    final batteryLevelSpots = batteryLevels
        .map(
          (e) => FlSpot(
            (e.key.difference(earliestDate).inMinutes / shortestIntervalMinutes)
                .round()
                .toDouble(),
            e.value.toDouble(),
          ),
        )
        .toList();

    final maxX = batteryLevelSpots.reduce((a, b) => a.x > b.x ? a : b).x;
    double intervalX = maxX <= 5 * 2 ? 2 : (maxX / 5).floor().toDouble() - 1;

    List<FlSpot> chargeIntervalSpots = [];
    bool rising = true;
    for (int i = 0; i < batteryLevelSpots.length - 1; i++) {
      // First spot
      if (i == 0) {
        final firstSpot = batteryLevelSpots[i];
        final nextSpot = batteryLevelSpots[i + 1];

        if (firstSpot.y <= nextSpot.y) {
          // Charged
          chargeIntervalSpots.add(FlSpot(firstSpot.x, 0));
          chargeIntervalSpots.add(FlSpot(nextSpot.x, 0));
        } else {
          chargeIntervalSpots.add(FlSpot(firstSpot.x, firstSpot.y));
          chargeIntervalSpots.add(FlSpot(nextSpot.x, nextSpot.y));
        }
        // Last spot
      } else {
        final currentSpot = batteryLevelSpots[i];
        final nextSpot = batteryLevelSpots[i + 1];

        if (currentSpot.y < nextSpot.y) {
          chargeIntervalSpots.add(FlSpot(nextSpot.x, 0));
          rising = true;
        } else if (currentSpot.y > nextSpot.y) {
          chargeIntervalSpots.add(FlSpot(nextSpot.x, nextSpot.y));
          rising = false;
        } else {
          if (rising) {
            chargeIntervalSpots.add(FlSpot(nextSpot.x, 0));
          } else {
            chargeIntervalSpots.add(FlSpot(nextSpot.x, nextSpot.y));
          }
        }
      }
    }

    return SizedBox(
      width: this.width,
      height: this.height,
      child: LineChart(
        LineChartData(
          betweenBarsData: [
            BetweenBarsData(
              fromIndex: 0,
              toIndex: 1,
              color: colorNewHighlightGreen.withAlpha(150),
            ),
          ],
          minY: 0,
          maxY: 100,
          lineBarsData: [
            LineChartBarData(
              isStepLineChart: true,
              spots: batteryLevelSpots,
              color: colorNewHighlightBrown,
              dotData: FlDotData(
                show: false,
              ),
            ),
            LineChartBarData(
              show: false,
              isStepLineChart: true,
              spots: chargeIntervalSpots,
              dotData: FlDotData(
                show: false,
              ),
            ),
          ],
          titlesData: FlTitlesData(
            topTitles: AxisTitles(
              axisNameSize: 32,
              axisNameWidget: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 34),
                    child: RichText(
                      text: TextSpan(
                        text: "Battery",
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(color: Colors.black),
                        children: [
                          TextSpan(
                            text: " (%)",
                            style: Theme.of(context)
                                .textTheme
                                .bodySmall!
                                .copyWith(color: Colors.black),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              sideTitles: SideTitles(showTitles: false),
            ),
            rightTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                getTitlesWidget: (v, m) {
                  return SideTitleWidget(
                    meta: m,
                    child: Text(
                      "",
                    ),
                  );
                },
              ),
            ),
            leftTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                getTitlesWidget: (val, meta) {
                  return SideTitleWidget(
                    meta: meta,
                    child: Text(
                      meta.formattedValue,
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(fontSize: 8),
                    ),
                  );
                },
              ),
            ),
            bottomTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                maxIncluded: false,
                reservedSize: 35,
                interval: intervalX,
                getTitlesWidget: (val, meta) {
                  DateTime date = earliestDate.add(Duration(
                      minutes: (shortestIntervalMinutes * val).toInt()));
                  return SideTitleWidget(
                    meta: meta,
                    child: Column(
                      children: [
                        Text(
                          DateFormat("HH:mm").format(date),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(fontSize: 8),
                        ),
                        Text(
                          DateFormat("d MMM").format(date),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(fontSize: 6),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ), //
          ),
        ),
      ),
    );
  }
}
