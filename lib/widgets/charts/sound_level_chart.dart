import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:hiive/constants/app_theme.dart';
import 'package:intl/intl.dart';

class SoundLevelChart extends StatelessWidget {
  final List<MapEntry<DateTime, int>> soundLevelsArg;
  final double height;
  final double width;

  const SoundLevelChart({
    super.key,
    required this.soundLevelsArg,
    required this.height,
    required this.width,
  });

  @override
  Widget build(BuildContext context) {
    soundLevelsArg.sort((a, b) => a.key.compareTo(b.key));
    final soundLevels = soundLevelsArg; //.take(10).toList();
    for (int i = 0; i < soundLevels.length; i++) {
      print(soundLevels[i].key);
    }
    final Set<DateTime> allDates = {
      ...soundLevels.map((e) => e.key),
    };
    final Set<int> allValues = {
      ...soundLevels.map((e) => e.value),
    };
    final List<DateTime> allDatesSorted = allDates.toList();
    allDatesSorted.sort();

    DateTime earliestDate = allDates.reduce((a, b) => a.isBefore(b) ? a : b);

    int shortestIntervalMinutes = 666666;
    for (int i = 1; i < allDatesSorted.length; i++) {
      Duration interval = allDatesSorted[i].difference(allDatesSorted[i - 1]);
      if (interval.inMinutes < shortestIntervalMinutes) {
        shortestIntervalMinutes = (interval.inSeconds / 60).round();
      }
    }

    final highestValue = allValues.reduce((a, b) => a > b ? a : b);
    final soundLevelSpots = soundLevels
        .map(
          (e) => FlSpot(
            (e.key.difference(earliestDate).inMinutes / shortestIntervalMinutes)
                .round()
                .toDouble(),
            e.value.toDouble(),
          ),
        )
        .toList();

    final maxY = highestValue + 5;
    final maxX = soundLevelSpots.reduce((a, b) => a.x > b.x ? a : b).x;
    double intervalX = maxX <= 5 * 2 ? 2 : (maxX / 5).floor().toDouble() - 1;
    double intervalY = maxY <= 3 * 2 ? 2 : ((maxY) / 3).floor().toDouble() - 1;

    return SizedBox(
      width: this.width,
      height: this.height,
      child: LineChart(
        LineChartData(
          minY: 0,
          maxY: maxY.toDouble(),
          lineBarsData: [
            LineChartBarData(
              isStepLineChart: true,
              belowBarData: BarAreaData(
                show: true,
                color: colorNewHighlightOrange.withAlpha(50),
              ),
              spots: soundLevelSpots,
              color: colorNewHighlightBrown,
              dotData: FlDotData(
                show: false,
              ),
            ),
          ],
          titlesData: FlTitlesData(
            topTitles: AxisTitles(
              axisNameSize: 32,
              axisNameWidget: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 34),
                    child: RichText(
                      text: TextSpan(
                        text: "Activity",
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(color: Colors.black),
                        children: [
                          TextSpan(
                            text: " (dB SPL)",
                            style: Theme.of(context)
                                .textTheme
                                .bodySmall!
                                .copyWith(color: Colors.black),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              sideTitles: SideTitles(showTitles: false),
            ),
            rightTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                getTitlesWidget: (v, m) {
                  return SideTitleWidget(
                    meta: m,
                    child: Text(
                      "",
                    ),
                  );
                },
              ),
            ),
            leftTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                reservedSize: 35,
                maxIncluded: false,
                interval: intervalY,
                getTitlesWidget: (val, meta) {
                  return SideTitleWidget(
                    meta: meta,
                    child: Text(
                      meta.formattedValue,
                      style: Theme.of(context)
                          .textTheme
                          .bodySmall!
                          .copyWith(fontSize: 8),
                    ),
                  );
                },
              ),
            ),
            bottomTitles: AxisTitles(
              axisNameSize: 24,
              sideTitles: SideTitles(
                showTitles: true,
                maxIncluded: false,
                reservedSize: 35,
                interval: intervalX,
                getTitlesWidget: (val, meta) {
                  DateTime date = earliestDate.add(Duration(
                      minutes: (shortestIntervalMinutes * val).toInt()));
                  return SideTitleWidget(
                    meta: meta,
                    child: Column(
                      children: [
                        Text(
                          DateFormat("HH:mm").format(date),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(fontSize: 8),
                        ),
                        Text(
                          DateFormat("d MMM").format(date),
                          style: Theme.of(context)
                              .textTheme
                              .bodySmall!
                              .copyWith(fontSize: 6),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ), //
          ),
        ),
      ),
    );
  }
}
