import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:fpdart/fpdart.dart';
import 'package:hiive/data/api/endpoints/hive/create_hive_endpoint.dart';
import 'package:hiive/data/models/hive.dart';
import 'package:hiive/data/state/hives_notifier.dart';
import 'package:hiive/utils/misc.dart';

TaskEither<String, Hive?> createHiveDialog(BuildContext context) {
  return TaskEither.tryCatch(
    () {
      return showDialog<Hive>(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return CreateHiveDialog();
        },
      );
    },
    (error, stackTrace) {
      return error.toString();
    },
  );
}

TaskEither<String, Hive?> createOrChooseHiveDialog(BuildContext context) {
  return TaskEither.tryCatch(
    () {
      return showDialog<Hive>(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return CreateHiveDialog.allowChoose();
        },
      );
    },
    (error, stackTrace) {
      return error.toString();
    },
  );
}

class CreateHiveDialog extends ConsumerStatefulWidget {
  final bool allowChoose;

  const CreateHiveDialog({super.key, this.allowChoose = false});

  // Named constructor to allow choosing
  const CreateHiveDialog.allowChoose({super.key}) : allowChoose = true;

  @override
  ConsumerState<CreateHiveDialog> createState() => _CreateHiveDialogState();
}

class _CreateHiveDialogState extends ConsumerState<CreateHiveDialog> {
  Future<void>? _createHiveFuture;

  @override
  Widget build(BuildContext context) {
    List<Hive> hives = ref.watch(hivesNotifierProvider).value ?? [];
    TextEditingController newHiveEditingController = TextEditingController();

    return SimpleDialog(children: [
      Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (widget.allowChoose)
            Column(children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Choose hive",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
              ...hives.map(
                (hive) {
                  return SimpleDialogOption(
                    child: Text(hive.name,
                        style: Theme.of(context).textTheme.titleMedium),
                    onPressed: () async {
                      if (!context.mounted) return;
                      Navigator.of(context).pop(hive);
                    },
                  );
                },
              )
            ]),
          if (widget.allowChoose)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Container(
                  height: 1,
                  width: 75,
                  color: Colors.grey[300],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(
                    "OR",
                    style: Theme.of(context).textTheme.titleSmall,
                  ),
                ),
                Container(
                  height: 1,
                  width: 75,
                  color: Colors.grey[300],
                )
              ]),
            ),
          FutureBuilder(
            future: _createHiveFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        "Create hive",
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: TextFormField(
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Hive name",
                        ),
                        validator: null,
                        controller: newHiveEditingController,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FilledButton.tonal(
                          child: Text(
                            "Other",
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(fontWeight: FontWeight.w600),
                          ),
                          onPressed: () {
                            final hiveName = newHiveEditingController.text;
                            final createHiveData =
                                CreateHiveData(name: hiveName, isHiive: false);
                            _createHiveFuture = ref
                                .read(hivesNotifierProvider.notifier)
                                .createHive(createHiveData)
                                .then(
                              (result) async {
                                final Hive? res = result.match((error) {
                                  showSnackbar(context, error.toString());
                                  return null;
                                }, (hive) => hive);
                                if (context.mounted) {
                                  Navigator.of(context).pop(res);
                                }
                              },
                            );
                          },
                        ),
                        SizedBox(width: 16),
                        FilledButton(
                          child: Text(
                            "HIIVE",
                            style: Theme.of(context)
                                .textTheme
                                .bodyMedium!
                                .copyWith(fontWeight: FontWeight.w600),
                          ),
                          onPressed: () async {
                            final hiveName = newHiveEditingController.text;
                            final createHiveData =
                                CreateHiveData(name: hiveName, isHiive: true);
                            _createHiveFuture = ref
                                .read(hivesNotifierProvider.notifier)
                                .createHive(createHiveData)
                                .then(
                              (result) async {
                                final Hive? res = result.match((error) {
                                  showSnackbar(context, error.toString());
                                  return null;
                                }, (hive) => hive);
                                if (context.mounted) {
                                  Navigator.of(context).pop(res);
                                }
                              },
                            );
                          },
                        ),
                      ],
                    )
                  ],
                );
              }
            },
          ),
        ],
      ),
    ]);

    //return SimpleDialog(
    //  //title: Text(widget.allowChoose ? 'Choose or Create hive' : 'Create hive'),
    //  children: [
    //    Column(
    //      crossAxisAlignment: CrossAxisAlignment.center,
    //      children: [
    //        // Only show the list of things if "allowChoose" is true
    //        if (widget.allowChoose)
    //          Column(
    //            children: [
    //              Text("Choose hive"),
    //              ListView.builder(
    //                shrinkWrap: true,
    //                itemCount: hives.length,
    //                itemBuilder: (context, index) {
    //                  return ListTile(
    //                    title: Text(hives[index].name),
    //                    onTap: () {
    //                      Navigator.of(context).pop(Right(hives[index]));
    //                    },
    //                  );
    //                },
    //              ),
    //            ],
    //          ),

    //        Row(
    //          mainAxisAlignment: MainAxisAlignment.center,
    //          crossAxisAlignment: CrossAxisAlignment.center,
    //          children: [
    //            Expanded(
    //              child: Container(
    //                height: 1,
    //                color: Colors.black,
    //              ),
    //            ),
    //            Text("OR"),
    //            Expanded(
    //              child: Container(
    //                height: 1,
    //                color: Colors.black,
    //              ),
    //            ),
    //          ],
    //        ),

    //        // Show loading spinner or result based on future's connection state
    //        FutureBuilder(
    //          future: _createHiveFuture,
    //          builder: (context, snapshot) {
    //            return Column(
    //              crossAxisAlignment: CrossAxisAlignment.center,
    //              children: [
    //                Text("Choose hive"),
    //                Row(
    //                  children: [
    //                    FilledButton(
    //                      child: Text("HIIVE"),
    //                      onPressed: () {},
    //                    ),
    //                    FilledButton(
    //                      child: Text("Other"),
    //                      onPressed: () {},
    //                    )
    //                  ],
    //                )
    //              ],
    //            );
    //          },
    //        ),
    //      ],
    //    )
    //  ],
    //);
  }
}
