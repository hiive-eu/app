{
description = "Flutter";
inputs = {
  nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  flake-utils.url = "github:numtide/flake-utils";
};
outputs = { self, nixpkgs, flake-utils }:
  flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs {
        inherit system;
        config = {
          android_sdk.accept_license = true;
          allowUnfree = true;
        };
      };
      # https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/mobile/androidenv/compose-android-packages.nix
      main_build_tools_version = "34.0.0";
      androidComposition = pkgs.androidenv.composeAndroidPackages {
        buildToolsVersions = [ "30.0.3" main_build_tools_version "33.0.1"];
        platformVersions = [ "31" "33" "34" "35"];
        abiVersions = [ "armeabi-v7a" "arm64-v8a" ];
        includeNDK = true;
        includeEmulator = true;
      };
      androidCustomPackage = androidComposition.androidsdk;
      user = builtins.getEnv "USER";
    in
    {
      devShell =
        with pkgs; mkShell {
          ANDROID_SDK_ROOT = "${androidCustomPackage}/libexec/android-sdk";
          ANDROID_SDK_HOME = "${androidCustomPackage}/share/android-sdk";
          GRADLE_USER_HOME = "/home/${user}/.gradle";
          GRADLE_OPTS = "-Dorg.gradle.project.android.aapt2FromMavenOverride=${androidCustomPackage}/libexec/android-sdk/build-tools/${main_build_tools_version}/aapt2";
          buildInputs = [
            flutter
            android-studio
            androidCustomPackage # The customized SDK that we've made above
            jdk17
            google-chrome
            libsecret
          ];
        };
    });
}
