# *** NOT PRODUCTION READY ***

Known shortcomings:

* Make sure that licenses of used packages are not violated

Unknown shortcomings: most likely.

# Android

## Signing

Copy `key.properties` to `android/`

Copy `upload-keystore.jks` to `android/app/`

## Build Production

Run `scripts/android/build_production.sh`

## Build Development

Required environment variables:

* `HIIVE_HTTP_AUTH_USER`
* `HIIVE_HTTP_AUTH_PASS`

Run `scripts/android/build_development.sh`

## Upload

APK file can be shared in any way

# iOS

## Build Production

Run `scripts/ios/build_production.sh`

## Build Development

Required environment variables:

* `HIIVE_HTTP_AUTH_USER`
* `HIIVE_HTTP_AUTH_PASS`

Run `scripts/ios/build_development.sh`

## Upload

Upload iOS ipa file to Testflight using transporter

# State handling

Data state is kept in Riverpod notifiers, e.g. gateways are handled by the GatewaysNotifierProvider, and hives in HivesNotifierProvider.

These notifiers are also used to alter the state by the presentation layer. 
